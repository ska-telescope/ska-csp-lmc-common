SKA CSP LMC Common
==================

This project is developing the common package used by the Local Monitoring and Control prototype for Central Signal Procesing (CSP.LMC) of the Square Kilometer Array. This package is used for both prototypes developed for [Mid](https://gitlab.com/ska-telescope/ska-csp-lmc-mid) and [Low](https://gitlab.com/ska-telescope/ska-csp-lmc-low) telescopes.

## Table of contents
- [Documentation](#documentation)
- [Repository organization](#repository-organization)
- [Tests](#tests)
- [Known bugs](#known-bugs)
- [Troubleshooting](#troubleshooting)
- [License](#license)

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-csp-lmc-common/badge/?version=latest)](https://ska-telescope-ska-csp-lmc-common.readthedocs.io/en/latest/?badge=latest)

The documentation for this project, including the package description, Architecture description and the API modules can be found at SKA developer portal: 

[CSP.LMC common documentation](https://developer.skatelescope.org/projects/ska-csp-lmc-common/en/latest/index.html)


## Repository organization

The repository has the following organization:

* resources: contains the POGO files of the TANGO Devices of the project 
* docs: contains all the files to generate the documentation for the project.
* src: the folder with all the project source code
* tests: contains the unit tests for the code. 

## Tests

To perform **unit tests**, the following command will open a shell in a container with ska-csp-lmc-common already installed: 

```bash
make dev-container
```
After launching this command, the tests can be performed as usual: 

```bash
make python-test
```
In the same container also linting can be performed with the command: 

```bash
make python-lint
```

## Known bugs


## Troubleshooting


## License
See the [LICENSE](https://gitlab.com/ska-telescope/ska-csp-lmc-common/-/blob/master/LICENSE?ref_type=heads) file for details.

