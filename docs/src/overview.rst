###################
Package Overview
###################

Functionalities common to Low and Mid CSP.LMC includes: communication framework, logging, archiving, alarm generation,
subarraying, some of the functionality realated to handling observing mode changes, Pulsar Search and
Pulsar Timing, and to some extent Very Long Baseline Interferometry (VLBI).

The difference between LOW and MID CSP.LMC is mostly due to the different receivers (dishes vs stations) and different
CBF functionality and design. More than the 50% of the CSP.LMC functionality is common for both
telescopes.

The CSP.LMC Common Package comprises all the software components and functionalities common to LOW and MID CSP.LMC
and is used as a base for development of the Low CSP.LMC and Mid CSP.LMC software.

The *CSP.LMC Common Package* is delivered as a part of each CSP.LMC release, via a Python package that can be used as 
required for maintenance and upgrade.

CSP.LMC implements a high level interface (API) that Telescope Manager (TM), or other authorized
client, can use to monitor and control CSP as a single instrument.

At the same time, CSP.LMC provides high level commands that the TM can use to sub-divide the array
into up to 16 sub-arrays, i.e. to assign station/receptors to sub-arrays, and to operate each 
sub-array independently and concurrently with all other sub-arrays.
