:orphan:

.. _CSP-timingBeamCapability:

CSP.LMC Timing Beam Capability
==============================

(To be implemented)

The Timing Beam Capability exposes the attributes and commands to monitor and control beam-forming and PST
processing in a single beam. 

The mapping between an instance of the CSP Search Beam and the internal CSP Sub-element components performing
beam-forming and search is established at initialization and is permanent.

CSP.LMC TimingBeamCapability API Documentation
----------------------------------------------

(To be implemented)