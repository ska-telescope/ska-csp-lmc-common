Capabilities represent the CSP schedulable resources and provide API that can be used to
configure, monitor and control resources that implement signal processing functionality.
During normal operations, TM uses the sub-array API to assign capabilities to the sub-array, configure
sub-array Processing Mode, start and stop scan.

The CSP.LMC Common Package implements the capabilities that are shared between LOW and MID instances.

These are:

* :ref:`CSP Search Beam Capability <CSP-searchBeamCapability>`
* :ref:`CSP Timing Beam Capability <CSP-timingBeamCapability>`
* :ref:`CSP VLBI Beam Capability <CSP-VLBIBeamCapability>`
* :ref:`CSP Monitor Capability <CSP-MonitorCapability>`

Components listed above are implemented as TANGO devices, i.e. classes that implement standard TANGO API.
The CSP.LMC TANGO devices are based on the standard SKA1 TANGO Element Devices provided via the *SKA Base Classes package*.
