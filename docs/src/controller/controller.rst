The CSP controller provides API for monitor and control the CSP sub-system.
CSP Controller is the primary point of access for CSP Monitor and Control. 

CSP Controller maintains the pool of schedulable resources, and it can relies on the
CSP CapabilityMonitor devices, as needed.
The CSP Controller implements CSP sub-system-level status indicators, configuration parameters, 
housekeeping commands.



