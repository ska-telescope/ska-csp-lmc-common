##################
Package Components
##################

The top-level software components provided by CSP.LMC API are:

* :ref:`Csp Controller <CSP-Controller>`
* :ref:`Csp Subarray <CSP-Subarray>`
* CSP Alarm Handler (TBD)
* CSP Logger (TBD)
* CSP TANGO Facility Database (TBD)
* Input processor Capability (receptors/stations) (TBD)
* :ref:`Search Beam Capability <CSP-Capability>` (TBD)
* :ref:`Timing Beam Capability <CSP-Capability>` (TBD)
* :ref:`VLBI Beam Capability <CSP-Capability>` (TBD)

Components listed above are implemented as TANGO devices, i.e. classes that implement standard TANGO API.
The CSP.LMC TANGO devices are based on the standard SKA1 TANGO Element Devices provided via the *SKA Base Classes package*.

.. _CSP-Controller:

******************
CSP.LMC Controller
******************

.. include:: controller/controller.rst

.. _CSP-Subarray:

****************
CSP.LMC Subarray
****************

.. include:: subarray/subarray.rst

.. _CSP-Capability:

********************
CSP.LMC Capabilities
********************

.. include:: capability/capability.rst


