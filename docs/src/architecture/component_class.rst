The component class is a mediator between CSP.LMC and a Subsystem Device.  
It acts as an adapter and allows, when needed,  to execute specific instructions on a subsystem before invoking 
the required command. In other words, its functionalities are:

 * read and write of associated device's attributes;
 * command execution;
 * subscription of attributes on the corresponding Tango Device.
