The **MainTaskExecutor** Python class is an asynchronous executor of tasks, responsible for unpacking
the MainTask into Task/LeafTask and executing them. It can perform both parallel and serial tasks, while
delivering to other modules the execution of CSP.LMC internal tasks and to the DeviceExecutor the fulfilling of 
TANGO device tasks. In details:

 * it is instantiated in the CSP device Component Manager;
 * it uses a ThreadPool to execute tasks in parallel;
 * it unwraps the parallel and serial tasks, recursively submitting them to the parallel or sequential handler until the final task is a LeafTask;
 * it employs the ska-tango-base 'invoke_lrc' method to execute a TANGO command on a subsystem device;
 * it manages the command timeout value;
 * it forwards the abort event if occurs.
