The **MainTaskComposer** Python class is used to build a tree-like structure of Task and LeafTask from the definition
contained in the command_map, and from additional information present in the device Component Manager. 

The MainTaskComposer includes the logic to build different Task objects, both depending on their execution type 
(internal, device, etc) and on specific implementation based on the command destination's sub-system 
(CBF, PSS, PST).
