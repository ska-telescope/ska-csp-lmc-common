The **TaskTracker** Python class is responsible for tracking the completion of the sub-tasks within the MainTask.
It reports the completion of a task through a callback which adheres to the ska-tango-base architecture. In details:

 * it monitors the task completion;
 * it signals via callback the task completion;
 * it updates the task and its attributes when the task status changes;
 * it aggregates TaskStatus, ResultCode and messages.
