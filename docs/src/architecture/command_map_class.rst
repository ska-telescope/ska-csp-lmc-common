A specific Python class (**CommandMap**) is used as a knowledge base to list the structure of all CSP.LMC commands,
defining all the sub-tasks in which a main task can be divided into. The definition is implemented as a JSON string
in order to be human readable and easily updated.

Commands are defined for both Controller and Subarray classes, including various attributes that are used for their 
execution management, like 'type' and 'allowed_states'. Even CSP.LMC internal commands are defined in the map, whereas
the 'command_name' parameter corresponds to the method signature within CSP.LMC codebase.

The main benefit of using the CommandMap class is the decoupling between command definition and implementation, allowing
the developer to update the command structure without modifying the underlying code.

Here is an example of a CommandMap's dictionary regarding the **Common** domain and the **Controller** device:

.. code-block:: python
   :caption: Command Map - Common repository - Controller device

    "on": {
        "type": "parallel",
        "allowed_states": {
            "attr_name": "state",
            "attr_value": [
                DevState.OFF,
                DevState.STANDBY,
                DevState.UNKNOWN,
            ],
        },
        "tasks": {
            "csp_subs": {"command_name": "on"},
            "pst": {"command_name": "on"},
            "pss": {"command_name": "on"},
            "cbf": {"command_name": "on"},
        },
    },
    "off": {
        "type": "parallel",
        "allowed_states": {
            "attr_name": "state",
            "attr_value": [
                DevState.ON,
                DevState.STANDBY,
                DevState.UNKNOWN,
            ],
        },
        "tasks": {
            "csp_subs": {"command_name": "off"},
            "pst": {"command_name": "off"},
            "pss": {"command_name": "off"},
            "cbf": {"command_name": "off"},
        },
    },
    "standby": {},
    "reset": {
        "type": "parallel",
        "allowed_states": {
            "attr_name": "state",
            "attr_value": [
                DevState.OFF,
                DevState.ON,
                DevState.STANDBY,
                DevState.UNKNOWN,
                DevState.FAULT,
            ],
        },
        "tasks": {
            "csp_subs": {"command_name": "reset"},
            "pst": {"command_name": "reset"},
            "pss": {"command_name": "reset"},
            "cbf": {"command_name": "reset"},
        },
    }
