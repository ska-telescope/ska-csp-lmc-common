**********************
Software Architecture
**********************

The architecture of CSP.LMC is shared between the Controller and the Subarray. Both of them communicate with three 
sub-systems: CBF, PSS and PST. The Controller must also access the CSP.LMC Subarrays and the Capabilities device 
manager to report the information on the resources.

In the figure below, the C&C view of CSP.LMC controller is provided. The case of CSP.LMC subarray is identical, where 
Subsystem's Controller are substituted with correspondant Subsystem's subarrays and no other CSP.LMC subarrays are 
controlled. Further diagrams and a more comprehensive description of its component can be found at 
`this page <https://confluence.skatelescope.org/pages/viewpage.action?pageId=148818174>`_.

.. image:: ../images/ControllerDiagram.jpg
  :width: 1200

The main operations of CSP are carried out into the three sub-elements. The interaction between the CSP Controller and 
the subordinate sub-systems devices are mediated through a Python class that works as a proxy (Component Class).
This approach has the advantage of abstraction. 

Since version 0.11.0 the state machine of ska-tango-base is no longer used. The motivation of this choice is described  
`here <https://confluence.skatelescope.org/display/SE/91+Beyond+the+State+Machine>`_. For this reason, custom state 
models are implemented for the Operational, Observing and Health State (:ref:`CSP State Models`).

####################################
Interface to subsystem TANGO devices
####################################

Specific operations on a sub-element can be done by specializing the proxy class for each sub-system and the 
corresponding functions are maintained in a specific part of the code.

Csp sub-system Component class
******************************

   .. include:: component_class.rst


Connector class
***************

   .. include:: connector_class.rst

##################
Commands execution
##################

A command issued on the CSP Controller or CSP Subarray by a TANGO client breaks up, nearly always, into several commands 
(>=3), that are forwarded to the connected CSP sub-systems (CBF, CSP, PST, PSS).

In order to avoid confusion in the nomenclature, given the multiple meanings of the word 'command', the following 
naming has been estabilished in the CSP.LMC codebase:

 * **MainTask**: the command issued from a client towards the CSP.LMC system. It is usually broken up into several tasks.
 * **Task**: the object that incapsulates the logic of a command (name, type, callbacks, etc.). It can be further broke up in different subtasks (Task or LeafTask) in a recursive structure.
 * **LeafTask**: the last Task in a task-tree structure, i.e. the smallest unit of execution. It can be either a command issued on a SubSystem device or an internal CSP.LMC command.

The commands execution architecture is capable to support any combination of the following execution types: Parallel, 
Sequential, CSP.LMC Internal and TANGO Device. Hence, for example, a MainTask can be broke up in a several sets of 
parallel tasks which will be issued on the relative sub-systems.

The CSP Controller or Subarray TANGO device has to be able to invoke the command on a sub-element and monitors its 
execution, detecting its progress and its final status (success/failure).

The sequence of operations to be performed are the following:

 * check the initial device state to determine if the command is allowed;
 * decompose the client command (MainTask) into its task-tree structure;
 * forward and execute the commands (i.e. Task, LeafTask) following their execution type (see Command_Map example below);
 * wait for the final status (the one expected after the end of successful execution) and detect possible conditions of failures;
 * implement support for timeout;
 * report the end of the command.

The execution of a command, is tracked and reported by the LRC attributes, leveraging the use of the BaseClasses API 'invoke_lrc' 
in order to obtain a more coherent and decoupled architecture. In particular, the command results are handled by the following 
elements: [TaskStatus, (ResultCode, message)]. 

The **TaskStatus** adheres to its ska-tango-base associated state machine and monitors
the progress of the command (i.e. MainTask), while the **ResultCode** is mainly used to catch queuing, exceptions, aborts and other events 
during the command execution.

Moreover, since a MainTask is an aggregation of more than one Tasks, a basic implementation of the aggregation of multiple TaskStatus 
and ResultCode is in place (detailed info can be found `at this link <https://confluence.skatelescope.org/pages/viewpage.action?pageId=296965777>`_).

In the figure below an example data flow diagram of a CSP command is provided.

.. image:: ../images/Command_Dataflow.jpg
  :align: center
  :height: 800

In the next figure, an UML class diagram of the **commands** package is provided.

.. image:: ../images/CommandsClassDiagram.png
  :width: 1200

Further diagrams and a more comprehensive description of the commands architecture can be found at
`this confluence page <https://confluence.skatelescope.org/display/SE/CSP+LMC+commands>`_.

Command Map
***********

.. include:: command_map_class.rst

MainTask Composer
*****************

.. include:: maintask_composer_class.rst

Task Tracker
*****************

.. include:: task_tracker_class.rst

MainTask Executor
*****************

.. include:: maintask_executor_class.rst
