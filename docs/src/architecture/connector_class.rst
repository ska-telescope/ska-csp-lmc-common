Connector Class is class working as interface to the TANGO system.  It relies on TangoClient class of ska-tmc-common 
package developed by NCRA team, and it has the purpose to communicate with the device proxy of Sub System TANGO device
for all the functionalities used by the Component classe.

One of the main advantage to have this class, it the possibility to be easily mocked during the tests. 