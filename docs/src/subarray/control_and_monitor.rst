:orphan:

.. _subarray-control-monitor:

Control and Monitoring
======================

Each CSP Subarray maintains and report the status and state transitions for the 
CSP subarray as a whole and for individual assigned resources.

In addition to pre-configured status reporting, a CSP subarray makes provision for the TM and any authorized client, to obtain the value of any subarray attribute.
