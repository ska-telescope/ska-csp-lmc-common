The core CSP functionality, configuration and execution of signal processing, is configured, controlled 
and monitored via subarrays.

CSP Subarray makes provision to TM to configure a subarray, select Processing Mode and related parameters, 
specify when to start/stop signal processing and/or generation of output products.  
TM accesses directly a CSP Subarray to:

* :ref:`Assign resources <subarray-assign-resources>`
* :ref:`Configure a scan <subarray-scan-configuration>`
* :ref:`Control and monitor states/operations <subarray-control-monitor>`
