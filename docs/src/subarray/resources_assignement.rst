:orphan:

.. _subarray-assign-resources:

Resources assignment
====================
The assignment of Capabilities to a subarray (*subarray composition*) is performed 
in advance of a scan configuration.
Assignable Capabilities for CSP Subarrays are:

* receptors (MID) or stations (LOW)
* tied-array beams: Search Beams, Timing Beams and Vlbi Beams.

In general resource assignment to a subarray is exclusive, but in some cases the same Capability instance
may be used in shared manner by more then one subarray.

Inherent Capabilities
=====================
Each CSP subarray has also a set of permanently assigned *inherent Capabilities*: the number and type is different
for LOW and MID instance.

Only the Inherent Capabilities related to the Processing Mode are common to both instances.

These are:

* Correlation
* PSS
* PST
* VLBI

An inherent Capability can be enabled or disabled, but cannot assigned or removed to/from a subarray. 
