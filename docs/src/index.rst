.. CSP LMC Prototype documentation master file, created by
   sphinx-quickstart on Thu Jun 13 16:15:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CSP LMC COMMON Package
=======================

A CSP LMC common package has been developed since general requirements for the monitor and control functionalities
are the same in both telescopes. In addition two of three other CSP Sub-elements, namely PSS and PST, have the same
functionalities and use the same design for both the telescopes.

.. toctree::
   :maxdepth: 1
   :caption: Overview:

   overview.rst

.. toctree::
   :maxdepth: 1
   :caption: Architecture
   
   architecture/index

.. .. toctree::
..    :maxdepth: 2
..    :caption: Tango devices

..    .. It should be reworked
..       Capability <tango_device/capability/index>

.. toctree::
   :maxdepth: 1
   :caption: Components:

   csp_lmc_common.rst

.. toctree::
   :maxdepth: 1
   :caption: Software API

   api/index

.. toctree::
   :maxdepth: 1
   :caption: Releases

   CHANGELOG.rst

.. toctree::
   :maxdepth: 1
   :caption: Deployment

   Gitlab README<../../README>
