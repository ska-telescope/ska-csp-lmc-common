####################################
CSP.LMC CapabilityDevice Attributes
####################################

The following tables contain information about the CSP LMC attributes of the Base Capability Tango device.

CSP.LMC Base Capability common attributes
------------------------------------------

The following table contain information about common abstract attributes which have their own implementation
in the specific Capability device classes (e.g. MidCspCapabilityFsp, MidCspCapabilityVcc).

.. list-table::
   :widths: 15 25 15 2 2 2
   :header-rows: 1

   * - Name
     - Description
     - Type
     - R/W 
     - Change event
     - Archived
   * - State
     - State
     - List of DevState
     - R
     - yes
     - yes
   * - healthState
     - List of the Health States of the capability. It interprets the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - List of Enum (HealthState)
     - R
     - yes
     - yes
   * - adminMode
     - The admin mode reported for this device. It may interpret the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum (AdminMode)
     - R/W
     - yes
     - yes
   * - isCommunicating
     - Whether the TANGO device is communicating with the controlled component
     - List of Bool
     - R
     - yes
     - yes
   * - fqdnAssigned
     - FQDNs of the assigned capability
     - List of String
     - R
     - yes
     - yes
   * - subarrayMembership
     - Each string contains the IDs of the subarrays to which a capability belongs to
     - List of String
     - R
     - yes
     - yes


CSP.LMC Base Capability PST attributes
------------------------------------------

The following table contain information about PST monitoring attributes which are forwarded by CapabilityDevice.
These attributes are provided by PST (see https://developer.skao.int/projects/ska-pst/en/latest/operation/monitoring.html)

.. list-table::
   :widths: 15 25 5 5 2 2 2
   :header-rows: 1

   *  - Name
      - Description
      - Type
      - PST Subcomponent
      - R/W 
      - Change event
      - Archived
   * - availableDiskSpace
     - available space on the disk that PST.BEAM is writing to, in bytes
     - Int
     - DSP
     - R
     - yes
     - yes
   * - availableRecordingTime
     - available time, in seconds, for writing available
     - Float
     - DSP
     - R 
     - yes
     - yes
   * - diskCapacity
     - total capacity of the disk that DSP is writing to, in bytes
     - Int
     - DSP
     - R
     - yes
     - yes
   * - diskUsedPercentage
     - used space on the disk that DSP is writing to as a percentage. This is 100.0 * (diskCapacity - availableDiskSpace)/availableDiskSpace.
     - Float
     - DSP
     - R
     - yes
     - yes
   * - dataReceiveRate
     - current data receive rate from the CBF interface in Gb/s
     - Float
     - RECV
     - R
     - yes
     - yes
   * - dataReceived
     - total amount of data received from CBF interface for current scan in Bytes
     - Int
     - RECV
     - R
     - yes
     - yes
   * - dataDropRate
     - current rate of CBF ingest data being dropped or lost in Bytes/s
     - Float
     - RECV
     - R
     - yes
     - yes
   * - dataDropped
     - total number of bytes dropped in the current scan
     - Int
     - RECV
     - R
     - yes
     - yes
   * - dataRecordRate
     - current rate of writing to the disk, in byte/s
     - Float
     - DSP
     - R
     - yes
     - yes
   * - dataRecorded
     - number of bytes written during scan
     - Int
     - DSP
     - R
     - yes
     - yes
   * - ringBufferUtilisation
     - the percentage of the ring buffer elements that are full of data
     - Float
     - SMRB
     - R
     - yes
     - yes
   * - expectedDataRecordRate
     - the expected rate of data to be received by PST Beam component
     - Float
     - DSP
     - R
     - yes
     - yes


CSP.LMC Base Capability monitor attributes (FSP)
--------------------------------------------------
The following table contain information about FSP Capability monitoring attributes
which can possibly be suitable for other Capability devices and thus included in the abstract
Base Capability device (To Be Discussed).

.. list-table::
   :widths: 15 25 15 2 2 2
   :header-rows: 1

   * - Name
     - Description
     - Type
     - R/W 
     - Change event
     - Archived
   * - FunctionMode
     - List of function mode (currently for all FSPs)
     - List of String
     - R
     - yes
     - yes

CSP.LMC Base Capability monitor attributes (VCC)
--------------------------------------------------
The following table contain information about VCC Capability monitoring attributes
which can possibly be suitable for other Capability devices and thus included in the abstract
Base Capability device (To Be Discussed).

.. list-table::
   :widths: 15 25 15 2 2 2
   :header-rows: 1

   * - Name
     - Description
     - Type
     - R/W 
     - Change event
     - Archived
   * - SimulationMode
     - List of simulation mode (currently for all VCCs)
     - List of String
     - R
     - yes
     - yes
   * - BandActive
     - List of the active band label (currently for all VCCs)
     - List of String
     - R
     - yes
     - yes
   * - ReceptorID
     - Receptor IDs
     - List of String
     - R
     - yes
     - yes

CSP.LMC Base Capability PSS attributes
--------------------------------------------------
TBD
