####################################
CSP.LMC CapabilityDevice Properties
####################################

The following tables contain information about the CSP LMC properties of the Base Capability Tango device.

.. list-table::
   :widths: 15 25 15 2
   :header-rows: 1

   * - Name
     - Description
     - Type
     - Default Value 
   * - ConnectionTimeout
     - Connection timeout
     - UShort
     - 60
   * - PingConnectionTime
     - Ping connection time
     - UShort
     - 5

