==================
Command subpackage
==================

.. automodule:: ska_csp_lmc_common.commands

.. toctree::
       
   Sub-system commands<component_command>
   Base commands<base_commands>
   Observing commands<observing_commands>
   Macro commands<macro_commands>


