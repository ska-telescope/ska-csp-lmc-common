========================
Macro Component Commands
========================

.. autoclass:: ska_csp_lmc_common.commands.macro_command.MacroComponentCommand
   :members:
   :member-order:
   :noindex:
   :private-members:
   :show-inheritance:
   :undoc-members:
   
   .. automethod:: __init__

