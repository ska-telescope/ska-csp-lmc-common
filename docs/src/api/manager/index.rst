==================
Manager subpackage
==================

.. automodule:: ska_csp_lmc_common.manager

.. toctree::
       
   Controller Component Manager<controller_component_manager>
   Subarray Component Manager<subarray_component_manager>
   Event Manager<event>
   Component Manager configurator<manager_configurator>
