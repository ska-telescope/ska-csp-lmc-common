Event Manager
=============
.. autoclass:: ska_csp_lmc_common.manager.EventManager
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order:
   :noindex:
   :private-members:
   
   .. automethod:: __init__