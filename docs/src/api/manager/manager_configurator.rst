Component Manager Configurator
==============================
.. autoclass:: ska_csp_lmc_common.manager.manager_configuration.ComponentManagerConfiguration
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order:
   :noindex:
   :private-members:
   
   .. automethod:: __init__