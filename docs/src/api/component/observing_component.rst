Observing Component
===================

.. autoclass:: ska_csp_lmc_common.observing_component.ObservingComponent
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order:
   :noindex:
   :private-members: