========================
CSP Sub-System Component
========================

.. toctree::
       
   Base Component<base_component>
   Observing Component Subarray<observing_component>
   Sub-system controller components <controller_components>
   Sub-system observing components <subarray_components>
