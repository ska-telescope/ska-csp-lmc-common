Operational State Model
====================================

.. autoclass:: ska_csp_lmc_common.model.OpStateModel
   :members:
   :member-order:
   :noindex:
   :private-members:
   :show-inheritance:
   :undoc-members:
    
   .. automethod:: __init__


Health State Model
====================================

.. autoclass:: ska_csp_lmc_common.model.HealthStateModel
   :members:
   :member-order:
   :noindex:
   :private-members:
   :show-inheritance:
   :undoc-members:

   .. automethod:: __init__

Observing State Model
====================================

.. autoclass:: ska_csp_lmc_common.model.ObsStateModel
   :members:
   :member-order:
   :noindex:
   :private-members:
   :show-inheritance:
   :undoc-members:

   .. automethod:: __init__