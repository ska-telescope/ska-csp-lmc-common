Controller Operational State Model
====================================

.. autoclass:: ska_csp_lmc_common.controller.controller_op_state.ControllerOpStateModel
   :members:
   :member-order:
   :noindex:
   :private-members:
   :show-inheritance:
   :undoc-members:

Controller Health State Model
====================================

.. autoclass:: ska_csp_lmc_common.controller.controller_health_state.ControllerHealthModel
   :members:
   :member-order:
   :noindex:
   :private-members:
   :show-inheritance:
   :undoc-members: