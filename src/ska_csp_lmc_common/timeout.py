import time

# pylint: disable=invalid-name
# pylint: disable=missing-function-docstring


# pylint: disable-next=missing-class-docstring
class Timeout:
    def __init__(self, default_timeout=0):
        self._timeout = default_timeout
        self._start_time = 0

    @property
    def duration(self):
        return self._timeout

    @property
    def start_time(self):
        return self._start_time

    def set(self, value):
        self._timeout = value

    def start(self):
        self._start_time = time.time()

    def expired(self):
        if self._start_time:
            t = time.time()
            if (t - self._start_time) > self._timeout:
                return True
        return False
