from __future__ import annotations

import logging
from functools import partial
from typing import Any, Callable, Dict, Optional

from ska_csp_lmc_common.commands.task import LeafTask, Task, TaskExecutionType

module_logger = logging.getLogger(__name__)


# pylint: disable=too-many-branches
# pylint: disable=inconsistent-return-statements
# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-nested-blocks


class MainTaskComposer:
    """Class that create the MainTaskComposer object, to be called in component
    manager in each command"""

    def __init__(
        self: MainTaskComposer,
        name: str,
        command_map: dict,
        component_manager,
        callback: Callable,
    ):
        """
        The MainTaskComposer compose a CSP.LMC MainTask in its structure of
        tasks and leaftasks starting from the definition contained in the
        command_map and on the information present in component manager

        :param name: the command name
        :param command_map: the dictionary with the structure of the command
            in tasks and leaftasks
        :param component_manager: the CSP component Manager (coulbe Controller
            or Subarray)
        :param callback: the command completed callback defined in the
            component manager
        """
        self.name = name
        self.cm = component_manager
        self.command_map = command_map
        self.callback = callback

    def compose(self: MainTaskComposer):
        """
        Compose the MainTask starting from the command map"""

        task_map = self.command_map[self.name]

        # skip_subtasks = task_map.get("skip_subtasks")
        # if not skip_subtasks:
        #     skip_subtasks = False
        skip_subtasks = self._get_skip_subtasks(task_map)

        allowed_conditions = task_map.get("allowed_states")
        allowed_attr_name = allowed_conditions["attr_name"]
        allowed_values = allowed_conditions["attr_value"]

        draft_task = self._build_task(
            self.name,
            task_map,
            allowed_attr_name,
            allowed_values,
            skip_subtasks,
            self.callback,
        )
        return self._clean_task(draft_task)

    def _clean_task(self: MainTaskComposer, task: Task) -> Task | None:
        """
        Recursively clean tasks by removing tasks with empty subtasks.
        If a task has empty subtasks, it is removed.
        If subtasks of a task are cleaned and become empty, it is
        also removed.

        :param task: The Task to be cleaned.
        :return: The cleaned Task, or None if the task is empty.
        """
        # Base case: if task is a LeafTask, return it as is
        if isinstance(task, LeafTask):
            return task

        # Recursively clean each subtask
        cleaned_subtasks = []
        for subtask in task.subtasks:
            cleaned_subtask = self._clean_task(
                subtask
            )  # Clean the subtask recursively
            module_logger.debug(f"subtask name: {subtask.name}")
            if cleaned_subtask is not None:  # Keep non-empty subtasks
                cleaned_subtasks.append(cleaned_subtask)

        # Update the current task's subtasks to the cleaned list
        task.subtasks = cleaned_subtasks
        # If no subtasks remain, return None (this task will be removed)
        if not task.subtasks:
            return None

        # Otherwise, return the cleaned task and instantiate the tracker
        task.instantiate_tracker()
        return task

    def _get_skip_subtasks(self: MainTaskComposer, task_map: Dict) -> bool:
        """Retrieve the `skip_subtasks` flag if set.

        :param task_map: the map describing the task.

        :return: the `skip_subtasks` flag value.
        """
        skip_subtasks = task_map.get("skip_subtasks")
        if not skip_subtasks:
            skip_subtasks = False
        return skip_subtasks

    def _build_task(
        self: MainTaskComposer,
        name: str,
        task_map: dict,
        allowed_attr_name,
        allowed_values: list,
        skip_subtasks: bool = None,
        callback=None,
    ):
        """
        Recursively builds Task and Subtask objects from dictionary data.

        :param name: Name of the task.
        :param task_map: Dictionary representing a task or subtask.
        :param allowed_attr_name: the name of the attribute which allow the
            issue of a task.
        :param allowed_values: the attribute values which allow the issue
            of a task.
        :param skip_subtasks: If set to True, all subsequent subtasks in a
            sequence will be skipped if a preceding task fails.
        :param: callback: the task callback.
        :return: Task object or a SubTask object.
        """

        # if type and tasks keys are present in task_map it means that we need
        # to create a Task with subtasks indicated
        if "type" in task_map and "tasks" in task_map:
            subtasks = []
            task_type = task_map.get("type")
            for subtask_name, subtask_map in task_map.get("tasks").items():
                # if subtask_map contains the skip_subtasks get it and store
                # in the subtask. if it is not there, None is stored
                # skip_subtasks = subtask_map.get("skip_subtasks")
                # if not skip_subtasks:
                #     skip_subtasks = False
                skip_subtasks = self._get_skip_subtasks(subtask_map)

                subtask = self._build_task(
                    subtask_name,
                    subtask_map,
                    allowed_attr_name,
                    allowed_values,
                    skip_subtasks=skip_subtasks,
                )
                if (
                    subtask is not None
                ):  # avoid the case subtask is None, e.g. no resources
                    subtasks.append(subtask)
            return Task(
                name=name,
                subtasks=subtasks,
                task_type=TaskExecutionType[task_type.upper()],
                skip_subtasks=skip_subtasks,
                completed_callback=callback,
            )

        # if pst, csp, cbf or pss it means that the command has to be
        # submitted to subsystem

        if handler_method := self._get_task_handler(name):
            return handler_method(
                task_map, allowed_attr_name, allowed_values, skip_subtasks
            )
        raise KeyError(f"Unsupported task name: {name}")

    def _get_task_handler(
        self: MainTaskComposer, name: str
    ) -> Optional[Callable]:
        """
        Retrieves the handler method for a given task name.

        :param name: Name of the sub-system.

        :return: Handler method or None if not found.
        """
        handlers = {
            "pst": self._create_pst_task,
            "csp_subs": self._create_csp_subarrays_task,
            "internal": self._create_internal_task,
            "cbf": partial(self._create_subsystem_task, "cbf"),
            "pss": partial(self._create_subsystem_task, "pss"),
        }
        return handlers.get(name)

    # pylint: disable=unused-argument
    def _create_internal_task(
        self: MainTaskComposer,
        task_map: Dict[str, Any],
        allowed_attr_name,
        allowed_values: list,
        skip_subtasks,
    ) -> LeafTask:
        """
        Creates an Internal LeafTask.

        :param task_map: Dictionary containing task details.
        :param allowed_attr_name: the name of the attribute which allow the
            issue of a task.
        :param allowed_values: the attribute values which allow the issue
            of a task.
        :param skip_subtasks: If set to True, all subsequent subtasks in a
            sequence will be skipped if a preceding task fails.

        :return: Internal LeafTask object.
        """
        command_name = task_map["command_name"]
        module_logger.info(f"command_name: {command_name}")
        return LeafTask(
            name=f"internal_{command_name}",
            target=self.cm,
            command_name=command_name,
            task_type=TaskExecutionType.INTERNAL,
            skip_subtasks=skip_subtasks,
        )

    def _create_subsystem_task(
        self: MainTaskComposer,
        name: str,
        task_map: Dict[str, Any],
        allowed_attr_name,
        allowed_values: list,
        skip_subtasks,
    ) -> Optional["LeafTask"]:
        """
        Creates a Subsystem LeafTask for 'cbf' or 'pss'.

        :param name: Name of the subsystem ('cbf' or 'pss').
        :param task_map: dictionary containing task details.
        :param allowed_attr_name: the name of the attribute which allow the
            issue of a task.
        :param allowed_values: the attribute values which allow the issue
            of a task.
        :param skip_subtasks: If set to True, all subsequent subtasks in a
            sequence will be skipped if a preceding task fails.

        :return: Subsystem LeafTask object or None.
        """
        command_name = task_map["command_name"]
        for fqdn in self.cm.resources:
            if name in fqdn:
                try:
                    component = self.cm.online_components[fqdn]
                    device_attr_value = getattr(component, allowed_attr_name)
                    if device_attr_value in allowed_values:
                        return LeafTask(
                            name=f"{command_name}_{name}",
                            target=component,
                            command_name=command_name,
                            task_type=TaskExecutionType.DEVICE,
                            skip_subtasks=skip_subtasks,
                            argin=self.cm.resources[fqdn],
                        )
                except AttributeError as exc:
                    module_logger.error(
                        f"The device {fqdn} has not the component map "
                        f"attribute {allowed_attr_name}: {exc}"
                    )
        module_logger.warning(
            f"No suitable components found for subsystem task '{name}'"
            f" with command '{command_name}'."
        )
        return None

    def _create_pst_task(
        self: MainTaskComposer,
        task_map: dict,
        allowed_attr_name,
        allowed_values: list,
        skip_subtasks,
    ):
        """Create a parallel task on all the pst beams that are supposed to
        receive the command

        :param task_map: dictionary containing task details.
        :param allowed_attr_name: the name of the attribute which allow the
            issue of a task.
        :param allowed_values: the attribute values which allow the issue
            of a task.
        :param skip_subtasks: If set to True, all subsequent subtasks in a
            sequence will be skipped if a preceding task fails.

        :return: a parallel Task to run on all the PST beams.
        """
        command_name = task_map["command_name"]
        subtasks = []
        for fqdn in self.cm.resources:
            try:
                if "pst" in fqdn:
                    component = self.cm.online_components[fqdn]
                    device_attr_value = getattr(component, allowed_attr_name)
                    if device_attr_value in allowed_values:
                        if subtask := LeafTask(
                            f"{command_name}_pst{fqdn[-2:]}",
                            component,
                            command_name,
                            argin=self.cm.resources[fqdn],
                        ):
                            subtasks.append(subtask)
            except AttributeError as exc:
                module_logger.error(
                    f"The device {fqdn} has not the component map "
                    f"attribute {allowed_attr_name}: {exc}"
                )

        return Task(
            f"{command_name}_pst",
            subtasks=subtasks,
            task_type=TaskExecutionType.PARALLEL,
            skip_subtasks=skip_subtasks,
        )

    def _create_csp_subarrays_task(
        self: MainTaskComposer,
        task_map: Dict,
        allowed_attr_name,
        allowed_values: list,
        skip_subtasks,
    ):
        """Create a parallel task on all csp subarrays that are online and
        connected (case of csp controller)

        :param task_map: dictionary containing task details.
        :param allowed_attr_name: the name of the attribute which allow the
            issue of a task.
        :param allowed_values: the attribute values which allow the issue
            of a task.
        :param skip_subtasks: If set to True, all subsequent subtasks in a
            sequence will be skipped if a preceding task fails.

        :return: a parallel Task to run on all the CSP Subarrays.
        """
        subtasks = []
        command_name = task_map["command_name"]
        for fqdn, component in self.cm.online_components.items():
            if "csp/subarray" in fqdn:
                # subarrays for real sub-system do not
                # support the Off command.
                if command_name == "off":
                    module_logger.warning(
                        f"Command Off not forwarded to device {fqdn}"
                        "because CSP subsytems subarrays do not support"
                        " the command"
                    )
                    continue
                try:
                    device_attr_value = getattr(component, allowed_attr_name)
                    if device_attr_value in allowed_values:
                        subtasks.append(
                            LeafTask(
                                f"{command_name}_csp_sub{fqdn[-2:]}",
                                component,
                                command_name,
                            )
                        )

                except AttributeError as exc:
                    module_logger.error(
                        f"The device {fqdn} has not the component map "
                        f"attribute {allowed_attr_name}: {exc}"
                    )

        if subtasks:
            return Task(
                f"{command_name}_csp_subs",
                subtasks=subtasks,
                task_type=TaskExecutionType.PARALLEL,
                skip_subtasks=skip_subtasks,
            )

        return None
