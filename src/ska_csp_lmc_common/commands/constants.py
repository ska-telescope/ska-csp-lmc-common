"""This module provides the constant values used by
command  processes"""

# number of workers used by the MainTaskExecutor
# TODO(SP-4456): justify this value
MAX_NUM_THREADS = 10

# total command timeout (in seconds)
# it is bounded to the execution of a MainTask
# (which may contain one or more internal/device commands)
# Value derived from a discussion in
# https://jira.skatelescope.org/browse/SKB-474
COMMAND_TIMEOUT = 30
