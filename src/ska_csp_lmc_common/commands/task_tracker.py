from __future__ import annotations

import logging
import threading
import traceback

# import enum
from typing import Any, Callable, Dict, Optional, Tuple

from ska_control_model import ResultCode, TaskStatus
from tango import DevError

module_logger = logging.getLogger(__name__)

# pylint: disable=too-many-arguments
# pylint: disable=too-many-instance-attributes
# pylint: disable=broad-exception-caught


class SubtaskResult:
    """Store the subtask result information
    (name, status, result_code, message)"""

    def __init__(
        self,
        name: str,
        status: TaskStatus,
        result: Tuple[ResultCode, str],
        device_name: Optional[str] = None,
    ):
        """
        Initialize the class with subtask details, including status, result
        code, message, and optionally the TANGO subordinate device that
        originated the message.

        :param name: the task name
        :param status: the task status
        :param result: the result code as a tuple
        :param device_name: if not None, the subordinate device originating
            the message.
        """
        self.name = name
        self.status = status
        self.result = result
        self.result_code = ResultCode.UNKNOWN
        self.message = "no message"
        self.update(status, result, device_name)

    def update(self, status, result, device_name):
        """
        Update Subtask attributes.
        It can be called with 'status' or 'result' set to None.
        """
        # retrieve the sub-system from the name
        msg = ""  # Set a default value for msg if not provided
        if status is not None:
            self.status = status
        if result:
            # Check if result contains one or two elements
            if len(result) == 2:
                result = tuple(result)
                rcode, msg = result
            else:
                rcode = result
                # Handle wrong result format coming from PST
                if isinstance(result, str):
                    if result == "Completed":
                        rcode = ResultCode.OK
                        msg = "Completed with success"
                    else:
                        rcode = ResultCode.FAILED
                        msg = result
            self.result_code = ResultCode(rcode)
            if device_name is not None:
                msg = f"{device_name}: {msg}"
            self.message = msg


class TaskTracker:
    """Class for the tracker of the task"""

    def __init__(
        self: TaskTracker,
        task_name: str,
        total_subtasks: int,
        completed_callback: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ):
        """
        :param task_name: the task name the tracker belongs to
        :param total_subtasks: the number of task's subtasks
        :param completed_callback: the parent task method the task invokes to
            report its completion
        :param logger: the logger for this object
        """
        self.task_name = task_name

        self.total_subtasks = total_subtasks
        self.completed_callback = completed_callback
        self.logger = logger or module_logger
        self._lock = threading.Lock()

        self.subtasks: Dict[str, SubtaskResult] = {}
        self.completed_subtasks = 0
        self.overall_result: ResultCode = ResultCode.UNKNOWN
        self.overall_status: TaskStatus = TaskStatus.QUEUED
        self.overall_resultcode: Tuple[ResultCode, str] = (
            ResultCode.UNKNOWN,
            "",
        )
        self.error = None
        self.task_evt = threading.Event()
        self.progress = 0

    def wait_for_task_completion(self, timeout_event=None) -> bool:
        """
        Wait for task completion, checking if the timeout has expired.

        :param timoeut_event: Timeout event flag to check for expiration,
            or None if no timeout check is required.
        :return: True if the wait ends without timeout expiration,
            otherwise False.
        """
        while not self.task_evt.is_set():
            if timeout_event and timeout_event.is_set():
                return False
            self.task_evt.wait(timeout=0.1)
        return True

    def signal_task_completion(self: TaskTracker) -> None:
        """
        Signal the completion of the task.
        """
        self.task_evt.set()

    def _is_task_already_completed(self: TaskTracker, task_name: str) -> bool:
        """
        Check if the subtask notifying a progress, is already
        in a completed state.

        :param task_name: the name of the subtask notifying a progress.

        :return: True if the task has received all notifications from its
          subtasks; otherwise, False.
        """
        if self.subtasks and task_name in self.subtasks:
            if self.subtasks[task_name].status in [
                TaskStatus.COMPLETED,
                TaskStatus.FAILED,
                TaskStatus.ABORTED,
            ]:
                # the task has already been completed/failed/aborted.
                # this callback event should be skipped to not
                # alter the progress counter of the tracker.
                self.logger.info(
                    f"Received an unexpected status update. "
                    f"The task status for {task_name} is already set to"
                    f" {TaskStatus(self.subtasks[task_name].status).name}"
                    " This condition is possibly due to a timeout."
                )
                return True
        return False

    def update_task(
        self: TaskTracker,
        task_name: str = None,
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: Any | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ) -> None:
        """
        A method that is passed to all the task's subtasks and is called by
        them upon their completion.
        It presents the same signature of a BC LrcCallback.

        :param task_name: the name of the subtask notifing a progress
        :param status: the subtask's status
        :param progress: the subtask's progress
        :param result: the subtask's result
        :param error: the subtask's exception
        """
        device_name = None
        try:
            device_name = kwargs["device_name"]
        except KeyError:
            pass
        msg = (
            f"Tracker of {self.task_name} received update from"
            f" subtask {task_name}"
        )
        if status is not None:
            msg += f" status : {TaskStatus(status).name}"
        if result is not None:
            msg += f" result : {result}"
        self.logger.debug(msg)
        skip_update = False
        with self._lock:
            skip_update = self._is_task_already_completed(task_name)
            # if the task is not completed, update the information
            if not skip_update:
                if status in [
                    TaskStatus.COMPLETED,
                    TaskStatus.FAILED,
                    TaskStatus.ABORTED,
                    TaskStatus.REJECTED,
                ]:
                    self.completed_subtasks += 1
                # update the subtasks dictionary
                if self.subtasks and task_name in self.subtasks:
                    self.subtasks[task_name].update(
                        status, result, device_name
                    )
                else:
                    self.subtasks[task_name] = SubtaskResult(
                        task_name, status, result, device_name=device_name
                    )
                # What happens if more than 1 subtask reports an error?
                if error:
                    self.error = error
                self._update_progress(progress, task_name)
                self.overall_status = self._update_overall_status()
            self.overall_result = self._update_overall_result()
            self._check_completion()

    def _check_completion(self: TaskTracker) -> None:
        """
        Checks whether all subtasks of the task have completed;
        if they have, invokes the completion callback with the updated
        task status and result information.
        """
        if self.completed_subtasks != self.total_subtasks:
            return
        try:
            status = self.overall_status
            result = self.overall_result
            msg = self._chain_message()
            self.logger.info(
                f"task_name: {self.task_name} "
                f"status: {TaskStatus(status).name} ({status}) "
                f"result: {ResultCode(result).name} ({result}) "
                f"msg: {msg}"
            )
            if self.completed_callback:
                callback_args = {
                    "task_name": self.task_name,
                    "status": status,
                    "result": [result, msg],
                }

                self.overall_resultcode = (result, msg)

                if self.error is not None:
                    self.logger.debug(f"Error is detected: {self.error }")
                    callback_args["error"] = self.error

                self.completed_callback(**callback_args)
        # pylint: disable-next=broad-except
        except Exception as exc:
            self.logger.error(traceback.format_exc())
            self.logger.error(f"Check completion failed with exception {exc}")
        finally:
            # stop the task wait
            self.logger.debug(f"Signal completion for {self.task_name}")
            self.signal_task_completion()

    def _chain_message(self: TaskTracker) -> str:
        """
        Chain the messages together to report all the information.

        :return: string with the chained result message if the status of the
            command is FAILED or REJECTED. Otherwise empty message.
        """
        internal_message = ""
        message = ""
        for subtask in self.subtasks.values():
            internal_message += f" - {subtask.name}: {subtask.message}\n"
            if subtask.status in (
                [
                    TaskStatus.FAILED,
                    TaskStatus.REJECTED,
                    TaskStatus.ABORTED,
                ]
            ):
                message += f"{subtask.message}\n"

        if self._update_overall_status() == TaskStatus.COMPLETED:
            message = (
                f"Task {self.task_name} completed with result"
                f" {ResultCode(self.overall_result).name}"
            )
        message = message.strip("\n")
        self.logger.debug(f"internal chain message: \n {internal_message}")
        if message:
            self.logger.debug(f": \n{message}")

        return message

    def _update_overall_result(self: TaskTracker) -> ResultCode:
        """Calculate the overall result code based on subtask results.

        :return: the ResultCode calculated according to the subtasks ResultCode
        """
        result_codes = [
            subtask.result_code for subtask in self.subtasks.values()
        ]
        result = ResultCode.UNKNOWN

        if result_codes:
            if all(code == ResultCode.OK for code in result_codes):
                result = ResultCode.OK
            elif any(code == ResultCode.FAILED for code in result_codes):
                result = ResultCode.FAILED
            elif any(code == ResultCode.STARTED for code in result_codes):
                result = ResultCode.STARTED
            elif all(code == ResultCode.QUEUED for code in result_codes):
                result = ResultCode.QUEUED
            elif any(code == ResultCode.ABORTED for code in result_codes):
                result = ResultCode.ABORTED
            elif any(code == ResultCode.REJECTED for code in result_codes):
                result = ResultCode.FAILED
            else:
                result = ResultCode.UNKNOWN

        return result

    def _update_overall_status(self) -> TaskStatus:
        """Update the overall task status and result based on subtask states.

        :return: the TaskStatus calculated according to the subtasks
        TaskStatus
        """
        statuses = [subtask.status for subtask in self.subtasks.values()]
        status = TaskStatus.QUEUED

        if statuses:
            if all(status == TaskStatus.COMPLETED for status in statuses):
                status = TaskStatus.COMPLETED
            elif any(status == TaskStatus.ABORTED for status in statuses):
                status = TaskStatus.ABORTED
            elif any(status == TaskStatus.IN_PROGRESS for status in statuses):
                status = TaskStatus.IN_PROGRESS
            elif any(status == TaskStatus.FAILED for status in statuses):
                status = TaskStatus.FAILED
            elif any(status == TaskStatus.REJECTED for status in statuses):
                status = TaskStatus.REJECTED
            else:
                status = TaskStatus.QUEUED

        return status

    def _update_progress(
        self: TaskTracker, progress: int, subtask_name: str
    ) -> None:
        """Evaluate the task's progress percentage.

        :param progress (int): percentage of progress.
            If not added is considered equal to 100
        :param subtask_name (str):
        """
        if not progress:
            progress = 100

        new_progress = int(progress * self.get_progress() / 100.0)

        # Log progress change only if it differs from the current progress
        if new_progress != self.progress:
            self.logger.info(
                f"Task {self.task_name}.{subtask_name} progress "
                f"{self.completed_subtasks}/{self.total_subtasks}"
            )
            self.logger.debug(
                f"Task {self.task_name} progress = {new_progress}%"
            )
            self.progress = new_progress

    def get_progress(self) -> float:
        """Return the progress of task execution

        :return: percentage of the tasks execution as float"""
        return (
            float(self.completed_subtasks) / float(self.total_subtasks) * 100
        )

    def get_status(self) -> TaskStatus:
        """Return the status of the task.

        :return: the TaskStatus
        """
        return self._update_overall_status()

    def get_result(self) -> ResultCode:
        """Return the status of the task.

        :return: the TaskStatus
        """
        return self._update_overall_result()
