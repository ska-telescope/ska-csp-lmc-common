from __future__ import annotations  # allow forward references in type hints

import enum
import logging
import traceback
from typing import Any, Callable, List, Optional

from ska_csp_lmc_common.commands.task_tracker import TaskTracker

module_logger = logging.getLogger(__name__)
# pylint: disable=too-many-arguments
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-function-args


class TaskExecutionType(enum.IntEnum):
    """An enumeration for handling different TaskExecution Types."""

    SEQUENTIAL = 0
    PARALLEL = 1
    DEVICE = 2
    INTERNAL = 3


class BaseTask:
    """Parent class for tasks"""

    def __init__(
        self: BaseTask,
        name: str,
        task_type: Optional[TaskExecutionType] = TaskExecutionType.SEQUENTIAL,
        skip_subtasks: Optional[bool] = None,
        completed_callback: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ):
        """
        The Task executes several subtasks (Task or LeafTask) that can
        be run insequence or in parallel.

        :param name: the task name
        :param task_type: 'parallel' or 'sequential'.
        :param skip_subtasks: if True, the flag is allow to interrupt a
        sequential task in case one of the subtask failes.
        :param logger: the logger to be used by this object.
        """
        self.name = name
        self.task_type = task_type
        self.logger = logger or module_logger
        self.tracker = TaskTracker(self.name, 1, completed_callback)
        self.lrcsubscriptions = None
        self.skip_subtasks = skip_subtasks

    def parent_callback(self: BaseTask) -> Callable:
        """
        The parent task callback invoked by a subtask's task.
        """
        return self.tracker.update_task

    def set_completed_callback(
        self: BaseTask,
        callback: Callable,
    ):
        """Set the completed callback for the task tracker"""
        original_callback = self.tracker.completed_callback

        def _callback(**kwargs):
            if original_callback is not None:
                try:
                    original_callback()
                # pylint: disable-next=broad-except
                except Exception:
                    self.logger.warning(
                        f"Original callback of task {self.name} not set"
                    )
                    self.logger.warning(traceback.format_exc())
            if callback is not None:
                callback(**kwargs)

        self.tracker.completed_callback = _callback


class LeafTask(BaseTask):
    """Class to describe the last task in the task tree"""

    def __init__(
        self: LeafTask,
        name: str,
        target: Any,
        command_name: str,
        task_type: Optional[TaskExecutionType] = TaskExecutionType.DEVICE,
        skip_subtasks: Optional[bool] = None,
        completed_callback: Optional[Callable] = None,
        argin: Any = None,
        logger: Optional[logging.Logger] = None,
    ):
        """
        The LeafTask executes a single command on a TANGO device or a method
        of the device ComponentManager.

        be run insequence or in parallel.
        :param name: the task name
        :param target: the target on which the command is executed.
        :param command_name: the command executed by this task.
        :param task_type: 'internal' or 'device'.
        :param skip_subtasks: if True, the flag is allow to interrupt a
        sequential task in case one of the subtask failes.
        :param logger: the logger to be used by this object.
        """
        super().__init__(
            name, task_type, skip_subtasks, completed_callback, logger
        )
        self.target = target
        self.command_name = command_name
        self.argin = argin

    def __eq__(self, other):
        """Evaluate the equality between LeafTasks, useful in unit test"""
        if not isinstance(other, LeafTask):
            return False
        conditions = [
            self.name == other.name,
            self.target == other.target,
            self.command_name == other.command_name,
            self.task_type == other.task_type,
            self.argin == other.argin,
        ]
        for i, check in enumerate(conditions):
            if check is False:
                self.logger.error(
                    f"Condition n{i+1} in LeafTask {self.name} not satisfied"
                )
        return all(conditions)


class Task(BaseTask):
    """Class to describe a task"""

    def __init__(
        self: Task,
        name: str,
        subtasks: List[Task | LeafTask],
        task_type: Optional[TaskExecutionType] = TaskExecutionType.SEQUENTIAL,
        skip_subtasks: Optional[bool] = None,
        completed_callback: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ):
        """
        The Task executes several subtasks (Task or LeafTask) that can
        be run insequence or in parallel.

        :param name: the task name
        :param task_type: 'parallel' or 'sequential'.
        :param subtasks: List of Task/LeafTask objects.
        :param skip_subtasks: if True, the flag is allow to interrupt a
        sequential task in case one of the subtask failes.
        :param completed_callback: the callback  invoked when the task ends.
        :param task_abort_event: the event set when Abort is invoked.
        :param logger: the logger to be used by this object.
        """
        super().__init__(
            name, task_type, skip_subtasks, completed_callback, logger
        )
        self.subtasks = subtasks
        self.completed_callback = completed_callback
        # (TODO): remove this instantiation since is done in another method
        self.tracker = TaskTracker(
            self.name, len(subtasks), completed_callback
        )

    def instantiate_tracker(self):
        """
        Instantiate the tracker
        """
        self.tracker = TaskTracker(
            self.name, len(self.subtasks), self.completed_callback
        )

    def __eq__(self, other):
        """Evaluate the equality between Tasks, useful in unit test"""
        if not isinstance(other, Task):
            return False

        # the list of conditions to be compared
        conditions = [
            self.name == other.name,
            self.task_type == other.task_type,
        ]
        # add the singular check for equality on subtask
        conditions.extend(
            subtask == other.subtasks[i]
            for i, subtask in enumerate(self.subtasks)
        )
        # print information to retrieve the inequality in case of errors
        for i, check in enumerate(conditions):
            if check is False:
                self.logger.error(
                    f"Condition n{i+1} in task {self.name} not satisfied"
                )
                if i >= 2:
                    self.logger.error(
                        f"Error in comparing subtask {self.subtasks[i-2].name}"
                    )

        return all(conditions)
