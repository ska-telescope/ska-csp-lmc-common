"""This module provides for specialization of the base class
task Executor that performs asynchronous execution of tasks."""

from __future__ import annotations

import concurrent.futures
import json
import logging
import threading
from functools import partial
from typing import Any, Callable, Optional

from ska_control_model import ResultCode, TaskStatus

from ska_csp_lmc_common.commands.constants import (
    COMMAND_TIMEOUT,
    MAX_NUM_THREADS,
)
from ska_csp_lmc_common.commands.device_executor import DeviceTaskExecutor
from ska_csp_lmc_common.commands.task import LeafTask, Task, TaskExecutionType

module_logger = logging.getLogger(__name__)

# pylint: disable=too-many-arguments
# pylint: disable=broad-exception-caught
# pylint: disable=too-many-instance-attributes


class MainTaskExecutor:
    """
    An asynchronous executor of tasks responsible for unpacking
    the CSP Main Task into Task/LeafTask and executing them.
    It can perform both parallel and serial tasks.
    """

    def __init__(
        self: MainTaskExecutor,
        prefix="Main",
        workers=MAX_NUM_THREADS,
        command_timeout=COMMAND_TIMEOUT,
        logger=None,
    ):
        """Manage the execution of a main task in the CSP.

        :param prefix: the prefix to add to the thread name
        :param logger: the logger to be used by this object.
        """
        self.logger = logger or module_logger
        self._abort_event = threading.Event()
        self._skip_subtask_event = threading.Event()
        self._max_workers = workers
        self._executor = concurrent.futures.ThreadPoolExecutor(
            max_workers=self._max_workers,
            thread_name_prefix=f"{prefix}TaskThreadPool",
        )
        self._device_executor = DeviceTaskExecutor(logger)
        self._submit_lock = threading.Lock()
        self._command_timeout = command_timeout
        self._timeout_event = threading.Event()
        self.stop_event = threading.Event()
        self.main_task = None

    @property
    def timeout_event(self: MainTaskExecutor) -> threading.Event:
        """Retrun the timeout event"""
        return self._timeout_event

    def set_command_timeout(self, timeout: int):
        """Set the command timeout"""
        self._command_timeout = timeout

    def get_command_timeout(self) -> int:
        """Return the command timeout"""
        return self._command_timeout

    def _call_task_callback(
        self: MainTaskExecutor,
        task_callback: Callable | None,
        **kwargs: Any,
    ) -> None:
        """Call the callback if it exists and add Kwargs arguments to it

        :param task_callback: the callback to be called when the status
            or progress of the task execution changes
        :param kwargs: keyword arguments to the function
        """
        if task_callback is not None:
            task_callback(**kwargs)

    def submit_main_task(
        self: MainTaskExecutor,
        task: Task | LeafTask,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Submit a new CSP Main Task which may contain one or more Tasks.

        :param task: the Main Task to be executed.
        :param task_callback: the callback to be called when the status
            or progress of the task execution changes

        :return: (TaskStatus, message)
        """
        self.main_task = task

        def run_main_task():
            """
            Method to run the Main Task in a separate thread
            """
            # clear the timeout and skip_subtask event before the execution of
            # a new task
            self._timeout_event.clear()
            self._skip_subtask_event.clear()
            self._submit_task(task, task_callback)

        with self._submit_lock:
            try:
                self.logger.info("Starting CSP Main Task Submission")
                # Create a thread for running the task
                task_thread = threading.Thread(target=run_main_task)
                task_thread.start()

                # Wait for the thread to complete with a timeout
                task_thread.join(self._command_timeout)

                if task_thread.is_alive():
                    # If the task is still running, raise a TimeoutError
                    self._timeout_event.set()
                    self.logger.warning(
                        f"Task execution exceeded {self._command_timeout} "
                        f" seconds timeout"
                    )
                task.tracker.wait_for_task_completion()
                self.logger.info("End of the MainTask")
            except RuntimeError:
                self._handle_task_exception(
                    task_callback,
                    message="Queue is aborting",
                    status=TaskStatus.REJECTED,
                )
            except Exception as exc:  # pylint: disable=broad-except
                self._handle_task_exception(
                    task_callback,
                    message=f"Unhandled exception: {str(exc)}",
                    status=TaskStatus.FAILED,
                )

    def _submit_task(
        self: MainTaskExecutor,
        task: Task,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """Submit a new Task.

        :param task: the Task to be executed.
        :param task_callback: the callback to be called when the status
            or progress of the task execution changes

        :return: (TaskStatus, message)
        """
        try:
            self.logger.info(
                f"Submitting task '{task.name}' "
                f"type: {TaskExecutionType(task.task_type).name.lower()}"
            )
            if task.task_type == TaskExecutionType.PARALLEL:
                self._parallel_execution(task, task_callback)
            elif task.task_type == TaskExecutionType.SEQUENTIAL:
                self._sequential_execution(task, task_callback)
            elif task.task_type == TaskExecutionType.INTERNAL:
                self._internal_execution(task, task_callback)
            elif task.task_type == TaskExecutionType.DEVICE:
                self._device_execution(task, task_callback)
            else:
                return (
                    TaskStatus.REJECTED,
                    f"Execution type {task.task_type} not supported",
                )
        except Exception as exc:  # pylint: disable=broad-except
            self._handle_task_exception(
                task_callback,
                message=f"Command failed with exception: {exc}",
                status=TaskStatus.FAILED,
            )

        return TaskStatus.COMPLETED, f"Completed Task {task.name}"

    def _sequential_execution(
        self: MainTaskExecutor,
        task: Task,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Execute a sequential task.

        :param task: the Task to be executed.
        :param task_callback: the callback invoked by the current task to
            report its progress or completion

        :return: (TaskStatus, message)
        """
        thread = threading.current_thread()
        self.logger.debug(
            f"Running sequential task {task.name} in thread {thread.name}"
        )

        if self._abort_event.is_set():
            self.logger.debug(
                f"sequential task {task.name} start to be ABORTED"
            )
            # ResultCode is set to ABORTED because the sequential is
            # still in progress but its subtasks are being aborted
            result = [ResultCode.ABORTED, f"{task.name}"]
        else:
            result = [ResultCode.STARTED, f"{task.name}"]
        self._call_task_callback(
            task_callback,
            task_name=task.name,
            status=TaskStatus.IN_PROGRESS,
            progress=0,
            result=result,
        )
        parent_callback = task.parent_callback()
        task.set_completed_callback(task_callback)

        for subtask in task.subtasks:
            self.logger.info(
                f"Executing {task.name}.{subtask.name} sequentially"
            )
            self._submit_task(subtask, parent_callback)
            subtask.tracker.wait_for_task_completion()
            self.logger.debug(
                f"Sequential execution of task {task.name}"
                f".{subtask.name} completed"
            )
            # Verify if the current command encountered a failure
            # If a task or one of its subtasks is allowed to fail
            # (skip_subtasks=False) , proceed; otherwise, all
            # subsequent subtasks will be skipped (not executed).
            if subtask.skip_subtasks or task.skip_subtasks:
                self._check_skip_flag(task)

        return TaskStatus.IN_PROGRESS, f"SEQUENTIAL execution of {task.name}"

    def _parallel_execution(
        self: MainTaskExecutor,
        task: Task,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Execute a parallel task.

        :param task: the Task to be executed.
        :param task_callback: the callback invoked by the current task to
            report its progress or completion

        :return: (TaskStatus, message)
        """
        if self._abort_event.is_set():
            self.logger.debug(
                f"parallel task {task.name} " "start to be ABORTED"
            )
            # ResultCode is set to ABORTED because the parallel is
            # still in progress but its subtasks are being aborted
            # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            result = [ResultCode.ABORTED, f"{task.name}"]
        else:
            result = [ResultCode.STARTED, f"{task.name}"]
        thread = threading.current_thread()
        self.logger.debug(f"Running task {task.name} in thread {thread.name}")
        # signal the parent task that the parallel task has started
        self._call_task_callback(
            task_callback,
            task_name=task.name,
            progress=0,
            status=TaskStatus.IN_PROGRESS,
            result=result,
        )
        parent_callback = task.parent_callback()
        task.set_completed_callback(task_callback)

        futures = [
            self._executor.submit(self._submit_task, subtask, parent_callback)
            for subtask in task.subtasks
        ]
        self.logger.debug(f"Waiting for the end of task {task.name}")

        futures.append(
            self._executor.submit(task.tracker.wait_for_task_completion)
        )

        # synchronize to wait for the end of the parallel execution
        concurrent.futures.wait(futures)  # TODO(Stefano): Timeout here
        # self.logger.info(f"Parallel task {task.name} completed")

        # if task.skip_subtasks:
        #     # for parallel task, only the flag assign to the task is
        #       considered
        #     self._check_skip_flag(task)

        return TaskStatus.COMPLETED, f"PARALLEL execution of {task.name}"

    def is_leaf_task_discarded(
        self: MainTaskExecutor, task: LeafTask, callback: Callable
    ) -> bool:
        """
        Verify if the main task timeout event or the skip event are set.
        If it is the case, invoke the callback with TaskStatus.REJECTED,
        result ResultCode.FAILED.
        In this case, the leaf task is not executed at all.

        :param task: the discarded task
        :param callback: the callback to invoke

        :return: True if the timeout expired, otherwise False
        """
        if self._timeout_event.is_set() or self._skip_subtask_event.is_set():
            status = TaskStatus.REJECTED
            msg = ""
            if task.task_type == TaskExecutionType.DEVICE:
                main_msg = (
                    f"LRC execution of command {task.command_name} on "
                    f"{task.target.fqdn} is discarded because"
                )
            else:
                main_msg = (
                    f"Execution of CM method {task.command_name} "
                    "is discarded because"
                )

            if self._timeout_event.is_set():
                msg = (
                    f"{main_msg} the timeout of {self._command_timeout} sec."
                    " has been reached."
                )
            elif self._skip_subtask_event.is_set():
                msg = (
                    f"{main_msg} a failure was detected in the execution of "
                    "one of the preceding subtasks, and the task does not "
                    "allow the continuation of the other operations"
                )

            self._call_task_callback(
                callback,
                task_name=task.name,
                status=status,
                result=[ResultCode.REJECTED, msg],
            )
            return True
        return False

    def is_leaf_task_aborted(
        self: MainTaskExecutor, task: LeafTask, callback: Callable
    ) -> bool:
        """
        Verify if the abort event is set and in case invoke the
        callback with TaskStatus.ABORTED, result ResultCode.ABORTED.
        In this case, the leaf task is not executed at all.

        :param task: the aborted task
        :param callback: the callback to invoke

        :return: True if the abort flag is set, otherwise False
        """
        if self._abort_event.is_set():
            status = TaskStatus.ABORTED
            if task.task_type == TaskExecutionType.DEVICE:
                msg = (
                    f"LRC command {task.command_name} on "
                    f"{task.target.fqdn} not executed "
                    "due to an abort request in progress."
                )
            else:
                msg = (
                    f"CM method {task.command_name} not"
                    " executed due to an abort request in progress."
                )
                task.tracker.signal_task_completion()
            self.logger.info(msg)
            self._call_task_callback(
                callback,
                task_name=task.name,
                status=status,
                result=[ResultCode.ABORTED, msg],
            )
            return True
        return False

    def _internal_execution(
        self: MainTaskExecutor,
        task: LeafTask,
        task_callback: Optional[Callable] = None,
    ):
        """
        Execute an internal CSP operation.

        :param task: The task to execute.
        :param task_callback: Callback for task progress and completion.
        """
        status = TaskStatus.COMPLETED
        result_code = ResultCode.OK
        msg = ""
        task.set_completed_callback(task_callback)
        parent_callback = task.parent_callback()
        if not (
            self.is_leaf_task_aborted(task, parent_callback)
            or self.is_leaf_task_discarded(task, parent_callback)
        ):

            thread_name = threading.current_thread().name
            self.logger.debug(
                f"Running task {task.name} in thread {thread_name}"
            )

            try:
                command_name = task.command_name
                internal_method = getattr(task.target, command_name)

                # Call the target method with or without arguments
                if task.argin:
                    status, result_code = internal_method(task.argin)
                else:
                    status, result_code = internal_method()

                msg = (
                    f"Execution of the task {task.name} completed. "
                    f"ResultCode: {result_code.name}"
                )

            except Exception as exc:
                msg = (
                    f"Execution of task {task.name}"
                    f" failed with exception: {exc}"
                )
                self.logger.error(msg)
                status, result_code = TaskStatus.FAILED, ResultCode.FAILED

            task.tracker.signal_task_completion()

            # Invoke parent callback with task result
            self._call_task_callback(
                parent_callback,
                task_name=command_name,
                status=status,
                result=[result_code, msg],
            )

    def _device_execution(
        self: MainTaskExecutor,
        task: LeafTask,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Execute a LRC on a TANGO device through the DeviceTaskExecutor.

        :param LeafTask: the LeafTask to be executed on a device.
        :param task_callback: the callback invoked by the current task to
            report its progress or completion

        :return: (TaskStatus, message)
        """

        parent_callback = task.parent_callback()
        task.set_completed_callback(task_callback)
        if (not self.is_leaf_task_aborted(task, parent_callback)) and (
            not self.is_leaf_task_discarded(task, parent_callback)
        ):
            try:
                thread = threading.current_thread()
                self.logger.debug(
                    f"Running task {task.name} in thread" f" {thread.name}"
                )
                # Need to do this check otherwise a None argument
                # is transformed in a string 'null'
                if task.argin is not None:
                    # only if it's a dictionary
                    if isinstance(task.argin, dict):
                        task.argin = json.dumps(task.argin)
                task.lrc_subscriptions = self._device_executor.invoke_lrc(
                    task.target,
                    partial(
                        parent_callback,
                        task_name="lrcCallback",
                        source_dev=task.target.name,
                        device_name=task.target.name,
                    ),
                    task.command_name,
                    task.argin,
                )
                if not task.tracker.wait_for_task_completion(
                    self._timeout_event
                ):
                    msg = (
                        f"Timeout expired executing {task.command_name}"
                        f" command on {task.target.fqdn}"
                    )
                    self.logger.warning(msg)
                    # Here, we need to simulate the callback coming
                    # from `lrcCallback`,which is why the
                    # `task_name'='lrcCallback'` instead of
                    # instead of `task_name=task.name`.
                    self._call_task_callback(
                        parent_callback,
                        task_name="lrcCallback",
                        status=TaskStatus.FAILED,
                        result=[ResultCode.FAILED, msg],
                        device_name=task.target.name,
                    )
                # status = TaskStatus.IN_PROGRESS
                # msg = f"LRC execution of {task.lrc_subscriptions.command_id}"
            # pylint: disable-next=broad-except
            except Exception as exc:
                msg = f"Task {task.name} failed with exception: {exc}"
                self.logger.error(msg)
                task_status = TaskStatus.COMPLETED
                if "cbf" in task.target.name:
                    task_status = TaskStatus.FAILED
                self._call_task_callback(
                    parent_callback,
                    task_name=task.name,
                    status=task_status,
                    result=[ResultCode.FAILED, msg],
                    device_name=task.target.name,
                    error=exc,
                )
            # return (status, msg)

    def _check_skip_flag(self: MainTaskExecutor, task: Task):
        """
        Given that the `skip_subtasks` flag for the task or subtask is set,
        this function checks whether the input task has failed. If so, it
        ensures the `skip_subtask_event` flag is set, provided it is not
        already set. All subsequent subtasks will be skipped.

        :param task: sequential task
        """

        if not self._skip_subtask_event.is_set():
            task_result = task.tracker.get_result()
            if task_result == ResultCode.FAILED:
                self.logger.warning(
                    f"A task/subtask of the sequential task `{task.name}` "
                    "has failed: all subsequent subtasks/tasks will be "
                    "rejected."
                )
                self._skip_subtask_event.set()

    def shutdown(self: MainTaskExecutor, wait_flag: bool = True) -> None:
        """
        Shutdown the main ThreadPoolExecutor

        :param wait_flag: blocking flag. If True (default) the thread is
        completed before return.
        """
        self._executor.shutdown(wait=wait_flag)

    def clear_abort_flag(self: MainTaskExecutor):
        """Clear the abort flag"""
        self._abort_event.clear()

    def abort(
        self: MainTaskExecutor, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Abort the task executor.

        :param task_callback: the callback to be called when the status
            or progress of the task execution changes

        :return: (TaskStatus, message)
        """
        if task_callback is not None:

            task_callback(
                status=TaskStatus.IN_PROGRESS,
                result=[ResultCode.STARTED, "Abort Started"],
            )

        self._abort_event.set()
        return TaskStatus.IN_PROGRESS, "Aborting tasks"

    def _handle_task_exception(
        self: MainTaskExecutor, task_callback, message, status
    ) -> tuple[TaskStatus, str]:
        """Helper to handle task failures.

        :param task_callback: the callback invoked by the current task to
            report its progress or completion
        :param message: the message of the exception.
        :param status: the TaskStatus of the Task.

        :return: (TaskStatus, message)
        """
        self.logger.warning(message)
        if task_callback:
            task_callback(status=status, result=[ResultCode.FAILED, message])
        return status, message
