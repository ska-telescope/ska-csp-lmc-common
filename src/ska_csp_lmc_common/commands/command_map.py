# How to create/populate a command map:
# - the most external key refers to the name of the Job (i.e. the external
#   task)
# - a Task is characterized by having mandatory "type" and "tasks" keywords;
#   in this case the name of the task is the "parent" key;
# - "cbf" and "pss" keywords create LeafTasks on cbf or pss connected
#   subsystem (i.e. subarray or controller), the command_name is the Tango
#   Command Name of the subsystem;
# - "internal" keyword creates a LeafTask of type "internal" with component
#   manager as target and no argin; command_name is the method_name
# - "pst" will create a parallel task with LeafTasks on all applicable beams
#   (i.e. online and/or specified in input)
# - "csp_subs" will create a parallel task with LeafTasks on all online
#   subarrays with no argin (only for controller)
# - resources: A dictionary where the keys are the FQDNs of the subsystems
#   to which the command should be forwarded. If the corresponding entry is
#   None`, the command has no argument.
# - skip_subtasks (boolean): This flag specifies the behavior of
#   tasks/subtasks when they belong to a sequential task. The flag can be
#   specified either at the task level or the subtask level (default False).
#   In both cases, if the tagged task/subtask encounters a failure condition
#   during the execution of preceding tasks, the subsequent tasks will not be
#   executed.
#   NOTE: The flag has no effect if set to a subtask belonging to a parallel
#   task (no usecase for it)


from ska_control_model import ObsState
from tango import DevState


class BaseCommandMap:
    """
    Common Command Map.
    """

    def __init__(self):
        self.controller_command_map = {
            "on": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "state",
                    "attr_value": [
                        DevState.OFF,
                        DevState.STANDBY,
                        DevState.UNKNOWN,
                    ],
                },
                "tasks": {
                    "csp_subs": {"command_name": "on"},
                    "pst": {"command_name": "on"},
                    "pss": {"command_name": "on"},
                    "cbf": {"command_name": "on"},
                },
            },
            "off": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "state",
                    "attr_value": [
                        DevState.ON,
                        DevState.STANDBY,
                        DevState.UNKNOWN,
                    ],
                },
                "tasks": {
                    "csp_subs": {"command_name": "off"},
                    "pst": {"command_name": "off"},
                    "pss": {"command_name": "off"},
                    "cbf": {"command_name": "off"},
                },
            },
            "standby": {},
            "reset": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "state",
                    "attr_value": [
                        DevState.OFF,
                        DevState.ON,
                        DevState.STANDBY,
                        DevState.UNKNOWN,
                        DevState.FAULT,
                    ],
                },
                "tasks": {
                    "csp_subs": {"command_name": "reset"},
                    "pst": {"command_name": "reset"},
                    "pss": {"command_name": "reset"},
                    "cbf": {"command_name": "reset"},
                },
            },
        }

        self.subarray_command_map = {
            "on": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "state",
                    "attr_value": [
                        DevState.OFF,
                        DevState.STANDBY,
                        DevState.UNKNOWN,
                    ],
                },
                "tasks": {
                    "pss": {"command_name": "on"},
                    "cbf": {"command_name": "on"},
                },
            },
            "off": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "state",
                    "attr_value": [
                        DevState.ON,
                        DevState.STANDBY,
                        DevState.UNKNOWN,
                    ],
                },
                "tasks": {
                    "pss": {"command_name": "off"},
                    "cbf": {"command_name": "off"},
                },
            },
            "assign": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "obs_state",
                    "attr_value": [ObsState.EMPTY, ObsState.IDLE],
                },
                "tasks": {
                    "internal": {"command_name": "connect_to_pst"},
                    "pss": {"command_name": "assignresources"},
                    "cbf": {"command_name": "assignresources"},
                },
            },
            "release": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "obs_state",
                    "attr_value": [ObsState.IDLE],
                },
                "tasks": {
                    "internal": {"command_name": "disconnect_from_pst"},
                    "pss": {"command_name": "releaseresources"},
                    "cbf": {"command_name": "releaseresources"},
                },
            },
            "release_all": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "obs_state",
                    "attr_value": [ObsState.IDLE],
                },
                "tasks": {
                    "internal": {"command_name": "disconnect_from_all_pst"},
                    "pss": {"command_name": "releaseallresources"},
                    "cbf": {"command_name": "releaseallresources"},
                },
            },
            "configure": {
                "type": "sequential",
                "allowed_states": {
                    "attr_name": "obs_state",
                    "attr_value": [ObsState.READY, ObsState.IDLE],
                },
                "tasks": {
                    "configure_pst_pss": {
                        "type": "parallel",
                        "tasks": {
                            "pst": {"command_name": "configurescan"},
                            "pss": {"command_name": "configurescan"},
                        },
                    },
                    "internal": {
                        "command_name": "update_json",
                        "skip_subtasks": True,
                    },
                    "cbf": {"command_name": "configure"},
                },
            },
            "scan": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "obs_state",
                    "attr_value": [ObsState.READY],
                },
                "tasks": {
                    "pst": {"command_name": "scan"},
                    "pss": {"command_name": "scan"},
                    "cbf": {"command_name": "scan"},
                },
            },
            "end_scan": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "obs_state",
                    "attr_value": [ObsState.SCANNING],
                },
                "tasks": {
                    "pst": {"command_name": "endscan"},
                    "pss": {"command_name": "endscan"},
                    "cbf": {"command_name": "endscan"},
                },
            },
            "abort": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "obs_state",
                    "attr_value": [
                        ObsState.SCANNING,
                        ObsState.CONFIGURING,
                        ObsState.READY,
                        ObsState.IDLE,
                        ObsState.RESOURCING,
                    ],
                },
                "tasks": {
                    "pst": {"command_name": "abort"},
                    "pss": {"command_name": "abort"},
                    "cbf": {"command_name": "abort"},
                },
            },
            "gotoidle": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "obs_state",
                    "attr_value": [
                        ObsState.READY,
                    ],
                },
                "tasks": {
                    "pst": {"command_name": "gotoidle"},
                    "pss": {"command_name": "gotoidle"},
                    "cbf": {"command_name": "gotoidle"},
                },
            },
            "reset": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "state",
                    "attr_value": [
                        DevState.OFF,
                        DevState.ON,
                        DevState.STANDBY,
                        DevState.UNKNOWN,
                        DevState.FAULT,
                    ],
                },
                "tasks": {
                    "pst": {"command_name": "reset"},
                    "pss": {"command_name": "reset"},
                    "cbf": {"command_name": "reset"},
                },
            },
            "restart": {
                "type": "sequential",
                "allowed_states": {
                    "attr_name": "obs_state",
                    "attr_value": [ObsState.FAULT, ObsState.ABORTED],
                },
                "tasks": {
                    "restart_subsystems": {
                        "type": "parallel",
                        "tasks": {
                            "pst": {"command_name": "obsreset"},
                            "pss": {"command_name": "restart"},
                            "cbf": {"command_name": "restart"},
                        },
                    },
                    "internal": {"command_name": "disconnect_from_all_pst"},
                },
            },
        }

    def get_subarray_command_map(self):
        """Returns the current controller subarray map."""
        return self.subarray_command_map

    def get_controller_command_map(self):
        """Returns the current controller command map."""
        return self.controller_command_map
