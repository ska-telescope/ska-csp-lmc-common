from __future__ import annotations

import logging
import threading
import traceback
from queue import Empty, Full, Queue
from typing import Callable, Dict, List, Optional

import tango
from ska_control_model import AdminMode, HealthState, ObsState

# Tango imports
from tango import AttrQuality, EventType

from ska_csp_lmc_common.component import Component, CspEvent
from ska_csp_lmc_common.model import (
    HealthStateModel,
    ObsStateModel,
    OpStateModel,
)

# pylint: disable=too-many-instance-attributes
# pylint: disable=unnecessary-pass
# pylint: disable=invalid-name
# pylint: disable=broad-except

module_logger = logging.getLogger(__name__)


class BaseEventManager:
    """Aggregator class to manage the event subscriptions."""

    def __init__(
        self: BaseEventManager,
        task_callback: Callable,
        max_queue_size: Optional[int] = 100,
        logger: Optional[logging.Logger] = None,
    ):
        """Initialize the BaseEventManager for the current device.

        Handles the subscription and un-subscription  of attributes on the CSP
        sub-system components.
        It also performs the aggregation of tha main attributes (State,
        healthState and obsState) of the CSP subordinate sub-systems to
        evaluate the state of the CSP as a whole.

        :param logger: The device or python logger if default is None.
        """
        self._list_of_components = []
        self._attrs_subscribed = {}  # device fqdn, list subscribed attributes
        self.logger = logger or module_logger
        self.lock = threading.Lock()
        self._event_queue = Queue(max_queue_size)
        self._stop = False
        self._update_attribute = task_callback
        self._worker_thread = threading.Thread(
            name="event_manager",
            target=self._run,
            daemon=True,
        )
        try:
            self._worker_thread.start()
        except Exception as e:
            self.logger.error(f"Event Manager worker thread not started: {e}")

    @property
    def attrs_subscribed(
        self: BaseEventManager,
    ) -> Dict[str, List[str]]:
        """Return the dictionary with the list of attributes subscribed for
        each CSP sub-system component.

        :return: The list of attributes to subscribe on each sub-system
            component.
        """
        return self._attrs_subscribed

    @property
    def running(self: BaseEventManager) -> bool:
        """Return true if the worker thread is running
        :rtype bool
        """
        return not self._stop

    def stop(self: BaseEventManager):
        """
        Stop the running BaseEventManager worker thread.
        """
        self._stop = True
        self.logger.debug("Joining the thread")
        self._worker_thread.join()

    def _run(self: BaseEventManager) -> None:
        """Enqueue the event in the BaseEventManager queue"""
        while not self._stop:
            try:
                event = self._event_queue.get(block=True, timeout=1.0)
                self._process_event(event)
                self._event_queue.task_done()
            except Empty:
                pass
            except Exception as err:
                self.logger.error(f"Received error in event run: {err}")
        self.logger.warning("Stop flag set: exiting from BaseEventManager")

    #########################
    #
    # Class protected methods
    #
    # ########################
    def _process_event(self: BaseEventManager, evt: CspEvent) -> None:
        # pylint: disable=unused-argument
        """
        Handle the event fetched from the queue.

        :param evt: an object with the event data
        """
        self.logger.warning("To specialize!!")
        raise NotImplementedError

    def _add_component(self: BaseEventManager, component: Component):
        """Update the internal list of the CSP sub-system components.

        :param component: the sub-system Component instance
        """
        if component not in self._list_of_components:
            self._list_of_components.append(component)

    ######################
    #
    # Class public methods
    #
    ######################

    def subscribe_event(
        self: BaseEventManager,
        component: Component,
        attr_name: str,
        evt_type: EventType,
    ) -> None:
        """Subscribe for an event on the required CSP sub-system component and
        register a callback for the events fired by the device monitored by
        the sub-system component.

        :param component: the target sub-element component
        :param attr_name: the name of the attribute for which events are
            subscribed
        :param evt_type: the event type: CHANGE_EVENT, PERIODIC_EVENT, etc.
        """
        subscribe_succeeded = False
        with tango.EnsureOmniThread():
            if (component.fqdn not in self._attrs_subscribed) or (
                attr_name not in self._attrs_subscribed[component.fqdn]
            ):
                subscribe_succeeded, _ = component.subscribe_attribute(
                    attr_name, evt_type, self.evt_callback
                )

                with self.lock:
                    if subscribe_succeeded:
                        if component.fqdn not in self._attrs_subscribed:
                            self._attrs_subscribed[component.fqdn] = []
                        self._attrs_subscribed[component.fqdn].append(
                            attr_name
                        )
                        self.logger.debug(
                            f"component {component.fqdn} attrs_subscribed"
                            f" {self._attrs_subscribed[component.fqdn]}"
                        )
                        self._add_component(component)
                        self.logger.debug(
                            f"attributes subscribed: {self._attrs_subscribed}"
                        )
                    else:
                        self.logger.warning(
                            f"Subscription for attribute {attr_name} on device"
                            f" {component.fqdn} failed!"
                        )
            else:
                self.logger.debug(
                    f"Attribute {attr_name} on device"
                    f" {component.fqdn} already subscribed"
                )

    def unsubscribe_event_on_component(
        self: BaseEventManager,
        component: Component,
        attrs_list: Optional[List] = None,
    ) -> None:
        """Unsubscribe from events for the attributes specified in the input
        list.

        :param component: the component with the subscribed events
        :param attrs_list: the list of attributes to unsubscribe events on
        """
        with self.lock:
            if not attrs_list and component.fqdn in self._attrs_subscribed:
                attrs_list = self._attrs_subscribed[component.fqdn]
            if subscribed_attrs_init := component.event_attrs:
                # unsubscribe the events for the attributes on the component
                component.unsubscribe_attribute(attrs_list)
                # get the list of attributes still subscribed on the component
                subscribed_attrs = component.event_attrs
                # get the list of unsubscribed attribute events
                unsubscribed = set(subscribed_attrs_init) - set(
                    subscribed_attrs
                )
                # remove the reference to  the unsubscribed attribute event
                # from the dictionary of events
                for attr_name in unsubscribed:
                    self.logger.debug(self._attrs_subscribed[component.fqdn])
                    self._attrs_subscribed[component.fqdn].remove(attr_name)

            else:
                self._attrs_subscribed[component.fqdn] = []

    def unsubscribe_all_events(self: BaseEventManager) -> None:
        """Unsubscribe for all the events on all the attribute events."""
        for component in self._list_of_components:
            self.unsubscribe_event_on_component(component)

    def evt_callback(self: BaseEventManager, evt: CspEvent) -> None:
        """The BaseEventManager callback invoked on event reception.

        The callback put the received event into the BaseEventManager queue.

        :param evt: an object with the event data
        """
        try:
            self.logger.debug(
                f"put event on {evt.attr_name} "
                f"{evt.component.name} in the queue"
            )
            self._event_queue.put(evt)
        except Full:
            self.logger.warning("The queue is full")
        except Exception as e:
            self.logger.error(f"error in evt_callback {e}")


class EventManager(BaseEventManager):
    """Aggregator class to manage the event subscriptions."""

    def __init__(
        # pylint: disable=too-many-arguments
        self: EventManager,
        device_type: str,
        task_callback: Callable,
        health_model: HealthStateModel,
        op_state_model: OpStateModel,
        obs_state_model: ObsStateModel = None,
        max_queue_size: Optional[int] = 100,
        logger: Optional[logging.Logger] = None,
    ):
        """Initialize the EventManager for the current device.

        Handles the subscription and un-subscription  of attributes on the CSP
        sub-system components.
        It also performs the aggregation of tha main attributes (State,
        healthState and obsState) of the CSP subordinate sub-systems to
        evaluate the state of the CSP as a whole.

        :param logger: The device or python logger if default is None.
        """
        self.device_type = device_type
        self.health_model = health_model
        self.obs_state_model = obs_state_model
        self.op_state_model = op_state_model
        super().__init__(task_callback, max_queue_size, logger=logger)

    #########################
    #
    # Class protected methods
    #
    # ########################

    def _process_event(self: EventManager, evt: CspEvent) -> None:
        """
        Handle the event fetched from the queue.

        :param evt: an object with the event data
        """
        # attr_name = evt.attr_name
        self.logger.info(
            f"EventManager received event on :"
            f" {evt.component.fqdn}/{evt.attr_name}."
            f" value: {evt.value}"
        )
        # pylint: disable-next=fixme
        # TODO: check next line!!!
        # may be second check only is enough
        # if we change first condition to (not evt.value) tests fail!!
        if evt.value is None and evt.quality == AttrQuality.ATTR_INVALID:
            self.logger.warning(
                "No update for"
                f" {evt.component.fqdn}/{evt.attr_name} attribute. Received"
                " invalid value because of errors"
            )
            return
        # invoke the function to calculate the wrap-up value
        # Still to implement the behavior for attributes different from
        # State, healthState, obsState
        try:
            # self._update_subsystem_attribute(evt)
            func_name = f"_evaluate_csp_{evt.attr_name}"
            self.logger.debug(f"Func name: {func_name}")
            f = self.__class__.__dict__[func_name]
            # invoke the attribute model callback
            self.logger.debug(
                f"Start Processing {evt.attr_name} at time {evt.time}"
            )
            f(self, evt)
        except KeyError as key_error:
            # to catch exception on the method existence
            # (default tatsk_callback)
            if self._update_attribute:
                self.logger.debug(
                    f"Function {key_error} does not exist. Invoke default"
                    " attribute updater callback"
                )
                self._update_attribute(evt.attr_name, evt.value)

        # pylint: disable-next=broad-except
        except Exception as e:
            # to catch exception eventually raised by the method
            self.logger.error(e)

    def _evaluate_csp_state(self: EventManager, evt: CspEvent) -> None:
        """Evaluate the CSP Device State as aggregation of the state of the
        subordinate components.

        It also re-evaluates the CSP Device healthState that can be affected
        by the change in the state of one or more CSP sub-system components.

        This method relies on the device OpStateModel to perform the
        aggregation of the CSP sub-systems operational states.

        :param evt: an object containing the event data.
        """
        self.op_state_model.component_op_state_changed(
            evt.component, evt.value
        )

        self.health_model.update_health()
        self._update_subsystem_attribute(evt)

    def _evaluate_csp_obsstate(self: EventManager, evt: CspEvent) -> None:
        """evaluate the CSP Device observing state as aggregation of the
        observing state of the CSP subordinate sub-systems.

        This method relies on the device ObsStateModel to perform the
        aggregation of the CSP sub-systems observing states.

        :param evt: the CspEvent received from a subordinate component
        """
        self.obs_state_model.component_obs_state_changed(
            evt.component, evt.value
        )
        evt.value = ObsState(evt.value)
        self._update_subsystem_attribute(evt)

    def _evaluate_csp_adminmode(self: EventManager, evt: CspEvent) -> None:
        """TBD."""
        evt.value = AdminMode(evt.value)
        self._update_subsystem_attribute(evt)

    def _evaluate_csp_healthstate(self: EventManager, evt: CspEvent) -> None:
        """Evaluate the CSP Device health state as aggregation of the health
        state of the CSP subordinate sub-systems.

        This method relies on the device HealthStateModel to perform the
        aggregation of the CSP sub-systems health values.

        :param evt: the CspEvent received from a subordinate component
        """
        self.logger.debug(
            f"Evaluate CSP health_state on dev {evt.component.fqdn} new value:"
            f" {evt.value}"
        )
        self.health_model.component_health_changed(evt.component, evt.value)
        evt.value = HealthState(evt.value)
        self._update_subsystem_attribute(evt)

    def _update_subsystem_attribute(self: EventManager, evt: CspEvent):
        """
        After an event on a subsystem, update the corresponding attribute
        on the device

        :param evt: the CspEvent received from a subordinate component
        """

        acronym = evt.component.name.split("-")[0].capitalize()

        root = acronym if acronym != "Pst" else "PstBeams"

        if self.device_type == "controller":
            if "subarray" in evt.component.name:
                return
            root = f"csp{root}"
        elif self.device_type == "subarray":
            root = (
                root[0].lower() + root[1:]
            )  # first letter must be lower case
            if acronym != "Pst":
                root = f"{root}Subarray"

        attr_type = "state" if "state" in evt.attr_name else "mode"
        attribute_name = (
            evt.attr_name.split(attr_type)[0].capitalize()
            + attr_type.capitalize()
        )
        attribute_name = f"{root}{attribute_name}"
        value = (
            [evt.component.fqdn, evt.value.name]
            if acronym == "Pst"
            else evt.value
        )

        if self._update_attribute:
            self._update_attribute(attribute_name, value)


class CapabilityEventManager(BaseEventManager):
    """Specialization to hanle events in Capability devices"""

    def _process_event(self: BaseEventManager, evt: CspEvent) -> None:
        """
        Handle the event fetched from the queue.

        :param evt: an object with the event data
        """
        attr_name = evt.attr_name
        self.logger.info(
            f"CapabilityEventManager received event on :{attr_name} device:"
            f" {evt.component.fqdn} {evt.value}"
        )

        if evt.value is None and evt.quality == AttrQuality.ATTR_INVALID:
            self.logger.warning(
                "No update for"
                f" {evt.component.fqdn}/{attr_name} attribute. Received"
                " invalid value because of errors"
            )
            return
        try:
            self.logger.debug(
                f"Start Processing {evt.attr_name} at time {evt.time}"
            )
            if self._update_attribute:
                self._update_attribute(
                    evt.component.fqdn, evt.attr_name, evt.value
                )
        except Exception:
            self.logger.error("Update attribute error")
            self.logger.error(traceback.format_exc())
