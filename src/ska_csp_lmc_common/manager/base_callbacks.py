from __future__ import annotations  # allow forward references in type hints

import threading
from typing import Any, List

from ska_control_model import CommunicationStatus, TaskStatus
from tango import DevError

from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.observing_component import ObservingComponent

# pylint: disable=protected-access
# pylint: disable=broad-except
# pylint: disable=unused-argument
# pylint: disable=too-many-arguments


class BaseInitCallbacks:
    """Class defining the callbacks to be invoked when connection with
    the subordinate sub-systems is established or failed.

    This is a base class for both controller and subarray
    """

    def __init__(self, cm, logger):
        self.name = self.__class__.__name__
        self.cm = cm
        self.logger = logger

    def succeeded(
        self: BaseInitCallbacks, stop_event: threading.Event
    ) -> None:
        """
        This method is called when the connection with all CSP
        subordinate subsystems is successfully established.
        It subscribes to the main attributes of the CSP subordinate
        subsystems and the CSP.LMC Subarrays. The state of the CSP
        Controller is evaluated independently through events,
        and the controller version ID is stored in the public attributes.

        :param stop_event: the flag set at device re-initialization

        NOTE: the succeeded method is called when initialization
            procedure ends on ALL the sub-systems.
            This behavior should be changed so that the connection
            with each sub-system is established separately.
            Do we need to consider the CSP 'connected' as soon as one
            sub-system has been detected and responsive?
        """
        # force the evaluation of the health and operational state
        self.cm.health_model.is_disabled(False)
        self.cm.op_state_model.is_disabled(False)
        for component in self.cm.online_components.values():
            if stop_event.is_set():
                self.logger.info(
                    "Stop flag set. Exiting from the connection thread."
                )
                return
            default_attributes = [
                "state",
                "healthstate",
                "adminmode",
            ]
            if isinstance(component, ObservingComponent):
                default_attributes.append("obsstate")
            self.check_and_subscribe(default_attributes, component)

        self.logger.info(
            "Number of controlled components:"
            f" {len(self.cm.online_components)}"
        )
        self.read_component_versions()
        if self.cm.is_communicating:
            return
        self.cm.is_communicating = True
        self.cm._update_communication_state(CommunicationStatus.ESTABLISHED)
        if self.cm._update_device_property_cbk:
            self.cm._update_device_property_cbk(
                "isCommunicating", self.cm.is_communicating
            )

    def check_and_subscribe(
        self: BaseInitCallbacks,
        default_attributes: List[str],
        component: Component,
    ) -> None:
        """
        This method do the subscription of the default attributes on the
        controller subsystems after a successful connection.

        :param default_attributes: attributes to be subscribed
        :param component: component object of connected subsystems
        """
        self.logger.info(f"Component {component.fqdn} ok")
        # we should set the flag is_communicating = True
        self.cm._components[component.fqdn] = component
        # subscribe for the State, healthState and adminMode
        # attributes
        # pylint: disable-next=fixme
        # TODO: consider also subscription of other attributes.
        # A sub-set of SKA SCM attributes are sub-scribed on
        # all the CSP Controller Sub-systems
        self.cm.subscribe_attributes_for_change_events(
            component, default_attributes
        )
        # each sub-system can specify a list of attributes to
        # subscribe for events (not only change)
        self.cm.subscribe_attributes_for_change_events(
            component, component.attrs_for_change_events
        )
        self.cm.subscribe_attributes_for_periodic_events(
            component, component.attrs_for_periodic_events
        )

    def failed(self: BaseInitCallbacks) -> None:
        """This method is called by InitObserver on initialization
        failure.

        CSP Controller state/healthState is forced to FAULT/FAILED.
        """
        self.logger.error("Initialization failed")
        # force the state/healthState to FAULT/FAILED
        self.cm.health_model.component_fault(True)
        self.cm.op_state_model.component_fault(True)
        self.cm.is_communicating = False
        if self.cm._update_device_property_cbk:
            self.cm._update_device_property_cbk(
                "isCommunicating", self.cm.is_communicating
            )

    def handler_to_invoke(
        self: BaseInitCallbacks,
        stop_event: threading.Event,
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: Any | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ) -> None:
        """
        The handler to invoke when CSP initialization completes.

        :param stop_event: event flag set at device re-initialization
        :param status: the task status
        :param progress: the progress of execution as percentage.
        :param result: tuple with the task result code and the message
        :param error: the exception, is raised
        """
        if status:
            if status == TaskStatus.COMPLETED:
                self.succeeded(stop_event)
            elif status == TaskStatus.FAILED:
                self.failed()

    def read_component_versions(self: BaseInitCallbacks) -> None:
        """Method defined by the child classes"""


class BaseStopCallbacks:
    """
    Class to handle callbacks at disconnection,
    """

    def __init__(
        self: BaseStopCallbacks,
        cm,
        logger,
    ) -> None:
        self.name = self.__class__.__name__
        self.cm = cm
        self.logger = logger

    def _disconnect(self: BaseStopCallbacks, stop_event: threading.Event):
        """
        Disconnect from all the sub-systems.

        :param stop_event: event flag set at device re-initialization.
        """
        self.cm.health_model.is_disabled(False)
        self.cm.op_state_model.is_disabled(False)
        for _component in self.cm.components.values():
            if stop_event.is_set():
                self.logger.info(
                    "Stop flag set. Exiting from the connection thread."
                )
                return
            if _component.proxy:
                _component.disconnect()
                # unsubscribe only the attributes of the subordinate devices
                # that are part of the System Under Control (SUT).
                self.cm.evt_manager.unsubscribe_event_on_component(_component)
        # self.cm.evt_manager.unsubscribe_all_events()
        self.cm.is_communicating = False
        self.cm.op_state_model.is_disabled(True)
        self.cm.health_model.is_disabled(True)
        self.cm._update_device_property_cbk(
            "isCommunicating", self.cm.is_communicating
        )
