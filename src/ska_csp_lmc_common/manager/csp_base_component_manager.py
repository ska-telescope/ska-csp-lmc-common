from __future__ import annotations

import logging
import threading
import time
import traceback
from functools import partial
from typing import Any, Callable, Dict, List, Optional, Tuple

import tango
from ska_control_model import AdminMode, CommunicationStatus, TaskStatus
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskExecutorComponentManager
from ska_tango_base.faults import CommandError
from tango import DevError, DevState

from ska_csp_lmc_common import release
from ska_csp_lmc_common.commands.main_task_composer import MainTaskComposer
from ska_csp_lmc_common.commands.main_task_executor import MainTaskExecutor
from ska_csp_lmc_common.commands.task import LeafTask, Task, TaskExecutionType
from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.controller import (
    ControllerHealthModel,
    ControllerOpStateModel,
)
from ska_csp_lmc_common.manager.base_callbacks import (
    BaseInitCallbacks,
    BaseStopCallbacks,
)
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.observing_component import ObservingComponent
from ska_csp_lmc_common.subarray import (
    SubarrayHealthModel,
    SubarrayOpStateModel,
)
from ska_csp_lmc_common.utils.cspcommons import (
    method_name,
    read_release_version,
)
from ska_csp_lmc_common.utils.decorators import reset_resources

module_logger = logging.getLogger(__name__)

# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-public-methods
# pylint: disable=too-many-arguments
# pylint: disable=eval-used
# pylint: disable=unused-argument
# pylint: disable=broad-exception-caught


class CSPBaseComponentManager(TaskExecutorComponentManager):
    """An abstract base class for component manager for CSP Tango devices"""

    def __init__(
        self: CSPBaseComponentManager,
        op_state_model: ControllerOpStateModel | SubarrayOpStateModel,
        health_model: ControllerHealthModel | SubarrayHealthModel,
        properties: ComponentManagerConfiguration,
        max_workers: Optional[int] = 1,
        update_device_property_cbk: Optional[Callable] = None,
        communication_state_changed_callback: Callable[
            [CommunicationStatus], None
        ] = None,
        component_state_changed_callback: Callable[
            [dict[str, Any]], None
        ] = None,
        logger: Optional[logging.Logger] = None,
    ):
        """
        Initialise a new instance.

        :param op_state_model: operational state model specialized for CSP
            device
        :param health_model: health state model specialized for CSP device
        :param properties: Tango properties of the device
        :param update_device_property_cbk: callback to update device properties
        :param max_workers: option maximum number of workers in the pool
        :param component_state_changed_callback: callback to be
            called when the component state changes
        :param communication_state_changed_callback: callback to be
            called when the status of the communications channel between
            the component manager and its component changes
        :param logger: the logger to be used by this object.
        """

        super().__init__(max_workers)
        self.op_state_model = op_state_model
        self.logger = logger or module_logger
        self.device_properties: ComponentManagerConfiguration = properties
        self._connected: bool = False
        self._components: Dict[str, Component | ObservingComponent] = {}
        self.health_model = health_model
        self._admin_mode = AdminMode.OFFLINE
        self._store_admin_mode = AdminMode.OFFLINE
        self._update_device_property_cbk = update_device_property_cbk
        self.evt_manager = None
        self._main_task_executor = None
        self._csp_version = ""
        self._csp_build_state = ""
        self._pss_version = ""
        self._cbf_version = ""
        self._pst_version = []
        self._json_component_versions = ""
        self._max_workers = max_workers
        self._command_timeout = 0
        self._access_lock = threading.Lock()
        self.resources = {}
        self._init_version_info(release)

    @property
    def components(
        self: CSPBaseComponentManager,
    ) -> Dict[str, Component | ObservingComponent]:
        """Return the CSP sub-systems components.

        :return: The CSP sub-systems components.
        """
        return self._components

    @property
    def online_components(
        self: CSPBaseComponentManager,
    ) -> Dict[str, Component]:
        """Return the dictionary with the list of the CSP subordinate sub-
        systems that are online.

        :return: The online components organized as a dictionary.
        """
        online_components: Dict[str, Component] = {
            fqdn: component
            for fqdn, component in self._components.items()
            if component.proxy
        }
        return online_components

    @property
    def is_communicating(self: CSPBaseComponentManager) -> bool:
        """Return whether communication with the component is established.

        :return: whether there is currently a connection to the component.
        """
        with self._access_lock:
            return self._connected

    @is_communicating.setter
    def is_communicating(self: CSPBaseComponentManager, value):
        with self._access_lock:
            self._connected = value

    @property
    def csp_version(self: CSPBaseComponentManager) -> str:
        """Return the component version id.

        :return: the component version id.
        """
        return self._csp_version

    @property
    def csp_build_state(self: CSPBaseComponentManager) -> str:
        """Return the component build state.

        :return: the component build state.
        """
        return self._csp_build_state

    @property
    def cbf_version(self: CSPBaseComponentManager) -> str:
        """Return the CBF component version.

        :return: the component version.
        """
        return self._cbf_version

    @property
    def pss_version(self: CSPBaseComponentManager) -> str:
        """Return the PSS component version.

        :return: the component version.
        """
        return self._pss_version

    @property
    def pst_version(self: CSPBaseComponentManager) -> str:
        """Return the PST component version.

        :return: the component version.
        """
        return self._pst_version

    @property
    def json_component_version(self: CSPBaseComponentManager) -> str:
        """Return the version of all components in json string format.

        :return: the version of all components in json string forma.
        """
        return self._json_component_versions

    @property
    def command_timeout(self: CSPBaseComponentManager) -> int:
        """Return the configured command timeout"""
        return self._command_timeout

    @command_timeout.setter
    def command_timeout(self: CSPBaseComponentManager, value: int) -> None:
        """Configure the timeout (in sec.) applied to any command.

        :raises: ValueError if the value is not strictly positive.
        """
        if value <= 0:
            msg = f"Try to set timeout to {value}.Timeout shall be > 0"
            raise ValueError(msg)
        self._command_timeout = value

    ##############################
    # Class protected methods
    ##############################

    def _init_version_info(self: CSPBaseComponentManager, release_file):
        """Method invoked to read version id and build state from release"""

        self._csp_version, self._csp_build_state = read_release_version(
            release_file
        )

    def _subscribe_attribute(
        self: CSPBaseComponentManager,
        component: Component,
        attr_name: str,
        event_type: tango.EventType,
    ) -> None:
        """Subscribe an attribute on a component for the required event type.

        :param component: the CSP sub-system to which the attributes belongs
        :param attr_name: the attribute to subscribe for the event
        :param event_type: the event type: change, periodic, etc.
        """
        self.evt_manager.subscribe_event(
            component, attr_name.lower(), event_type
        )

    def _subscribe_attributes_for_events(
        self: CSPBaseComponentManager,
        component: Component,
        list_of_attributes: List[str],
        event_type: tango.EventType,
    ) -> None:
        """Subscribe a set of attributes on a component for the required
        events.

        :param component: the CSP sub-system to which the attributes belongs
        :param list_of_attributes: the list of attributes to subscribe for the
            event
        :param event_type: the event type: change, periodic, etc.
        """
        if list_of_attributes:
            for attr_name in list_of_attributes:
                self._subscribe_attribute(component, attr_name, event_type)

    def _task_submitter(
        self: CSPBaseComponentManager,
        task_name: str,
        task_callback: Optional[Any] = None,
        argin: Optional[Any] = None,
        **kwargs: Any,
    ) -> Tuple[TaskStatus, str]:
        """
        Common method to submit a command

        :param task_name: the name of the task to be submitted
        :param task_callback: method called to update the task result
            related attributes
        :param argin: input argument of the task

        :return: a tuple with *TaskStatus* and the task_callback
        """
        # NOTE: task_callback and argin are optional in the command,
        # we have to build the list accordingly
        args = [task_name, argin]
        args = [args[i] for i in range(2) if args[i] is not None]
        task_status, msg = self._task_executor.submit(
            eval(f"self._{task_name}"),
            args=args,
            kwargs=kwargs,
            task_callback=task_callback,
            # is_cmd_allowed = eval(f"self.is_{task_name}_allowed"),
        )

        self.logger.info(
            f"Task {task_name} status {task_status}. Message: {msg}"
        )
        return task_status, msg

    @reset_resources
    def _reset(
        self: CSPBaseComponentManager,
        task_name: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
        **kwargs: Any,
    ) -> None:
        """Reset the CSP Device from a FAULT state.

        If only CSP device is in FAULT, its operational state
        is re-evaluated and the op state model faulty flag reset to False.

        :param task_name: the name of the task
        :param task_callback: method called to update the task result
            related attributes
        :param abort_event: the event set on abort request.
        :return: a tuple with *ResultCode* and an informative message.
        """

        def reset_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **callback_kwargs: Any,
        ) -> None:
            """Method invoked when the CSP Reset task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            if self.op_state_model.faulty:
                self.op_state_model.component_fault(False)
                if self.health_model.faulty:
                    self.health_model.component_fault(False)
            task_callback(
                status=status,
                result=result,
            )

        try:
            input_command_list = [
                cmp.fqdn
                for cmp in self.online_components.values()
                if cmp.state == DevState.FAULT
            ]
            for fqdn in input_command_list:
                self.resources[fqdn] = None
            self.logger.debug(
                f"Reset on {len(self.resources.keys())} components"
            )
            command_map = kwargs.get("command_map")
            composer = MainTaskComposer(
                task_name,
                command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    reset_completed,
                )
            else:
                # only CSP Device is in FAULT: force the device to re-
                # evaluate its state
                # pylint: disable-next=fixme
                # TODO: Consider if a restart of the device is required. In
                # this case it is necessary to call the device Init method or
                # connect to the Admin device and invoke the DevRestart
                # command.
                if self.op_state_model.faulty:
                    self.logger.debug(
                        "CSP device operational state is faulty"
                        f" {self.op_state_model.faulty}"
                    )
                    self.op_state_model.component_fault(False)
                    if self.health_model.faulty:
                        self.health_model.component_fault(False)
                task_callback(
                    status=TaskStatus.COMPLETED,
                    result=[
                        ResultCode.OK,
                        f"Task {task_name} completed on CSP device",
                    ],
                )
        except CommandError as exc:
            self.logger.error(f"CommandError exception: {str(exc)}")
            task_callback(
                status=TaskStatus.REJECTED,
                result=[ResultCode.REJECTED, str(exc)],
            )
        # pylint: disable-next=broad-except
        except Exception as exc:
            warning_msg = (
                f"Internal SW error executing task {task_name} on CSP"
                f" device. Received exception: {exc}"
            )
            self.logger.error(warning_msg)
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                status=TaskStatus.FAILED,
                result=(ResultCode.FAILED, warning_msg),
            )

    ######################
    # INIT methods
    ######################

    def _create_init_callbacks(self) -> BaseInitCallbacks:
        """
        Abstract method to create initialization callbacks.
        Must be implemented by child classes.
        """
        raise NotImplementedError(
            "Child classes must implement _create_init_callbacks"
        )

    def _create_stop_callbacks(self) -> BaseStopCallbacks:
        """
        Abstract method to create stop callbacks.
        Must be implemented by child classes.
        """
        raise NotImplementedError(
            "Child classes must implement _create_stop_callbacks"
        )

    def _create_start_subtasks(
        self: CSPBaseComponentManager, component: Component
    ) -> List[Task]:
        """
        Create the task to initialize the connection
        with one sub-system.

        :param component: the target of the task

        :return: the parent task.
        """
        leaf_tasks = []
        if not component.proxy:
            lft_0 = LeafTask(
                f"connect-{component.name}",
                self,
                "_connect_device",
                task_type=TaskExecutionType.INTERNAL,
                argin=component,
                logger=self.logger,
            )
            leaf_tasks.append(lft_0)

        lft_1 = LeafTask(
            f"set-adminmode-{component.name}",
            self,
            "_set_adminmode",
            task_type=TaskExecutionType.INTERNAL,
            argin=component,
            logger=self.logger,
        )
        leaf_tasks.append(lft_1)

        task_serial = Task(
            f"Start-{component.name}",
            leaf_tasks,
            TaskExecutionType.SEQUENTIAL,
            logger=self.logger,
        )
        return task_serial

    def _create_start_task(self: CSPBaseComponentManager) -> Task:
        """
        Create the task responsible for establishing the connection
        with all the subordinate subsystems in the CSP.

        :return: the Task, otherwise None
        :raise: ValueError exception if the building of the main
            task fails with an exception.
        """
        start_comm_task = []
        # Create internal tasks responsible for establishing
        # the connection the subordinate subsystems and setting
        # their admin mode to ONLINE.
        try:
            for fqdn, component in self._components.items():
                if (
                    fqdn in self.online_components
                    and component.admin_mode == self._admin_mode
                ):
                    continue
                task_serial = self._create_start_subtasks(component)
                if task_serial:
                    start_comm_task.append(task_serial)
        except Exception as e:
            msg = "Failure in building the main task" f"Got error  {e}"
            self.logger.error(msg)
            raise ValueError(msg) from e

        if start_comm_task:
            self._update_communication_state(
                CommunicationStatus.NOT_ESTABLISHED
            )
            return Task(
                "Start-communication",
                start_comm_task,
                TaskExecutionType.PARALLEL,
                logger=self.logger,
            )
        return None

    def _create_stop_task(
        self: CSPBaseComponentManager, components_to_disconnect: List[str]
    ) -> Task:
        """
        Create the main task responsible for disconnecting
        the CSP from all its subordinate subsystems.

        :param components_to_disconnect: the list of the
            subsystems's FQDN to disconnect from cSP control.

        :return: the Task, otherwise None
        """
        leaf_tasks = []
        # Create internal tasks responsible for disconnecting
        # CSP from its subordinate subsystems.
        for fqdn in components_to_disconnect:
            if fqdn in self.online_components:
                component = self.online_components[fqdn]
                leaf_tasks.append(
                    LeafTask(
                        f"disconnect-{component.name}",
                        self,
                        "_disconnect_device",
                        task_type=TaskExecutionType.INTERNAL,
                        argin=component,
                        logger=self.logger,
                    )
                )
        if leaf_tasks:
            return Task(
                "Stop-communication",
                leaf_tasks,
                TaskExecutionType.PARALLEL,
                logger=self.logger,
            )
        self.logger.info("All sub-systems already disconnected")
        return None

    def _start_communicating(
        self: CSPBaseComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> None:
        """
        Method submitted into main task executor for init command
        """
        # Create the callback
        if not task_callback:
            init_callbacks = self._create_init_callbacks()
            task_callback = init_callbacks.handler_to_invoke

        if self.is_communicating:
            self.logger.debug(
                f"Set component adminMode to: {self._admin_mode}"
            )
            if self._admin_mode == self._store_admin_mode:
                self.logger.debug(
                    f"Adminmode is already set to {self._admin_mode}."
                )
        try:
            main_task = self._create_start_task()
            if main_task:
                # Set a different MainTaskExecutor timeout for init command
                connection_timeout = int(
                    self.device_properties.ConnectionTimeout
                )
                self._main_task_executor.set_command_timeout(
                    connection_timeout
                )
                self._main_task_executor.submit_main_task(
                    main_task,
                    partial(
                        task_callback,
                        self._main_task_executor.stop_event,
                    ),
                )
                # reset the timeout to the default command timeout
                command_timeout = int(
                    self.device_properties.DefaultCommandTimeout
                )
                self.logger.debug(
                    "Setting the command timeout to default value: "
                    f"{command_timeout}"
                )

                self._main_task_executor.set_command_timeout(command_timeout)
        # pylint: disable-next=broad-except
        except Exception as err:
            warning_msg = (
                f"Internal SW error executing in task {main_task.name} on CSP"
                f" Component. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                result=(ResultCode.FAILED, warning_msg),
                status=TaskStatus.FAILED,
            )

    def _stop_communicating(
        self: CSPBaseComponentManager,
        list_of_component: List[str],
        task_callback: Callable = None,
    ) -> None:
        """
        Method submitted into main task executor for disconnect
        """
        if not self.is_communicating:
            if self._communication_state == CommunicationStatus.DISABLED:
                return
            return
        # Create the callback
        stop_callbacks = self._create_stop_callbacks()

        try:
            main_task = self._create_stop_task(list_of_component)
            if main_task:
                # Set a different MainTaskExecutor timeout for init command
                command_timeout = int(
                    self.device_properties.DefaultCommandTimeout
                )
                self._main_task_executor.set_command_timeout(command_timeout)
                self._main_task_executor.submit_main_task(
                    main_task,
                    partial(
                        stop_callbacks.handler_to_invoke,
                        self._main_task_executor.stop_event,
                    ),
                )
        # pylint: disable-next=broad-except
        except Exception as err:
            warning_msg = (
                f"Internal SW error executing in task {main_task.name} on CSP"
                f" Component. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                result=(ResultCode.FAILED, warning_msg),
                task=TaskStatus.FAILED,
            )

    def _set_adminmode(
        self: CSPBaseComponentManager,
        component: Component,
    ) -> Tuple[TaskStatus, ResultCode]:

        msg = ""
        task_status = TaskStatus.COMPLETED
        result_code = ResultCode.FAILED

        try:
            if component.proxy:
                if component.read_attr("adminmode") != self._admin_mode:
                    component.admin_mode = self._admin_mode
                result_code = ResultCode.OK
                time.sleep(0.5)
        except ValueError:
            self.logger.error(
                f"Exception occurred:\n {traceback.format_exc()}"
            )
            msg = (
                "Failure in setting the adminMode on the"
                f"device {component.fqdn}"
            )
        # if the failing component is CBF, the failure as to
        # be reported as TaskStatus.FAILED, ResultCode.FAILED
        # to clearly distinguish it from failures of other components.
        if "cbf" in component.fqdn and result_code == ResultCode.FAILED:
            raise ValueError(msg)
        return (task_status, result_code)

    def _connect_device(
        self: CSPBaseComponentManager,
        component: Component,
    ) -> Tuple[TaskStatus, ResultCode]:
        """
        INTERNAL METHOD

        Establishes a connection with a CSP subsystem TANGO device.
        Returns a TaskStatus FAILED and ResultCode FAILED if CBF connection
        fails. For other subsystems, returns COMPLETED with ResultCode FAILED
        to indicate a degraded operation mode.

        :return: a tuple with the task status and the result code.
        """

        def completed() -> bool:
            """
            Checks if the component's is alive and responding.
            """
            if component.proxy:
                state = component.read_attr("state")
                self.logger.debug(
                    f"{component.name} current state is: " f"{state}"
                )
                return True
            return False

        msg = ""
        timeout_event = self._main_task_executor.timeout_event
        stop_event = self._main_task_executor.stop_event
        ping_time = int(self.device_properties.PingConnectionTime)
        if stop_event.is_set():
            self.logger.info("The device received a re-init command!")
        while not timeout_event.is_set() or stop_event.is_set():
            try:
                if not component.proxy:
                    component.connect()
                if completed():
                    return (TaskStatus.COMPLETED, ResultCode.OK)
                self.logger.info(
                    f"Retrying connection to {component.fqdn}"
                    f" in {ping_time} seconds"
                )
                stop_event.wait(ping_time)
            except Exception as exc:
                self.logger.error(
                    f"Exception occurred:\n {traceback.format_exc()}"
                )
                msg = (
                    f"Connection to device {component.fqdn}"
                    f"failed with the following exception {exc}"
                )
                break
        if timeout_event.is_set():
            msg = (
                f"Connection to the device {component.fqdn} timeouted "
                f" after {self.device_properties.ConnectionTimeout} sec."
            )
        elif stop_event.is_set():
            msg = (
                f"Connection to device {component.fqdn} stopped"
                f" by the re-initialization of the device"
            )
        # if the failing component is CBF, the failure as to
        # be reported as TaskStatus.FAILED, ResultCode.FAILED
        # to clearly distinguish it from failures of other components.
        self.logger.error(msg)
        if "cbf" in component.fqdn:
            raise ValueError(msg)

        return (TaskStatus.COMPLETED, ResultCode.FAILED)

    def _disconnect_device(self: CSPBaseComponentManager, component):
        def completed():
            """
            Check if the component's admin mode matches
            the expected state.
            """
            self.logger.debug(
                f"Component {component.name} adminmode:"
                f" {component.read_attr('adminmode')}"
            )
            return component.read_attr("adminmode") == self._admin_mode

        msg = ""
        task_status, result_code = TaskStatus.COMPLETED, ResultCode.FAILED
        stop_event = self._main_task_executor.stop_event
        timeout_event = self._main_task_executor.timeout_event
        self.logger.info(
            f"Disconnect device {component.fqdn}"
            f" Set adminmode to {self._admin_mode}"
        )
        try:
            if component.proxy:

                component.admin_mode = self._admin_mode
                while not timeout_event.is_set() or not stop_event.is_set():
                    if completed():
                        return (TaskStatus.COMPLETED, ResultCode.OK)
                    stop_event.wait(0.5)
        except (ValueError, Exception):
            self.logger.error(
                f"Exception occurred:\n {traceback.format_exc()}"
            )
            msg = (
                "Failure in setting the adminMode on the"
                f"device {component.fqdn}"
            )
        # if the failing component is CBF, the failure as to
        # be reported as TaskStatus.FAILED, ResultCode.FAILED
        # to clearly distinguish it from failures of other components.
        if timeout_event.is_set():
            msg = (
                f"Disconnection from the device {component.fqdn} timeouted "
                f" after {self.device_properties.ConnectionTimeout} sec."
            )
        elif stop_event.is_set():
            msg = (
                f"Disconnection from device {component.fqdn} stopped"
                f" by the re-initialization of the device"
            )
        if "cbf" in component.fqdn and result_code == ResultCode.FAILED:
            raise ValueError(msg)
        return (task_status, result_code)

    ######################
    # Class public methods
    ######################

    def init(self: CSPBaseComponentManager):
        """Initialize the Component Manager"""
        default_timeout = 5
        if hasattr(self.device_properties, "DefaultCommandTimeout"):
            _default_timeout = int(
                self.device_properties.DefaultCommandTimeout
            )
            if _default_timeout > 0:
                default_timeout = _default_timeout
        self._command_timeout = default_timeout
        self.logger.debug(
            f"command timeout initialized to: {self._command_timeout}"
        )

        self.logger.debug("Initialize CSP Main task executor")
        self._main_task_executor = MainTaskExecutor(
            prefix="Main", logger=self.logger
        )

    def reset(
        self: CSPBaseComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
        **kwargs: Any,
    ) -> Tuple[TaskStatus, str]:
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,
            **kwargs,
        )

    def subscribe_attributes_for_change_events(
        self: CSPBaseComponentManager,
        component: Component,
        list_of_attributes: List[str],
    ) -> None:
        """Subscribe a set of attributes on a component for change events.

        :param component: the CSP sub-system to which the attributes belong
        :param list_of_attributes: a list the attributes to subscribe for
            change events. For each attribute is specified if the updated
            value has to be aggregated with the values coming from the other
            sub-systems.
        """
        return self._subscribe_attributes_for_events(
            component, list_of_attributes, tango.EventType.CHANGE_EVENT
        )

    def subscribe_attributes_for_periodic_events(
        self: CSPBaseComponentManager,
        component: Component,
        list_of_attributes: Dict[str, bool],
    ) -> None:
        """Subscribe a set of attributes on a component for periodic events.

        :param component: the CSP sub-system to which the attributes belong
        :param list_of_attributes: a list the attributes to subscribe for
            periodic events. For each attribute is specified a flag
            enabling/disabling the aggregation with the other sub-systems
            attributes.
        """
        return self._subscribe_attributes_for_events(
            component, list_of_attributes, tango.EventType.PERIODIC_EVENT
        )

    def off(
        self: CSPBaseComponentManager, task_callback: Optional[Callable] = None
    ):
        raise NotImplementedError

    def on(
        self: CSPBaseComponentManager, task_callback: Optional[Callable] = None
    ):
        raise NotImplementedError

    def standby(
        self: CSPBaseComponentManager, task_callback: Optional[Callable] = None
    ):
        raise NotImplementedError

    def start_communicating(self: CSPBaseComponentManager):
        raise NotImplementedError

    def stop_communicating(self: CSPBaseComponentManager):
        raise NotImplementedError
