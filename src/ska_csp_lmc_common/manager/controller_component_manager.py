from __future__ import annotations  # allow forward references in type hints

import json
import logging
import threading
from typing import Any, Callable, List, Optional, Tuple

from ska_control_model import (
    AdminMode,
    CommunicationStatus,
    ObsState,
    ResultCode,
    TaskStatus,
)
from ska_tango_base.faults import CommandError
from tango import DevError

from ska_csp_lmc_common.commands.command_map import BaseCommandMap
from ska_csp_lmc_common.commands.main_task_composer import MainTaskComposer
from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.controller import (
    ControllerHealthModel,
    ControllerOpStateModel,
)
from ska_csp_lmc_common.controller.controller_factory import (
    ControllerComponentFactory,
)
from ska_csp_lmc_common.manager.base_callbacks import (
    BaseInitCallbacks,
    BaseStopCallbacks,
)
from ska_csp_lmc_common.manager.csp_base_component_manager import (
    CSPBaseComponentManager,
)
from ska_csp_lmc_common.manager.event_manager import EventManager
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.utils.cspcommons import (
    list_of_subsystem_fqdns,
    method_name,
)
from ska_csp_lmc_common.utils.decorators import reset_resources

# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-locals
# pylint: disable=too-many-arguments
# pylint: disable=arguments-differ
# pylint: disable=too-many-branches
# pylint: disable=attribute-defined-outside-init
# pylint: disable=protected-access
# pylint: disable=arguments-differ
# pylint: disable=abstract-method
# pylint: disable=invalid-name
# pylint: disable=inconsistent-return-statements
# pylint: disable=no-else-continue
# pylint: disable=broad-except
# pylint: disable=unused-argument
# pylint: disable=eval-used

module_logger = logging.getLogger(__name__)


class CSPControllerComponentManager(CSPBaseComponentManager):
    """Component Manager for the Csp Controller."""

    def __init__(
        self: CSPControllerComponentManager,
        op_state_model: ControllerOpStateModel,
        health_model: ControllerHealthModel,
        properties: ComponentManagerConfiguration,
        update_device_property_cbk: Callable,
        communication_state_changed_callback: Callable[
            [CommunicationStatus], None
        ] = None,
        component_state_changed_callback: Callable[
            [dict[str, Any]], None
        ] = None,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """The Component Manager for the CSP Controller device.

        :param op_state_model: The Operational State Model to evaluate the CSP
                Controller device state
        :param health_model: The Health State Model to evaluate the CSP
                Controller health state.
        :param properties: A class instance whose properties are the CSP
                Controller device properties.
        :param update_device_property_cbk: The CSP Controller method invoked
                to update the properties. Defaults to None.
        :param logger: The device or python logger if default is None.
        """
        super().__init__(
            op_state_model,
            health_model,
            properties,
            1,
            update_device_property_cbk,
            communication_state_changed_callback,
            component_state_changed_callback,
            logger,
        )
        self.CbfCtrlFqdn = None
        self.PssCtrlFqdn = None
        self.PstBeamsFqdn = None
        self.CspSubarrays = None
        self._command_map = BaseCommandMap().get_controller_command_map()
        # self_resources: python dictionary
        # key: sub-system fqdn
        # value: argin for command.
        # The dictionary allows specifying which subsystems the command
        # should be propagated to and, when required, the command's
        # argument. If the command does not accept any input argument,
        # this should be specified as `None`.
        self.resources = {}

    ##############################
    # Class protected methods
    ##############################

    def _create_init_callbacks(self) -> ControllerInitCallbacks:
        """Create controller-specific initialization callbacks."""
        return ControllerInitCallbacks(self, self.logger)

    def _create_stop_callbacks(self) -> ControllerStopCallbacks:
        """Create controller-specific stop callbacks."""
        return ControllerStopCallbacks(self, self.logger)

    def _controller_factory(
        self: CSPControllerComponentManager, logger: logging.Logger
    ) -> ControllerComponentFactory:
        """Configure the Factory to create the sub-systems components.

        This class is specialized in Mid.CSP and Low.CSP.

        :param logger: the logger for this instance.

        :return: A factory object to create CSP Controller sub-systems
            observing components.
        """
        return ControllerComponentFactory(logger)

    def _add_subsystem_components(self: CSPControllerComponentManager) -> None:
        """Instantiate a Python class for each CSP sub-system component.

        Each sub-system component works as an adapter and cache for the
        associated CSP sub-system. The sub-system component reports the
        information about one CSP subordinate device and provides the methods
        or calls to operate on the associate sub-system TANGO device.

        On failure, the state and health state of the CSP Controller is forced
        to FAULT/FAILED.
        """
        try:
            # !! NOTE: the type of the attributes retrieved from the device
            # properties is <str>
            # !! Remember to convert to the proper data type before using
            # them!!
            if hasattr(self.device_properties, "CspCbf"):
                self.CbfCtrlFqdn = self.device_properties.CspCbf
            if hasattr(self.device_properties, "CspPss"):
                self.PssCtrlFqdn = self.device_properties.CspPss
            if hasattr(self.device_properties, "CspPstBeams"):
                self.PstBeamsFqdn = self.device_properties.CspPstBeams
                if isinstance(self.device_properties.CspPstBeams, str):
                    self.PstBeamsFqdn = [self.device_properties.CspPstBeams]
            if hasattr(self.device_properties, "CspSubarrays"):
                self.CspSubarrays = self.device_properties.CspSubarrays
                if isinstance(self.device_properties.CspSubarrays, str):
                    self.CspSubarrays = [self.device_properties.CspSubarrays]
            list_of_fqdn = list_of_subsystem_fqdns(
                self.CbfCtrlFqdn,
                self.PssCtrlFqdn,
                self.PstBeamsFqdn,
                self.CspSubarrays,
            )
            for fqdn in list_of_fqdn:
                domain_name = fqdn.split("/")[0]
                family_name = fqdn.split("/")[1]
                sub_system_identifier = domain_name[-3:]
                if (
                    family_name == "subarray"
                    and sub_system_identifier == "csp"
                ):
                    sub_system_identifier = family_name
                factory = self.component_factory
                self._components[fqdn] = factory.get_component(
                    fqdn, sub_system_identifier
                )
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(
                "Error in defining the list of ComponentManager"
                f" properties: {e}"
            )
            # Here we force the opState and healthState to FAULT/FAILED
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)

    def _populate_subsystems_list(
        self: CSPControllerComponentManager, argin: List[str]
    ):
        """Populate a list with the Csp Subsystems components that the
        controller has to operate on. If argin is an empty list, the CSP
        Controller operates on all the online Subsystems. If any fqdn is
        invalid raise a CommandError.

        :param argin: List of fqdn of controller's subsystems to operate
            using On and Off command

        :return argout: List of components to operate on
        :raise CommandError: in case command input argument as only invalid
            fqdn
        """
        if argin:
            valid_fqdns = [
                fqdn for fqdn in argin if (fqdn in self.online_components)
            ]
            invalid_fqdns = [
                fqdn for fqdn in argin if fqdn not in self.online_components
            ]
            if invalid_fqdns:
                self.logger.error(
                    f"Argin fqdn device is not online: {invalid_fqdns}"
                )
        else:
            valid_fqdns = self.online_components
        if not valid_fqdns:
            raise CommandError(f"Invalid command input argument {argin}")

        return valid_fqdns

    @reset_resources
    def _on(
        self: CSPControllerComponentManager,
        task_name: str,
        argin: List[Component],
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Method submitted into thread executor for on command

        :param task_name: the name of the task
        :param argin: list with FQDNs of the CSP Sub-systems Controllers to
            operate on. If the list is empty operated on all of them.
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        def on_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP On task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            task_callback(
                status=status,
                result=result,
            )

        try:
            input_command_list = self._populate_subsystems_list(argin)
            self.resources = {}
            for fqdn in input_command_list:
                self.resources[fqdn] = None
            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    on_completed,
                )
            else:
                task_callback(
                    status=TaskStatus.COMPLETED,
                    result=(
                        ResultCode.OK,
                        "All devices are already in ON state",
                    ),
                )

        # pylint: disable-next=broad-except
        except CommandError as exc:
            self.logger.error(f"CommandError exception: {str(exc)}")
            task_callback(
                status=TaskStatus.REJECTED,
                result=[ResultCode.REJECTED, str(exc)],
            )
        except Exception as err:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Controller. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                status=TaskStatus.FAILED,
                result=[ResultCode.FAILED, warning_msg],
            )

    @reset_resources
    def _off(
        self: CSPControllerComponentManager,
        task_name,
        argin: List[Component],
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:  # type: ignore
        """
        Method submitted into thread executor for off command

        :param task_name: the name of the task
        :param argin: list with FQDNs of the CSP Sub-systems Controllers to
            operate on. If the list is empty operated on all of them.
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        def off_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP Off task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            task_callback(
                status=status,
                result=result,
            )

        try:
            input_command_list = self._populate_subsystems_list(argin)
            for fqdn in input_command_list:
                self.resources[fqdn] = None
            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    off_completed,
                )
            else:
                task_callback(
                    status=TaskStatus.COMPLETED,
                    result=[
                        ResultCode.OK,
                        "All devices are already in OFF state",
                    ],
                )

        # pylint: disable-next=broad-except
        except CommandError as exc:
            task_callback(
                status=TaskStatus.REJECTED,
                result=[ResultCode.REJECTED, exc],
            )
        except Exception as err:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Controller. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                status=TaskStatus.FAILED,
                result=[ResultCode.FAILED, warning_msg],
            )

    @reset_resources
    def _standby(
        self: CSPControllerComponentManager,
        task_name: str,
        argin: List[Component],
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:  # type: ignore
        """
        Method submitted into thread executor for standby command

        Currently the command is rejected because not used by the sub-systems.

        :param task_name: the name of the task
        :param argin: list with FQDNs of the CSP Sub-systems Controllers to
            operate on. If the list is empty operated on all of them.
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        # Currently this part of the code is not used. The command is rejected
        # because not used by the sub-systems.

        warning_msg = (
            "The command should not arrive here."
            "The command is rejected by the CSP subsystems"
        )
        result_code = ResultCode.FAILED, warning_msg
        task_status = TaskStatus.COMPLETED
        task_callback(
            status=task_status,
            result=result_code,
        )

    ##############################
    # Class public methods
    ##############################

    def init(self: CSPControllerComponentManager) -> None:
        """
        Method to initialize the Controller ComponentManager.
        Instantiate:

        - the command factory
        - the Component factory
        - the event manager
        - a Component object for each CSP sub-system defined in the CSP
          Subarray Device Properties.
        """
        super().init()
        self.component_factory = self._controller_factory(self.logger)
        self._add_subsystem_components()
        self.evt_manager = EventManager(
            "controller",
            self._update_device_property_cbk,
            self.health_model,
            self.op_state_model,
            None,
            100,
            self.logger,
        )

    def start_communicating(self: CSPControllerComponentManager) -> None:
        """Method to start the monitoring of the CSP subordinate devices, It is
        invoked when the adminMode of the device is ONLINE or ENGINEERING.

        Establish the connection with the CSP subordinate sub-systems
        Controller devices and with the CSP Subarrays. On success, the main
        sub-systems attributes are subscribed for events.
        """
        start_thread = threading.Thread(target=self._start_communicating)
        start_thread.start()

    def _stop_communicating_ordered_list(self: CSPControllerComponentManager):
        return list_of_subsystem_fqdns(
            self.CbfCtrlFqdn,
            self.PssCtrlFqdn,
            self.PstBeamsFqdn,
            self.CspSubarrays,
        )

    def stop_communicating(self: CSPControllerComponentManager):
        """Method invoked when the CSP subarray is set OFFLINE. CSP Controller
        stop monitoring the subordinate sub-systems. Events on attributes are
        un-subscribed and proxies with sub-system devices are invalidated.

        TODO: Consider to maintain the proxy and subscription points active
              blocking only the aggregation and reporting of the events to the
              CSP device. In DISABLE state the main operations are prohibited
              and only 'emergency' actions could be allowed.

        :raise: ValueError exception if at least one subarrays is not in EMPTY.
        """
        # check if at least one CSP subarray is not EMPTY. In this case the
        # Controller does not execute the request
        csp_subarray_not_empty_lst = []
        for fqdn in self.CspSubarrays:
            if fqdn in self.online_components:
                if self.online_components[fqdn].obs_state != ObsState.EMPTY:
                    csp_subarray_not_empty_lst.append(fqdn)
        if csp_subarray_not_empty_lst:
            raise ValueError(
                "The CSP Controller can't be set OFFLINE because"
                " the following CSP Subarrays are not EMPTY: "
                f"{csp_subarray_not_empty_lst}"
            )

        # build the list of CSP subordinated devices, putting the CSP
        # subarrays as last so that the events generated by the
        # sub-systems' sub-arrays are not lost
        order_list_subarrays_last = self._stop_communicating_ordered_list()
        stop_thread = threading.Thread(
            target=self._stop_communicating,
            args=(order_list_subarrays_last, None),
        )
        stop_thread.start()

    def reset(
        self: CSPControllerComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        return super().reset(task_callback, command_map=self._command_map)

    def on(
        self: CSPControllerComponentManager,
        argin: List[Component],
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Turn the CSP on. This operation is done powering on all subsystem
        in parallel way.

        :param argin: list with FQDNs of the CSP Sub-systems Controllers to
            operate on. If the list is empty operated on all of them.
        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *TaskStatus* and an informative message.
        """
        # pylint: disable=arguments-renamed
        return self._task_submitter(
            task_name=method_name(), task_callback=task_callback, argin=argin
        )

    def off(
        self: CSPControllerComponentManager,
        argin: List[Component],
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Turn the CSP off.

        :param argin: list with FQDNs of the CSP Sub-systems Controllers to
            operate on. If the list is empty operated on all of them.
        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *TaskStatus* and an informative message.
        """
        # pylint: disable=arguments-renamed
        return self._task_submitter(
            task_name=method_name(), task_callback=task_callback, argin=argin
        )

    def standby(
        self: CSPControllerComponentManager,
        argin: List[Component],
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        # pylint: disable=too-many-locals
        # pylint: disable=arguments-renamed
        """Send the component to low-power mode. This operation is done on one
        sub-system after the other in an ordered way.

        Currently the command is rejected because not used by the sub-systems.

        :param argin: list with FQDNs of the CSP Sub-systems Controllers to
            operate on. If the list is empty operated on all of them.

            CSP Sub-system controllers are put in low-power mode, while the
            subarrays are switched off (observations not available in low
            power mode)
        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *TaskStatus* and an informative message.
        """
        msg = (
            "standby command rejected, as it is unimplemented "
            "for CSP subsystems."
        )

        return (TaskStatus.REJECTED, msg)

    def get_cbf_simulation_mode(self: CSPControllerComponentManager) -> bool:
        """Get the Cbf Simulation Mode attribute.

        :return: Cbf Simulation mode flag.
        """
        try:
            return self.online_components[self.CbfCtrlFqdn].read_attr(
                "simulationMode"
            )
        except KeyError:
            self.logger.warning("Failed reading the CBF simulation Mode")
            return True

    def set_cbf_simulation_mode(
        self: CSPControllerComponentManager, value
    ) -> None:
        """Set the CBF Simulation Mode of the device.

        :param value: desired Simulation Mode of the device.
        """
        try:
            self.online_components[self.CbfCtrlFqdn].write_attr(
                "simulationMode", value
            )
        except KeyError:
            self.logger.warning("Failed writing the CBF simulation Mode")


class ControllerInitCallbacks(BaseInitCallbacks):
    """Class defining the callbacks to be invoked when connection with
    the subordinate sub-systems is established or failed."""

    def read_component_versions(
        self: ControllerInitCallbacks,
    ) -> None:
        """
        Build a JSON formatted string with the SW versions of each
        CSP Controller subordinate sub-system.

        Example:

        '{"mid_csp_cbf/sub_elt/controller": "0.11.4",
        "mid-csp/subarray/01": "0.16.3",
        "mid-csp/subarray/02": "0.16.3",
        "mid-csp/subarray/03": "0.16.3"}'
        """
        component_version_json = {}
        try:
            for (
                component_fqdn,
                component,
            ) in self.cm.online_components.items():
                component_version = "NA"
                try:
                    component_version = component.read_attr("versionId")
                    self.logger.info(
                        "The component version:" f"{component_version}."
                    )
                except Exception as e:
                    self.logger.warning(
                        "Attribute version Id not found for "
                        f"component {component_fqdn}. Error: {e}"
                    )

                if component_fqdn == self.cm.CbfCtrlFqdn:
                    self.cm._cbf_version = component_version
                elif (
                    self.cm.PssCtrlFqdn
                    and component_fqdn == self.cm.PssCtrlFqdn
                ):
                    self.cm._pss_version = component_version
                elif (
                    self.cm.PstBeamsFqdn
                    and component_fqdn in self.cm.PstBeamsFqdn
                ):
                    self.cm._pst_version.append(component_version)
                else:
                    self.logger.debug(
                        "Attribute version Id not required for "
                        f"component {component_fqdn}"
                    )
                component_version_json[component_fqdn] = component_version
        except Exception as e:
            self.logger.warning(f"Error reading the component version: {e}.")
        self.cm._json_component_versions = json.dumps(component_version_json)


class ControllerStopCallbacks(BaseStopCallbacks):
    """
    Class to handle callbacks at disconnection.
    Controller specialization
    """

    def succeeded(self: ControllerStopCallbacks, stop_event: threading.Event):
        """
        This method is called when the CSP successfully disconnects from its
        subsystems. It unsubscribes from the previously subscribed attributes
        on each CSP subordinate subsystem.
        """
        # here we force the evaluation of the health and operational
        # state
        self._disconnect(stop_event)

    def handler_to_invoke(
        self: ControllerStopCallbacks,
        stop_event: threading.Event,
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: Any | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ) -> None:
        """
        The handler to invoke when disconnection ends.

        :param stop_event: event flag set at device re-initialization
        :param status: the task status
        :param progress: the progress of execution as percentage.
        :param result: tuple with the task result code and the message
        :param error: the exception, if raised
        """
        if result:
            if result[0] == ResultCode.OK:
                self.succeeded(stop_event)
            elif result[0] == ResultCode.FAILED:
                self.failed()

    def failed(
        self: ControllerStopCallbacks,
    ):
        """This method is called when disconnection task fails."""
        self.logger.error("Disconnection failed")
        for _, component in self.cm.components.items():
            if component.admin_mode == AdminMode.OFFLINE:
                self.cm.evt_manager.unsubscribe_event_on_component(component)
                component.disconnect()

        self.cm.is_communicating = False
        self.cm.op_state_model.is_disabled(True)
        self.cm.health_model.is_disabled(True)
        self.cm._update_communication_state(CommunicationStatus.DISABLED)
