from __future__ import annotations  # allow forward references in type hints

import logging
from typing import Dict, Optional

import tango

# pylint: disable=invalid-name


class ComponentManagerConfiguration:
    """Class to store the device properties of the controlling TANGO Device to
    pass to the ComponentManager."""

    def __init__(
        self: ComponentManagerConfiguration,
        dev_name: str,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialize the class with the name of the device. The device name
        is needed to retrieve the device properties from the TANGO DB.

        :param dev_name: the device name for which the device properties list
            is retrieved from the TANGO DB
        :param logger: a logger for this instance
        """
        self.logger = logger
        self.dev_name = dev_name
        self.properties = self.get_device_properties()

    def get_device_properties(
        self: ComponentManagerConfiguration,
    ) -> Dict[str, str]:
        """Retrieve the list of the Tango properties of the device registered
        within the TANGO DB.

        Format the information as a dictionary where each entry is the
        property name and the value is the property value (as a string).

        :return: A dictionary with the property name and the associated value.
        """
        db = tango.Database()
        properties: Dict[str, str] = {}
        try:
            property_list = db.get_device_property_list(self.dev_name, "*")
            for prop_name in property_list:
                # skip the properties related to the polled attributes
                if "polled_attr" in prop_name:
                    continue
                property_datum = db.get_device_property(
                    self.dev_name, prop_name
                )
                # property_value is a list!
                # build a dictionary where to each property is associated
                # a single value if the list has lenght 1.
                for property_name, property_value in property_datum.items():
                    if len(property_value) > 1:
                        properties[property_name] = property_value
                    else:
                        properties[property_name] = property_value[0]
        except Exception as e:
            # pylint: disable-next=raise-missing-from
            raise ValueError(e)
        return properties

    def add_attributes(self: ComponentManagerConfiguration) -> None:
        """Add the device properties as attribute of the class."""
        if not self.properties:
            return
        for prop_name, property_value in self.properties.items():
            setattr(self, prop_name, property_value)
