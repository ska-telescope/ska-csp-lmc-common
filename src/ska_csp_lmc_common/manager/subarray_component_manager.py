from __future__ import annotations

import functools
import json
import logging
import threading
import traceback
from typing import Any, Callable, Dict, List, Optional, Tuple

# SKA imports
from ska_control_model import (
    CommunicationStatus,
    ObsMode,
    ObsState,
    ResultCode,
    TaskStatus,
)
from ska_tango_base.faults import CommandError
from tango import DevError

from ska_csp_lmc_common.commands.command_map import BaseCommandMap

# local imports
from ska_csp_lmc_common.commands.main_task_composer import MainTaskComposer
from ska_csp_lmc_common.commands.main_task_executor import MainTaskExecutor
from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.manager import EventManager
from ska_csp_lmc_common.manager.base_callbacks import (
    BaseInitCallbacks,
    BaseStopCallbacks,
)
from ska_csp_lmc_common.manager.csp_base_component_manager import (
    CSPBaseComponentManager,
)
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.subarray import (
    SubarrayHealthModel,
    SubarrayObsStateModel,
    SubarrayOpStateModel,
)
from ska_csp_lmc_common.subarray.json_configuration_parser import (
    JsonConfigurationParser,
)
from ska_csp_lmc_common.subarray.subarray_factory import (
    ObservingComponentFactory,
)
from ska_csp_lmc_common.utils.cspcommons import (
    list_of_subsystem_fqdns,
    method_name,
)
from ska_csp_lmc_common.utils.decorators import (
    check_device_on,
    kwarged,
    reset_resources,
    transaction_id,
)

module_logger = logging.getLogger(__name__)


# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments

# pylint: disable=protected-access
# pylint: disable=abstract-method
# pylint: disable=invalid-name
# pylint: disable=arguments-renamed
# pylint: disable=arguments-differ
# pylint: disable=duplicate-code
# pylint: disable=broad-except
# pylint: disable=too-many-locals
# pylint: disable=unused-argument


class CSPSubarrayComponentManager(CSPBaseComponentManager):
    """Component Manager for the Csp Subarray."""

    def __init__(
        self: CSPSubarrayComponentManager,
        health_model: SubarrayHealthModel,
        op_state_model: SubarrayOpStateModel,
        obs_state_model: SubarrayObsStateModel,
        properties: ComponentManagerConfiguration,
        update_device_property_cbk: Callable,
        logger: Optional[logging.Logger] = None,
        communication_state_changed_callback: Callable[
            [CommunicationStatus], None
        ] = None,
        component_state_changed_callback: Callable[
            [dict[str, Any]], None
        ] = None,
    ) -> None:
        """Instantiate the Component Manager for the Csp Subarray.

        :param health_model: The Health State Model to evaluate the CSP
            Subarray health state.
        :param op_state_model: The Operational State Model to evaluate the CSP
            Subarray device operational state.
        :param obs_state_model: The Observing State Model to evaluate the CSP
            Subarray device observing state.
        :param properties: A class instance whose properties are the CSP
            Subarray TANGO Device Properties.
        :param update_device_property_cbk: The CSP Subarray method invoked to
            update the properties. Defaults to None.
        :param logger: The device or python logger if default is None.
        """
        super().__init__(
            op_state_model,
            health_model,
            properties,
            1,
            update_device_property_cbk,
            communication_state_changed_callback,
            component_state_changed_callback,
            logger,
        )

        self.obs_state_model = obs_state_model

        self.PssSubarrayFqdn = ""
        self.CbfSubarrayFqdn = ""
        self.PstBeamsFqdn = []
        self._assigned_resources = []
        self._assigned_pst_beam = []

        self.component_factory = None
        self.endscan_event = threading.Event()
        self._sub_id = 0
        self._scan_id = 0
        self._assigned_timing_beams_ids = []
        self._valid_scan_configuration = ""
        self._config_id = ""
        self._obs_modes = [
            ObsMode.IDLE,
        ]
        self._command_map = BaseCommandMap().get_subarray_command_map()
        # self_resources: python dictionary
        # key: sub-system fqdn
        # value: argin for command.
        # The dictionary allows specifying which subsystems the command
        # should be propagated to and, when required, the command's
        # argument. If the command does not accept any input argument,
        # this should be specified as `None`.
        self.resources = {}

    @property
    def obs_modes(self: CSPSubarrayComponentManager) -> List[ObsMode]:
        """Return the obsmodes supported by the subarray.

        :return: The list of obsModes supported by the subarray.
        """
        return self._obs_modes

    @property
    def obs_state(self: CSPSubarrayComponentManager) -> ObsState:
        """Return the subarray obsState

        :return: The subarray obsState.
        """
        return self.obs_state_model.obs_state

    @property
    def sub_id(self: CSPSubarrayComponentManager) -> int:
        """Return the subarray identification number.

        :return: The identification number of the CSP Subarray handled by the
            manager.
        """
        return self._sub_id

    @property
    def valid_scan_configuration(self: CSPSubarrayComponentManager) -> str:
        """Return the programmed configuration."""
        return self._valid_scan_configuration

    @property
    def assigned_timing_beams_ids(
        self: CSPSubarrayComponentManager,
    ) -> List[int]:
        """Return the list of assigned timing beams IDs."""
        return self._assigned_timing_beams_ids

    @assigned_timing_beams_ids.setter
    def assigned_timing_beams_ids(
        self: CSPSubarrayComponentManager, value: List[int]
    ) -> None:
        """Configure the list of assigned timing beams IDs.

        This value is configured at CM initialization.
        The subarray_id of the beam component is set to the corresponding
        subarray ID.
        """
        self.logger.debug(f"Set assigned_timing_beams_ids to {value}")
        self._assigned_timing_beams_ids = value

        for fqdn, component in self._components.items():
            if int(fqdn[-2:]) in self._assigned_timing_beams_ids:
                component.subarray_id = self.sub_id
                self.logger.debug(
                    f"set {component.fqdn} "
                    f"sub_id to {component.subarray_id}"
                )

    @property
    def assigned_timing_beams_state(
        self: CSPSubarrayComponentManager,
    ) -> List[int]:
        """Return the state of the assigned timing beams"""
        beams_state = []
        for fqdn, component in self.online_components.items():
            if self.PstBeamsFqdn and fqdn in self.PstBeamsFqdn:
                beams_state.append(component.state)
        return beams_state

    @property
    def assigned_timing_beams_health_state(
        self: CSPSubarrayComponentManager,
    ) -> List[int]:
        """Return the state of the assigned timing beams"""
        beams_health = []
        for fqdn, component in self.online_components.items():
            if self.PstBeamsFqdn and fqdn in self.PstBeamsFqdn:
                beams_health.append(component.health_state)
        return beams_health

    @property
    def assigned_resources(
        self: CSPSubarrayComponentManager,
    ) -> List[str]:
        """Return the FQDN od the resources assigned to the subsystems"""
        return self._assigned_resources

    @property
    def config_id(
        self: CSPSubarrayComponentManager,
    ) -> str:
        """Return the ID of the received configuration.

        :return: a string with the ID of the received configuration.
        """
        return self._config_id

    @property
    def obs_mode(
        self: CSPSubarrayComponentManager,
    ) -> List[str]:
        """Return the list of observation modes for the subarray.

        :return: a list of strings with the ObSMode.
        """
        return self._obs_modes

    #########################
    # Class protected methods
    #########################

    def _create_init_callbacks(self) -> BaseInitCallbacks:
        """Create subarray-specific initialization callbacks."""
        return SubarrayInitCallbacks(self, self.logger)

    def _create_stop_callbacks(self) -> BaseStopCallbacks:
        """Create subarray-specific stop callbacks."""
        return SubarrayStopCallbacks(self, self.logger)

    def _observing_factory(
        self: CSPSubarrayComponentManager, logger: logging.Logger
    ) -> ObservingComponentFactory:
        """Configure the Factory to create the CSP sub-systems components.

        This class is specialized in Mid.CSP and Low.CSP.

        :param logger: the logger for this instance.

        :return: A factory object to create CSP Subarray sub-systems observing
            components.
        """
        return ObservingComponentFactory(logger)

    def _add_subsystems_components(self: CSPSubarrayComponentManager) -> None:
        """Instantiate a Python class for each CSP sub-system subordinate
        device.

        Each sub-system component works as an adapter and cache for the
        associated CSP sub-system and provides the methods or calls to operate
        on the associate sub-system TANGO device.

        On failure, the state and health state of the CSP Subarray is forced
        to FAULT/FAILED.
        """
        try:
            if hasattr(self.device_properties, "CbfSubarray"):
                self.CbfSubarrayFqdn = self.device_properties.CbfSubarray
            if hasattr(self.device_properties, "PssSubarray"):
                self.PssSubarrayFqdn = self.device_properties.PssSubarray
            # PstBeams properties specifies a list of fqdn
            if hasattr(self.device_properties, "PstBeams"):
                self.PstBeamsFqdn = self.device_properties.PstBeams
                if isinstance(self.device_properties.PstBeams, str):
                    self.PstBeamsFqdn = [self.device_properties.PstBeams]
            list_of_fqdn = list_of_subsystem_fqdns(
                self.CbfSubarrayFqdn,
                self.PssSubarrayFqdn,
                self.PstBeamsFqdn,
            )

            for fqdn in list_of_fqdn:
                domain_name = fqdn.split("/")[0]
                sub_system_identifier = domain_name[-3:]
                self._components[fqdn] = self.component_factory.get_component(
                    fqdn, sub_system_identifier
                )
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(
                "Error in defining the list of ComponentManager"
                f" properties: {e}"
            )
            # Here we force the opState and healthState to FAULT/FAILED
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)

    def _task_submitter(
        self: CSPSubarrayComponentManager,
        task_name: str,
        task_callback: Callable = None,
        argin: Optional[Any] = None,
        **kwargs: Any,
    ) -> Tuple[TaskStatus, str]:
        """
        Common method to submit a command

        :param task_name: the name of the task to be submitted
        :param argin: input argument of the task

        :return: a tuple with *TaskStatus* and an informative msg
        """
        # pylint: disable-next=unused-variable
        task_status, msg = super()._task_submitter(
            task_name, task_callback, argin, **kwargs
        )
        if task_status == TaskStatus.FAILED:
            self.obs_state_model.component_action_driven(False)
        return task_status, msg

    def _json_configuration_parser(
        self: CSPSubarrayComponentManager,
        argin,
    ) -> JsonConfigurationParser:
        """Configure the class that handles the parsing of the input JSON files
        sent as argument of AssignResources, Configure and Scan.

        This class is specialized in Mid.CSP and Low.CSP.
        """
        return JsonConfigurationParser(self, argin, self.logger)

    @reset_resources
    def _on(
        self: CSPSubarrayComponentManager,
        task_name: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Method submitted into thread executor for on command

        :param task_name: the name of the task
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        def on_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP On task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            task_callback(
                status=status,
                result=result,
            )

        try:
            for fqdn in self.online_components:
                self.resources[fqdn] = None
            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    on_completed,
                )
            else:
                task_callback(
                    status=TaskStatus.COMPLETED,
                    result=[
                        ResultCode.OK,
                        ("All sub-systems are already in ON state",),
                    ],
                )

        # pylint: disable-next=broad-except
        except CommandError as exc:
            self.logger.error(f"CommandError exception: {str(exc)}")
            task_callback(
                status=TaskStatus.REJECTED,
                result=[ResultCode.REJECTED, str(exc)],
            )
        except Exception as err:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Controller. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                status=TaskStatus.FAILED,
                result=(ResultCode.FAILED, warning_msg),
            )

    @reset_resources
    def _off(
        self: CSPSubarrayComponentManager,
        task_name,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:  # type: ignore
        """
        Method submitted into thread executor for off command

        :param task_name: the name of the task
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        def off_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP Off task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            task_callback(
                status=status,
                result=result,
            )

        try:
            for fqdn in self.online_components:
                self.resources[fqdn] = None

            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    off_completed,
                )
            else:
                task_callback(
                    status=TaskStatus.COMPLETED,
                    result=(ResultCode.OK, "No device to switch off"),
                )

        # pylint: disable-next=broad-except
        except CommandError as exc:
            task_callback(
                status=TaskStatus.REJECTED,
                result=(ResultCode.REJECTED, exc),
            )
        except Exception as err:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Controller. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                status=TaskStatus.FAILED,
                result=(ResultCode.FAILED, warning_msg),
            )

    def force_health_state(
        self: CSPSubarrayComponentManager, faulty_flag: bool
    ) -> None:
        """
        Method to set/unset the subarray healthState to FAILED.

        :param faulty_flag: whether to force the healthstate to FAILED or not.
        """
        self.health_model.component_fault(faulty_flag)

    def update_json(self: CSPSubarrayComponentManager):
        """
        Internal function TBD

        Update the configuration script adding the PST Beam(s)
        addresses.

        :param original_dict: the original input configuration.
        :param updated_info: the updated configuration.
        """

        self.logger.debug("Method to be specialized in CSP Mid/Low")
        return TaskStatus.COMPLETED, ResultCode.OK

    def update_obs_mode(
        self: CSPSubarrayComponentManager,
        resources_to_send: Dict,
    ):
        """Method to update obsmode based on the obsstate and configuration"""
        if resources_to_send and ObsMode.IDLE in self._obs_modes:
            self._obs_modes.pop(ObsMode.IDLE)
            self.update_attribute("obsMode", self._obs_modes)

    def reset_obs_mode(self: CSPSubarrayComponentManager):
        """Reset the obsmode to IDLE"""
        self._obs_modes = [
            ObsMode.IDLE,
        ]
        self.update_attribute("obsMode", self._obs_modes)

    def _reset_obs_model_action_driven(
        self: CSPSubarrayComponentManager, action: str, status: TaskStatus
    ):
        """
        Re-evaluate the obsState value when the task fails or it's rejected.

        Note: the `update_command_info` method of the CommandTracker invokes
        the `perform_action` method of the ObsState model only when the task
        status is COMPLETED.

        :param action: the action to perform, as a combination of the
            command name and the string 'completed'.
        :param status: the task status value
        """
        if status and status in [TaskStatus.FAILED, TaskStatus.REJECTED]:
            # re-evaluate the obsState
            self.obs_state_model.perform_action(f"{action}_completed")

    @reset_resources
    def _configure(
        self: CSPSubarrayComponentManager,
        task_name: str,
        argin: dict,
        task_callback: Callable = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Method submitted into thread executor for configure command.

        :param task_name: the name of the task
        :param argin: configuration input for Subarray
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        @kwarged
        def configure_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP Configure task completes.
            It updates the configuration information on success.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            if result and result[0] == ResultCode.OK:
                self.logger.debug(
                    "Configuration completed. Update information"
                )

                # store configuration file
                self._valid_scan_configuration = json.dumps(
                    kwargs["configure_script"]
                )
            # The obsMode is always updated regardless of the result code.
            # value. This is because not all subsystems communicate the
            # command result code.
            self.update_obs_mode(self.resources)
            task_callback(
                status=status,
                result=result,
            )
            self._reset_obs_model_action_driven("configure", status)

        self.logger.debug(f"obs-state is: {self.obs_state_model.obs_state}")
        json_configuration_parser = self._json_configuration_parser(argin)

        try:
            # resource_to_send: python dict
            # key: sub-system fqdn
            # value: dict with the whole sub-system configuration
            self.resources = json_configuration_parser.configure()
        except (ValueError, KeyError) as err:
            warning_msg = str(err)
            self.logger.error(warning_msg)
            task_callback(
                result=(ResultCode.FAILED, warning_msg),
                status=TaskStatus.COMPLETED,
            )
            return  # Exit early for configuration failure

        try:
            self._config_id = json_configuration_parser.get_id()

            # Empty sections in the configuration file
            # Very unlikely to happen
            if len(self.resources.keys()) == 0:
                task_callback(
                    result=(ResultCode.FAILED, "No components to configure"),
                    status=TaskStatus.COMPLETED,
                )
                return  # Exit early for configuration failure

            self.logger.debug(
                f"Configuring {len(self.resources.keys())} components"
            )
            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    functools.partial(
                        configure_completed,
                        configure_script=argin,
                    ),
                )

        # pylint: disable-next=broad-except
        except Exception as err:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Subarray. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                result=(ResultCode.FAILED, warning_msg),
                status=TaskStatus.FAILED,
            )

    @reset_resources
    def _abort(self, task_callback: Optional[Callable] = None):
        """
        Create a thread pool to perform the following action:
          - create abort command for the CSP subarrays
          - send abort command to any running stoppable CSP subarray task
          - shutdown the executors (CSPexecutor, subarray_executor,
            clean priority queue)
          - re-init the executors (CSPexecutor, subarray_executor,
            priority queue)


        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *ResultCode* and an informative message string.
        """

        @kwarged
        def abort_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ):
            """
            Method invoked on abort completion to reset the obsMode.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """

            # Note subsystems do not send the result code for the ABORT
            # so check on result code is not possible
            # if result:
            #     if result[0] == ResultCode.OK:
            self.logger.debug(
                "Abort completed. Update information "
                f"status: {status} result: {result}"
            )
            self.logger.info(
                "Abort completed. Update information "
                f"status: {status} result: {result}"
            )
            if status and status not in [
                TaskStatus.IN_PROGRESS,
                TaskStatus.QUEUED,
            ]:
                self.reset_obs_mode()
            task_callback(status=status, result=result)

        # Initialize result_code to avoid
        # "reference before assignment" error
        self.resources = {}
        for fqdn, _ in self.online_components.items():
            self.resources[fqdn] = None

        result_code = ResultCode.UNKNOWN
        message = ""
        error_details = None
        try:

            abort_executor = MainTaskExecutor(
                prefix="Abort",
                logger=self.logger,
            )

            composer = MainTaskComposer(
                "abort",
                self._command_map,
                self,
                task_callback,
            )
            abort_task = composer.compose()
            self.logger.debug(
                "Number of commands to execute:" f" {len(abort_task.subtasks)}"
            )
            message = "Abort command completed with success"
            if abort_task and abort_task.tracker.total_subtasks:
                abort_executor.submit_main_task(
                    abort_task,
                    abort_completed,
                )
            else:
                # what happens if total_subtasks == 0?
                # force the CSP Subarray obstate to ABORTED
                self.obs_state_model.component_aborted(True)
                abort_completed(
                    status=TaskStatus.COMPLETED,
                    result=(ResultCode.OK, "Abort completed"),
                )
            # pylint: disable-next=broad-except
        except Exception as abort_exc:
            message = (
                f"Internal SW error executing in ABORT task on CSP"
                f" Subarray Received exception: {abort_exc}"
            )
            self.logger.error(message)
            error_details = abort_exc
            # !!!!!!!!!!!!!!!
            # Not sure to set the device state in FAULT
            # The TANGO state represents the state of the component.
            # This does not include the software.
            # here we need raise an error on a specific attribute
            # reporting the Internal Software state.
            task_status = TaskStatus.FAILED
            result_code = ResultCode.FAILED
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            result = result_code, message
            callback_args = {
                "status": task_status,
                "result": [result, message],
            }
            if error_details is not None:
                callback_args["error"] = error_details
            task_callback(**callback_args)

    @reset_resources
    def _obsreset(
        self: CSPSubarrayComponentManager,
        task_name: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Method submitted into thread executor for obsreset command

        Currently the command is rejected because not used by the sub-systems.

        :param task_name: the name of the task
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """
        # Currently this part of the code is not used. The command is rejected
        # because not used by the sub-systems.

        warning_msg = (
            "The command should not arrive here."
            "The command is rejected by the CSP subsystems"
        )
        result_code = ResultCode.FAILED, warning_msg
        task_status = TaskStatus.COMPLETED
        task_callback(
            status=task_status,
            result=result_code,
        )

    @reset_resources
    def _restart(
        self: CSPSubarrayComponentManager,
        task_name: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Method submitted into thread executor for restart command

        :param task_name: the name of the task
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        @kwarged
        def restart_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ):
            """Method invoked when the CSP Restart task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            self.logger.info("Restart completed")
            task_callback(
                status=status,
                result=result,
            )

        try:
            for fqdn in self.online_components:
                self.resources[fqdn] = None

            self.logger.info(
                f"Restart on {len(self.resources.keys())} components"
            )
            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                # There are subsystems in DevState==ON and
                # ObsState in FAULT/ABORTED
                self._main_task_executor.submit_main_task(
                    main_task,
                    restart_completed,
                )
            elif self.obs_state_model.aborted:
                info_msg = (
                    "No CSP sub-system is in ABORTED. Restart only CSP"
                    " Subarray"
                )
                # All the CSP Sub-systems are not in ABORTED but
                # the CSP Subarray is in obsState ABORTED.
                # In this case the Restart command
                # reset the CSP Subarray aborted condition
                # and update the obsState
                self.obs_state_model.component_aborted(False)
                result_code = [ResultCode.OK, info_msg]
                task_callback(
                    status=TaskStatus.COMPLETED,
                    result=result_code,
                )
            elif self.obs_state_model.faulty:
                # no commands to invoke on sub-systems
                info_msg = (
                    "No CSP sub-system is in FAULT. Restart only CSP"
                    " Subarray"
                )
                self.logger.debug(f"!!!! {info_msg}")
                # All the CSP Sub-systems are not in FAULT but
                # the CSP Subarray is in obsState FAULT.
                # Can this happen? In this case the ObsReset command
                # reset the CSP Subarray faulty condition
                # reset the fault condition and update CSP Subarray state
                self.obs_state_model.component_fault(False)
                result_code = [ResultCode.OK, info_msg]
                task_callback(
                    status=TaskStatus.COMPLETED,
                    result=result_code,
                )
            else:
                info_msg = "Nothing in fault, nothing to reset."
                result_code = [ResultCode.OK, info_msg]
                task_callback(
                    status=TaskStatus.COMPLETED,
                    result=result_code,
                )
        except CommandError as exc:
            self.logger.error(f"CommandError exception: {str(exc)}")
            task_callback(
                status=TaskStatus.REJECTED,
                result=[ResultCode.REJECTED, str(exc)],
            )
        # pylint: disable-next=broad-except
        except Exception as err:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Subarray. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            # !!!!!!!!!!!!!!!
            # Not sure to set the device state in FAULT
            # The TANGO state represents the state of the component.
            # This does not include the software.
            # here we need raise an error on a specific attribute
            # reporting the Internal Software state.
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                status=TaskStatus.FAILED,
                result=(ResultCode.FAILED, warning_msg),
            )

    @reset_resources
    def _assign(
        self: CSPSubarrayComponentManager,
        task_name: str,
        argin: dict,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ):
        """
        Method submitted into thread executor for assignresources command

        :param task_name: the name of the task
        :param argin: resources to be assigned to Subarray
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        @kwarged
        def assign_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP AssignResources task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            task_callback(
                status=status,
                result=result,
            )
            self._reset_obs_model_action_driven("assign", status)

        self.logger.debug(
            f"ASSIGN: obs_state: "
            f"{self.obs_state_model.obs_state} {id(task_abort_event)}"
        )

        try:
            self.resources = self._json_configuration_parser(
                argin
            ).assignresources()

            if not self.resources:
                raise Exception("Error in parsing the resource configuration")

            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    assign_completed,
                )
            else:
                info_msg = (
                    "No CSP sub-system can Assign Resources, nothing to be"
                    " done."
                )
                self.logger.warning(info_msg)
                result_code = ResultCode.OK, info_msg
                task_status = TaskStatus.COMPLETED
                task_callback(
                    status=task_status,
                    result=result_code,
                )
        # pylint: disable-next=broad-except
        except Exception as e:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Subarray. Received exception: {e}"
            )
            self.logger.error(warning_msg)
            self.logger.error(traceback.format_exc())
            result_code = ResultCode.FAILED, warning_msg
            task_status = TaskStatus.COMPLETED
            task_callback(
                status=task_status,
                result=result_code,
            )

    def connect_to_pst(
        self: CSPSubarrayComponentManager,
    ) -> Tuple[TaskStatus, ResultCode]:
        """Method invoked to connect to requested pst beams during the
        assignment of resources.
        This method is invoked to:

        - update the Pst beams allocated to the subarray
        - subscribe for Pst Beams attributes
        - release all the Pst Beams erroneously allocated to the subarray
        - update the list of allocated resources on all subsystems:
          cbf, pst, pss

        Returns:
            Tuple[TaskStatus, ResultCode]: status and result code of the
            operation
        """

        self.logger.info("Connecting to assigned PST beams")

        status = TaskStatus.COMPLETED
        result_code = ResultCode.OK

        if not self.PstBeamsFqdn:
            self.logger.warning("no Pst beams present in deployment")
            return status, result_code

        try:
            default_attributes = [
                "state",
                "healthstate",
                "obsState",
                "adminmode",
            ]
            for fqdn, resource in self.resources.items():
                if "pst" in fqdn and resource["subarray_id"] == self.sub_id:
                    component = self.components[fqdn]
                    component.assignresources(resource)
                    if component.subarray_id == self.sub_id:
                        if (
                            component.capability_id
                            not in self._assigned_timing_beams_ids
                        ):
                            self.subscribe_attributes_for_change_events(
                                component, default_attributes
                            )
                            self.subscribe_attributes_for_change_events(
                                component,
                                component.attrs_for_change_events,
                            )

                            self._assigned_timing_beams_ids.append(
                                component.capability_id
                            )
                            self._assigned_pst_beam.append(fqdn)
                    elif component.subarray_id:
                        self.evt_manager.unsubscribe_event_on_component(
                            component
                        )
                        component.disconnect()
            self.logger.debug(
                f"assigned timing beams:{self._assigned_timing_beams_ids}"
            )
            self.update_attribute(
                "assignedTimingBeamIDs", self._assigned_timing_beams_ids
            )
            # assigned Resources is updated via events. No need to call here!

            self.update_assigned_resources("pst", self._assigned_pst_beam)
            self.update_attribute(
                "assignedResources", self._assigned_resources
            )
        except Exception as exc:
            self.logger.error(
                f"Connecting to assigned PST beams failed with error: {exc}"
            )
            status = TaskStatus.FAILED
            result_code = ResultCode.FAILED
        return status, result_code

    def disconnect_from_all_pst(self: CSPSubarrayComponentManager):
        """Method invoked to disconnect from all Pst beams allocated to the
        subarray.
        This method is just a wrapper of the method used to disconnect
        a single pst beam, but the self.resources attributed updated to
        disconnect from all of them
        """

        for fqdn in self.online_components:
            if int(fqdn[-2:]) in self._assigned_timing_beams_ids:
                self.resources[fqdn] = {"subarray_id": self.sub_id}
        return self.disconnect_from_pst()

    def disconnect_from_pst(self: CSPSubarrayComponentManager):
        """Method invoked to disconnect from Pst Beam when they are released.
        This method is invoked to:

        - update the Pst beams allocated to the subarray
        - unsubscribe the released Pst Beams attributes
        - update the list of allocated resources on all subsystems:
          cbf, pst, pss
        """
        removed_beams_ids = []

        status = TaskStatus.COMPLETED
        result_code = ResultCode.OK
        if not self.PstBeamsFqdn:
            self.logger.warning("no Pst beams present in deployment")
            return status, result_code

        try:
            for fqdn, resource in self.resources.items():
                if (
                    "pst" in fqdn
                    and int(fqdn[-2:]) in self._assigned_timing_beams_ids
                    and resource["subarray_id"] == self.sub_id
                ):
                    if fqdn not in self.PstBeamsFqdn:
                        raise Exception(
                            f"Impossible to remove {fqdn}: "
                            "not present in deployment"
                        )

                    component = self.components[fqdn]
                    component.releaseresources(resource)

                    if component.subarray_id:
                        raise Exception(
                            "Failure in removing pst beam from subarray"
                        )

                    if (
                        component.capability_id
                        not in self._assigned_timing_beams_ids
                    ):
                        raise Exception(
                            "Pst beam was not assigned to subarray"
                        )

                    self.evt_manager.unsubscribe_event_on_component(component)
                    removed_beams_ids.append(component.capability_id)

                    if self._assigned_pst_beam:
                        self._assigned_pst_beam.remove(fqdn)

                    assigned_beams_ids = set(
                        self._assigned_timing_beams_ids
                    ) - set(removed_beams_ids)
                    self._assigned_timing_beams_ids = list(assigned_beams_ids)
                    # update the attributes on the subarray device
                    self.update_attribute(
                        "assignedTimingBeamIDs",
                        self._assigned_timing_beams_ids,
                    )

                    self.update_assigned_resources(
                        "pst", self._assigned_pst_beam
                    )
                    self.update_attribute(
                        "assignedResources", self._assigned_resources
                    )

        except Exception as exc:
            self.logger.error(
                "Disconnecting from assigned PST beams "
                f"failed with error: {exc}"
            )

            status = TaskStatus.FAILED
            result_code = ResultCode.FAILED

        return status, result_code

    @reset_resources
    def _release(
        self: CSPSubarrayComponentManager,
        task_name: str,
        argin: dict,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ):
        """
        Method submitted into thread executor for release command

        :param task_name: the name of the task
        :param argin: resources to be released by Subarray
        :param: task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        @kwarged
        def release_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP End Scan task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            self.logger.info("Release Resources completed.")
            task_callback(
                status=status,
                result=result,
            )
            self._reset_obs_model_action_driven("release", status)

        self.logger.debug(
            f"RELEASE: obs_state: "
            f"{self.obs_state_model.obs_state} {id(task_abort_event)}"
        )

        try:
            self.resources = self._json_configuration_parser(
                argin
            ).releaseresources()

            if not self.resources:
                raise Exception("Error in parsing the resource configuration")

            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    release_completed,
                )
            else:
                info_msg = (
                    "No CSP sub-system can Release Resources, nothing to be"
                    " done."
                )
                self.logger.warning(info_msg)
                result_code = ResultCode.OK, info_msg
                task_status = TaskStatus.COMPLETED
                task_callback(
                    status=task_status,
                    result=result_code,
                )
        # pylint: disable-next=broad-except
        except Exception as e:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Subarray. Received exception: {e}"
            )
            self.logger.error(warning_msg)
            self.logger.error(traceback.format_exc())
            result_code = ResultCode.FAILED, warning_msg
            task_status = TaskStatus.COMPLETED
            task_callback(
                status=task_status,
                result=result_code,
            )

    @reset_resources
    def _release_all(
        self: CSPSubarrayComponentManager,
        task_name: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ):
        """
        Method submitted into thread executor for release_all command

        :param task_name: the name of the task
        :param task_callback: method called to update the task result
        :param: task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        @kwarged
        def release_all_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP End Scan task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            self.logger.info("Release Resources completed.")
            task_callback(
                status=status,
                result=result,
            )
            self._reset_obs_model_action_driven("release_all", status)

        self.logger.debug(
            f"RELEASE ALL: obs_state: "
            f"{self.obs_state_model.obs_state} {id(task_abort_event)}"
        )

        try:
            for fqdn in self.online_components:
                self.resources[fqdn] = None

            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    release_all_completed,
                )
            else:
                info_msg = (
                    "No CSP sub-system can Release Resources, nothing to be"
                    " done."
                )
                self.logger.warning(info_msg)
                result_code = ResultCode.OK, info_msg
                task_status = TaskStatus.COMPLETED
                task_callback(
                    status=task_status,
                    result=result_code,
                )
        # pylint: disable-next=broad-except
        except Exception as e:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Subarray. Received exception: {e}"
            )
            self.logger.error(warning_msg)
            self.logger.error(traceback.format_exc())
            result_code = ResultCode.FAILED, warning_msg
            task_status = TaskStatus.COMPLETED
            task_callback(
                status=task_status,
                result=result_code,
            )

    @reset_resources
    def _scan_preparation(self, json_input: dict) -> bool:
        """Preliminary check for the scan command and
        store scan id.

        :param json_input: scan input json (dict)
        """
        input_ok = True
        if json_input:
            self._store_scan_id(json_input)
            for fqdn, component in self.online_components.items():
                # Add only the sub-system components that are in READY state
                if component.obs_state == ObsState.READY:
                    self.resources[fqdn] = json_input
        else:
            error_msg = "Error in parsing the scan input json file"
            self.logger.error(error_msg)
            input_ok = False

        return input_ok

    def _store_scan_id(self, json_input: dict) -> None:
        """Extract and store the Scan Id from the scan input json.

        :param json_input: scan input json (dict)
        """
        self._scan_id = json_input["scan_id"]
        self.update_attribute("scanID", int(self._scan_id))

    def _scan(
        self: CSPSubarrayComponentManager,
        task_name: str,
        argin: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Method submitted into thread executor for scan command

        :param task_name: the name of the task
        :param argin: resources to be assigned to Subarray
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        @kwarged
        def scan_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP SCAN task completes.
            It updates the configuration information on success.

            :param is_task_succeeded: whether the task completed
                with success.
            :param scan_script: the dictionary loaded from the JSON
                scan script received from the client.
            """
            self.logger.debug("Scan completed. Update information")
            # The obsMode is always updated regardless of the result code.
            # value. This is because not all subsystems communicate the
            # command result code.
            self.update_obs_mode(kwargs["json_input"])
            self.obs_state_model.obs_state = ObsState.SCANNING
            task_callback(
                status=status,
                result=result,
            )

        self.logger.debug(f"In: {task_name}")
        try:
            if self._scan_preparation(argin):

                composer = MainTaskComposer(
                    task_name,
                    self._command_map,
                    self,
                    task_callback,
                )
                main_task = composer.compose()
                if main_task and main_task.tracker.total_subtasks:
                    self._main_task_executor.submit_main_task(
                        main_task,
                        functools.partial(
                            scan_completed,
                            json_input=argin,
                        ),
                    )
            else:
                error_msg = "Error in parsing the scan input json file"
                self.logger.error(error_msg)
                result_code = ResultCode.FAILED, error_msg
                task_status = TaskStatus.COMPLETED
                task_callback(
                    status=task_status,
                    result=result_code,
                )
        # pylint: disable-next=broad-except
        except Exception as e:
            warning_msg = (
                f"Internal SW error executing  in task {task_name} on CSP"
                " Subarray. " + f"Received exception: {e}"
            )
            self.logger.error(warning_msg)
            self.logger.error(traceback.format_exc())
            result_code = ResultCode.FAILED, warning_msg
            task_status = TaskStatus.COMPLETED
            task_callback(
                status=task_status,
                result=result_code,
            )

    @reset_resources
    def _end_scan(
        self: CSPSubarrayComponentManager,
        task_name: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Method submitted into thread executor for end_scan command

        :param task_name: the name of the task
        :param: task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        @kwarged
        def end_scan_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP End Scan task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            self.logger.info("End Scan completed.")
            task_callback(
                status=status,
                result=result,
            )

        try:
            for fqdn, _ in self.online_components.items():
                self.resources[fqdn] = None

            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    end_scan_completed,
                )
            else:
                info_msg = (
                    "No CSP sub-system in ObsState.SCANNING, nothing to be"
                    " done."
                )
                self.logger.warning(info_msg)
                result_code = ResultCode.OK, info_msg
                task_status = TaskStatus.COMPLETED
                task_callback(
                    status=task_status,
                    result=result_code,
                )
        # pylint: disable-next=broad-except
        except Exception as e:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Subarray. Received exception: {e}"
            )
            self.logger.error(warning_msg)
            result_code = ResultCode.FAILED, warning_msg
            task_status = TaskStatus.COMPLETED
            task_callback(
                status=task_status,
                result=result_code,
            )

    @check_device_on
    @reset_resources
    def _gotoidle(
        self: CSPSubarrayComponentManager,
        task_name: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Method submitted into thread executor for gotoidle command

        :param task_name: the name of the task
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        def gotoidle_completed(
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """Method invoked when the CSP GoToIdle task completes.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if raised
            """
            self._obs_modes = [
                ObsMode.IDLE,
            ]
            self.update_attribute("obsMode", self._obs_modes)
            self.logger.info("GoToIdle completed")
            task_callback(
                status=status,
                result=result,
            )

        try:
            for fqdn in self.online_components:
                self.resources[fqdn] = None
            self.logger.debug(
                f"Go to Idle on {len(self.resources.keys())} components"
            )
            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    gotoidle_completed,
                )
            else:
                info_msg = f"No CSP sub-system to execute {task_name}."
                self.logger.warning(info_msg)
                result_code = ResultCode.OK, info_msg
                task_status = TaskStatus.COMPLETED
                task_callback(
                    status=task_status,
                    result=result_code,
                )

        # pylint: disable-next=broad-except
        except CommandError as exc:
            self.logger.error(f"CommandError exception: {str(exc)}")
            task_callback(
                status=TaskStatus.REJECTED,
                result=[ResultCode.REJECTED, str(exc)],
            )
        except Exception as err:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Subarray. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                status=TaskStatus.FAILED,
                result=(ResultCode.FAILED, warning_msg),
            )

    ##############################
    # Class public methods
    ##############################

    def init(self: CSPSubarrayComponentManager):
        """
        Initialize the Subarray ComponentManager.
        Instantiate:

        - the command factory
        - the json parser for the configuration files
        - the event manager
        - an ObservingComponent object for each CSP sub-system defined in the
          CSP Subarray Device Properties.
        """
        super().init()
        self._sub_id = int(self.device_properties.SubID)
        # self.command_factory = CommandFactory(self.logger)
        self.component_factory = self._observing_factory(self.logger)
        self._add_subsystems_components()
        self.evt_manager = EventManager(
            "subarray",
            self.update_attribute,
            self.health_model,
            self.op_state_model,
            self.obs_state_model,
            100,
            self.logger,
        )

    def start_communicating(self: CSPSubarrayComponentManager):
        """Method to start the monitoring of the CSP subordinate devices. It
        establishes the connection with the CSP subordinate sub-systems
        observing devices. On success, the main sub-systems attributes are
        subscribed for events.

        It is invoked when the adminMode of the device is OFFLINE or
        ENGINEERING.
        """
        start_thread = threading.Thread(target=self._start_communicating)
        start_thread.start()

    def stop_communicating(self: CSPSubarrayComponentManager) -> None:
        """Method invoked when the CSP Subarray TANGO Device is set OFFLINE.

        In this case the CspSubarrayComponentManager stop monitoring the
        subordinate devices.

        The admin mode is forwarded to all the online subordinate components.
        If the request fails, only the component offline are disconnected.
        """
        stop_thread = threading.Thread(
            target=self._stop_communicating,
            args=(list(self.online_components.keys()), None),
        )
        stop_thread.start()

    # def connect_to_capabilities(self: CSPSubarrayComponentManager):
    #     """Abstract method.
    #     To be specialized in Mid and Low to establish connection
    #     with the CSP.LMC Capability devices.
    #     """
    #     self.logger.warning(
    #         "To be implemented in Mid/Low Subarray Component Manager"
    #     )

    def on(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Enable the CSP Subarray to perform observing tasks. The command is
        forwarded to all the CSP Sub-systems observing devices (subarrays or
        _create_abort_cmd beams) that are currently online.

        :param task_callback: method called to update the task result
            related attributes

        :return a tuple with *TaskStatus* and an informative message.
        """
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,  # command_id
        )

    def off(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Disable the CSP Subarray to perform observing tasks. The command is
        forwarded to all the CSP Sub-systems observing devices (subarrays or
        beams) that are currently online.

        The command can be invoked from *any* observing state with the result
        that any ongoing process, is aborted and all the assigned resources
        released.

        :param task_callback: method called to update the task result
            related attributes
        :return: a tuple with *TaskStatus* and an informative message.
        """

        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,  # command_id
        )

    @check_device_on
    @transaction_id
    @reset_resources
    def configure(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
        **argin: dict,
    ) -> Tuple[TaskStatus, str]:
        """Configure the CSP Subarray to perform observing tasks. The command
        is forwarded to all the CSP Sub-systems observing devices (subarrays
        or beams) that are currently online.

        :param argin: The CSP Subarray configuration data
        :param task_callback: method called to update the task result
            related attributes
        :return: a tuple with *TaskStatus* and an informative message.
        """
        self._config_id = ""
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,  # command_id
            argin=argin,
        )

    @reset_resources
    def abort(
        self: CSPSubarrayComponentManager,
        task_callback: Callable,
    ) -> Tuple[TaskStatus, str]:
        """Abort any running stoppable CSP subarray task. The command is
        forwarded to all the CSP Sub-systems observing devices (subarrays or
        beams) that are currently online.

        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *TaskStatus* and an informative message.
        """
        # set the abort flag and update the task status to IN_PROGRESS
        # shutdown the ThreadPool: new commands are rejected

        self.logger.debug("Invoked abort tasks")
        task_status, msg = self._main_task_executor.abort(task_callback)
        threading.Thread(
            target=self._abort,
            args=(task_callback,),
        ).start()
        return task_status, msg

    def obsreset(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Reset a FAULT or ABORTED condition of the CSP Subarray. The command
        is forwarded to all the CSP Sub-systems observing devices (subarrays
        or beams) that are currently online and in FAULT or ABORTED observing
        state.

        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *TaskStatus* and an informative message.
        """
        msg = (
            "Obsreset command rejected, as it is not used by"
            " CSP subsystems."
        )
        return (TaskStatus.REJECTED, msg)

    def restart(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Restart a CSP Subarray in FAULT or ABORTED observing state.

        The command is forwarded to all the CSP Sub-systems
        observing devices (subarrays or beams) that are
        currently online.

        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *TaskStatus* and an informative message.
        """
        self._main_task_executor.clear_abort_flag()
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,  # command_id
        )

    @check_device_on
    @transaction_id
    def assign(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
        **argin: dict,
    ) -> Tuple[TaskStatus, str]:
        """Load the configuration from file specifying the resources for
        subsystems. Forward the command to all the subsystems.

        :param argin: the information with the resources to allocate to the
            CSP Subarray
        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *TaskStatus* and an informative message.
        """
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,  # command_id
            argin=argin,
        )

    def update_assigned_resources(
        self: CSPSubarrayComponentManager,
        resource_type: str,
        resource_value: List[str],
    ) -> None:
        """
        Update the list of the FQDN resources assigned to the subarray.

        :param resource_type: the type of the resources handled by the
            capabilities
        :param resource_value: the list of assigned resources' FQDNs.
        """

        def _remove_all_pst_beams(input_list: List[str]):
            return [item for item in input_list if "pst" not in item]

        if resource_type == "pst":
            # remove all the assigned pst beams and then add the
            # updated list
            self._assigned_resources = _remove_all_pst_beams(
                self.assigned_resources
            )
            self._assigned_resources.extend(resource_value)
            self._assigned_resources = sorted(self._assigned_resources)

    @check_device_on
    @transaction_id
    def release(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]],
        **argin: dict,
    ) -> Tuple[TaskStatus, str]:
        """Forward the command to release assigned resounces to all the
        subsystems.

        :param argin: the information with the resources to remove from the
            CSP Subarray
        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *TaskStatus* and an informative message.
        """
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,  # command_id
            argin=argin,
        )

    @check_device_on
    def release_all(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Forward the command to release all assigned resources to all the
        subsystems.

        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *TaskStatus* and an informative message.
        """
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,  # command_id
        )

    @check_device_on
    @transaction_id
    def scan(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
        **argin: dict,
    ) -> Tuple[TaskStatus, str]:
        """Start a Scan. The command is forwarded to all the CSP Sub-systems
        observing devices (subarrays or beams) that are currently online.

        :param task_callback: method called to update the task result
            related attributes
        :param argin: the scan information.

        :return: a tuple with *TaskStatus* and an informative message.
        """
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,  # command_id
            argin=argin,
        )

    @check_device_on
    def end_scan(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Stop gracefully the execution of a scan.

        The command is forwarded to all the CSP Sub-systems
        observing devices (subarrays or beams) that in scanning.

        :param task_callback: method called to update the task result
            related attributes

        :return: a tuple with *TaskStatus* and an informative message.
        """
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,  # command_id
        )

    def update_attribute(self, property_name, property_value):
        """Update attribute"""
        if self._update_device_property_cbk:
            self._update_device_property_cbk(property_name, property_value)

    @check_device_on
    def gotoidle(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Gotoidle the CSP Subarray to perform observing tasks. The command
        is forwarded to all the CSP Sub-systems observing devices (subarrays
        or beams) that are currently online.

        :param task_callback: method called to update the task result
            related attributes
        :return: a tuple with *TaskStatus* and an informative message.
        """
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,
        )

    # Alias for gotoidle
    deconfigure = gotoidle

    def reset(
        self: CSPSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        return super().reset(task_callback, command_map=self._command_map)


class SubarrayInitCallbacks(BaseInitCallbacks):
    """Class defining the callbacks to be invoked when connection with
    the subordinate sub-systems is established or failed on CSP.LMC Subarray
    """

    def check_and_subscribe(
        self: SubarrayInitCallbacks,
        default_attributes: List[str],
        component: Component,
    ) -> None:
        """Check if the Pst beams belong to the current subarray and then
        subscribe the subsystems default attribute calling the parent method.

        Check on PST beams is required during a re-initialization. Currently
        information are asked directly to the PstBeam devices, in future the
        needed information could be retrieved by the CSP Capabilities.

        :param default_attributes: attributes to be subscribed
        :param component: component object of connected subsystems"""

        _skip = False
        if self.cm.PstBeamsFqdn and component.fqdn in self.cm.PstBeamsFqdn:
            # read PstBeam subarray affiliation. This is configured at
            # CM initialization
            sub_id = component.subarray_id
            self.logger.debug(f"{component.fqdn} sub_id {sub_id}")
            self.logger.debug(f"Current id: {self.cm.sub_id}")
            if sub_id != int(self.cm.sub_id):
                # set the PST beam component to disconnect
                # because it doesn't belong to the current
                # Subarray
                component.disconnect()
                _skip = True
        if not _skip:
            default_attributes.append("obsstate")
            super().check_and_subscribe(default_attributes, component)

    def read_component_versions(
        self: SubarrayInitCallbacks,
    ):
        """
        Read the software version ID for the subarrays
        Not yet implemented
        """
        # not yet implemented. This function is defined only
        # for the controller

    # def succeeded(
    #     self: SubarrayInitCallbacks, stop_event: threading.Event
    # ) -> None:
    #     """
    #     Method invoked when communication is successfully established

    #     :param stop_Evet: event flag set at device re-initialization.
    #     """
    #     super().succeeded(stop_event)
    #     self.cm.connect_to_capabilities()


class SubarrayStopCallbacks(BaseStopCallbacks):
    """
    Class to handle callbacks at disconnection.
    Subarray specialization
    """

    def handler_to_invoke(
        self: SubarrayStopCallbacks,
        stop_event: threading.Event,
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: Any | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ) -> None:
        """
        The handler to invoke when disconnection ends.

        :param stop_event: event flag set at device re-initialization
        :param status: the task status
        :param progress: the progress of execution as percentage.
        :param result: tuple with the task result code and the message
        :param error: the exception, if raised
        """
        if result:
            if result[0] in [ResultCode.OK, ResultCode.FAILED]:
                self._disconnect(stop_event)
