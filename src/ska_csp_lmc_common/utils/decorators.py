import functools
from functools import wraps
from typing import Any, Callable, TypeVar, cast

from ska_ser_log_transactions import transaction
from ska_tango_base.faults import ComponentError
from tango import DevState

Wrapped = TypeVar("Wrapped", bound=Callable[..., Any])
# pylint: disable=redefined-outer-name
# pylint: disable=invalid-name


def reset_resources(func: Wrapped):
    """Decorator which reset the self.resources variable
    for each command, in order to avoid resources overlapping or
    remnants from previous commands"""

    @functools.wraps(func)
    def _wrapper(cm: Any, *args: Any, **kwargs: Any):
        cm.resources = {}
        return func(cm, *args, **kwargs)

    return cast(Wrapped, _wrapper)


def transaction_id(func):
    """Add a transaction id to the input of the decorated method.

    The input of the decorated method is a json string.
    """

    @functools.wraps(func)
    def wrap(*args, **kwargs):

        obj = args[0]
        argin = kwargs

        with transaction(
            func.__name__, argin, logger=obj.logger
        ) as transaction_id:
            argin["transaction_id"] = transaction_id
        return func(*args, **argin)

    return wrap


def check_device_on(func: Wrapped) -> Wrapped:
    """
    Return a function that checks the component device state
    then calls another function.

    This is based on the check_on decorator of ska_tango_base

    The component needs to be turned on, and not faulty, in order for
    the function to be called.

    This function is intended to be used as a decorator:

    .. code-block:: python

        @check_device_on
        def scan(self):
            ...

    :param func: the wrapped function

    :return: the wrapped function
    """

    @functools.wraps(func)
    def _wrapper(cm: Any, *args: Any, **kwargs: Any) -> Wrapped:
        """
        Check that the component is on and not faulty
        before calling the function.

        This is a wrapper function that implements the functionality of
        the decorator.

        :param cm: the component manager
        :param args: positional arguments to the wrapped function
        :param kwargs: keyword arguments to the wrapped function

        :raises ComponentError: when not powered on

        :return: whatever the wrapped function returns
        """
        if cm.op_state_model.op_state != DevState.ON:
            raise ComponentError("Component is not ON")
        return func(cm, *args, **kwargs)

    return cast(Wrapped, _wrapper)


def kwarged(func):
    """Method to allow passing multiple args to partial function"""

    @wraps(func)
    def wrapper(**kwargs):
        return func(**kwargs)

    return wrapper
