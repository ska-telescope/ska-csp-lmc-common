"""Generate gate values for a vector of data for test-monitoring package
   From SP-1518
   This list is not exhaustive nor a must have list.

   1- mean and st dev
"""

#
# 2023 C.Baffa  - Licence GPL3
#

import logging
import statistics as stat

module_logger = logging.getLogger(__name__)


def meanstd_data(data: list):
    """
    Compute mean and standard deviation of input data list.

    :param data: imput data list from which mean and standard
    deviation are calculated

    :return: (mean_data, mean_stdev) return the calculation results

    :raise: a ValueError exception if the inputis an empty list or does
    not contains only float and/or int elements.
    :raise: StatisticsError exception if the input list has <2 elements.
    """
    mean_data = 0.0
    mean_stdev = 0.0
    if data and all(isinstance(item, (int, float)) for item in data):
        num_values = len(data)
        mean_data = stat.mean(data)
        mean_stdev = stat.stdev(data)
        module_logger.debug(
            f"meanstd_data: {num_values}, "
            f"{round(mean_data, 5)}, "
            f"{round(mean_stdev, 5)}"
        )
    else:
        raise ValueError(
            "the input data list does not contains only"
            "int or float values or it is empty: {data}"
        )

    return (round(mean_data, 5), round(mean_stdev, 5))
