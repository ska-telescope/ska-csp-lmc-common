import inspect
from typing import Dict, Tuple

import tango
from ska_tango_base.commands import ResultCode


def method_name():
    """
    :return: name of caller method
    """
    return inspect.currentframe().f_back.f_code.co_name


def list_of_subsystem_fqdns(*args):
    """Build a list with all the CSP sub-systems fqdn retrieved fron the
    Device property.

    .. note::

        When a device property contains list of strings the
        returned value is of tango.StdStringVector type.
        Example:
        CspCbf: 'low-cbf/control/0'
        PstBeams: ['low-pst/beam/01, 'low-pst/beam/02]
        The property PstBeams is of type tango.StdStringVector.

    :param args: a list of CSP device properties defining the
        CSP sub-systems devices FQDNS.
    :type args: list

    :return: a list with all the FQDNs
    """
    output_list = []
    for _args in args:
        if _args:
            # pylint: disable-next=consider-merging-isinstance
            if isinstance(_args, tango.StdStringVector) or isinstance(
                _args, list
            ):
                output_list += _args
            elif isinstance(_args, str):
                output_list.append(_args)
            else:
                continue
    return output_list


def result_cmd_code(result_code, cmd_name):
    """Return the proper message depending on the command resul code returned.

    :param result_code: base classes command result code
    :type result_code: tuple: (code, message)
    :param cmd_name: the command name
    :type cmd_name: string
    :return: the message
    :rtype: string
    """
    label = (ResultCode(result_code)).name
    return (result_code, f"CSP Controller: {cmd_name} execution is {label}")


def read_release_version(release):
    """
    Read release file to get version id and build state.

    :param release: the release file of the project
    :return: the tuple (version_id, build_state)
    """
    build_state = (
        f"{release.name}, {release.version}, " + f"{release.description}"
    )
    version_id = release.version
    return version_id, build_state


def convert_list_to_dict(input_list):
    """
    Return a dictionary from a list.
    The key-value is the i entry, the value the i+1
    """

    result_dict = dict(
        map(
            lambda i: (input_list[i], input_list[i + 1]),
            range(len(input_list) - 1)[::2],
        )
    )
    return result_dict


def get_interface_version(input_dict: Dict) -> Tuple[int]:
    """Return the uri interface version as a tuple (major, minor)"""
    (major_version, minor_version) = (0, 0)
    if "interface" in input_dict:
        version = input_dict["interface"]
        version_num = version.rsplit("/", 1)[1]
        (major_version, minor_version) = version_num.split(".")
    return int(major_version), int(minor_version)
