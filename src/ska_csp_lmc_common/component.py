from __future__ import annotations  # allow forward references in type hints

import logging
import traceback
from typing import Any, Callable, Dict, List, Optional

# Tango imports
import tango

# SKA imports
from ska_control_model import AdminMode, HealthState
from tango import (
    AttrQuality,
    Database,
    DevFailed,
    DevState,
    EventSystemFailed,
    EventType,
)

# local imports
from ska_csp_lmc_common.connector import Connector

module_logger = logging.getLogger(__name__)

# pylint: disable=too-many-instance-attributes
# pylint: disable=protected-access
# pylint: disable=raise-missing-from
# pylint: disable=invalid-name
# pylint: disable=inconsistent-return-statements
# pylint: disable=broad-except


# pylint: disable-next=missing-class-docstring
class CspEvent:
    def __init__(self, component, attr_name=None):
        self.attr_name = attr_name
        self.component = component
        self.quality = AttrQuality.ATTR_INVALID
        self.value = None
        self.time = 0
        self.type = EventType.CHANGE_EVENT


class Component:
    """Interface class to a sub-system device."""

    def __init__(
        self: Component, fqdn: str, name: str, weight: int = 0, logger=None
    ) -> None:
        """Initialize the component instance.

        :param fqdn: the sub-system FQDN
        :param name: the component name (for ex. 'cbf-ctrl', 'pss-ctrl',
            'pst-beam-1', etc)
        :param name: string
        :param weight: the sub-system 'weight'. CBF sub-system has an
            higher impact on the CSP.LMC functionalities.
        :param logger: a logger for this instance
        """
        self.name = name
        self._fqdn = fqdn
        self._connector = None
        self.weight = weight
        self._event_id = {}
        self._db_not_defined_cnt = 0
        self._evt_mgr_callback = {}
        self._attributes = {}
        self._polling_not_started: Dict[str, int] = {}
        self._db_not_defined_cnt = 0
        self.logger = logger or module_logger
        self.attrs_for_change_events: list[str] = []
        self.attrs_for_periodic_events: Dict[str, bool] = {}
        self.set_component_offline(AdminMode.OFFLINE)
        # attributes name that are different from CSP device
        # and sub-component. They need a map to perform subscription
        # enable when CBF add push event to those attribute
        # [csp_attr_name, sub-component_attr_name]
        # self.attr_name_exception = {
        #     "sourceDishVccConfig": "sourceSysParam",
        #     "dishVccConfig": "sysParam",
        # }

    # define __hash__(), __key()__ and __eq__() methods
    # to use component object as a key in a python dictionary
    def __key(self: Component):
        """Define the __key() method for the Component class to use this object
        as a key in a python dictionary."""
        return (self._fqdn, self.name, self.weight)

    def __hash__(self: Component):
        """Define the __hash__() method for the Component class to use this
        object as a key in a python dictionary."""
        return hash(self.__key())

    def __eq__(self: Component, other: Component):
        """Define the __eq__() method for the Component class to use this
        object as a key in a python dictionary."""
        return (
            self.__class__ == other.__class__ and self.__key() == other.__key()
        )

    @property
    def event_id(
        self: Component,
    ) -> List[int]:
        """Return the list of registered events.

        :return: A list with the ID of the events subscribed on the component.
        """
        return list(self._event_id.values())

    @property
    def event_attrs(self: Component) -> List[str]:
        """Return the list of attributes subscribed for events.

        :return: A list with the attributes subscribed on the component.
        """
        return list(self._event_id.keys())

    @property
    def fqdn(self: Component):
        """Return the FQDN of the sub-system associated to the current
        component."""
        return self._fqdn

    @property
    def proxy(self: Component):
        """Return the DeviceProxy with the CSP sub-system TANGO device if this
        is reachable, otherwise None."""
        return self._connector.deviceproxy if self._connector else None

    @property
    def state(self: Component):
        """Return the sub-system state.

        :return: the sub-system State if updated via events or via direct read,
            UNKNOWN on failure
        """
        return self._get_attribute("state")

    @property
    def health_state(self: Component):
        """Return the sub-system health_state.

        :return: the sub-system healthState if updated via events or via direct
            read, UNKNOWN on failure
        """
        return self._get_attribute("healthstate")

    @property
    def admin_mode(self: Component):
        """Return the sub-system admin_mode.

        :return: the sub-system adminmode if updated via events or via direct
            read, UNKNOWN on failure
        """
        return self._get_attribute("adminmode")

    @admin_mode.setter
    def admin_mode(self: Component, value: AdminMode) -> None:
        try:
            self.logger.debug(f"Writing admin mode to {value}")
            self.write_attr("adminmode", value)
            self.logger.debug(f"Written admin={value} on {self.name}")
        except ValueError as e:
            self.logger.error(e)

    @property
    def long_running_commands_in_queue(self: Component):
        """Return the sub-system longRunningCommandsInQueue.

        :return: the sub-system longRunningCommandsInQueue if updated via
            events or via direct read, UNKNOWN on failure
        """
        return self._get_attribute("longRunningCommandsInQueue")

    @property
    def long_running_command_status(self: Component):
        """Return the sub-system longRunningCommandStatus.

        :return: the sub-system longRunningCommandStatus if updated via
            events or via direct read, UNKNOWN on failure
        """
        return self._get_attribute("longRunningCommandStatus")

    ##########################
    # Private Methods
    ##########################

    def _get_attribute(self: Component, attr_name: str) -> Any:
        """Return the value of the required attribute. If the attribute is not
        initialized, its value is retrieved via direct read on the sub-
        system.name.

        :param attr_name: the name of the attribute

        :return: the attribute value on success, None otherwise
        """

        attr_name = attr_name.lower()
        if attr_name not in self._attributes:
            self.logger.debug(f"{attr_name} not initialized. Going to read it")
            try:
                self._attributes[attr_name] = None
                self._attributes[attr_name] = self.read_attr(attr_name)
            except ValueError as err_msg:
                self.logger.warning(err_msg)

        return self._attributes[attr_name]

    def _update_component_info(
        self: Component, recv_evt: tango.EventData, *new_evt: CspEvent
    ) -> bool:
        """Method to update the sub-system component manager internal status
        when an event generated by the sub-system TANGO device is caught.

        :param recv_evt: the event generated by the CSP sub-system TANGO device
        :param new_evt: the eve/nt forwarded back to the CSP TANGO device.
        :return: True to forward the event value back to the CSP device.
        """

        fwd_event = new_evt[0]
        # get the device name
        # dev_name = recv_evt.device.dev_name()
        fwd_event.value = recv_evt.attr_value.value
        # the attribute name is already stored into fwd_event
        fwd_event.quality = AttrQuality.ATTR_VALID
        # self.setattr(fwd_event.attr_name, fwd_event.value)
        self._attributes[fwd_event.attr_name] = fwd_event.value
        # self.logger.info(
        #    f"Received event on {dev_name}/{fwd_event.attr_name} value:"
        #    f" {fwd_event.value}"
        # )
        return True

    def _handle_event_errors(self: Component, recv_event, fwd_event: CspEvent):
        """Method to handle the error conditions on received events. Events
        with errors are not always propagated back to the main CSP device.

        :param recv_event: the received event
        :return: True if the received event is forwarded back to the CSP
            device, otherwise False
        """

        # Set forward_event_back = False to not forward events back to CSP
        # device
        forward_event_back = False
        dev_name = recv_event.device.dev_name()
        attr_name = fwd_event.attr_name
        for item in recv_event.errors:
            # analyse the error:
            if item.reason in ["API_EventTimeout", "API_CantConnectToDevice"]:
                # ping the device to get info about the running state of
                # the device
                # the device is registered into the DB but is not yet started
                # ...
                try:
                    # ping the device to detect if it's running
                    recv_event.device.ping()
                    # in this case the TANGO framework accesses in reading the
                    # attribute -> No need to do here (TO VERIFY)
                    # device_attr = recv_event.device.read_attribute(attr_name)
                    # fwd_event.attr_value = device_attr.value
                except tango.DevFailed as tango_error:
                    self.logger.error(
                        f"Error in accessing {attr_name}:"
                        f" {tango_error.args[0].reason}"
                    )
                    for _item in tango_error.args:
                        if _item.reason == "API_CantConnectToDevice":
                            # the device is no more running -> set the
                            # attribute to unknown attribute
                            forward_event_back = True
                            admin_value = self._attributes["adminmode"]
                            self.set_component_unknown(admin_value)
                            fwd_event.value = self._attributes[attr_name]
                            fwd_event.quality = AttrQuality.ATTR_VALID
            elif item.reason in [
                "API_AttributePollingNotStarted",
                "API_AttrNotFound",
                "API_CommandFailed",
                "API_DSFailedRegisteringEvent",
            ]:
                # API_AttributePollingNotStarted
                # Cause: the attribute to subscribe is not configured for
                # polling neither to push events
                # API_AttrNotFound: the attribute does not exist on the
                # subscribed device (and subscribe_event has been invoked with
                # stateless = True)
                # With these errors also API_CommandFailed and
                # API_DSFailedRegisteringEvent are received.
                # These errors arrives every 10 secs (keep-alive thread): here
                # we try to filter out the messages to reduce the logs.
                sec = recv_event.reception_date.tv_sec
                try:
                    _ = self._polling_not_started[recv_event.attr_name]
                    if (
                        sec - self._polling_not_started[recv_event.attr_name]
                        >= 30
                    ):
                        self._polling_not_started[recv_event.attr_name] = sec
                        self.logger.warning(f"{dev_name}: {item.desc}")
                except KeyError:
                    self.logger.warning(f"{dev_name}: {item.desc}")
                    self._polling_not_started[recv_event.attr_name] = sec
            elif item.reason == "API_PollThreadOutOfSync":
                try:
                    self.logger.warning(
                        f"Received API_PollThreadOutOfSync on "
                        f"{dev_name}/{attr_name}."
                    )
                    # force the reading of the attribute value
                    # attr_value = self.read_attr(attr_name)
                    # fwd_event.value = attr_value
                    # forward_event_back = True
                # pylint: disable-next=broad-except
                except Exception as e:
                    self.logger.error(
                        f"Still error in accessing {attr_name}"
                        f" on {dev_name}: {e}"
                    )
            else:
                log_msg = (
                    f"{item.reason}: on attribute {str(recv_event.attr_name)}"
                )
                self.logger.warning(log_msg)
        return forward_event_back

    def _push_event(
        self: Component, recv_event: tango.EventData, device_attr_name=None
    ) -> None:
        """Callback function invoked when an event is received. The method
        checks for errors: when a *loss of connection* is detected, the value
        of the attribute is set to UNKNOWN, if the attribute support this
        value, otherwise to None (with quality factor set to INVALID). In the
        first case the attribute is updated inside the component class, too.
        After all checks, the method invokes the callback register at
        subscription, if any, passing as argument an instance of the CspEvent
        class with the new value.

        :param recv_event: The received event data class
        :param device_attr_name: attribute name specified by the component
            class. It help to manage the attribute name mismatch between
            component device and CSP device

        :return: None
        """

        # pylint: disable-next=fixme
        # TODO: take into consideration the possibility to add the logic to
        # handle events different from change/periodic/archive
        update_event = False
        # the event forwarded to the CSP TANGO device
        fwd_event = CspEvent(self)
        if not device_attr_name:
            # get the attribute name
            token_position = recv_event.attr_name.rfind("/")
            # !!!!
            # NOTE: need to save the position of attr_name to avoid
            # conflicts between python-format and python-lint
            # !!!!
            first_char_position = token_position + 1
            attr_name = recv_event.attr_name[first_char_position:]
        else:
            # the previous step are performed by specific component
            # (i.e mid CBF)
            attr_name = device_attr_name
        # store attribute name, reception time and event type
        fwd_event.attr_name = attr_name.lower()
        fwd_event.time = recv_event.reception_date
        fwd_event.type = recv_event.event
        # check for any errors
        if not recv_event.err:
            update_event = self._update_component_info(recv_event, fwd_event)
        else:
            update_event = self._handle_event_errors(recv_event, fwd_event)
        if update_event and self._evt_mgr_callback[attr_name]:
            if "longrunningcommand" in attr_name:
                # temporary patch tp not proagate LRC attrs
                # after EventManager update we can remove this
                return
            return self._evt_mgr_callback[attr_name](fwd_event)
        return

    ########################
    # Call public methods
    ########################

    def set_component_disconnected(self: Component):
        """This method is called when the CSP TANGO Device adminMode is set to
        OFFLINE.

        In this case the CSP Device componentManager does no longer monitor the
        component and its information are reported as unknown. The component
        admin mode is left unchanged.
        """

        self._connector = None
        self._attributes["healthstate"] = HealthState.UNKNOWN
        self._attributes["state"] = DevState.UNKNOWN
        # Do we need to adminmode OFFLINE???
        # self._attributes["adminmode"] = AdminMode.OFFLINE

    def set_component_unknown(
        self: Component, admin_mode_value: AdminMode
    ) -> None:
        """This method is called when the component experiences a loss of
        connection. In this context, this method sets the State and healthState
        attribute to UNKNOWN. For the other attributes, the value is set to the
        default value None and quality_factor to ATTR_INVALID.

        :param args: an instance of the CspEvent class with the
                     new values.
        :return: None
        """
        self._attributes["healthstate"] = HealthState.UNKNOWN
        self._attributes["state"] = DevState.UNKNOWN
        self._attributes["adminmode"] = admin_mode_value

    def set_component_offline(
        self: Component, admin_mode_value: AdminMode
    ) -> None:
        """This method is called when the received event is related to a device
        not registered into the DB or its admin mode is OFFLINE/NOT-FITTED In
        this context, this method sets the State or healthState attribute to
        UNKNOWN. For the other attributes, the value is set to the default
        value None and quality_factor to ATTR_INVALID.

        :param admin_mode_value: the value of the CSP sub-system device
            adminMode.

        :return: None
        """
        self._attributes["healthstate"] = HealthState.UNKNOWN
        self._attributes["state"] = DevState.DISABLE
        self._attributes["adminmode"] = admin_mode_value

    def connect(self: Component) -> Connector | None:
        """Establish the connection with a sub-system device. Connection
        retries happen with a interval configured via the device property
        PingConnectionTime. If the subordinate device is not registered into
        the TANGO DB, the CSP Controller/Subarray device tries connection up to
        3 times before throwing an exception and considering the sub-system not
        on-line (available). This approach is related to the deployment
        procedure: each sub-system configures the TANGO DB independently,
        through a configurator process. It may happen that the CSP
        Controller/Subarray is already running while the configurator of one or
        more sub-systems is still writing the TANGO DB. In this case, the CSP
        would not be able to detect the sub-system because the DB is fully
        configured. Retry operations provide more time to wait for the end of
        the TANGO DB configuration.

        :return: The Connector on success, otherwise None

        :raise: a DevFailed exception on connection failure.
        """
        self.logger.info(f"Connecting to {self.fqdn}")
        try:
            proxy = Connector(self.fqdn, logger=self.logger)
            self.logger.debug(f"Connector proxy: {proxy}")
            proxy.is_alive()
            self._connector = proxy
            # self.update_attributes()
        except DevFailed as df:
            self.logger.warning(f"Failed to connect to {self.fqdn}")
            if df.args[0].reason == "API_DeviceNotExported":
                # The device is defined into the DB but is not running.
                # Retry the adminMode value from the DB before retrying the
                # connection.
                self.logger.debug(
                    f"{self.fqdn} is defined in DB but is not running, "
                    "retrying..."
                )
                db = Database()
                try:
                    attr_properties = db.get_device_attribute_property(
                        self.fqdn, {"adminMode": ["__value"]}
                    )
                    self.logger.debug(f"attr_properties: {attr_properties}")
                    admin_memorized = int(
                        attr_properties["adminMode"]["__value"][0]
                    )
                    if admin_memorized in [
                        AdminMode.ONLINE,
                        AdminMode.ENGINEERING,
                        AdminMode.OFFLINE,
                    ]:
                        self.logger.warning(
                            f"Device {self.fqdn} is not running: adminMode is"
                            f" {AdminMode(admin_memorized).name}"
                        )
                        if admin_memorized == AdminMode.OFFLINE:
                            self.set_component_offline(admin_memorized)
                        else:
                            self.set_component_unknown(admin_memorized)
                    else:
                        self.set_component_offline(admin_memorized)
                        self.logger.error(
                            f"Failure connecting to {self.fqdn}: adminMode is "
                            f"{AdminMode(admin_memorized).name}"
                        )
                        tango.Except.re_throw_exception(
                            df,
                            "Device is disabled",
                            f"Device {self.fqdn} has adminMode"
                            f" {AdminMode(admin_memorized).name}",
                            "connect()",
                            tango.ErrSeverity.ERR,
                        )
                except KeyError as key_error:
                    self.logger.debug(f"No key {key_error} found")
            elif df.args[0].reason == "DB_DeviceNotDefined":
                # throw the exception only after 3 attempts. It may happen
                # that the configurator of one or more sub-ordinate components
                # has not yet finished but the CSP device is already running.
                # In this case CSP device fails to connect
                self._db_not_defined_cnt += 1
                if self._db_not_defined_cnt % 3 == 0:
                    self.logger.warning(
                        f"Device {self.fqdn} not defined into the DB"
                        f" ({self._db_not_defined_cnt})"
                    )
                if self._db_not_defined_cnt > 3:
                    self._db_not_defined_cnt = 0
        return self._connector

    def disconnect(self: Component) -> None:
        """Invalidate the connection with the CSP Sub-subsystem and report
        the main SKA SCM attributes accordingly to the expected values."""
        if self._event_id:
            self.logger.debug(
                f"Unsubscribing attributes on component {self.name}"
            )
            self.unsubscribe_attribute()
        self.set_component_disconnected()
        self.logger.debug(
            f"connector {self._connector} event_id: {self._event_id}"
        )

    def read_attr(self: Component, attribute: str) -> Any:
        """Return the value of the requested attribute.

        :param attribute: the attribute name

        :return: the attribute value, if the attribute does exist on
            the sub-system device

        :raise: a ValueError exception if the attribute does not exist or
            read failure.
        """
        try:
            attribute = attribute.lower()
            value = self._connector.get_attribute(attribute).value
            # if the read value is not equal to the stored one,
            # then the updated of the attribute on the device can be
            # forced:
            # - building a CspEvent for this attribute
            # - pushing it via push_event
            # A monitoring process can be implemented to do this
            # periodically.
            self._attributes[attribute] = value
            return value
        except Exception as e:
            # pylint: disable-next=fixme
            # TODO: raise correct exception
            raise ValueError(f"Failure in reading {attribute}!: {e}")

    def write_attr(self: Component, attribute: str, value: Any) -> None:
        """Set the value of the requested attribute.

        :param attribute: the attribute name
        :param value: the value to set

        :raise: a ValueError exception if the attribute does not exist
        """
        # pylint: disable-next=fixme
        # TODO: add exception failure
        try:
            self._connector.set_attribute(attribute, value)

        except Exception as e:
            # pylint: disable-next=fixme
            # TODO: raise correct exception
            raise ValueError(f"Failure in writing {attribute}!: {e}")

    def force_attribute_update(self: Component, attr_name: str) -> None:
        """
        Update the attribute via a direct read. it also invokes the _push_event
        method to the EventManager internal attribute value.

        :param attr_name: the name of the attribute forced to read.

        """
        try:
            dev_attr = self._connector.get_attribute(attr_name)
            value = dev_attr.value
            if value != self._attributes[attr_name]:
                # the new value is stored via the call to _push_event
                # build and fill the tango event data
                evt = tango.EventData()
                evt.attr_name = f"{self.fqdn}:/{attr_name}"
                evt.event = "change"
                evt.attr_value = dev_attr
                evt.reception_date = dev_attr.time
                self._push_event(evt)
        except Exception as e:
            self.logger.error(
                f"Cannot force update of {attr_name}. Error: {e}"
            )

    def run(
        self: Component,
        command_name: str,
        async_flag: bool = True,
        argument: Any = None,
        callback: Optional[Callable] = None,
    ) -> Any:
        """Execute a command on the target device.

        :param command_name: the command name
        :param async_flag: set the execution model (async/sync)
        :param argument: the command argument, if any
        :param callback: callable called when the command ends on the
            target device, if any

        """
        if async_flag:
            return self._connector.send_command_async(
                command_name, argument, callback
            )
        return self._connector.send_command(command_name, argument)

    def read_timeout(self: Component, command_name: str) -> int:
        """Read the timeout configured for a command.

        :param command_name: the command name

        :return: the timeout configured (in secs) or 0 on failure
        """
        timeout_attribute = f"{command_name}durationexpected"
        try:
            return self.read_attr(timeout_attribute)
        # pylint: disable-next=broad-except
        except Exception:
            # self.logger.debug(e)
            self.logger.debug(
                f"Failed to read attribute: {timeout_attribute}."
                " Using default value."
            )
            return 0

    def subscribe_attribute(
        self: Component,
        attr_name: str,
        event_type: tango.EventType,
        evt_mgr_callback: Optional[Callable] = None,
    ) -> bool:
        """Subscribe to any event.

        :param attr_name: the attribute name
        :param event_type: the event type (CHANGE_EVENT, PERIODIC, etc..)
        :param evt_mgr_callback: the EventManager method called when the event
            is received.

        :return: True on subscription success, otherwise False.
        """

        ret_code = True
        ret_exception = None
        attr_name = attr_name.lower()
        if attr_name not in self._event_id:
            self._evt_mgr_callback[attr_name] = evt_mgr_callback
            try:
                event_id = self._connector.subscribe_attribute(
                    attr_name, event_type, self._push_event
                )
                self._event_id[attr_name] = event_id
            # pylint: disable-next=broad-except
            except EventSystemFailed as exc:
                self.logger.error("Subscribe attribute failure")
                self.logger.error(traceback.format_exc())
                ret_exception = exc
                ret_code = False

            except Exception as exc:
                self.logger.error("Subscribe attribute failure")
                self.logger.error(traceback.format_exc())
                ret_code = False
                ret_exception = exc
        return (ret_code, ret_exception)

    def subscribe_lrc_attribute(
        self: Component,
        attr_name: str,
        event_type: tango.EventType,
        callback: Optional[Callable] = None,
    ):
        """Subscribe LRC attributes to track the command execution.

        :param attr_name: the attribute name
        :param event_type: the event type (CHANGE_EVENT, PERIODIC, etc..)
        :param callback: the method invoked when event is received

        :return: True on subscription success, otherwise False.
        """
        ret_code = True
        ret_exception = None
        event_id = -1
        attr_name = attr_name.lower()
        if "longrunning" in attr_name:
            try:
                event_id = self._connector.subscribe_attribute(
                    attr_name, event_type, callback
                )
                self.logger.debug(f"subscribe event_id = {event_id}")
            # pylint: disable-next=broad-except
            except (EventSystemFailed, Exception) as exc:
                self.logger.error("Subscribe attribute failure")
                self.logger.error(traceback.format_exc())
                ret_exception = (
                    exc if isinstance(exc, EventSystemFailed) else None
                )
                ret_code = False
        return event_id, ret_code, ret_exception

    def unsubscribe_lrc_attribute(
        self: Component,
        event_id: int,
    ):
        """UnSubscribe LRC attributes to track the command execution.

        :param event_id: the id associated to the event.

        """
        exc = None
        try:
            self._connector.unsubscribe_attribute(event_id)
        # pylint: disable-next=broad-except
        except EventSystemFailed as exc:
            self.logger.warning(f"Unsubscribe({event_id}) failed with: {exc}")
        return exc

    def unsubscribe_attribute(
        self: Component,
        attr_list: List[str] = None,
    ) -> None:
        """Unsubscribe the event on the specified attributes. If the event_id
        dictionary is empty, the event_callback is un-registered.

        :param attr_list: the list of attributes to un-subscribe event on. If
            the list is empty, all the subscribed events on the sub-system are
            un-subscribed.

        :return: None (?) <=== CHECK
        """

        self.logger.debug(f"event_id:{self._event_id}")
        if not attr_list:
            attr_list = list(self._event_id.keys())
        self.logger.debug(
            f"attributes to unsubscribe:{attr_list} on component {self.fqdn}"
        )
        attrs_to_remove = []
        for attr_name in attr_list:
            try:
                _id = self._event_id[attr_name]
                self.logger.debug(
                    f"Going to unsubscribe attribute {attr_name} event id"
                    f" {_id}"
                )
                self._connector.unsubscribe_attribute(_id)
                # build the list of the attribute/id couple to remove from the
                # member _event_id dictionary
                attrs_to_remove.append(attr_name)
            # pylint: disable-next=broad-except
            except KeyError as e:
                self.logger.debug(
                    f"{self.fqdn}: Attribute {e} already unsubscribed."
                )
            except Exception as e:
                self.logger.warning(
                    f"Failed to unsubscribe attribute {attr_name}: {e}"
                )
        # remove the un-subscribed events from the dictionary
        self.logger.debug(f"Attributes to remove: {attrs_to_remove}")
        for attr_name in attrs_to_remove:
            self._event_id.pop(attr_name)
            self._evt_mgr_callback.pop(attr_name)
        if not self._event_id:
            self._evt_mgr_callback = {}
