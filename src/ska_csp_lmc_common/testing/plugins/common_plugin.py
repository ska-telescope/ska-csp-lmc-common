import logging

import pytest
from ska_control_model import HealthState, ObsState
from tango import DevState

from ska_csp_lmc_common.controller import (
    ControllerHealthModel,
    ControllerOpStateModel,
)
from ska_csp_lmc_common.subarray import (
    SubarrayHealthModel,
    SubarrayObsStateModel,
    SubarrayOpStateModel,
)

# pylint: disable=missing-function-docstring


module_logger = logging.getLogger(__name__)


def callback_called(value):
    return value


def callback_up_prop_called(name, value):
    return name, value


test_callbacks = {
    "update_state": callback_called,
    "update_health_state": callback_called,
    "update_obs_state": callback_called,
    "update_admin_mode": callback_called,
    "update_property": callback_up_prop_called,
}


@pytest.fixture(scope="session")
def ctrl_op_state_model():
    yield ControllerOpStateModel(
        DevState.INIT,
        test_callbacks["update_state"],
        module_logger,
    )


@pytest.fixture(scope="session")
def ctrl_health_state_model():
    yield ControllerHealthModel(
        HealthState.UNKNOWN,
        test_callbacks["update_health_state"],
        module_logger,
    )


@pytest.fixture(scope="session")
def subarray_op_state_model():
    yield SubarrayOpStateModel(
        DevState.INIT, test_callbacks["update_state"], module_logger
    )


@pytest.fixture(scope="session")
def subarray_health_state_model():
    yield SubarrayHealthModel(
        HealthState.UNKNOWN,
        test_callbacks["update_health_state"],
        module_logger,
    )


@pytest.fixture(scope="session")
def subarray_obs_state_model():
    yield SubarrayObsStateModel(
        ObsState.EMPTY, test_callbacks["update_obs_state"], module_logger
    )
