"""This module contains pytest fixtures and other test setups for the
ska.csp_lmc unit tests."""

import json
import logging
from functools import partial

import mock
import pytest
from tango.test_context import DeviceTestContext

# pylint: disable=unused-argument
# pylint: disable=redefined-outer-name

module_logger = logging.getLogger(__name__)


def pytest_addoption(parser):
    """Pytest hook; implemented to add the `--true-context` option, used to
    indicate that a true Tango subsystem is available, so there is no need for
    a MultiDeviceTestContext.

    :param parser: the command line options parser
    :type parser: an argparse parser
    """
    parser.addoption(
        "--true-context",
        action="store_true",
        help=(
            "Tell pytest that you have a true Tango context and don't "
            "need to spin up a Tango test context"
        ),
    )


# pylint: disable-next=missing-function-docstring
def pytest_generate_tests(metafunc):
    if "devices_list" in metafunc.fixturenames:
        metafunc.parametrize("devices_list", metafunc.cls.devices_list)


def pytest_itemcollected(item):
    """pytest hook implementation; add the "forked" custom mark to all tests
    that use the `device_context` fixture, causing them to be sandboxed in
    their own process.

    param item: the collected test for which this hook is called type item: a
    collected test
    """
    if "device_under_test" in item.fixturenames:
        item.add_marker("forked")


def pytest_configure(config):
    """pytest hook, used here to register custom marks to get rid of spurious
    warnings."""
    config.addinivalue_line(
        "markers",
        "mock_device_proxy: the test requires tango.DeviceProxy to be mocked",
    )


def mock_device_properties(device_info):
    """Method to mock the device properties from the TANGO DB."""
    properties = device_info["properties"]
    _properties = {}
    for prop_name, property_value in properties.items():
        if "polled_attr" in prop_name:
            continue
        for item in property_value:
            if len(property_value) > 1:
                if prop_name not in _properties:
                    _properties[prop_name] = []
                _properties[prop_name].append(item)
            else:
                _properties[prop_name] = item
    return _properties


@pytest.fixture(scope="class")
def device_under_test(request, device_info):
    """Creates and returns a DeviceProxy under a DeviceTestContext.

    :param device_info: Information about the device under test that is
        needed to stand the device up in a DeviceTestContext, such as
        the device class and properties
    :type device_info: dict
    """
    memorized_attr = {}
    if "attribute_properties" in device_info:
        for attr_name in device_info["attribute_properties"]:
            try:
                memorized_list = device_info["attribute_properties"][
                    attr_name
                ]["__value"]
                if memorized_list[0]:
                    memorized_attr[attr_name] = memorized_list[0]
            except KeyError:
                pass
    module_logger.debug(f"memorized attributes: {memorized_attr}")

    with mock.patch(
        "ska_csp_lmc_common"
        + ".manager.manager_configuration"
        + ".ComponentManagerConfiguration"
        + ".get_device_properties"
    ) as mock_prop:
        mock_prop.side_effect = partial(mock_device_properties, device_info)
        with DeviceTestContext(
            device_info["class"],
            properties=device_info["properties"],
            memorized=memorized_attr,
            process=False,
        ) as device_under_test:
            yield device_under_test


def _load_data_from_json(path):
    """Loads a dataset from a named json file.

    :param path: path to the JSON file from which the dataset is to be
        loaded.
    :type name: string
    """
    with open(path, "r", encoding="UTF-8") as json_file:
        return json.load(json_file)


def _load_devices(path, device_names):
    """Loads device configuration data for specified devices from a specified
    JSON configuration file.

    :param path: path to the JSON configuration file
    :type path: string
    :param device_names: names of the devices for which configuration
        data should be loaded
    :type device_names: list of string
    """
    configuration = _load_data_from_json(path)
    devices_by_class = {}

    servers = configuration["servers"]
    for server in servers:
        module_logger.info(f"server:{server}")
        for device_name in servers[server]:
            if device_name in device_names:
                for class_name, device_info in servers[server][
                    device_name
                ].items():
                    if class_name not in devices_by_class:
                        devices_by_class[class_name] = []
                    for fqdn, device_specs in device_info.items():
                        devices_by_class[class_name].append(
                            {"name": fqdn, **device_specs}
                        )

    devices_info = []
    # pylint: disable-next=consider-using-dict-items
    for device_class in devices_by_class:
        device_info = []
        for device in devices_by_class[device_class]:
            device_info.append(device)

        devices_info.append({"class": device_class, "devices": device_info})

    module_logger.info(f"devices_info:{devices_info}")
    return devices_info


@pytest.fixture(scope="module")
def devices_to_load(request):
    """Fixture that returns the "devices_to_load" variable from the module
    under test. This variable is a dictionary containing three entries:

    * "path": the path to a JSON file containing device configuration
      information in dsconfig format
    * "package": the package from which classes will be loaded; for
      example, if the package is "ska.low.mccs", then if the JSON
      configuration file refers to a class named "MccsMaster", then this
      will be interpretated as the ska.low.mccs.MccsMaster class
    * "devices": a list of names of the devices that are to be loaded.
    """
    return getattr(request.module, "devices_to_load")


@pytest.fixture(scope="module")
def device_to_load(request):
    """Fixture that returns the "device_to_load" variable from the module under
    test. This variable is a dictionary containing three entries:

    * "path": the path to a JSON file containing device configuration
      information in dsconfig format
    * "package": the package from which classes will be loaded; for
      example, if the package is "ska.low.mccs", then if the JSON
      configuration file refers to a class named "MccsMaster", then this
      will be interpretated as the ska.low.mccs.MccsMaster class
    * "device": the name of the devices that is to be loaded.
    """
    return getattr(request.module, "device_to_load", None)


@pytest.fixture(scope="module")
def device_info(device_to_load):
    """Constructs a device_info dictionary in the form required by
    tango.test_context.DeviceTestContext, with the device as specified by the
    device_to_load fixture.

    :param device_to_load: fixture that provides a specification of the
        device that is to be included in the devices_info dictionary
    :type device_to_load: specification of devices to be loaded
    :type device_to_load: dictionary
    """
    devices = _load_devices(
        path=device_to_load["path"], device_names=[device_to_load["device"]]
    )
    module_logger.debug(f"devices:{devices}")
    module_logger.debug(f"devices[0]:{devices[0]}")
    class_to_import = devices[0]["class"]
    module_logger.info(f"class to import:{class_to_import}")
    package = __import__(device_to_load["package"], fromlist=class_to_import)
    module_logger.info(f"package:{package}")

    return {
        "class": getattr(package, devices[0]["class"]),
        "properties": devices[0]["devices"][0]["properties"],
        "attribute_properties": devices[0]["devices"][0][
            "attribute_properties"
        ],
    }


@pytest.fixture(scope="module")
def devices_info(devices_to_load):
    """Constructs a devices_info dictionary in the form required by
    tango.test_context.MultiDeviceTestContext, with devices as specified by the
    devices_to_load fixture.

    :param devices_to_load: fixture that provides a specification of the
        devices that are to be included in the devices_info dictionary
    :type devices_to_load: specification of devices to be loaded
    :type devices_to_load: dictionary
    """
    devices = _load_devices(
        path=devices_to_load["path"], device_names=devices_to_load["devices"]
    )

    classes_to_import = list(set(device["class"] for device in devices))
    package = __import__(
        devices_to_load["package"], fromlist=classes_to_import
    )

    for device in devices:
        device["class"] = getattr(package, device["class"])

    return devices
