import ast
import json
import logging
import time
from collections import defaultdict
from queue import Queue

import pytest
import tango
from pytest_bdd import given, parsers, then, when
from ska_control_model import (
    AdminMode,
    HealthState,
    LoggingLevel,
    ObsMode,
    ObsState,
    SimulationMode,
)
from tango import DevState

from ska_csp_lmc_common.testing.poller import probe_poller

# pylint: disable=too-many-arguments
# pylint: disable=too-many-branches
# pylint: disable=protected-access
# pylint: disable=global-statement
# pylint: disable=logging-fstring-interpolation
# pylint: disable=invalid-name
# pylint: disable=eval-used
# pylint: disable=missing-function-docstring
# pylint: disable=broad-except
# pylint: disable=too-many-instance-attributes

module_logger = logging.getLogger(__name__)

temp_dev_info = None


class TestDevice:
    """A class responsible for creation of device proxies and step definitions
    for bdd tests."""

    def __init__(
        self,
        name: str = None,
        repository: str = None,
        test_context: str = None,
    ) -> None:

        self.name = name
        self.adm_name = None
        self.repository = repository
        self.test_context = test_context
        self.temp_dev_info = None
        self.cmd_queue = Queue(20)
        self.archive_count = defaultdict(lambda: 0)
        self.proxy = self.get_device_proxy()

    def get_device_proxy(self):
        idx = ""
        klass = self.name
        if klass[0:3] in ["Vcc"]:
            idx = klass[-3:]
            klass = klass[:-3]
        elif klass[-2:] in ["01", "02", "03", "04"]:
            idx = klass[-2:]
            klass = klass[:-2]

        if "Capability" not in klass:
            if self.test_context == "real" and "Pst" in klass:
                klass = klass[3:]
        database = tango.Database()
        # Get list of exported device for a class.
        instance_list = database.get_device_exported_for_class(klass)
        for instance in instance_list.value_string:
            try:
                if idx in instance:
                    device = tango.DeviceProxy(instance)
                    class_name = klass
                    if klass[0:3] in ["Sim", "Mid", "Low"]:
                        class_name = klass[3:]
                    elif klass[-9:] == "Simulator":
                        class_name = klass[:-9]
                    if klass[-15:] == "SimulatorDevice":
                        class_name = klass[:-15]
                    self.name = class_name + idx
                    self.adm_name = device.adm_name()
                    return device
            except tango.DevFailed as df:
                module_logger.info(
                    f"error: {df.args[0].desc}- {df.args[0].origin}"
                )
                continue
        raise Exception(f"Could not find {self.name}")

    def initialize_device(self):
        dev_name = self.proxy.name()
        module_logger.info(f"Reinitialize {dev_name} at :{time.asctime()}")
        retry_count = 0
        ret = False
        while retry_count < 5:
            try:
                self.proxy.init()
                module_logger.info(
                    f"Device {self.name} restarted at :{time.asctime()}"
                    f" retry: {retry_count}"
                )
                ret = True
                break
            except Exception as e:
                module_logger.warning(
                    f" initialize_device: retry: {retry_count}"
                )
                retry_count += 1
                if retry_count == 5:
                    module_logger.error(
                        f"initialize_device: got exception {e} "
                    )

                time.sleep(1.5)
        return ret

    def ping_device(self):
        timeout = 10
        total = 0
        step = 1
        while True:
            try:
                self.proxy.ping()
                # dev_name = self.proxy.name()
                self.proxy.state()
                if (
                    "sim" not in self.proxy.name()
                    and "pst" not in self.proxy.name()
                ):
                    self.proxy.logginglevel = LoggingLevel.DEBUG
                module_logger.info(
                    f"{self.name} available at {time.asctime()} -> {total}"
                )
                break
            # pylint: disable-next=broad-except
            except Exception as e:
                if total > timeout:
                    module_logger.error(f"ping_device: got exception {e}")
                    assert (
                        False
                    ), f"Initialziation of device {self.name} failed"
                time.sleep(step)
                total += step
                continue

    def restart_device_server(self):
        module_logger.warning(
            f"Restarting device {self.name} via admin device {self.adm_name}"
        )
        device_adm = tango.DeviceProxy(self.adm_name)
        # Kill the device server, the k8s command retries to restart it up to
        # 60 times
        device_adm.kill()
        # Check that restart has finished
        timeout = 60
        total = 0
        step = 1
        while True:
            try:
                self.proxy.ping()
                break
            # pylint: disable-next=broad-except
            except Exception:
                if total > timeout:
                    assert False, f"Initialziation of {self.name} failed"
                time.sleep(step)
                total += step
                continue
        # Even though we wait for the device to be pingable again, it doesn't
        # seem to show up in TangoDB, strange... 60 seconds were empirically
        # determined
        time.sleep(60)

    def send_command(self, command_name):
        """Send command without command argument."""
        command = getattr(self.proxy, command_name)
        self._send(command, command_name=command_name)

    def send_command_with_argument(self, command_name, argument):
        """Send command that need command argument."""
        conversion_needed = False
        command = getattr(self.proxy, command_name)

        if "[" in argument:
            conversion_needed = True
        self._send(
            command,
            argument,
            command_name=command_name,
            is_conversion_needed=conversion_needed,
        )

    def send_command_with_argument_from_file(self, command_name, filename):
        """Send command that need command argument read from a file."""
        command = getattr(self.proxy, command_name)
        with open(f"tests/test_data/{filename}", encoding="UTF-8") as f:
            argument = f.read().replace("\n", "")

        self._send(command, argument, command_name=command_name)

    def _send(
        self,
        command,
        argument=None,
        command_name=None,
        is_conversion_needed=False,
    ):
        """Get command Id from sent command and store it in a queue.

        :param argument: string argument of the command
        :param command_name: string command name
        :param is_conversion_needed: flag needed to conver some arguments to
                                    tango devstring
        """
        try:
            if argument:
                if is_conversion_needed:
                    argument = eval(argument)
                _, command_id = command(argument)
            else:
                _, command_id = command()

            if isinstance(command_id, list):
                command_id = command_id[0]
            if self._is_long_command(command_id, command_name):
                self.cmd_queue.put(command_id)

        # pylint: disable-next=broad-except
        except Exception as e:
            # NOTE: error in command should not block the test execution.
            # Failures have to be caught in further steps
            module_logger.warning(e)

    def set_exception(self):
        self.proxy.SetException(True)

    def set_timeout(self):
        self.proxy.raise_timeout = True

    def raise_fault(self, fault):
        self.proxy.raise_fault = fault

    def disable_device(self, disabled):
        if disabled:
            self.proxy.adminmode = 1
        else:
            # Mid Cbf Controller set the adminmode=online of all cbf devices
            # (including subarrays) at the end of the initialization.
            # To avoid conflicts in test, we are setting online as well.
            # The possibility to set in ENGINEERING for Low is mantained.
            module_logger.info(
                f"setting admin mode to {0} at {time.asctime()}"
            )
            self.proxy.adminmode = 0

    def set_attribute(self, attribute, value):
        if "adminmode" in attribute.lower():
            value = AdminMode[value.upper()]
            module_logger.info(f"set admin mode to {value}")
        elif "simulationmode" in attribute.lower():
            value = SimulationMode[value.upper()]
            module_logger.info(
                f"set SimulationMode to {SimulationMode(value).name}"
            )
        elif "testalarm" in attribute.lower():
            _value = False
            if value == "True":
                _value = True
            value = _value
        try:
            self.proxy.write_attribute(attribute, value)
        except Exception as err:
            module_logger.error(
                f"Error in setting {attribute} to value " f"{value}: {err}"
            )

    def read_attribute(self, attribute):
        module_logger.info(f" Reading attribute: {attribute}")
        return self.proxy.read_attribute(attribute).value

    def get_attribute(self, attribute, value, timeout=10):
        command_id = None
        attr_type = ""

        attribute_lowercase = attribute.lower()
        if "longrunning" in attribute_lowercase:
            if not self.cmd_queue.empty():
                command_id = self.cmd_queue.get()
                module_logger.info(f"Reading command id: {command_id}")

        if "healthstate" in attribute_lowercase:
            expected_value = HealthState[value.upper()]

        elif "obsstate" in attribute_lowercase:
            expected_value = ObsState[value.upper()]

        elif "adminmode" in attribute_lowercase:
            expected_value = AdminMode[value.upper()]

        elif "state" in attribute_lowercase:
            expected_value = getattr(DevState, value.upper())

        elif "commandresult" in attribute_lowercase:
            expected_value = eval(value)
            attr_type = "commandresult"
            module_logger.info(
                "commandResult:"
                f" {self.proxy.read_attribute(attribute).value}"
            )
        elif "simulationmode" in attribute_lowercase:
            expected_value = getattr(eval(attribute), value)

        elif "iscommunicating" in attribute_lowercase:
            expected_value = eval(value)
            attr_type = "iscommunicating"

        else:
            expected_value = eval(value)

        probe_poller(
            self.proxy,
            attribute,
            expected_value,
            timeout,
            command_id=command_id,
        )

        if attribute_lowercase == "assignedtimingbeamids":
            attr_type = "assignedtimingbeamids"

        if attr_type:
            module_logger.info(
                f"{time.asctime()} - {attr_type}:"
                f" {self.proxy.read_attribute(attribute).value}"
            )

    def get_attribute_in_interval(self, attribute, value):

        ret = False
        t_start = time.time()
        list_name = ast.literal_eval(value)
        while time.time() - t_start < 10:
            attr_val = self.proxy.read_attribute(attribute).value
            if "obsstate" in attribute.lower():
                obsstate = ObsState(attr_val).name
                if obsstate in list_name:
                    module_logger.info(
                        f"current obsstate: {obsstate} expected {list_name}"
                    )
                    ret = True
                    break
            else:
                time.sleep(0.1)
        assert ret

    def state(self):
        return self.proxy.state()

    def subarray_membership(self):
        return self.proxy.subarrayMembership

    def straight_to(self, attribute, value):
        if "obsState" in attribute or "ObsState" in attribute:
            self.proxy.forceobsstate(ObsState[value.upper()].value)
            module_logger.info(
                f"Forcing {attribute} on {self.proxy} to {value}"
            )
        if "obsMode" in attribute or "ObsMode" in attribute:
            self.proxy.forceobsmode(ObsMode[value.upper()].value)
            module_logger.info(
                f"Forcing {attribute} on {self.proxy} to {value}"
            )
        elif "HealthState" in attribute or "healthState" in attribute:
            self.proxy.forcehealthstate(HealthState[value.upper()].value)
            module_logger.info(
                f"Forcing {attribute} on {self.proxy} to {value}"
            )
        elif attribute.lower() in "state":
            self.proxy.forcestate(DevState[value.upper()].value)
            module_logger.info(
                f"Forcing {attribute} on {self.proxy} to {value}"
            )
        else:
            raise Exception(f"Straight to is not implemented for {attribute}")
        self.get_attribute(
            attribute,
            value,
        )

    def return_device_name(self):
        return self.name

    def simulation_mode(self):
        return self.proxy.read_attribute(SimulationMode)

    def _is_long_command(self, command_id, command_name):
        """
        The aim of this function is to verify if the command sent is a
        long command attribute. The long running command_id contains the
        name of the command, while the fast command has only the id.

        :param command_id: str i.e. 1684354589.1904433_31110856541473_Configure
        :param command_name: str i.e. Configure
        """
        if "_" in command_id:
            list_command_structure = command_id.split("_")
            if len(list_command_structure) > 1:
                cmd_name = list_command_structure[-1]
                if command_name.lower() == "gotoidle":
                    # normally the command name is sadded to the command id,
                    # on the contrary, GoToIdle command that is recorder a End
                    command_name = "End"

                if cmd_name.lower() in command_name.lower():
                    module_logger.debug(
                        f"command {(command_id)} is added to command queue"
                    )
                    return True
        return False

    def archive_callback(self, event):
        if not event.err:
            attr_name = event.attr_value.name.lower()
            self.archive_count[attr_name] += 1
            module_logger.info(
                f"Received {self.archive_count[attr_name]}"
                f" archive events on {self.name}/{attr_name}"
                f" at time {time.asctime()}"
            )
            module_logger.info(
                f"{self.name}/{attr_name} value is {event.attr_value.value}"
            )
        else:
            module_logger.error("Received archive event error!")

    def check_subarray_init_state(self):
        self.get_attribute("State", "OFF")
        self.get_attribute("HealthState", "OK")
        self.get_attribute("ObsState", "EMPTY")
        # Mid Cbf Controller set the adminmode=online of all cbf devices
        # (including subarrays) at the end of the initialization.
        # To avoid conflicts in test, we are setting online as well.
        self.get_attribute("AdminMode", "ONLINE")

    def check_controller_init_state(self):
        module_logger.info(f"Check Controller {self.name} init state")
        module_logger.info("Check state")
        self.get_attribute("State", "OFF")
        self.get_attribute("HealthState", "OK", timeout=15)
        self.get_attribute("AdminMode", "ONLINE")

    def check_beam_init_state(self):
        self.get_attribute("State", "OFF")
        self.get_attribute("HealthState", "OK")
        self.get_attribute("ObsState", "IDLE")
        self.get_attribute("AdminMode", "ONLINE")


#  -------------- SETUP --------------

# definition of all controllers, subarrays and beams
# implemented for each testing system in tests/integration/step_defs
# or tests/simulated-systems/step_defs

#  -------------- STEP DEFINITIONS --------------


@given(parsers.parse("Beam{number} is fresh initialized"))
def fresh_new_beam(number, all_beams):

    module_logger.debug(f"Fresh beam {number}")
    devices_to_reinit = []
    for device in all_beams.values():
        device_name = device.return_device_name()
        if number not in device_name:
            continue
        module_logger.debug(f"Reinit {device}")
        devices_to_reinit.append(device)
    for device in devices_to_reinit:
        device.initialize_device()
    for device in devices_to_reinit:
        device.ping_device()
        device.check_beam_init_state()


@given(parsers.parse("All devices adminMode is {value}"))
@then(parsers.parse("All devices adminMode is {value}"))
def devices_adminmode(value, all_subarrays, all_controllers):
    """
    Check that all the Subarrays and Controllers devices are
    in the same adminMode.
    """
    module_logger.info(f"all devices are {value}")
    all_devices = {**all_controllers, **all_subarrays}
    for device in all_devices.values():
        device.get_attribute("adminmode", value)


@given(parsers.parse("Subarray{number} is fresh initialized"))
def fresh_new_subarray(number, all_subarrays):
    """
    Re-initialize a CSP Sub-array.

    The CSP Subarray with the number specified is re-initialized.
    Its adminmode is set to DISABLE, all subordinate CSP
    sub-systems sub-arrays are re-initialized and then the CSP Subarray
    is set to online.
    """
    devices_to_reinit = []
    csp_subarray_name = f"CspSubarray{number}"
    csp_subarray_device = None

    module_logger.debug(f"Fresh subarray {number}")
    for device in all_subarrays.values():
        device_name = device.return_device_name()
        if number not in device_name:
            continue
        devices_to_reinit.append(device)
        module_logger.debug(f"devices to reinitialize: {device_name}")
        if device_name == csp_subarray_name:
            module_logger.info(f"{time.asctime()} - Disabling {device_name}")
            device.disable_device(True)
            device.get_attribute("State", "DISABLE")
            device.get_attribute("iscommunicating", "False")
            csp_subarray_device = device
            time.sleep(1)
    # re-init all the subordinate devices and the CSP subarray
    for device in devices_to_reinit:
        device.initialize_device()
    # ping all the devices to see if are on-line
    for device in devices_to_reinit:
        device.ping_device()
    time.sleep(1)
    module_logger.info(f"{time.asctime()} Re-enabling {csp_subarray_name}")
    # Re-enable the CSP Subarray and check for the status of all the
    # subarrays devices
    if csp_subarray_device:
        csp_subarray_device.disable_device(False)
        csp_subarray_device.get_attribute("isCommunicating", "True")
    for device in devices_to_reinit:
        device_name = device.return_device_name()
        device.check_subarray_init_state()


@given(parsers.parse("Controller is fresh initialized"))
def fresh_new_controller(all_controllers):
    for device in all_controllers.values():
        device_name = device.return_device_name()
        if device_name == "CspController":
            module_logger.info(f"Disabling {device_name}")
            device.disable_device(True)
            device.get_attribute("State", "DISABLE")
            time.sleep(1)

    for device in all_controllers.values():
        device.initialize_device()
    for device in all_controllers.values():
        device.ping_device()

    for device in all_controllers.values():
        # Wait until controller is in
        # State=OFF,
        # HealthState=OK,
        # AdminMode=ONLINE
        device_name = device.return_device_name()
        if device_name == "CspController":
            module_logger.info(f"Re-enabling {device_name}")
            device.disable_device(False)
            device.get_attribute("isCommunicating", "True")
            time.sleep(2)
    for device in all_controllers.values():
        device.check_controller_init_state()


@given("All subsystems are fresh initialized")
def fresh_new_subsystems(all_controllers, all_subarrays, all_beams):

    # Order of iteration is important. CspController has to be last.
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    for device in all_subarrays.values():
        device_name = device.return_device_name()
        module_logger.info(f"Device: {device_name}")
        if "CspSubarray" in device_name:
            module_logger.info(f"Disabling {device_name}")
            device.disable_device(True)
            device.get_attribute("State", "DISABLE")
            device.get_attribute("isCommunicating", "False")
    for device in all_controllers.values():
        device_name = device.return_device_name()
        if device_name == "CspController":
            time.sleep(1)
            module_logger.info(f"Disabling {device_name}")
            device.disable_device(True)
            device.get_attribute("State", "DISABLE")
            device.get_attribute("isCommunicating", "False")
    time.sleep(1)
    # restart all devices
    for device in all_subsystems.values():
        device.initialize_device()
    for device in all_subsystems.values():
        device.ping_device()
    time.sleep(1)
    # Order of iteration is important. CspController has to be last.
    for device in all_subarrays.values():
        device_name = device.return_device_name()
        if "CspSubarray" in device_name:
            module_logger.info(f"Re-enabling {device_name}")
            device.disable_device(False)
            device.get_attribute("isCommunicating", "True")
    for device in all_controllers.values():
        device_name = device.return_device_name()
        if device_name == "CspController":
            module_logger.info(f"Re-enabling {device_name}")
            device.disable_device(False)
            device.get_attribute("isCommunicating", "True")
    time.sleep(1)

    # check initial state of all beams
    for device in all_beams.values():
        device.check_beam_init_state()
    # check initial state of all subarrays
    for device in all_subarrays.values():
        device.check_subarray_init_state()
    # check initial state of all controllers
    for device in all_controllers.values():
        device.check_controller_init_state()


@given(parsers.parse("{device_name} is removed from tangodb"))
def device_remove_tangodb(
    device_name, all_controllers, all_subarrays, all_beams
):
    """Remove device from tango database."""
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    dev = all_subsystems[device_name]
    db = tango.Database()
    global temp_dev_info
    temp_dev_info = db.get_device_info(dev.proxy.dev_name())
    db.delete_device(dev.proxy.dev_name())


@pytest.fixture()
def device_readd_tangodb():
    """Re-add device to tango database in teardown."""
    print("SETUP")
    yield None

    print("TEARDOWN")
    global temp_dev_info
    if temp_dev_info is None:
        return
    db = tango.Database()

    # Create DbDevInfo structure and add device to database
    dev_info = tango.DbDevInfo()
    dev_info.name = temp_dev_info.name
    dev_info._class = temp_dev_info.class_name
    dev_info.server = temp_dev_info.ds_full_name
    db.add_device(dev_info)

    # Create DbDevExportInfo structure and export device
    dev_export = tango.DbDevExportInfo()
    dev_export.name = temp_dev_info.name
    dev_export.ior = temp_dev_info.ior
    dev_export.version = temp_dev_info.version
    dev_export.pid = temp_dev_info.pid
    hosts = db.get_host_list()
    for host in hosts:
        # Check for instance of server name in host name
        if temp_dev_info.ds_full_name.split("/")[0] in host:
            dev_export.host = host
            break
    db.export_device(dev_export)

    # Reset temporary dev info
    temp_dev_info = None


@given(parsers.parse("{device_name} is initialized"))
def dev_init(device_name, all_controllers, all_subarrays, all_beams, all_vccs):
    all_subsystems = {
        **all_controllers,
        **all_subarrays,
        **all_beams,
        **all_vccs,
    }
    device = all_subsystems[device_name]
    device.initialize_device()
    device.ping_device()
    # Check that initialization has finished, comparing state to NOT INIT
    timeout = 20
    total = 0
    step = 1
    while True:
        if device.state() != DevState.INIT:
            break
        if total > timeout:
            assert (
                False
            ), f"Waiting for initialization for {device.dev_name()} timedout"
        time.sleep(step)
        total += step


@when(parsers.parse("{device_name} is restarted"))
def device_server_restarted(
    device_name, all_controllers, all_subarrays, all_beams
):

    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}

    device = all_subsystems[device_name]
    device.restart_device_server()


@given(
    parsers.parse(
        "{command_name} command is issued on {device_name} (no argument)"
    )
)
@when(
    parsers.parse(
        "{command_name} command is issued on {device_name} (no argument)"
    )
)
def send_command(
    command_name, device_name, all_controllers, all_subarrays, all_beams
):
    module_logger.info(
        f"Executing command {command_name} at time {time.asctime()}"
    )
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    all_subsystems[device_name].send_command(command_name)


@given(
    parsers.parse(
        "{command_name} command is issued on {device_name} with argument"
        " {argument}"
    )
)
@when(
    parsers.parse(
        "{command_name} command is issued on {device_name} with argument"
        " {argument}"
    )
)
def send_command_with_argument(
    command_name,
    device_name,
    argument,
    all_controllers,
    all_subarrays,
    all_beams,
):

    module_logger.info(
        f"Executing command {command_name} at time {time.asctime()}"
    )
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    all_subsystems[device_name].send_command_with_argument(
        command_name, argument
    )


@given(
    parsers.parse(
        "{command_name} command is issued on {device_name} (argument from"
        " file: {filename})"
    )
)
@when(
    parsers.parse(
        "{command_name} command is issued on {device_name} (argument from"
        " file: {filename})"
    )
)
def send_command_with_argument_from_file(
    command_name,
    device_name,
    filename,
    all_controllers,
    all_subarrays,
    all_beams,
):
    module_logger.info(
        f"Executing command {command_name} at time {time.asctime()}"
    )
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    all_subsystems[device_name].send_command_with_argument_from_file(
        command_name, filename
    )


@when(parsers.parse("{device_name} raise an exception"))
def raise_exception(device_name, all_controllers, all_subarrays, all_beams):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems[device_name]
    device.set_exception()
    device.get_attribute("raise_exception", "True")


@when(parsers.parse("{device_name} raise a timeout"))
def raise_timeout(device_name, all_controllers, all_subarrays, all_beams):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems[device_name]
    device.set_timeout()
    device.get_attribute("raise_timeout", "True")


@when(parsers.parse("{device_name} raise a fault"))
def raise_fault(device_name, all_controllers, all_subarrays, all_beams):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    module_logger.info("raise a fault")
    device = all_subsystems[device_name]
    device.raise_fault(True)


@given(parsers.parse("{device_name} {attribute} are sent straight to {value}"))
@when(parsers.parse("{device_name} {attribute} are sent straight to {value}"))
def straight_to(
    device_name, attribute, value, all_controllers, all_subarrays, all_beams
):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems[device_name]
    device.straight_to(attribute, value)


@given(parsers.parse("{device_name} {attribute} is {value}"))
@then(parsers.parse("{device_name} {attribute} is {value}"))
@when(parsers.parse("{device_name} {attribute} is {value}"))
def attribute_of_device(
    device_name,
    attribute,
    value,
    all_controllers,
    all_subarrays,
    all_beams,
    all_capabilities,
):
    # add logs temporarily to resolve the issue with init
    module_logger.info(f"Checking {attribute} on {device_name} in {value}")
    all_subsystems = {
        **all_controllers,
        **all_subarrays,
        **all_beams,
        **all_capabilities,
    }
    if device_name in all_subsystems:
        device = all_subsystems[device_name]
        device.get_attribute(attribute, value)
    else:
        module_logger.warning(
            f"The device {device_name} is not in the list "
            f"of all devices: {all_subsystems}"
        )


@given(parsers.parse("{device_name} {attribute} is in {value}"))
@then(parsers.parse("{device_name} {attribute} is in {value}"))
@when(parsers.parse("{device_name} {attribute} is in {value}"))
def attribute_of_device_in_interval(
    device_name, attribute, value, all_controllers, all_subarrays, all_beams
):
    # add logs temporarily to resolve the issue with init
    module_logger.info(f"Checking {attribute} on {device_name} in {value}")
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems[device_name]
    device.get_attribute_in_interval(attribute, value)


def subscribe_archive_event(device, attribute: str, callback=None):
    device.archive_count[attribute] = 0
    device.proxy.subscribe_event(
        attribute, tango.EventType.ARCHIVE_EVENT, callback
    )


@given(parsers.parse("{device_name} {attribute} is subscribed for archiving"))
@then(parsers.parse("{device_name} {attribute}  is subscribed for archiving"))
@when(parsers.parse("{device_name} {attribute}  is subscribed for archiving"))
def subscribe_archive_attribute(
    device_name, attribute, all_controllers, all_subarrays
):
    # wait to flush all previous events

    time.sleep(2)
    # add logs temporarily to resolve the issue with init
    module_logger.info(
        f"{time.asctime()} subscribe archive attribute {attribute} "
        f"on {device_name}"
    )
    all_subsystems = {**all_controllers, **all_subarrays}
    device = all_subsystems[device_name]
    subscribe_archive_event(device, attribute, device.archive_callback)


@then(
    parsers.parse("{device_name} pushed {value} archive events on {attribute}")
)
def check_archive_event(
    device_name, attribute, value, all_controllers, all_subarrays
):
    # add logs temporarily to resolve the issue with init
    module_logger.info(f"check archive event on {attribute} on {device_name}")
    all_subsystems = {**all_controllers, **all_subarrays}
    device = all_subsystems[device_name]
    if device.archive_count[attribute] == int(value):
        assert True
    else:
        # wait 1 sec to verify that the counter has been updated
        time.sleep(2)
        assert device.archive_count[attribute] == int(value)


@given(parsers.parse("{device_name} set {attribute} to {value}"))
@when(parsers.parse("{device_name} set {attribute} to {value}"))
def set_attribute_of_device(
    device_name,
    attribute,
    value,
    all_controllers,
    all_subarrays,
    all_beams,
    all_capabilities,
):
    all_subsystems = {
        **all_controllers,
        **all_subarrays,
        **all_beams,
        **all_capabilities,
    }
    if device_name in all_subsystems:
        device = all_subsystems[device_name]
        device.set_attribute(attribute, value)
    else:
        module_logger.warning("This device is not in list of all devices.")


@given("Csp Subarray setup as IDLE with random resources")
def subarray_setup_IDLE(all_controllers, all_subarrays, all_beams, all_vccs):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems["CspSubarray01"]
    if device.proxy.state() != DevState.ON:
        device.send_command("On")
        device.get_attribute("commandResult", "('on', '0')")

    if device.repository == "mid":
        vcc_ok(all_vccs, all_controllers, all_subarrays, all_beams)

    device.get_attribute("state", "ON")
    device.send_command_with_argument_from_file(
        "AssignResources",
        "test_AssignResources_random.json",
    )

    all_subarrays["CbfSubarray01"].get_attribute("ObsState", "IDLE")
    device.get_attribute("ObsState", "IDLE")
    device.get_attribute("commandResult", "('assignresources', '0')")


@given("Csp Subarray setup as READY with random resources")
def subarray_setup_READY(all_controllers, all_subarrays, all_beams, all_vccs):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems["CspSubarray01"]
    subarray_setup_IDLE(all_controllers, all_subarrays, all_beams, all_vccs)
    device.send_command_with_argument_from_file(
        "Configure",
        "test_Configure_random.json",
    )
    all_subarrays["CbfSubarray01"].get_attribute("ObsState", "READY")
    device.get_attribute("ObsState", "READY")
    device.get_attribute("commandResult", "('configure', '0')")


@given("Csp Subarray setup as SCANNING with random resources")
def subarray_setup_SCANNING(
    all_controllers, all_subarrays, all_beams, all_vccs
):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems["CspSubarray01"]
    subarray_setup_READY(all_controllers, all_subarrays, all_beams, all_vccs)
    device.send_command_with_argument_from_file(
        "Scan",
        "test_Scan_random.json",
    )
    all_subarrays["CbfSubarray01"].get_attribute("ObsState", "SCANNING")
    device.get_attribute("ObsState", "SCANNING")
    device.get_attribute("commandResult", "('scan', '1')")


@given("Csp Subarray setup as ABORTED with random resources")
def subarray_setup_ABORTED(
    all_controllers, all_subarrays, all_beams, all_vccs
):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems["CspSubarray01"]
    subarray_setup_SCANNING(
        all_controllers, all_subarrays, all_beams, all_vccs
    )
    device.send_command("Abort")
    all_subarrays["CbfSubarray01"].get_attribute("ObsState", "ABORTED")
    device.get_attribute("ObsState", "ABORTED")
    device.get_attribute("commandResult", "('abort', '0')")


@given(parsers.parse("Vcc are properly registered"))
def vcc_ok(all_vccs, all_controllers, all_subarrays, all_beams):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    cbf_ctrl = all_subsystems["CbfController"]

    for vcc in all_vccs.values():
        if vcc.proxy.state() in (DevState.OFF, DevState.STANDBY):
            module_logger.info("Restarting Vcc")
            vcc.send_command("On")
            probe_poller(vcc.proxy, "state", DevState.ON, time=10)
        if vcc.proxy.subarrayMembership != 0:
            vcc.proxy.subarrayMembership = 0

    time.sleep(1)
    module_logger.info(f"Reading VCC state {time.asctime()}")
    vcc_state = cbf_ctrl.proxy.reportVCCState
    module_logger.info(f"reportVCCState {vcc_state} {time.asctime()}")
    if list(vcc_state) == [
        DevState.UNKNOWN,
        DevState.UNKNOWN,
        DevState.UNKNOWN,
        DevState.UNKNOWN,
    ]:

        module_logger.info(f"Switch off CbfController {time.asctime()}")
        cbf_ctrl.send_command("Off")
        probe_poller(cbf_ctrl.proxy, "state", DevState.OFF, time=10)
        module_logger.info(f"Switch on CbfController {time.asctime()}")
        cbf_ctrl.send_command("On")
        probe_poller(cbf_ctrl.proxy, "state", DevState.ON, time=10)
        probe_poller(cbf_ctrl.proxy, "reportVCCState", [0, 0, 0, 0], time=10)
        module_logger.info(f"End vcc_ok {time.asctime()}")


@given(parsers.parse("Scan lasts for {time_length} seconds"))
@then(parsers.parse("Scan lasts for {time_length} seconds"))
@when(parsers.parse("Scan lasts for {time_length} seconds"))
def scan_run(time_length):
    # this method together with commands to start and end a scan simulates how
    # this command would be invoked by TMC
    module_logger.info(f"Running scan for {time_length} seconds")
    time.sleep(10)
    module_logger.info("Scan finished.")


def command_invalid_input(
    command_name, is_pst_present, key_to_remove, telescope
):
    """
    Test that command is rejected after telmodel validation
    if input is invalid
    """

    csp_s1 = tango.DeviceProxy(f"{telescope}-csp/subarray/01")
    assert csp_s1.state() == DevState.ON
    test_data_path = "/app/tests/test_data/"

    test_data_path = test_data_path + command_name

    if is_pst_present == "with PST":
        test_data_path = test_data_path + "_CBF_PSS_PST.json"
    elif is_pst_present == "without PST":
        test_data_path = test_data_path + "_CBF.json"
    else:
        raise ValueError("wrong syntax")

    # pylint: disable=unspecified-encoding
    with open(test_data_path, "r") as file:
        file_input = json.load(file)

    if command_name == "Configure":
        eval(
            f"file_input{key_to_remove[command_name][is_pst_present][0]}"
        ).pop(key_to_remove[command_name][is_pst_present][1])
    else:
        file_input.pop(key_to_remove[command_name][is_pst_present])

    file_input = json.dumps(file_input)

    with pytest.raises(tango.DevFailed):
        # pylint: disable=exec-used
        exec(f"csp_s1.{command_name}('{file_input}')")


@then("CspSubarray01 reject the command and raises an exception")
def fake_command_rejected():

    module_logger.info("command_input is not valid")
