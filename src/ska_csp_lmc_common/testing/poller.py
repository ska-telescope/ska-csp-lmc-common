import json
import logging
import time

import numpy
from ska_control_model import ResultCode
from tango import DeviceProxy, DevState

module_logger = logging.getLogger(__name__)

# pylint: disable=too-few-public-methods
# pylint: disable=redefined-outer-name
# pylint: disable=redefined-builtin
# pylint: disable=logging-fstring-interpolation
# pylint: disable=no-else-return
# pylint: disable=missing-function-docstring
# pylint: disable-next=missing-class-docstring


class Timeout:
    """Internal class to define a timeout check."""

    def __init__(self, duration):
        self.endtime = time.time() + duration

    def has_expired(self):
        """timeout check"""
        return time.time() > self.endtime


# pylint: disable=eval-used
# pylint: disable=too-many-arguments
# pylint: disable=broad-except
# pylint: disable=too-many-branches
class Probe:
    """Helper class that perform the attributes/command
    status/result verification"""

    def __init__(
        self, proxy, attr_name, expected_state, message="", command_id=None
    ):
        self.proxy = proxy
        self.attr_name = attr_name
        self.expected_state = expected_state
        self.message = message
        self.current_state = DevState.DISABLE
        self.command_id = None
        self.long_running_result_counter = 0
        if command_id:
            self.command_id = command_id

    def get_attribute_name(self):
        """Get the attribute name"""
        return self.attr_name

    def sample(self):
        """extract the state of client and store it."""
        if self.proxy:
            if self.attr_name.lower() == "longrunningcommandstatus":

                device_attr = getattr(self.proxy, self.attr_name.lower())
                lrc_status = self._extract_longrunningcommand_value(
                    device_attr
                )
                self.current_state = "N/A"
                if lrc_status:
                    self.current_state = lrc_status

            elif self.attr_name.lower() == "longrunningcommandresult":
                device_attr = getattr(self.proxy, self.attr_name.lower())
                self.current_state = self._extract_longrunningcommand_result(
                    device_attr
                )

            elif self.attr_name.lower() == "longrunningcommandattributes":
                device_attr = getattr(self.proxy, "longrunningcommandstatus")
                status_value = self._extract_longrunningcommand_value(
                    device_attr
                )
                if status_value:
                    if status_value in ["COMPLETED", "FAILED"]:
                        self.long_running_result_counter += 1
                else:
                    status_value = "N/A"

                device_attr = getattr(self.proxy, "longrunningcommandresult")
                result_value = self._extract_longrunningcommand_result(
                    device_attr
                )
                self.current_state = (result_value, status_value)

            elif (
                self.attr_name.lower() == "sourcedishvccconfig"
                or self.attr_name.lower() == "dishvccconfig"
            ):
                device_attr = getattr(self.proxy, self.attr_name.lower())
                # device_attr = device_attr.replace("  ", "")
                self.current_state = device_attr
                # expected_json = json.loads(self.expected_state)
                if isinstance(self.expected_state, str):
                    self.expected_state = json.loads(self.expected_state)
                if isinstance(self.current_state, str):
                    self.current_state = json.loads(self.current_state)
                self.expected_state = sorted(self.expected_state)
                self.current_state = sorted(self.current_state)
            elif self.attr_name.lower() == "obsmode":
                device_attr = self.proxy.read_attribute(self.attr_name)
                self.current_state = sorted(device_attr.value)
                self.expected_state = sorted(self.expected_state)
            elif isinstance(self.proxy, DeviceProxy):
                device_attr = self.proxy.read_attribute(self.attr_name)
                self.current_state = device_attr.value
            else:
                self.current_state = getattr(self.proxy, self.attr_name)
        else:
            self.current_state = self.attr_name
            raise Exception(
                "Proxy is not defined. It is not possible"
                " to extract value from long running command."
            )

    def is_satisfied(self):
        """Check if the state satisfies this test condition."""
        if self.command_id:
            self.message = f"{self.command_id}: "
            self.message += f"{self.attr_name} for {self.proxy} is not "
            self.message += f"{self.expected_state}. It is"
            self.message += f" {self.current_state}"
        else:
            self.message = f"{self.attr_name} for {self.proxy} is not "
            self.message += (
                f"{self.expected_state}. It is {self.current_state}"
            )

        if isinstance(self.current_state, numpy.ndarray):
            module_logger.info(f"{self.expected_state}-{self.current_state}")
            current_list = self.current_state.tolist()
            return self.expected_state == current_list
        elif isinstance(self.current_state, tuple):
            if self.expected_state == self.current_state:
                module_logger.info(
                    f"{self.expected_state}-{self.current_state}"
                )
            return self.expected_state == self.current_state
        return self.expected_state == self.current_state

    def _extract_longrunningcommand_value(self, attr_value: tuple):
        """Extract the result information associated with the desired
        command_id from:
        - long running command status/result.
        The expected tuple structure are:
        longRunningCommandStatus:
        ('command_id', 'TaskStatus', .. , 'command_id', 'TaskStatus')
        i.e. ('1684235818.1884599_34634052132593_AssignResources', 'COMPLETED',
              '1684235819.4827569_236625938266855_AssignResources', 'COMPLETED'
              )
        - longRunningCommandResult:
        ('command_id', '[TaskStatus, "message"]')
        i.e. ('1684236940.7847307_276521813445928_AssignResources',
              '[3, "Error in parsing the resource configuration"]')

        :param attr_value: tuple from which a value has to be extracted.

        :return value: return a string on success, otherwise `None`
        """

        if not self.command_id:
            raise Exception(
                "Command ID not defined. It is not possible"
                " to extract value from long running command."
            )
        value = None
        attr_value_list = [*attr_value]
        module_logger.debug(
            f"Probe {attr_value_list}," f"command id: {self.command_id}"
        )
        if isinstance(self.command_id, list):
            self.command_id = self.command_id[0]
        if self.command_id in attr_value_list:
            value = attr_value_list[attr_value_list.index(self.command_id) + 1]

        return value

    def _extract_longrunningcommand_result(self, attr_value: tuple):
        """Extract desired value from long running command result.
        The expected tuple structure is:
        ('command_id', '[TaskStatus, "message"]')
        i.e. ('1684236940.7847307_276521813445928_AssignResources',
              '[3, "Error in parsing the resource configuration"]')

        :param attr_value: tuple from which a value has to be extracted.

        :return value: integer >0 representing the result of the
            long running command on success, otherewise 4 (=ResultCode.UNKNOWN)
        """
        result = ResultCode.UNKNOWN
        value = self._extract_longrunningcommand_value(attr_value)
        if value:
            if not isinstance(eval(value), list):
                raise Exception(
                    "longrunningcommand_result has not "
                    f"the expected format: {attr_value}"
                )

            result = eval(value)[0]
        return result


class Poller:
    """Class that allows to poll the status of an attribute, command ..."""

    def __init__(self, timeout, interval):
        self.timeout = timeout
        self.interval = interval

    def check(self, probe: Probe):
        """Repeatedly check if the probe is satisfied.

        Assert false when the timeout expires.
        """
        timer = Timeout(self.timeout)
        probe.sample()
        while not probe.is_satisfied():
            if timer.has_expired():
                module_logger.debug(
                    f"Check Timeout on:{probe.get_attribute_name()}"
                )
                assert False, probe.message
            if probe.long_running_result_counter > 10:
                # the long running command status is COMPLETED/FAILED since
                # 10*polling time probably the result is wrong
                assert False, probe.message
            time.sleep(self.interval)
            probe.sample()
        module_logger.debug(f"Check success on: {probe.get_attribute_name()}")
        assert True


def probe_poller(
    object, attribute, expected_value, time, interval=0.1, command_id=None
):
    """Static function used to check the status of an attribute, command...

    :param object: the instance
    :type object: class or DeviceProxy
    :param attribute: the attribute name
    :type attribute: str
    :param expected_value: the exptected attribute value
    :type expected_value: any
    :param time: the time to poll
    :type time: float
    :param interval: polling time: defaults to 0.1.
    :type time: float
    :param command_id: str
    """
    probe = Probe(object, attribute, expected_value, command_id=command_id)
    Poller(time, interval).check(probe)
