import json
import logging
import random
import threading
import time
from functools import partial

import mock
import tango
from ska_control_model import AdminMode, ObsState, ResultCode
from tango import DevState

import ska_csp_lmc_common
from ska_csp_lmc_common.testing import mock_config
from ska_csp_lmc_common.testing.mock_attribute import AttributeDataFactory
from ska_csp_lmc_common.testing.test_utils import (
    create_dummy_event,
    mock_cbk_with_cmd_result,
    mock_cbk_with_err,
)

module_logger = logging.getLogger(__name__)

# pylint: disable=dangerous-default-value
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements
# pylint: disable=protected-access
# pylint: disable=unused-argument
# pylint: disable=inconsistent-return-statements
# pylint: disable=missing-function-docstring


class MockedConnector:
    """Helper class to mock a Connector class and its methods."""

    original_cls = ska_csp_lmc_common.connector.Connector
    instances = []
    event_subscription_map = {}
    callback_map = {}
    event_id_map = {}
    mock_attrs = {
        "deviceproxy": mock.MagicMock(),
        "deviceproxy.get_timeout_millisec": mock.MagicMock(return_value=2000),
    }
    attr_factory_class = AttributeDataFactory

    @classmethod
    def fake_subscribe_attribute(cls, fqdn, attr_name, evt_type, callback):
        cls.event_subscription_map[fqdn].update({attr_name: callback})
        evt_id = random.randrange(1, 1000)
        if fqdn not in cls.event_id_map:
            cls.event_id_map[fqdn] = {}
        cls.event_id_map[fqdn].update({attr_name: evt_id})

        # create the event with the default value for the attribute
        attr_factory = cls.attr_factory_class(fqdn)
        attr = attr_factory.create_attribute(attr_name)
        _event = create_dummy_event(fqdn, attr_name, attr.value)
        callback = cls.event_subscription_map[fqdn][attr_name]
        # invoke the registered callback to raise the event in the same thread
        # threading.Thread(target=callback, args=(_event,)).start()
        callback(_event)
        return evt_id

    @classmethod
    def fake_unsubscribe_attribute(cls, evt_id):
        key_to_remove = {}
        for fqdn, attr_dict in cls.event_id_map.items():
            for attr_name, _evt_id in attr_dict.items():
                if attr_name == "test-key-error-exception":
                    raise KeyError("Invalid id")
                if attr_name == "test-tango-exception":
                    tango.Except.throw_exception(
                        "API_CantConnectToDevice",
                        "This is error message for devfailed",
                        " ",
                        tango.ErrSeverity.ERR,
                    )
                if _evt_id == evt_id:
                    key_to_remove[fqdn] = attr_name
        if key_to_remove:
            for fqdn, attr_name in key_to_remove.items():
                cls.event_id_map[fqdn].pop(attr_name)

    @classmethod
    def fake_command_async(cls, fqdn, command_name, argument, callback):
        """Mock the command_async TANGO call.
        Rely on a global configuration for the MockedConnector class to
        generate different behavior during the execution of a command.

        If no failures are injected, the execution of this method raises
        the appropriate event for the required attribute (State, obsState,
        etc).
        """
        if "test-tango-exception" in fqdn:
            tango.Except.throw_exception(
                "API_CantConnectToDevice",
                "This is error message for devfailed",
                " ",
                tango.ErrSeverity.ERR,
            )
            return 0
        module_logger.debug(
            f"mock_exception {mock_config.mock_exception}"
            f" {mock_config.mock_fail_device} "
            f"timeout: {mock_config.mock_timeout}"
        )
        cls.callback_map[fqdn].update({command_name: callback})
        if mock_config.mock_event_failure:
            return 0
        if mock_config.mock_cbk_cmd:
            module_logger.debug(
                f"command result {mock_config.mock_cbk_cmd_result}"
            )
            cmd_event = mock_cbk_with_cmd_result(
                command_name, fqdn, mock_config.mock_cbk_cmd_result
            )
            callback = cls.callback_map[fqdn][command_name]
            # run the callback in a separate thread
            threading.Thread(target=callback, args=(cmd_event,)).start()
            return
            # callback(cmd_event)
        if mock_config.mock_timeout:
            # if we want a timeout, we don't raise any event
            if (
                mock_config.mock_fail_device
                and mock_config.mock_fail_device in fqdn
            ):
                return 0
        if mock_config.mock_exception:
            # module_logger.info("going to raise an exception!")
            if (
                mock_config.mock_fail_device
                and mock_config.mock_fail_device in fqdn
            ):
                raise ValueError(f"Device {fqdn} raised an exception!!")
        if mock_config.mock_cmd_fail and mock_config.mock_cbk_cmd_err:
            if (
                mock_config.mock_fail_device
                and mock_config.mock_fail_device in fqdn
            ):
                cmd_event = mock_cbk_with_err(
                    command_name,
                    fqdn,
                )
                # module_logger.info(f"Error: {cmd_event}")
                callback = cls.callback_map[cmd_event.device.dev_name()][
                    command_name
                ]
                # run the callback in a separate thread
                threading.Thread(target=callback, args=(cmd_event,)).start()
                return
        else:
            pass
        _event = None
        if command_name == "on":
            _event = create_dummy_event(fqdn, "state", DevState.ON)
            if (
                mock_config.mock_event
                and mock_config.mock_cmd_fail == command_name
            ):
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "state", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["state"]
        # if command_name == "standby":
        #     _event = create_dummy_event(fqdn, "state", DevState.STANDBY)
        #     if "subarray" in fqdn:
        #         _event = create_dummy_event(fqdn, "state", DevState.ON)
        #     if mock_config.mock_event:
        #         if fqdn == mock_config.mock_fail_device:
        #             _event = create_dummy_event(
        #                 fqdn, "state", mock_config.mock_event_value
        #             )
        #     callback = cls.event_subscription_map[fqdn]["state"]
        if command_name == "off":
            _event = create_dummy_event(fqdn, "state", DevState.OFF)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "state", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["state"]
        if command_name == "abort":
            _event = create_dummy_event(fqdn, "obsstate", ObsState.ABORTING)
            threading.Thread(target=callback, args=(_event,)).start()
            time.sleep(0.5)
            _event = create_dummy_event(fqdn, "obsstate", ObsState.ABORTED)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["obsstate"]
        if command_name == "restart":
            _event = create_dummy_event(fqdn, "obsstate", ObsState.RESTARTING)
            threading.Thread(target=callback, args=(_event,)).start()
            time.sleep(0.5)
            _event = create_dummy_event(fqdn, "obsstate", ObsState.EMPTY)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["obsstate"]
        if command_name in ("configure", "configurescan"):
            _event = mock_cbk_with_cmd_result(
                command_name, fqdn, (ResultCode.OK, "Command ok")
            )
            callback = cls.callback_map[fqdn][command_name]
            threading.Thread(target=callback, args=(_event,)).start()
            time.sleep(0.5)
            _event = create_dummy_event(fqdn, "obsstate", ObsState.READY)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["obsstate"]
        if command_name in ("assignresources", "addreceptors"):
            _event = mock_cbk_with_cmd_result(
                command_name, fqdn, (ResultCode.OK, "Command ok")
            )
            callback = cls.callback_map[fqdn][command_name]
            threading.Thread(target=callback, args=(_event,)).start()
            time.sleep(0.5)
            _event = create_dummy_event(fqdn, "obsstate", ObsState.IDLE)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["obsstate"]
        if command_name in ("releaseresources", "removereceptors"):
            _event = mock_cbk_with_cmd_result(
                command_name, fqdn, (ResultCode.OK, "Command ok")
            )
            callback = cls.callback_map[fqdn][command_name]
            threading.Thread(target=callback, args=(_event,)).start()
            time.sleep(0.5)
            _event = create_dummy_event(fqdn, "obsstate", ObsState.EMPTY)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["obsstate"]
        if command_name in ("releaseallresources", "removeallreceptors"):
            _event = mock_cbk_with_cmd_result(
                command_name, fqdn, (ResultCode.OK, "Command ok")
            )
            callback = cls.callback_map[fqdn][command_name]
            threading.Thread(target=callback, args=(_event,)).start()
            time.sleep(0.5)
            _event = create_dummy_event(fqdn, "obsstate", ObsState.EMPTY)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["obsstate"]
        if command_name == "reset":
            if not mock_config.mock_event:
                if "control" in fqdn:
                    _event = create_dummy_event(
                        fqdn, "state", DevState.STANDBY
                    )
                elif "subarray" in fqdn:
                    _event = create_dummy_event(fqdn, "state", DevState.OFF)
                elif "beam" in fqdn:
                    _event = create_dummy_event(fqdn, "state", DevState.OFF)
                else:
                    pass
            callback = cls.event_subscription_map[fqdn]["state"]
        if command_name == "gotoidle":
            _event = create_dummy_event(fqdn, "obsstate", ObsState.IDLE)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["obsstate"]
        if command_name == "obsreset":
            _event = create_dummy_event(fqdn, "obsstate", ObsState.IDLE)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["obsstate"]
        if command_name == "scan":
            _event = create_dummy_event(fqdn, "obsstate", ObsState.SCANNING)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
            callback = cls.event_subscription_map[fqdn]["obsstate"]
        if command_name == "endscan":
            _event = create_dummy_event(fqdn, "obsstate", ObsState.READY)
            if mock_config.mock_event:
                if fqdn == mock_config.mock_fail_device:
                    _event = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
            time.sleep(0.5)
            callback = cls.event_subscription_map[fqdn]["obsstate"]
        # run the callback in a separate thread
        if _event:
            threading.Thread(target=callback, args=(_event,)).start()
        return 0

    @classmethod
    # pylint: disable=too-many-return-statements
    # pylint: disable=too-many-locals
    def fake_command(cls, fqdn, command_name, argument):
        """Mock the command_inout TANGO call.
        Rely on a global configuration for the MockedConnector class to
        generate different behavior during the execution of a command.

        If no failures are injected, the execution of this method raises
        the appropriate event for the required attribute (State, obsState,
        etc).
        """
        if "test-tango-exception" in fqdn:
            tango.Except.throw_exception(
                "API_CantConnectToDevice",
                "This is error message for devfailed",
                " ",
                tango.ErrSeverity.ERR,
            )
            return 0
        module_logger.debug(
            f"mock_exception {mock_config.mock_exception}"
            f" {mock_config.mock_fail_device} "
            f"timeout: {mock_config.mock_timeout}"
        )

        if mock_config.mock_exception:
            # module_logger.info("going to raise an exception!")
            if (
                mock_config.mock_fail_device
                and mock_config.mock_fail_device in fqdn
            ):
                raise ValueError(f"Device {fqdn} raised an exception!!")
        if mock_config.mock_cmd_fail and mock_config.mock_cbk_cmd_err:
            if (
                mock_config.mock_fail_device
                and mock_config.mock_fail_device in fqdn
            ):
                cmd_event = mock_cbk_with_err(
                    command_name,
                    fqdn,
                )
                # module_logger.info(f"Error: {cmd_event}")
                callback = cls.callback_map[cmd_event.device.dev_name()][
                    command_name
                ]
                # run the callback in a separate thread
                threading.Thread(target=callback, args=(cmd_event,)).start()
                return
        else:
            pass

        if command_name == "on":
            # build a different command_id depending on the FQDN
            # fqdn expected format domain/faimily/device_XX
            command_id = f"12345678_{fqdn[-2:]}_On"

            def throw_event_on():
                # thow an event on the LRC status attribute
                # after 0.5 sec

                time_to_sleep = random.uniform(0.3, 1.5)
                if (
                    mock_config.mock_timeout
                    and mock_config.mock_fail_device
                    and mock_config.mock_fail_device in fqdn
                ):
                    time_to_sleep = 6
                module_logger.info(
                    f"run On command on {fqdn} time_to_sleep: {time_to_sleep}"
                )
                result = (
                    command_id,
                    json.dumps([0, f"On completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(fqdn, "state", DevState.ON)
                callback2 = cls.event_subscription_map[fqdn]["state"]
                time.sleep(time_to_sleep)
                if (
                    mock_config.mock_fail_device
                    and mock_config.mock_fail_device in fqdn
                    and mock_config.mock_event_failure
                ):
                    return
                callback(_event)
                callback1(_event1)
                callback2(_event2)

            threading.Thread(target=throw_event_on).start()
            return [[ResultCode.QUEUED], [command_id]]

        if command_name == "off":
            # build a different command_id depending on the FQDN
            # fqdn expected format domain/faimily/device_XX
            command_id = f"12345678_{fqdn[-2:]}_Off"

            def throw_event_off():
                # thow an event on the LRC status attribute
                # after 0.5 sec

                time_to_sleep = random.uniform(0.3, 1.5)
                if (
                    mock_config.mock_timeout
                    and mock_config.mock_fail_device
                    and mock_config.mock_fail_device in fqdn
                ):
                    time_to_sleep = 6
                module_logger.info(
                    f"run Off command on {fqdn} time_to_sleep: {time_to_sleep}"
                )
                result = (
                    command_id,
                    json.dumps([0, f"Off completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(fqdn, "state", DevState.ON)
                callback2 = cls.event_subscription_map[fqdn]["state"]
                time.sleep(time_to_sleep)
                if (
                    mock_config.mock_fail_device
                    and mock_config.mock_fail_device in fqdn
                    and mock_config.mock_event_failure
                ):
                    return
                callback(_event)
                callback1(_event1)
                callback2(_event2)

            threading.Thread(target=throw_event_off).start()
            return [[ResultCode.QUEUED], [command_id]]

        if command_name in ["assignresources", "addreceptors"]:
            # build a different command_id depending on the FQDN
            # fqdn expected format domain/faimily/device_XX
            command_id = f"12345678_{fqdn[-2:]}_AssignResources"
            time_to_sleep = random.uniform(0.3, 1.5)

            def throw_event_assign():
                result = (
                    command_id,
                    json.dumps([0, f"AssignResources completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(fqdn, "obsstate", ObsState.IDLE)
                callback2 = cls.event_subscription_map[fqdn]["obsstate"]
                time.sleep(time_to_sleep)
                callback(_event)
                callback1(_event1)
                callback2(_event2)

            threading.Thread(target=throw_event_assign).start()
            return [[ResultCode.QUEUED], [command_id]]

        if command_name in [
            "releaseresources",
            "releaseallresources",
            "removereceptors",
            "removeallreceptors",
        ]:
            # build a different command_id depending on the FQDN
            # fqdn expected format domain/faimily/device_XX
            command_id = f"12345678_{fqdn[-2:]}_ReleaseResources"
            time_to_sleep = random.uniform(0.3, 1.5)

            def throw_event_release():
                result = (
                    command_id,
                    json.dumps([0, f"ReleaseResources completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(fqdn, "obsstate", ObsState.EMPTY)
                callback2 = cls.event_subscription_map[fqdn]["obsstate"]
                time.sleep(time_to_sleep)
                callback(_event)
                callback1(_event1)
                callback2(_event2)

            threading.Thread(target=throw_event_release).start()
            return [[ResultCode.QUEUED], [command_id]]

        if command_name in ["configure", "configurescan"]:
            # build a different command_id depending on the FQDN
            # fqdn expected format domain/faimily/device_XX
            command_id = f"12345678_{fqdn[-2:]}_Configure"
            time_to_sleep = random.uniform(0.3, 1.5)

            def throw_event_cfg():
                result = (
                    command_id,
                    json.dumps([0, f"Configure completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(fqdn, "obsstate", ObsState.READY)
                callback2 = cls.event_subscription_map[fqdn]["obsstate"]
                time.sleep(time_to_sleep)
                callback(_event)
                callback1(_event1)
                callback2(_event2)

            threading.Thread(target=throw_event_cfg).start()
            return [[ResultCode.QUEUED], [command_id]]

        if command_name == "scan":
            command_id = f"12345678_{fqdn}_Scan"
            time_to_sleep = random.uniform(0.3, 1.5)
            module_logger.info(f"Scan for {fqdn} time_sleep {time_to_sleep}")

            def throw_event_scan():
                result = (
                    command_id,
                    json.dumps([0, f"Scan completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(
                    fqdn, "obsstate", ObsState.SCANNING
                )
                callback2 = cls.event_subscription_map[fqdn]["obsstate"]
                if (
                    mock_config.mock_event
                    and mock_config.mock_fail_device in fqdn
                ):
                    # Obstate.FAULT
                    _event3 = create_dummy_event(
                        fqdn, "obsstate", mock_config.mock_event_value
                    )
                    callback3 = cls.event_subscription_map[fqdn]["obsstate"]
                    time.sleep(time_to_sleep)
                    # the obsstate event has to be the first callback
                    callback3(_event3)
                    callback(_event)
                    callback1(_event1)

                else:
                    time.sleep(time_to_sleep)
                    callback2(_event2)
                    callback(_event)
                    callback1(_event1)

            threading.Thread(target=throw_event_scan).start()
            return [[ResultCode.QUEUED], [command_id]]

        if command_name in ["deconfigure", "gotoidle", "end"]:
            command_id = f"12345678_{fqdn}_Gotoidle"
            time_to_sleep = random.uniform(0.3, 1.5)
            module_logger.info(
                f"GoToIdle for {fqdn} time_sleep {time_to_sleep}"
            )

            def throw_event_gotoidle():
                result = (
                    command_id,
                    json.dumps([0, f"GoToIdle completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(fqdn, "obsstate", ObsState.IDLE)
                callback2 = cls.event_subscription_map[fqdn]["obsstate"]
                time.sleep(time_to_sleep)
                # the obsstate event has to be the first callback
                callback2(_event2)
                callback(_event)
                callback1(_event1)

            threading.Thread(target=throw_event_gotoidle).start()
            return [[ResultCode.QUEUED], [command_id]]

        if command_name == "endscan":
            command_id = f"12345678_{fqdn}_EndScan"
            time_to_sleep = random.uniform(0.3, 1.5)
            module_logger.info(
                f"EndScan for {fqdn} time_sleep {time_to_sleep}"
            )

            def throw_event_endscan():
                result = (
                    command_id,
                    json.dumps([0, f"Endscan completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(fqdn, "obsstate", ObsState.READY)
                callback2 = cls.event_subscription_map[fqdn]["obsstate"]
                time.sleep(time_to_sleep)
                # the obsstate event has to be the first callback
                callback2(_event2)
                callback(_event)
                callback1(_event1)

            threading.Thread(target=throw_event_endscan).start()
            return [[ResultCode.QUEUED], [command_id]]

        if "abort" in command_name:
            # build a different command_id depending on the FQDN
            # fqdn expected format domain/faimily/device_XX

            command_id = f"12345678_{fqdn}_Abort"
            time_to_sleep = random.uniform(0.3, 1.5)
            module_logger.info(f"Abort for {fqdn} time_sleep {time_to_sleep}")

            def throw_event_abort():
                result = (
                    command_id,
                    json.dumps([0, f"Abort completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(
                    fqdn, "obsstate", ObsState.ABORTED
                )
                callback2 = cls.event_subscription_map[fqdn]["obsstate"]
                time.sleep(time_to_sleep)
                callback(_event)
                callback1(_event1)
                callback2(_event2)

            threading.Thread(target=throw_event_abort).start()
            return [[ResultCode.QUEUED], [command_id]]

        if command_name == "restart":

            command_id = f"12345678_{fqdn}_Restart"
            time_to_sleep = random.uniform(0.3, 1.5)
            module_logger.info(
                f"Restart for {fqdn} time_sleep {time_to_sleep}"
            )

            def throw_event_restart():
                result = (
                    command_id,
                    json.dumps([0, f"Restart completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(fqdn, "obsstate", ObsState.EMPTY)
                callback2 = cls.event_subscription_map[fqdn]["obsstate"]
                time.sleep(time_to_sleep)
                callback(_event)
                callback1(_event1)
                callback2(_event2)

            threading.Thread(target=throw_event_restart).start()
            return [[ResultCode.QUEUED], [command_id]]

        if command_name == "obsreset":

            command_id = f"12345678_{fqdn}_ObsReset"
            time_to_sleep = random.uniform(0.3, 1.5)
            module_logger.info(
                f"ObsReset for {fqdn} time_sleep {time_to_sleep}"
            )

            def throw_event_obsreset():
                result = (
                    command_id,
                    json.dumps([0, f"Obsreset completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(fqdn, "obsstate", ObsState.IDLE)
                callback2 = cls.event_subscription_map[fqdn]["obsstate"]
                time.sleep(time_to_sleep)
                callback(_event)
                callback1(_event1)
                callback2(_event2)

            threading.Thread(target=throw_event_obsreset).start()
            return [[ResultCode.QUEUED], [command_id]]

        if command_name == "reset":
            command_id = f"12345678_{fqdn}_Reset"
            time_to_sleep = random.uniform(0.5, 1.5)
            module_logger.info(f"Reset for {fqdn} time_sleep {time_to_sleep}")

            def throw_event_reset():
                result = (
                    command_id,
                    json.dumps([0, f"Reset completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]
                _event2 = create_dummy_event(None, None, None)
                if "control" in fqdn:
                    _event2 = create_dummy_event(
                        fqdn, "state", DevState.STANDBY
                    )
                elif "subarray" in fqdn:
                    _event2 = create_dummy_event(fqdn, "state", DevState.OFF)
                elif "beam" in fqdn:
                    _event2 = create_dummy_event(fqdn, "state", DevState.OFF)
                else:
                    pass
                callback2 = cls.event_subscription_map[fqdn]["state"]
                time.sleep(time_to_sleep)
                callback(_event)
                callback1(_event1)
                if not mock_config.mock_event:
                    callback2(_event2)
                else:
                    callback2()

            threading.Thread(target=throw_event_reset).start()
            return [[ResultCode.QUEUED], [command_id]]

    @classmethod
    def fake_get_attribute(cls, fqdn, attr_name):
        if mock_config.mock_exception:
            raise ValueError(f"Error on attribute {attr_name}")
        attr_factory = cls.attr_factory_class(fqdn)
        attr = attr_factory.create_attribute(attr_name)
        return attr

    @classmethod
    def fake_set_attribute(cls, fqdn, attr_name, value):
        attr_factory = cls.attr_factory_class(fqdn)
        attr = attr_factory.create_attribute(attr_name)
        if attr_name.lower() == "adminmode":
            value = AdminMode(value)
        attr.value = value
        _event = create_dummy_event(fqdn, attr_name, value)
        if fqdn in cls.event_id_map and attr_name in cls.event_id_map[fqdn]:
            callback = cls.event_subscription_map[fqdn][attr_name]
            threading.Thread(target=callback, args=(_event,)).start()

    @classmethod
    def mocked_class(cls, device):
        fake_class = mock.MagicMock(spec=cls.original_cls, **cls.mock_attrs)
        fake_class.subscribe_attribute.side_effect = partial(
            cls.fake_subscribe_attribute, device
        )
        fake_class.unsubscribe_attribute.side_effect = (
            cls.fake_unsubscribe_attribute
        )
        fake_class.__class__ = cls
        fake_class._get_deviceproxy.return_value = mock.MagicMock()
        fake_class.is_alive.return_value = True
        fake_class.send_command_async.side_effect = partial(
            cls.fake_command_async, device
        )
        fake_class.send_command.side_effect = partial(cls.fake_command, device)
        fake_class.get_attribute.side_effect = partial(
            cls.fake_get_attribute, device
        )
        fake_class.set_attribute.side_effect = partial(
            cls.fake_set_attribute, device
        )
        return fake_class

    def __new__(cls, *args, **kwargs):
        device_name = args[0]
        cls.instances.append(cls.mocked_class(device_name))
        cls.event_subscription_map[device_name] = {}
        cls.callback_map[device_name] = {}
        return cls.instances[-1]
