"""This module provides MockedCommand for testing"""

import logging

from mock import MagicMock

module_logger = logging.getLogger(__name__)

# pylint: disable=too-many-instance-attributes
# pylint: disable=broad-except


class MockedCommand:
    """
    Class to mock a CSP.LMC ComponentCommand.
    """

    def __init__(self, name, logger=None):
        self.name = name
        self.notify_callback = MagicMock()
        self.command_ended = MagicMock()
        self.notify_callback = MagicMock()
        self.abort_request_pending = MagicMock()
        self.depend_on = None
        self.abort_event = None
        self.aborted = True
        self.logger = logger
        self.component = MagicMock()
        self.failure_raised = False
        self.exception = False
        self.success = False

    def run(self):
        """
        Run the command on the sub-system component.
        """
        if self.exception:
            raise ValueError("Exception in run!!")
        try:
            if self.abort_event and self.abort_event.is_set():
                self.aborted = True
            else:
                self.success = True
            self.command_ended()
        except Exception as e:
            self.logger.warning(f"Error {e}")
