import json
import logging
import os

# local imports
from ska_csp_lmc_common.manager import (
    CSPControllerComponentManager,
    CSPSubarrayComponentManager,
)
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_utils import (
    create_dummy_event,
    create_dummy_event_with_errs,
)

# pylint: disable=too-many-arguments
# pylint: disable=unused-argument
# pylint: disable=redefined-builtin
# pylint: disable=invalid-name
# pylint: disable=missing-function-docstring


module_logger = logging.getLogger(__name__)


test_properties_ctrl = {
    "ConnectionTimeout": "3",
    "PingConnectionTime": "1",
    "DefaultCommandTimeout": "5",
    "CspCbf": "common-cbf/control/0",
    "CspPss": "common-pss/control/0",
    "CspPstBeams": [
        "common-pst/beam/01",
        "common-pst/beam/02",
    ],
    "CspSubarrays": [
        "common-csp/subarray/01",
        "common-csp/subarray/02",
        "common-csp/subarray/03",
    ],
}
test_properties_subarray = {
    "SubID": "1",
    "ConnectionTimeout": "3",
    "PingConnectionTime": "1",
    "DefaultCommandTimeout": "5",
    "CbfSubarray": "common-cbf/subarray/01",
    "PssSubarray": "common-pss/subarray/01",
    "PstBeams": ["common-pst/beam/01", "common-pst/beam/02"],
}


def load_json_file(
    filename,
    string=False,
    remove=None,
):
    """Return the config dictionary or string given an input file with a json
    script."""
    file_to_load = os.getcwd() + "/tests/test_data/" + filename
    try:
        with open(file_to_load, encoding="UTF-8") as json_file:
            configuration_dict = json.loads(json_file.read().replace("\n", ""))
    except FileNotFoundError:
        # this is a patch to don't let integration tests fail in collection
        configuration_dict = {}
    if remove:
        if not isinstance(remove, list):
            raise Exception("'remove' is not a list")
        for item in remove:
            module_logger.warning(f"Removing {item}")
            configuration_dict.pop(item)

    output = configuration_dict
    if string:
        output = json.dumps(configuration_dict)
    return output


# pylint: disable-next=missing-class-docstring
class TestBase:
    dummy_event = {}
    devices_list = {}

    @classmethod
    def raise_event(cls, attr_name, devices=None):
        """Method used to fire an event using the event map built through the
        mock of the subscription. At each subscription call, the
        event_subscription_map dictionary is updated, recording for each device
        firing the event, the event callback to invoke at event reception and
        the fake event.

        :param attr_name: the attribute name
        :type attr_name: string
        :param devices: the list of devices firing the event

        :rtype: list of string
        """
        if not devices:
            devices = list(cls.devices_list.values())
        for device in devices:
            if MockedConnector.event_subscription_map[device]:
                MockedConnector.event_subscription_map[device][attr_name](
                    cls.dummy_event[device][attr_name]
                )

    @classmethod
    def _create_events(cls, attr_name, value=None, devices=None, err=False):
        """Create a set of mock events for the devices specified into the input
        list. Events can configured to contains both valid value or errors.

        :param attr_name: the attribute to subscribe
        :type attr_name: string
        :param value: the expected value for a valid event, None otherwise
        :type value:
        :param devices: the list of devices firing the event
        :type devices: list of string
        :param err: flag to create a valid or an erroneous event
        :type err: boolean

        :return: the event
        """
        if not devices:
            devices = list(cls.devices_list.values())
        for device in devices:
            if err:
                _event = create_dummy_event_with_errs(device, attr_name)
            else:
                _event = create_dummy_event(device, attr_name, value)
            if device in cls.dummy_event:
                cls.dummy_event[device].update({attr_name: _event})
            else:
                cls.dummy_event[device] = {}
                cls.dummy_event[device][attr_name] = _event

    @classmethod
    def create_events_with_errs(cls, attr_name, devices=None):
        cls._create_events(attr_name, value=None, devices=devices, err=True)

    @classmethod
    def create_events(cls, attr_name, value, devices=None):
        cls._create_events(attr_name, value=value, devices=devices)

    @classmethod
    def raise_event_on_component_run(
        cls,
        attr_name,
        value,
        sub_system,
        command,
        asynch=True,
        argument=None,
        callback=None,
    ):
        """This method mock the Component run method. This method has as input:

        - the command name
        - the asynch flag
        - the argument
        - the callback
        The method is executed with partial so that it is possible to pass to
        it also the attribute configured to fire the event and the sub-system.
        The first tow arguments are the extra information sent via partial
        """
        cls.create_events(
            attr_name,
            value,
            devices=[
                sub_system,
            ],
        )
        cls.raise_event(
            attr_name,
            devices=[
                sub_system,
            ],
        )

    @classmethod
    def raise_event_on_sub_system(
        cls, attr_name, value, sub_system, argument, callback
    ):
        """Helper function to raise a change event on a CSP sub-system
        attribute."""
        cls.create_events(
            attr_name,
            value,
            devices=[
                sub_system,
            ],
        )
        cls.raise_event(
            attr_name,
            devices=[
                sub_system,
            ],
        )

    @classmethod
    def raise_exception_on_sub_system(cls, sub_system, argument, callback):
        raise ValueError(f"Failure on {cls.devices_list[sub_system]}")


# pylint: disable-next=missing-class-docstring
class TestBaseController(TestBase):
    devices_list = {
        "cbf-ctrl": test_properties_ctrl["CspCbf"],
        "pss-ctrl": test_properties_ctrl["CspPss"],
        "pst-beam-01": test_properties_ctrl["CspPstBeams"][0],
        "pst-beam-02": test_properties_ctrl["CspPstBeams"][1],
        "subarray_01": test_properties_ctrl["CspSubarrays"][0],
        "subarray_02": test_properties_ctrl["CspSubarrays"][1],
        "subarray_03": test_properties_ctrl["CspSubarrays"][2],
    }

    @classmethod
    def raise_event_on_pss(
        cls, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a PSS Controller
        attribute."""
        cls.raise_event_on_sub_system(
            attr_name, value, cls.devices_list["pss-ctrl"], argument, callback
        )

    @classmethod
    def raise_exception_on_pss(cls, pss, argument, callback):
        """Helper function to raise a change event on a PSS Controller
        attribute."""
        cls.raise_exception_on_sub_system("pss-ctrl", argument, callback)

    @classmethod
    def raise_event_on_cbf(
        cls, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a CBF Controller
        attribute."""
        cls.raise_event_on_sub_system(
            attr_name, value, cls.devices_list["cbf-ctrl"], argument, callback
        )

    @classmethod
    def raise_exception_on_cbf(cls, argument, callback):
        """Helper function to raise a change event on a PST Controller
        attribute."""
        cls.raise_exception_on_sub_system("cbf-ctrl", argument, callback)

    @classmethod
    def raise_event_on_pst(
        cls, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a PST Controller
        attribute."""
        cls.raise_event_on_sub_system(
            attr_name, value, cls.devices_list["pst-ctrl"], argument, callback
        )

    @classmethod
    def raise_event_on_subarray(
        cls, number, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a CSP subarray."""
        subarray_name = f"subarray_{number}"
        cls.raise_event_on_sub_system(
            attr_name,
            value,
            cls.devices_list[subarray_name],
            argument,
            callback,
        )

    @classmethod
    def raise_event_on_pst_beams(
        cls, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a PST Subarray
        attribute."""
        cls.raise_event_on_sub_system(
            attr_name,
            value,
            cls.devices_list["pst-beam-01"],
            argument,
            callback,
        )
        cls.raise_event_on_sub_system(
            attr_name,
            value,
            cls.devices_list["pst-beam-02"],
            argument,
            callback,
        )

    @classmethod
    def raise_exception_on_pst(cls, argument, callback):
        """Helper function to raise a change event on a PST Controller
        attribute."""
        cls.raise_exception_on_sub_system("pst-ctrl", argument, callback)

    @classmethod
    def raise_callback_done_event(cls, cmd_event, argument, callback):
        MockedConnector.callback_map[cmd_event.device.dev_name()][
            cmd_event.cmd_name
        ](cmd_event)

    @classmethod
    def set_controllers_to_state(cls, object, state):
        cls.raise_event_on_cbf("state", state)
        cls.raise_event_on_pss("state", state)
        cls.raise_event_on_pst_beams("state", state)
        # pylint: disable-next=consider-merging-isinstance
        if isinstance(object, CSPSubarrayComponentManager) or isinstance(
            object, CSPControllerComponentManager
        ):
            probe_poller(object.op_state_model, "op_state", state, time=2)
        else:
            probe_poller(object, "state", state, time=2)

    @classmethod
    def set_subarrays_to_state(cls, object, state):
        cls.raise_event_on_subarray("01", "state", state)
        cls.raise_event_on_subarray("02", "state", state)
        cls.raise_event_on_subarray("03", "state", state)
        # pylint: disable-next=consider-merging-isinstance
        if isinstance(object, CSPSubarrayComponentManager) or isinstance(
            object, CSPControllerComponentManager
        ):
            probe_poller(object.op_state_model, "op_state", state, time=5)
        else:
            probe_poller(object, "state", state, time=5)

    @classmethod
    def set_subsystems_and_go_to_state(cls, object, state):
        cls.set_controllers_to_state(object, state)
        cls.set_subarrays_to_state(object, state)


# pylint: disable-next=missing-class-docstring
class TestBaseSubarray(TestBase):

    devices_list = {
        "cbf-subarray": test_properties_subarray["CbfSubarray"],
        "pss-subarray": test_properties_subarray["PssSubarray"],
        "pst-beam-01": test_properties_subarray["PstBeams"][0],
        "pst-beam-02": test_properties_subarray["PstBeams"][1],
    }

    @classmethod
    def raise_event_on_pss_subarray(
        cls, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a PSS Subarray
        attribute."""
        cls.raise_event_on_sub_system(
            attr_name,
            value,
            cls.devices_list["pss-subarray"],
            argument,
            callback,
        )

    @classmethod
    def raise_event_on_cbf_subarray(
        cls, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a CBF Subarray
        attribute."""
        cls.raise_event_on_sub_system(
            attr_name,
            value,
            cls.devices_list["cbf-subarray"],
            argument,
            callback,
        )

    @classmethod
    def raise_event_on_pst_beams(
        cls, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a PST Subarray
        attribute."""
        cls.raise_event_on_sub_system(
            attr_name,
            value,
            cls.devices_list["pst-beam-01"],
            argument,
            callback,
        )
        cls.raise_event_on_sub_system(
            attr_name,
            value,
            cls.devices_list["pst-beam-02"],
            argument,
            callback,
        )

    @classmethod
    def set_subsystems_and_go_to_obsstate(cls, object, obsstate):
        cls.raise_event_on_cbf_subarray("obsstate", obsstate)
        cls.raise_event_on_pss_subarray("obsstate", obsstate)
        if isinstance(object, CSPSubarrayComponentManager):
            probe_poller(object.obs_state_model, "obs_state", obsstate, time=2)
        else:
            probe_poller(object, "obsstate", obsstate, time=2)

    @classmethod
    def set_subsystems_and_go_to_state(cls, object, state):
        cls.raise_event_on_cbf_subarray("state", state)
        cls.raise_event_on_pss_subarray("state", state)
        cls.raise_event_on_pst_beams("state", state)
        # pylint: disable-next=consider-merging-isinstance
        if isinstance(object, CSPSubarrayComponentManager) or isinstance(
            object, CSPControllerComponentManager
        ):
            probe_poller(object.op_state_model, "op_state", state, time=1)
        else:
            probe_poller(object, "state", state, time=1)

    file_path = os.path.dirname(os.path.abspath(__file__))

    @classmethod
    def configuration_input(
        cls,
        filename="test_ConfigureScan_basic.json",
        string=False,
        remove=None,
    ):
        """Create the config string for CSP-CBF."""
        return load_json_file(filename=filename, string=string, remove=remove)

    @classmethod
    def assignresources_input(
        cls,
        filename="test_AssignResources_basic.json",
        string=False,
        remove=None,
    ):
        return load_json_file(filename=filename, string=string, remove=remove)
