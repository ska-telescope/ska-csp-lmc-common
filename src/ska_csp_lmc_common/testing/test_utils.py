import logging

import mock
import tango

# pylint: disable=too-many-arguments
# pylint: disable=unused-argument
# pylint: disable=redefined-builtin
# pylint: disable=invalid-name
# pylint: disable=missing-function-docstring


module_logger = logging.getLogger(__name__)


class SideEffect:
    """Callable class for the side-effect, which is maintaining some state
    about which underlying function to actually call, consecutively.

    From: https://python.tutorialink.com/pytest-mock-multiple-calls-of-same-
    method-with-different-side_effect/
    """

    def __init__(self, *fns):
        self.fs = iter(fns)

    def __call__(self, *args, **kwargs):
        f = next(self.fs)
        return f(*args, **kwargs)


def mocked_dev_error(reason):
    """Create a mocked DevError object.

    :param reason: the original failure reason
    :type reason: string
    """
    dev_error = mock.MagicMock()
    dev_error.desc = "Test error!!"
    dev_error.reason = reason
    return dev_error


def mock_cbk_with_err(command_name, fqdn):
    """Create a mocked CmdDone event with the *err* field set.

    :param command_name: the name of the command to execute
    :type command_name: string
    :param fqdn: the name of the device on which the command
    has been invoked
    :type fqdn: string
    :return: the mocked CmdDone event
    """
    fake_cmd_event = mock.MagicMock()
    fake_cmd_event.err = True
    fake_cmd_event.cmd_name = f"{command_name}"
    fake_cmd_event.device.dev_name.side_effect = lambda *args, **kwargs: fqdn
    fake_cmd_event.errors = (mocked_dev_error("API_CommandFailed"),)
    return fake_cmd_event


def mock_cbk_with_cmd_result(command_name, fqdn, result):
    """
    Create a mocked CmdDone event with the result
    code of the command = FAILED.
    :param command_name: the name of the command to execute
    :type command_name: string
    :param fqdn: the name of the device on which the command
    has been invoked
    :type fqdn: string
    :return: the mocked CmdDone event
    """
    fake_cmd_event = mock.MagicMock()
    fake_cmd_event.err = False
    fake_cmd_event.cmd_name = f"{command_name}"
    fake_cmd_event.device.dev_name.side_effect = lambda *args, **kwargs: fqdn
    fake_cmd_event.argout = result
    return fake_cmd_event


def create_dummy_event(fqdn, attr_name, event_value):
    """Create a mocked DataEvent object to test the event callback method
    associate to the attribute at subscription.

    :param fqdn: the subordinate component FQDN
    :type fqdn: string
    :param attr_name: the name of the attribute
    :type attr_name: string
    :param event_value: the event value
    :type event_value: type(attr_name)

    :return: the fake event
    """
    fake_event = mock.Mock()
    fake_event.err = False
    fake_event.attr_name = f"{fqdn}/{attr_name}"
    fake_event.attr_value.value = event_value
    fake_event.attr_value.name = attr_name
    fake_event.errors = ()
    fake_event.device.name = fqdn
    fake_event.device.dev_name.side_effect = lambda *args, **kwargs: fqdn
    return fake_event


def create_dummy_event_with_errs(fqdn, attr_name):
    """Create a mocked DataEvent object with errors. Used to test the event
    callback method registered at attribute subscription.

    :param fqdn: the device FQDN
    :type fqdn: string
    :param attr_name: the attribute name
    :type attr_name: string

    :return: the fake event
    """

    def fake_is_alive():
        tango.Except.throw_exception(
            "API_CantConnectToDevice",
            "This is error message for devfailed",
            " ",
            tango.ErrSeverity.ERR,
        )

    fake_event = mock.Mock()
    fake_event.err = True
    fake_event.attr_name = f"{fqdn}/{attr_name}"
    fake_event.attr_value = None
    fake_event.errors = (mocked_dev_error("API_EventTimeout"),)
    fake_event.device.name = fqdn
    fake_event.device.ping = fake_is_alive
    fake_event.device.dev_name.side_effect = lambda *args, **kwargs: fqdn
    return fake_event
