# The following entries allow to configure the response of
# the mocked device when a command is invoked on it.
#
# mock_exception:   raise a ValueError exeception on the device specified
#                   by the mock_fail_device entry. This entry has to be
#                   specified together the mock_fail_device.
# mock_timeout:     simulate a timeout on the device specified by the
#                   mock_fail_device entry
# mock_cmd_fail:    configure the failing command.
# mock_fail_device: configure the failing device
# mock_event_failure: no event is generated on attributes or commands
# mock_event:       whether the invoked command raises an event on
#                   the state/obsstate attribute different from the
#                   expected one. This entry has to be specified together
#                   the mock_event_value
# mock_event_value: the value of the event raised on the state/obsstate
#                   when the command is invoked. This entry has to be specified
#                   together the mock_event
# mock_cbk_cmd:     whether the command raises a CmdDone event. If True this
#                   entry has to be specified together the mock_cbk_cmd_result
# mock_cbk_cmd_result: the command result returned by the registered command
#                   callback. This entry has to be specified together the
#                   mock_cbk_cmd.
# mock_cbk_cmd_err: whether the CmdDone event has the flag error set. This
#                   entry has to be specified together the mock_fail_device
#                   and mock_cmd_fail.

mock_exception = False  # command exception
mock_timeout = False  # timeout condition
mock_cmd_fail = False  # the failing command
mock_fail_device = None  # the failing device
mock_event_value = None
mock_event_failure = False  # events are not generated
mock_event = False  # command arise a wrong value for the event
mock_cbk_cmd = False  # flag to signal the use of command callback
mock_cbk_cmd_result = (
    None  # the command result returned by the registered callback
)
mock_cbk_cmd_err = False  # the CmdDone event has flag error True
