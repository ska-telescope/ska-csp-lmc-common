import logging

from ska_control_model import AdminMode, HealthState, ObsMode, ObsState
from ska_control_model.pst_processing_mode import PstProcessingMode
from tango import DevState

module_logger = logging.getLogger(__name__)

# pylint: disable=dangerous-default-value
# pylint: disable=protected-access
# pylint: disable=unused-argument
# pylint: disable=inconsistent-return-statements
# pylint: disable=missing-function-docstring
# pylint: disable=missing-class-docstring


class AttributeDataFactory:
    attributes_ctrl_default = {
        "ondurationexpected": 1,
        "offdurationexpected": 1,
        "standbydurationexpected": 1,
        "healthstate": HealthState.OK,
        "adminmode": AdminMode.ONLINE,
        "state": DevState.STANDBY,
        "versionid": "0.16.0",
        "pstbeamsaddresses": ["low-pst/beam/01", "low-pst/beam/02"],
        "longrunningcommandstatus": ("", ""),
        "longrunningcommandprogress": ("", 0.0),
        "longrunningcommandresult": ("", [0, ""]),
    }

    attributes_subarray_default = {
        "ondurationexpected": 1,
        "offdurationexpected": 1,
        "assignresourcesdurationexpected": 3,
        "endscandurationexpected": 0.5,
        "gotoidledurationexpected": 0.5,
        "abortdurationexpected": 1,
        "restartdurationexpected": 1,
        "obsresetdurationexpected": 1,
        "healthstate": HealthState.OK,
        "adminmode": AdminMode.ONLINE,
        "state": DevState.OFF,
        "obsstate": ObsState.EMPTY,
        "obsmode": ObsMode.IDLE,
        "versionid": "0.16.0",
        "longrunningcommandstatus": ("", ""),
        "longrunningcommandprogress": ("", 0.0),
        "longrunningcommandresult": ("", [0, ""]),
    }

    attributes_beam_default = {
        "deviceID": 1,
        "ondurationexpected": 1,
        "offdurationexpected": 1,
        "endscandurationexpected": 0.5,
        "gotoidledurationexpected": 0.5,
        "abortdurationexpected": 1,
        "restartdurationexpected": 1,
        "obsresetdurationexpected": 1,
        "healthstate": HealthState.OK,
        "adminmode": AdminMode.ONLINE,
        "state": DevState.OFF,
        "obsstate": ObsState.IDLE,
        "availablediskspace": 0.0,
        "versionid": "0.16.0",
        "channelblockconfiguration": """{"num_channel_blocks": 1,
            "channel_blocks": [
                {"destination_host": "127.0.0.1",
                 "destination_port": 32080,
                 "start_pst_channel": 0,
                 "num_pst_channels": 432}
                 ]
                 }
            """,
        "pstprocessingmode": PstProcessingMode.IDLE.name,
        "datadropped": 0,
        "datadroprate": 0.0,
        "datareceived": 0,
        "datareceiverate": 0.0,
        "longrunningcommandstatus": ("", ""),
        "longrunningcommandprogress": ("", 0.0),
        "longrunningcommandresult": ("", [0, ""]),
    }
    attributes_test_lrc_default = {
        "healthstate": HealthState.OK,
        "adminmode": AdminMode.ONLINE,
        "state": DevState.STANDBY,
        "longrunningcommandstatus": ("12121212_Off", "COMPLETED"),
        "longrunningcommandprogress": ("12121212_Off", 0.0),
        "longrunningcommandresult": ("12121212_Off", [0, "All ok!"]),
    }
    attributes_pstcapability_default = {}

    def __init__(self, device):
        self._device = device

    def create_attribute(self, attr_name):
        if "control" in self._device:
            return AttributeData(
                attr_name, self.__class__.attributes_ctrl_default
            )
        if "subarray" in self._device:
            subarray_id = int(self._device[-2:])
            self.__class__.attributes_subarray_default["subarrayID"] = int(
                subarray_id
            )
            return AttributeData(
                attr_name, self.__class__.attributes_subarray_default
            )
        if "beam" in self._device:
            return AttributeData(
                attr_name, self.__class__.attributes_beam_default
            )
        if "pstcapability" in self._device:
            return AttributeData(
                attr_name, self.__class__.attributes_pstcapability_default
            )
        if "test" in self._device:
            return AttributeData(
                attr_name, self.__class__.attributes_test_lrc_default
            )
        raise ValueError("AttributeFactory: no valid device specified")


# pylint: disable-next=missing-class-docstring
class AttributeData:
    def __init__(self, attr_name, attributes):
        # global attributes
        self._attr_name = attr_name
        self._value = attributes

    @property
    def value(self):
        return self._value[self._attr_name]

    @value.setter
    def value(self, new_val):
        self._value[self._attr_name] = new_val
