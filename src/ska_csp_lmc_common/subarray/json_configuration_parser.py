from __future__ import annotations

import copy
import logging
from typing import Any, Dict, List, Optional

# pylint: disable=invalid-name
# pylint: disable=missing-function-docstring
# pylint: disable=too-many-branches
# pylint: disable=too-many-nested-blocks

module_logger = logging.getLogger(__name__)


class JsonConfigurationParser:
    """Creates the input for Subsystem's ComponentCommand."""

    def __init__(
        self: JsonConfigurationParser,
        component_manager,
        json_config: dict,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate the JsonConfigurationParser object.

        :param: the device component_manager
        :json_config: the dictionary with the configuration to
            apply.
        :logger: the logger for this object
        """
        self.cm = component_manager
        self.logger = logger or module_logger
        self.config = json_config
        # map between general label and subsystem's name
        # to specialize in Mid and Low
        self.key_to_subsystem_map = {
            "cbf": "cbf",
            "pss": "pss",
            "pst": "pst",
            "vlbi": "vlbi",
        }

    # def get_interface_version(self: JsonConfigurationParser) -> Tuple[int]:
    #     """Return the uri interface version as a tuple (major, minor)"""
    #     (major_version, minor_version) = (0, 0)
    #     if "interface" in self.config:
    #         version = self.config["interface"]
    #         version_num = version.rsplit("/", 1)[1]
    #         (major_version, minor_version) = version_num.split(".")
    #     return int(major_version), int(minor_version)

    def unused_section_for_subsystem(
        self: JsonConfigurationParser, subsystem: str
    ) -> List[str]:
        """
        Return the list of the subsystems whose configuration
        has to been removed from the main configuration dictionry.
        The configuration for the subsystem of interest is built by removing
        the sections related to other subsystems that may be present in
        the overall configuration.

        :param subsystem: the subsystem name: cbf, pss, pst, vlbi

        :return: the list of subsystem whose section has to be skipped
        """
        cbf_key = self.key_to_subsystem_map["cbf"]
        subsystem = self.key_to_subsystem_map[subsystem]
        vlbi_key = self.key_to_subsystem_map["vlbi"]
        list_of_sub_systems_to_skip = {
            cbf_key: ["pss", "pst", vlbi_key],
            "pss": [cbf_key, "pst", vlbi_key],
            "pst": [cbf_key, "pss", vlbi_key],
            vlbi_key: [cbf_key, "pss", "pst"],
        }
        return list_of_sub_systems_to_skip[subsystem]

    def get_section(self: JsonConfigurationParser, name: str) -> Any:
        """Returns the correspondent section of json configuration.

        :param name: the name of requested section
        :return: the section requested
        """
        try:
            return self.config[name]
        except KeyError as e:
            self.logger.error(e)
            raise ValueError(e) from e

    def get_id(self: JsonConfigurationParser) -> str:
        """Return the json configuration id.

        :return: the value of configuration id on success, otherwise an empty
            string.
        """

        config_id = ""
        try:
            common_sec = self.get_section("common")
            config_id = common_sec["config_id"]
        except (KeyError, ValueError):
            self.logger.debug("Configuration ID not specified")
        return config_id

    def generate_subsystem_config_dict(
        self: JsonConfigurationParser, sub_system: str
    ) -> Dict:
        """
        Generate the dictionary with the configuration for the
        specified sub-system.

        :param sub_system: the sub-system name: 'cbf',
            'pss', 'pst', 'vlbi'
        :return: a dictionary with the scan configuration for the
            sub-system.
        """
        list_of_sub_systems_to_skip = self.unused_section_for_subsystem(
            sub_system
        )
        output_config_dict = {}

        sub_system_key = self.key_to_subsystem_map[sub_system]
        # The dictionary for the sub-system is extracted only when
        # the sub-system key is present and the content of its section
        # is not empty
        if sub_system_key in self.config and self.config[sub_system_key]:
            # copy the dictionary to not modify the original one
            output_config_dict = copy.deepcopy(self.config)
            # build the dictionary with only information needed by
            # the required sub-system
            for to_remove in list_of_sub_systems_to_skip:
                if to_remove in output_config_dict:
                    output_config_dict.pop(to_remove)
        return output_config_dict

    def configure(self: JsonConfigurationParser) -> dict:
        """Retrieve the configuration for each sub-system.

        :return: a dictionary with the extracted resources for each sub-system.
            The dictionary is addressable via the sub-system fqdn.
        """
        resources_dict = {}
        try:
            # to avoid problem with data format for the value, we cast to int
            # both the read and the device property values
            common_dict = self.get_section("common")
            sub_id = int(common_dict["subarray_id"])
            if sub_id != int(self.cm.sub_id):
                raise ValueError(
                    "Wrong subarray id specified in the configuration script:"
                    f" value {common_dict['subarray_id']} does"
                    " not match the CSP Subarray ID"
                    f" ({int(self.cm.sub_id)})"
                )
            if self.generate_subsystem_config_dict("cbf"):
                resources_dict[self.cm.CbfSubarrayFqdn] = (
                    self.generate_subsystem_config_dict("cbf")
                )
                self.logger.debug(
                    f"cbf {resources_dict[self.cm.CbfSubarrayFqdn]}"
                )
            if self.generate_subsystem_config_dict("pss"):
                resources_dict[self.cm.PssSubarrayFqdn] = (
                    self.generate_subsystem_config_dict("pss")
                )
                self.logger.debug(
                    f"pss {resources_dict[self.cm.PssSubarrayFqdn]}"
                )
            if pst_cfg := self.generate_subsystem_config_dict("pst"):

                for fqdn in self.cm.PstBeamsFqdn:
                    resources_dict[fqdn] = pst_cfg

            if "vlbi" in self.config:
                self.logger.warning("Still to implement")
        except KeyError as k_err:
            self.logger.warning(
                f"The configuration script does not specify the key {k_err}"
            )
            raise
        except ValueError as v_err:
            self.logger.error(v_err)
            raise

        return resources_dict

    def assignresources(self: JsonConfigurationParser) -> dict:
        """Retrieve the resources to assign to each CSPvsub-system.

        To be specialized in MID and LOW.

        :return: a dictionary with the resources to assign to each sub-system.
            The dictionary is addressable via the sub-system fqdn.
        """
        resources = self.config

        try:
            resources_to_send = {}
            sub_id = resources["subarray_id"]
            if sub_id != int(self.cm.sub_id):
                self.logger.error(
                    "Wrong value for the subarray ID:"
                    f"{sub_id}/{self.cm.sub_id}"
                )
            else:
                if "dish" in resources:
                    if self.cm.device_properties.CbfSubarray:
                        resources_to_send[
                            self.cm.device_properties.CbfSubarray
                        ] = resources["dish"]
                    else:
                        self.logger.warning(
                            "receptors entry is detected in the config file "
                            "but the property CbfSubarray is not defined"
                        )
                if "pss" in resources:
                    if self.cm.device_properties.PssSubarray:
                        resources_to_send[
                            self.cm.device_properties.PssSubarray
                        ] = resources["pss"]
                    else:
                        self.logger.warning(
                            "pss entry is detected in the config file but the "
                            "property PssSubarray is not defined"
                        )
                if "pst" in resources:
                    if self.cm.PstBeamsFqdn:
                        beams_ids = resources["pst"]["beams_id"]
                        for fqdn in self.cm.PstBeamsFqdn:
                            if int(fqdn[-2:]) in beams_ids:
                                resources_to_send[fqdn] = {
                                    "subarray_id": self.cm.sub_id
                                }
                    else:
                        self.logger.warning(
                            "pst entry is detected in the config file but the "
                            "property PstBeamsFqdn is not defined"
                        )
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(
                f"Split of assignresources failed with error {e}"
            )
        return resources_to_send

    def releaseresources(self: JsonConfigurationParser) -> dict:
        """Retrieve the set of resources to release from each CSP
        sub-system.

        To be specialized in MID and LOW.

        :return: a dictionary with the resources to remove from each
            sub-system. The dictionary is addressable via the sub-system fqdn.
        """
        resources_to_remove = {}
        resources = self.config
        try:
            if "dish" in resources:
                resources_to_remove[self.cm.device_properties.CbfSubarray] = (
                    resources["dish"]
                )
            if "pss" in resources:
                resources_to_remove[self.cm.device_properties.PssSubarray] = (
                    resources["pss"]
                )
            if "pst" in resources:
                beams_ids = resources["pst"]["beams_id"]
                for fqdn in self.cm.PstBeamsFqdn:
                    if int(fqdn[-2:]) in beams_ids:
                        resources_to_remove[fqdn] = {
                            "subarray_id": self.cm.sub_id
                        }
        # pylint: disable-next=broad-except
        except Exception:
            warning_msg = (
                "releaseresources has to be implemented in Mid/Low Telescope"
            )
            self.logger.error(warning_msg)
        return resources_to_remove
