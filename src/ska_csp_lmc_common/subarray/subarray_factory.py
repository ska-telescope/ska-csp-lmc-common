from __future__ import annotations

import logging
from typing import Optional

from ska_csp_lmc_common.subarray import (
    CbfSubarrayComponent,
    PssSubarrayComponent,
    PstBeamComponent,
)

module_logger = logging.getLogger(__name__)


class ObservingComponentFactory:
    """Class to create adaptor for sub-system observing devices."""

    def __init__(
        self: ObservingComponentFactory,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate a new Observing component type object.

        :param logger: the logger to be used by this object.
        """
        self._creators = {
            "cbf": CbfSubarrayComponent,
            "pss": PssSubarrayComponent,
            "pst": PstBeamComponent,
        }
        self.logger = logger or module_logger

    # pylint: disable-next=missing-function-docstring
    def get_component(
        self: ObservingComponentFactory, fqdn: str, identifier: str
    ) -> CbfSubarrayComponent | PssSubarrayComponent | PstBeamComponent:
        """Create an ObservingComponent object for the subsystem specified
        by the FQDN and identifier.

        :param fqdn: The FQDN of the CSP subordinate sub-system
        :param identifier: a string with the name of the sub-system
        """
        creator = self._creators.get(identifier)
        if not creator:
            self.logger.error(f"identifier: {identifier}")
            self.logger.error(f"creators: {self._creators}")
            raise ValueError
        self.logger.info(f"Creating component {identifier}")
        return creator(fqdn, self.logger)
