from __future__ import annotations  # allow forward references in type hints

import logging
from typing import Callable, Dict, Optional

from ska_csp_lmc_common.observing_component import ObservingComponent

module_logger = logging.getLogger(__name__)

# pylint: disable=invalid-name
# pylint: disable=not-callable


class CbfSubarrayComponent(ObservingComponent):
    """Class to specialize communication and control of the CBF subarray."""

    def __init__(
        self: CbfSubarrayComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate a Component for the CBF subarray.

        :param fqdn: the FQDN of the CBF subarray device,
        :param logger: the logger for this object.
        """
        name = "cbf-subarray-" + fqdn[-2:]
        self._id = int(fqdn[-2:])
        self.pre_configure_callback: Callable = None

        super().__init__(fqdn, name, weight=1, logger=logger)

    # pylint: disable=unused-argument
    def update_pst_json_configuration(
        self: CbfSubarrayComponent, original_dict: Dict, updated_info: Dict
    ):
        """Update the configuration script adding the PST Beam(s)
        addresses.
        "To be specialized in Low/Mid if needed"

        :param original_dict: the original input configuration.
        :param updated_info: the updated configuration.
        """
        return original_dict
