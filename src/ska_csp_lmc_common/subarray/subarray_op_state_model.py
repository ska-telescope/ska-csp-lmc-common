# -*- coding: utf-8 -*-
#
# Code inspired by the HealthModel of the MCCS project.
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""An implementation of a opState model for subarrays."""
from __future__ import annotations

import logging
from typing import Callable

from tango import DevState

from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.model import OpStateModel

module_logger = logging.getLogger(__name__)

# pylint: disable=unused-argument

__all__ = ["SubarrayOpStateModel"]


class SubarrayOpStateModel(OpStateModel):
    """A simple operational state model that supports.

    * DevState.ON -- when the component is powered on.

    * DevState.OFF -- when the component is powered off.

    * DevState.STANDBY -- when the component is low-power mode.

    * DevState.DISABLE -- when the component is in OFFLINE administrative mode.

    * DevState.UNKNWON -- when communication with the component is not
      established.

    * DevState.FAILED -- when the component has failed
    """

    def __init__(
        self: SubarrayOpStateModel,
        op_state_init,
        op_state_changed_callback: Callable[[DevState], None],
        logger=None,
    ) -> None:
        """Initialise a new instance.

        :param op_state_init: The operation state of the device at
            initialization.
        :type op_state_init: DevState
        :param op_state_changed_callback: callback to be called whenever
            the operational state of the subarray (as evaluated by this model)
            changes.
        :type op_state_changed_callback: Callable
        """
        self.logger = logger or module_logger
        self._component_op_state = {}
        super().__init__(op_state_init, op_state_changed_callback, logger)

    @property
    def op_state(self: SubarrayOpStateModel) -> DevState:
        """Return the operational state.

        :return: the operational state state.
        :rtype: DevState
        """
        return self._op_state

    def update_op_state(self: SubarrayOpStateModel) -> None:
        """Update the operational state.

        This method calls the :py:meth:``evaluate_op_state`` method to figure
        out what the new operational state should be, and then updates the
        ``op_state`` attribute, calling the TANGO device callback if required.
        """
        op_state = self.evaluate_op_state()
        self.logger.debug(f"Final op-state: {op_state}")
        if self._op_state != op_state:
            self._op_state = op_state
            self._op_state_changed_callback(op_state)

    def evaluate_op_state(self: SubarrayOpStateModel) -> DevState:
        """Compute overall operational state of the CSP subarray device based
        on the fault and communication status of the subarray overall, together
        with the aggregation of the operational states of the subordinate sub-
        systems components.

        :return: an overall operational state of the subarray.
        :rtype: DevState
        """
        subarray_op_state = super().evaluate_op_state()
        self.logger.debug(
            f"Initial CSP subarray op-state: {subarray_op_state}"
        )
        if not self._component_op_state:
            return subarray_op_state

        # if the CSP Subarray opState is in FAULT for an internal error (i.e
        # not depending from the opStates of the sub-systems) this state has
        # to be maintained. The only way to exit from this state is to
        # Reset/Reinit the CSP Subarray.
        if (subarray_op_state == DevState.FAULT) and self._faulty:
            return subarray_op_state

        # CSP Subarray state is in DISABLE when commanded by the adminMode
        # (i.e. not depending from the opState of the sub-systems). This
        # state has to be maintained until adminMode has been re-enabled.
        if subarray_op_state == DevState.DISABLE and self.disabled:
            return subarray_op_state

        # loop on component to check if CBF has been detected
        for component, value in self._component_op_state.items():
            # skip PST beams not assigned to the subarray
            if "pst" in component.name and (not component.proxy):
                continue
            if component.weight:
                break
        else:
            # didn't find anything
            # No CBF detected ==> opState = FAILED
            self.logger.debug("No cbf component detected!")
            return DevState.FAULT

        for component, value in self._component_op_state.items():
            # skip PST beams not assigned to the subarray
            if "pst" in component.name and (not component.proxy):
                continue
            # if the CBF sub-system is DISABLE, CSP is not able to operate but
            # it can continue monitoring the other sub-systems components
            if component.weight:
                subarray_op_state = value
                if subarray_op_state in [
                    DevState.UNKNOWN,
                    DevState.DISABLE,
                    DevState.FAULT,
                ]:
                    subarray_op_state = DevState.FAULT
        return subarray_op_state

    def component_op_state_changed(
        self: SubarrayOpStateModel,
        component: Component,
        op_state: DevState | None,
    ) -> None:
        """Handle change in the operational state of a CSP sub-system
        subarray/beam.

        This is a callback hook, called by the EventManager when
        the operational state of a CSP sub-system subarray/beam changes.

        :param component: the sub-system component whose operational state has
            changed
        :type component: :py:class:`Component`
        :param op_state: the new operational state of the sub-system
            subarray/beam.
        :type op_state: DevState
        """
        if not op_state:
            op_state = component.state
        self._component_op_state[component] = op_state
        self.update_op_state()
