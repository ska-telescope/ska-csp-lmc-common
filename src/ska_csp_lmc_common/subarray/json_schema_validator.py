from __future__ import annotations

import json
import logging
from typing import Any

import jsonschema
from ska_tango_base.commands import JsonValidator
from ska_telmodel import schema

# pylint: disable=invalid-name
# pylint: disable=no-member
# pylint: disable=missing-function-docstring

module_logger = logging.getLogger(__name__)


class CspJsonValidator(
    JsonValidator
):  # pylint: disable=too-few-public-methods
    """
    An argument validator for JSON commands.

    It checks that the argument is called with no more than one
    positional argument and no keyword arguments, unpacks the positional
    argument (if any) from JSON into a dictionary, and validates against
    JSON schema.

    This class is a derivatio from JsonValidator of ska_tango_base
    """

    # pylint: disable=super-init-not-called
    def __init__(
        self: CspJsonValidator,
        command_name: str,
        input_schema=None,
        attrs_dict: dict = None,
        logger: logging.Logger | None = None,
    ) -> None:
        """
        Initialise a new instance.

        :param command_name: name of the command to be validated.
        :param input_schema: an optional schema against which the JSON
            argument should be validated. If not provided, a warning is
            issued.
        :param attrs_dict: an optional dictionary, providing the key
            device attributes that need to be checked in the JSON file
        :param logger: a logger for this validator to use
        """
        self._command_name = command_name
        self.logger = logger
        self._schema = input_schema or self.DEFAULT_SCHEMA
        self._attrs_dict = attrs_dict

    def validate(
        self: CspJsonValidator,
        *args: Any,
        **kwargs: Any,
    ) -> tuple[tuple[Any, ...], dict[str, Any]]:
        """
        Validate the command arguments.

        Checks that there is only one positional argument and no keyword
        arguments; unpacks the positional argument from JSON into a
        dictionary; and validate against the provided JSON schema.

        If "interface" is present int the input, use the telescope-model
        library to validate. Otherwise, either a schema can be provided
        or the DEFAULT_SCHEMA will be used.

        :param args: positional args to the command
        :param kwargs: keyword args to the command

        :returns: validated args and kwargs
        """
        assert not kwargs, (
            f"Command {self._command_name} was invoked with kwargs. "
            "JSON validation does not permit kwargs"
        )
        if args:
            assert len(args) == 1, (
                f"Command {self._command_name} was invoked "
                "with {len(args)} args. "
                "JSON validation only permits one positional argument."
            )
            decoded_dict = json.loads(args[0])

            if not decoded_dict:
                raise ValueError("Input json is Empty, command is rejected")

            if "interface" in decoded_dict.keys():
                # FIX: transaction_id is no present in the TelescopModel
                if "transaction_id" in decoded_dict.keys():
                    decoded_dict.pop("transaction_id")
                # strict validation raises an exception
                schema.validate(
                    decoded_dict["interface"], decoded_dict, strictness=2
                )
                if self._command_name.lower() == "assignresources":
                    self._validate_assignresources(
                        decoded_dict, self._attrs_dict
                    )
                elif self._command_name.lower() == "configure":
                    self._validate_configure(decoded_dict, self._attrs_dict)
                elif self._command_name.lower() == "releaseresources":
                    self._validate_releaseresources(
                        decoded_dict, self._attrs_dict
                    )
            else:
                warning_msg = (
                    f"JSON argument to command {self._command_name} "
                    "will only be validated as a dictionary."
                )
                jsonschema.validate(decoded_dict, self._schema)
                if self.logger:
                    self.logger.warning(warning_msg)
        else:
            decoded_dict = {}

        return (), decoded_dict

    def _validate_assignresources(
        self: CspJsonValidator, decoded_dict: dict, attrs_dict: dict
    ):
        """
        Coherence checks for the assingresources command

        :param decoded_dict: json input file decoded as a dictionary
        :param attrs_dict: device attributes to be checked in the json file

        :raise: ValueError if coherence check fails
        """
        # Validate subarray_id
        self._validate_subarray_id(decoded_dict, attrs_dict)
        # Validate pst beams
        self._validate_pst_beams_to_assign(decoded_dict, attrs_dict)

    def _validate_configure(
        self: CspJsonValidator, decoded_dict: dict, attrs_dict: dict
    ):
        """
        Coherence checks for the configure command

        :param decoded_dict: json input file decoded as a dictionary
        :param attrs_dict: device attributes to be checked in the json file
        """
        # Check if common section is present
        if "common" not in decoded_dict:
            self._log_and_raise_dict_error(
                KeyError, "common section not found"
            )
        # Validate subarray_id
        self._validate_subarray_id(decoded_dict, attrs_dict)

    def _validate_releaseresources(
        self: CspJsonValidator, decoded_dict: dict, attrs_dict: dict
    ):
        """
        Coherence checks for the releaseresources command

        :param decoded_dict: json input file decoded as a dictionary
        :param attrs_dict: device attributes to be checked in the json file
        """

    def _log_and_raise_dict_error(
        self: CspJsonValidator, exception_type, message: str
    ):
        """
        Common handling of error logging and raising

        :param exception_type: the type of error to be raised
        :param message: the error message
        """
        err = exception_type(message)
        if self.logger:
            self.logger.error(err)
        raise err

    def _validate_subarray_id(self, decoded_dict: dict, attrs_dict: dict):
        """
        Helper method to validate subarray_id from the given dictionaries.

        :param decoded_dict: json subarray subsection decoded as a dictionary
        :param attrs_dict: device attributes to be checked in the json file
        """
        # Necessary check for COMMON version of assignresources
        if "common" not in decoded_dict:
            if "subarray_id" not in decoded_dict:
                self._log_and_raise_dict_error(
                    KeyError, "subarray_id field not found"
                )
        else:
            if "subarray_id" not in decoded_dict["common"]:
                self._log_and_raise_dict_error(
                    KeyError, "subarray_id field not found"
                )
            else:
                decoded_dict = decoded_dict["common"]
        if attrs_dict is not None:
            sub_id = int(decoded_dict["subarray_id"])
            attr_sub_id = int(attrs_dict["subarray_id"])
            if sub_id != attr_sub_id:
                self._log_and_raise_dict_error(
                    ValueError,
                    f"Wrong value for the subarray ID: {sub_id}/{attr_sub_id}",
                )

    def _validate_pst_beams_to_assign(
        self: CspJsonValidator, decoded_dict: dict, attrs_dict: dict
    ) -> None:
        """
        Coherence check on the pst beams ID to assign.
        Verify if the list of beams to assign is a subset of
        the deployed PST beams.

        :param decoded_dict: input dictionary with the assignresources input
        :param attrs_dict: dictionary with the params to check

        :raise: ValueError if the coeherence check fails
        """
        # Extract the list of beam_id values from ["pst"]["beams_id"]
        if "pst" in decoded_dict:
            # the input script has been already validated so the
            # key 'beams_id' should be present. No check on it
            beam_id_list = decoded_dict["pst"]["beams_id"]
            # check if the required PST beams are in the list of the
            # deployed beams
            list_of_deployed_beams = []
            for beam in attrs_dict["deployed_timing_beams_id"]:
                list_of_deployed_beams.append(int(beam[-2:]))

            if not set(beam_id_list).issubset(set(list_of_deployed_beams)):
                self._log_and_raise_dict_error(
                    ValueError,
                    f"Incoherent configuration: the PST beam IDs "
                    f"{sorted(beam_id_list)} are not part of the "
                    f"deployed beam IDs {sorted(list_of_deployed_beams)}",
                )
            self.logger.info(
                "Semantic validation for AssignResources"
                "performed with success."
            )
