# -*- coding: utf-8 -*-
#
# Code inspired by the HealthModel of the MCCS project.
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""An implementation of a health model for subarrays."""
from __future__ import annotations

import logging
from typing import Callable

from ska_control_model import ObsState

from ska_csp_lmc_common.model import ObsStateModel
from ska_csp_lmc_common.observing_component import ObservingComponent

# pylint: disable=unused-argument

module_logger = logging.getLogger(__name__)


__all__ = ["SubarrayObsStateModel"]

# NOTE: SCANNING is not in the list because this case
#      must be handled with attention!!!
transitional_obs_state = [
    ObsState.RESTARTING,
    ObsState.RESETTING,
    ObsState.RESOURCING,
    ObsState.CONFIGURING,
    ObsState.ABORTING,
]


class SubarrayObsStateModel(ObsStateModel):
    """A simple observing state model that supports the Observing State as
    described in ADR-8.

    The subarray observing state at initialization is ObsState.EMPTY. This is
    the same value reported when communication with the component is not
    established or unknown.
    """

    def __init__(
        self: SubarrayObsStateModel,
        obs_state_init,
        obs_state_changed_callback: Callable[[ObsState], None],
        logger=None,
    ) -> None:
        """Initialise a new instance.

        :param obs_state_int: the observing state of the subarray at
            initialization.
        :type obs_state_int: ObsState
        :param obs_state_changed_callback: callback to be called whenever
            the observing state of the subarray (as evaluated by this model)
            changes.
        :type obs_state_changed_callback: Callable
        """
        self.logger = logger or module_logger
        self._component_obs_state = {}
        super().__init__(obs_state_init, obs_state_changed_callback, logger)

    def evaluate_obs_state(
        self: SubarrayObsStateModel,
    ) -> ObsState:
        """Compute overall observing state of the CSP Subarray based on the
        fault and communication status of the subarray overall, together with
        the aggregation of the operational states of the subordinate sub-
        systems components. as aggregation of the observing states of the
        subordinate sub-systems (subarrays/beams).

        :return: the overall observing State of the CSP subarray.
        :rtype: ObsState
        """
        csp_obs_state = super().evaluate_obs_state()
        self.logger.debug(
            f"Current obs_state: {ObsState(csp_obs_state).name} "
            f"action driven: {self.action_driven}"
        )
        cbf_obs_state = ObsState.EMPTY

        # if the CSP Subarray obsState is forced to FAULT or ABORTED
        # (i.e not depending from the obsState of the sub-systems)
        # this state has to be maintained. The only way to exit from
        # this state is to ObsReset/Restart/Reinit the CSP Subarray.
        if csp_obs_state == ObsState.FAULT and self.faulty:
            return csp_obs_state
        if csp_obs_state == ObsState.ABORTED and self.aborted:
            return csp_obs_state
        # if the ObsState Model is driven by the actions the current
        # obsState is returned.
        if not self.action_driven:
            # loop on all the obsState's sub-systems to retrieve the CBF one
            for component, value in self._component_obs_state.items():
                # skip PST beams not assigned to the subarray
                if "pst" in component.name and (not component.proxy):
                    continue
                if component.weight:
                    cbf_obs_state = value
                    break
            else:
                # No CBF detected ==> obs_State = FAULT
                self.logger.error("CBF sub-system not detected!")
                cbf_obs_state = ObsState.FAULT
            csp_obs_state = cbf_obs_state
            # the obsState of the other CSP sub-systems is checked only if the
            # CBF Subarray is not in FAULT
            if cbf_obs_state != ObsState.FAULT:
                for obs_state in self._component_obs_state.values():
                    if obs_state in transitional_obs_state:
                        csp_obs_state = obs_state
                        break
        return csp_obs_state

    def component_obs_state_changed(
        self: SubarrayObsStateModel,
        component: ObservingComponent,
        obs_state: ObsState | None,
    ) -> None:
        """Handle change in the observing state of the CSP sub-system
        subarrays/beams.

        This is a callback hook, called by the EventManager when
        the observing state of a CSP sub-system subarray/beam changes.

        :param component: the CSP sub-system component whose observing state
            has changed
        :type: :py:class:`Component`
        :param obs_state: the new obsState of the sub-system subarray/beam
        :type obs_state: ObsState
        """
        if not obs_state:  # check this line it should be 'is None'
            obs_state = component.obs_state
        self._component_obs_state[component] = obs_state
        self.update_obs_state()

    def perform_action(self: ObsStateModel, action: str) -> None:
        """Perform action to trigger the obs-state-model.

        :param action: The action to perform. It consists of the
            command name and the operation to be performed:
            *invoked* or *completed* separated by the char  '_'.
            For example: *assign_invoked*
        """
        transitional = {
            "assign": ObsState.RESOURCING,
            "release": ObsState.RESOURCING,
            "release_all": ObsState.RESOURCING,
            "configure": ObsState.CONFIGURING,
            "deconfigure": None,  # no initial transition
            "gotoidle": None,  # no initial transition
            "end_scan": None,  # no initial transition
            "scan": None,  # no initial transition
            "abort": ObsState.ABORTING,
            "obsreset": ObsState.RESETTING,
            "restart": ObsState.RESTARTING,
        }
        try:
            super().perform_action(action)
            # look for the last occurence of the '_' char to take
            # into account commands with '_' inside the name as
            # for example 'end_scan'
            command_name, request = action.rsplit("_", 1)
            obs_state = transitional[command_name]
            if request == "invoked":
                self.component_action_driven(True)
                if obs_state:
                    self.logger.debug(f"Command {command_name} invoked")
                    # set and update the observing state
                    self.obs_state = obs_state
            elif request == "completed":
                if self.obs_state == transitional[command_name] or (
                    not obs_state and self.obs_state != ObsState.ABORTING
                ):
                    self.logger.debug(f"Command {command_name} completed")
                    self.component_action_driven(False)
                    # force the update of the observing state
                    self.update_obs_state()
            else:
                raise ValueError(f"Invalid action {request}")
        except KeyError as err:
            self.logger.error(f"Invalid command {command_name} err: {err}")
        except ValueError as err:
            self.logger.error(f"Error in performing action {action}: {err}")
        # pylint: disable-next=broad-except
        except Exception as err:
            self.logger.error(
                f"General error in performing action {action}: {err}"
            )
