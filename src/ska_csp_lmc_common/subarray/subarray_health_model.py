# -*- coding: utf-8 -*-
#
# Code inspired by the HealthModel of the MCCS project.
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""An implementation of a health model for subarrays."""
from __future__ import annotations

import logging
from typing import Callable

from ska_control_model import AdminMode, HealthState
from tango import DevState

from ska_csp_lmc_common.model import HealthStateModel
from ska_csp_lmc_common.observing_component import ObservingComponent

# pylint: disable=too-many-branches
# pylint: disable=unused-argument
# pylint: disable=consider-using-max-builtin


module_logger = logging.getLogger(__name__)

__all__ = ["SubarrayHealthModel"]


class SubarrayHealthModel(HealthStateModel):
    """A simple health state model for the CSP Subarray that supports.

    * HealthState.OK -- when the component is fully operative.

    * HealthState.DEGRADED -- when the component is partially operative.

    * HealthState.UNKNWON -- when communication with the component is not
        established.

    * HealthState.FAILED -- when the component has failed
    """

    def __init__(
        self: SubarrayHealthModel,
        init_state: HealthState,
        health_changed_callback: Callable[[HealthState], None],
        logger=None,
    ) -> None:
        """Initialise a new instance.

        :param init_state: The health state of the subarray at initialization.
        :type init_state: HealthState
        :param health_changed_callback: a callback to be called when the
            health of the subarray (as evaluated by this model) changes
        :type health_state_callback: Callable
        """
        self._component_healths = {}
        self.logger = logger or module_logger
        # self._pst_beam_healths: dict[str, HealthState | None] = {}
        super().__init__(init_state, health_changed_callback, logger)

    def evaluate_health(
        self: SubarrayHealthModel,
    ) -> HealthState:
        """Compute overall health of the subarray.

        The overall health is based on the health and communication
        status of the subarray overall, together with the health of its
        stations and subarray beams.

        This implementation set the health of the CSP Subarray to the
        health of CBF Subarray component.

        :return: an overall health of the subarray.
        :rtype: HealthState
        """
        csp_healthstate = HealthState.OK

        def skip_unassigned_pst_beam(component):
            if "pst" in component.name and (not component.subarray_id):
                return True
            return False

        subarray_health = super().evaluate_health()
        self.logger.debug(
            f"Health  init value is {HealthState(subarray_health).name}"
            f" {self.faulty} disabled: {self.disabled}"
        )
        if not self._component_healths:
            return subarray_health

        # if is is failed, the contribute of the other component can't
        # recover
        if (subarray_health == HealthState.FAILED and self.faulty) or (
            subarray_health == HealthState.UNKNOWN and self.disabled
        ):
            return subarray_health

        for component, value in self._component_healths.items():
            # skip PST beams not assigned to the subarray
            if skip_unassigned_pst_beam(component):
                continue
            if component.weight:
                if component.state in [
                    DevState.FAULT,
                    DevState.UNKNOWN,
                    DevState.DISABLE,
                ]:
                    return HealthState.FAILED
                if csp_healthstate < value:
                    csp_healthstate = value
            else:
                if component.admin_mode not in [
                    AdminMode.ONLINE,
                    AdminMode.ENGINEERING,
                ]:
                    # skip component not administrative enabled
                    continue
                if value != HealthState.OK or component.state in [
                    DevState.FAULT,
                    DevState.UNKNOWN,
                ]:
                    if csp_healthstate < HealthState.DEGRADED:
                        csp_healthstate = HealthState.DEGRADED
        return csp_healthstate

    def component_health_changed(
        self: SubarrayHealthModel,
        component: ObservingComponent,
        health_state: HealthState | None,
    ) -> None:
        """Handle change in the health of the CSP Subarray subordinate sub-
        systems subarras/beams.

        This is a callback hook, called by the Event Manager when
        the health of a CSP Subarray subordinate sub-system changes.

        :param component: the sub-system component whose health has changed
        :type component: :py:class:`Component`
        :param health_state: the new health state of the sub-system.
        :type health_state: HealthState
        """
        if not health_state:
            health_state = component.health_state
        self._component_healths[component] = health_state
        self.update_health()
