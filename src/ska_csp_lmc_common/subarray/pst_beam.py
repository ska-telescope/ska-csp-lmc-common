from __future__ import annotations  # allow forward references in type hints

import logging
from typing import Callable, Dict, Optional

from ska_control_model import AdminMode, ObsState
from ska_control_model.pst_processing_mode import PstProcessingMode

from ska_csp_lmc_common.observing_component import ObservingComponent


# pylint: disable=unused-argument
class PstBeamComponent(ObservingComponent):
    """Adaptor class for a PstBeam device."""

    def __init__(
        self: PstBeamComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate a Component for the PST beam.

        :param fqdn: the FQDN of the PST Beam device,
        :param logger: the logger for this object.
        """
        name = "pst-beam-" + fqdn[-2:]
        super().__init__(fqdn, name, logger=logger)
        self._subarray_id = 0
        # NOTE: all of the dollowing subscription should go in a
        # dedicated capability deviceto do statistical analysis
        # of these data at the present these are too much frequent
        # and fill up logs of controller

        # self.attrs_for_change_events = {
        #     "availableDiskSpace": False,
        #     "availableRecordingTime": False,
        #     "dataReceiveRate": False,
        #     "dataReceived": False,
        #     "dataDropRate": False,
        #     "dataDropped": False,
        #     "dataRecordRate": False,
        #     "dataRecorded": False,
        #     "ringBufferUtilisation": False,
        #     "expectedDataRecordRate": False,
        # }

    @property
    def pstprocessingmode(
        self: PstBeamComponent,
    ) -> PstProcessingMode:
        """
        :return: observation modes of currently assigned beams on success,
            otherwise an empty list
        """
        return self.read_attr("pstprocessingmode")

    @property
    def subarray_id(self: PstBeamComponent):
        """Return the subarray membership."""
        return self._subarray_id

    @subarray_id.setter
    def subarray_id(self: PstBeamComponent, value: int):
        """Set the subarray membership.

        :param value: the subarray ID
        """
        self._subarray_id = value

    def set_component_disconnected(
        self: PstBeamComponent,
    ):
        """This method is called when the CSP TANGO Device adminMode is set to
        OFFLINE.

        In this case the CSP Device componentManager does no longer monitor the
        component and its information are reported as unknown. The component
        admin mode is not changed.
        """
        super().set_component_disconnected()
        self._attributes["obsstate"] = ObsState.IDLE

    def set_component_unknown(
        self: PstBeamComponent, admin_mode_value: AdminMode
    ) -> None:
        """Specialized version for observing sub-system components.

        :param admin_mode_value: the value of the CSP sub-system device
            adminMode.
        :return: None
        """
        # self._attributes['adminmode'] = admin_mode_value
        super().set_component_unknown(admin_mode_value)
        self._attributes["obsstate"] = ObsState.IDLE

    def set_component_offline(
        self: PstBeamComponent, admin_mode_value: AdminMode
    ) -> None:
        """Specialized version for observing sub-system components.

        :param admin_mode_value: the value of the CSP sub-system device
            adminMode.
        """
        super().set_component_offline(admin_mode_value)
        self._attributes["obsstate"] = ObsState.IDLE

    def releaseresources(
        self: PstBeamComponent,
        resources_dict: Dict,
        callback: Optional[Callable] = None,
    ) -> None:
        """Specialization of the releaseresources method for a PstBeam.
        The connection with the PstBeam Tango device is deleted.

        :param resources_dict: the dictionary with the subarray ID from whih
            the PST Beam has to be removed.
        :param callback: callable object invoked when command completes on the
            target TANGO Device.
        """
        sub_id = resources_dict["subarray_id"]
        if self._subarray_id == sub_id:
            self.disconnect()
            self._subarray_id = 0
        else:
            msg = f"PstBeam {self.fqdn} doesn't belong to subarray {sub_id}"
            self.logger.warning(msg)
            raise ValueError(msg)

    @property
    def channel_block_configuration(self: PstBeamComponent):
        """Return the channel block configuration"""
        return self.read_attr("channelblockconfiguration")

    def assignresources(
        self: PstBeamComponent,
        resources_dict: Dict,
        callback: Optional[Callable] = None,
    ) -> None:
        """Specialization of the method for a PstBeam.

        The connection with the PstBeam Tango device is established.

        :param resources_dict: the dictionary with the subarray ID to which
            the PST Beam is assigned.
        :param callback: callable object invoked when command completes on the
            target TANGO Device.
        """
        sub_id = resources_dict["subarray_id"]
        if not self._subarray_id:
            self._connector = self.connect()
            self.subarray_id = sub_id
        else:
            if self.subarray_id != sub_id:
                if self._connector:
                    self.logger.warning("This should not happen!")
                else:
                    self.logger.warning(
                        f"PstBeam {self.capability_id} assigned to "
                        f"subarray {self.subarray_id} instead of "
                        f"subarray {sub_id}"
                    )
                    self._subarray_id = 0
            else:
                self.logger.info(
                    f"PstBeam {self.capability_id} already assigned"
                    f" to {self._subarray_id}"
                )
