from __future__ import annotations  # allow forward references in type hints

import logging
from typing import Optional

from ska_csp_lmc_common.observing_component import ObservingComponent

# pylint: disable=invalid-name


class PssSubarrayComponent(ObservingComponent):
    """Class to specialize communication and control of the pss subarray."""

    def __init__(
        self: PssSubarrayComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate a Component for the PSS subarray.

        :param fqdn: the FQDN of the PSS subarray device,
        :param logger: the logger for this object.
        """
        name = "pss-subarray-" + fqdn[-2:]
        super().__init__(fqdn, name, logger=logger)
