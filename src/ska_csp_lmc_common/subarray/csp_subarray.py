from __future__ import annotations  # allow forward references in type hints

from ska_csp_lmc_common.observing_component import ObservingComponent


# pylint: disable-next=missing-class-docstring
class CspSubarrayComponent(ObservingComponent):
    def __init__(self, fqdn, sub_number, logger=None):
        sub_number = fqdn[-2:]
        super().__init__(fqdn, f"csp-subarray-{sub_number}", logger=logger)

    def force_health_state(self: CspSubarrayComponent, argin: bool) -> None:
        """
        Set the subarray health state to OK or FAILED based on whether
        the input value is False or True, respectively.
        """
        self.run("forcehealthstate", argument=argin, async_flag=False)
