# -*- coding: utf-8 -*-
#
# This file is part of the CspCapability project
#
# INAF - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""Base Capability Device"""

from __future__ import annotations

import logging
from typing import Any

from ska_tango_base.base import SKABaseDevice
from tango.server import attribute, device_property, run

from ska_csp_lmc_common.capability.capability_component_manager import (
    CapabilityComponentManager,
)
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)

module_logger = logging.getLogger(__name__)

# pylint: disable=attribute-defined-outside-init
# pylint: disable=protected-access
# pylint: disable=broad-except


class CspCapability(SKABaseDevice):
    """Abstract Capability device"""

    def __init__(
        self: CspCapability,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Initialise a new instance.

        :param args: positional arguments.
        :param kwargs: keyword arguments.
        """
        # This __init__ method is created for type-hinting purposes only.
        # Tango devices are not supposed to have __init__ methods,
        # And they have a strange __new__ method,
        # that calls __init__ when you least expect it.
        # So don't put anything executable in here
        # (other than the super() call).

        self._list_of_attributes = ["isCommunicating"]
        super().__init__(*args, **kwargs)

    def delete_device(self: CspCapability) -> None:
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        if not self.component_manager:
            return
        if self.component_manager.evt_manager:
            self.component_manager.evt_manager.stop()
            self.component_manager.evt_manager.unsubscribe_all_events()

    def _init_state_model(self: CspCapability) -> None:
        """Override the health and operational State models:

        current CSP.LMC implementation does no longer rely on the SKA State
        models and associated State Machine library.
        """
        super()._init_state_model()

        for attr in self._list_of_attributes:
            self.set_change_event(attr, True, False)
            # self.set_archive_event(attr, True, False)

    def set_component_manager(
        self, cm_configuration: ComponentManagerConfiguration
    ) -> CapabilityComponentManager:
        """Configure the ComponentManager for the CSP Capability device.

        :param cm_configuration: A class with all the device properties
            accessible as attributes

        :return: The CSP Capability ComponentManager
        """
        return CapabilityComponentManager(
            cm_configuration,
            update_device_property_cbk=self.update_device_attribute,
            logger=self.logger,
        )

    def create_component_manager(
        self: CspCapability,
    ) -> CapabilityComponentManager:
        """Override the base method.

        :returns: The CSP CapabilityComponentManager
        """
        cm_configuration = ComponentManagerConfiguration(
            self.get_name(), self.logger
        )
        cm_configuration.add_attributes()
        return self.set_component_manager(cm_configuration)

    def update_device_attribute(self, attr_name: str, attr_value: Any) -> None:
        """General method invoked by the ComponentManager to push an event
        on a device attribute properly configured to push events from the
        device.

        :param attr_name: the TANGO attribute name
        :param attr_value: the attribute value
        """
        if attr_name == "state":
            if attr_value != self.get_state():
                self.set_state(attr_value)
                self.push_change_event("state", attr_value)
        elif attr_name == "healthstate":
            if attr_value != self._health_state:
                self._health_state = attr_value
                self.push_change_event("healthstate", attr_value)
        else:
            self.push_change_event(attr_name, attr_value)

    @attribute(dtype="str")
    def buildState(self: CspCapability) -> str:
        """
        Read the Build State of the device.

        :return: the build state of the device
        """
        return self.component_manager.csp_build_state

    @attribute(dtype="str")
    def versionId(self: CspCapability) -> str:
        """
        Return the SW version of the current device.

        The SW version is equal to the release number
        of the CSP Helm chart and also of the containerized
        image.

        :return: the version id of the device
        """
        return self.component_manager._csp_version

    # -----------------
    # Device Properties
    # -----------------

    ConnectionTimeout = device_property(dtype="DevUShort", default_value=60)

    PingConnectionTime = device_property(dtype="DevUShort", default_value=5)

    # ----------
    # Attributes
    # ----------

    @attribute(
        dtype="DevBoolean",
        label="isCommunicating",
        doc="Whether the device is communicating with the component "
        "under control",
    )
    def isCommunicating(self: CspCapability) -> bool:
        """Whether the TANGO device is communicating with the controlled
        component."""
        return self.component_manager.is_communicating


# ----------
# Run server
# ----------


# pylint: disable-next=missing-function-docstring
def main(args=None, **kwargs):
    return run((CspCapability,), args=args, **kwargs)


if __name__ == "__main__":
    main()
