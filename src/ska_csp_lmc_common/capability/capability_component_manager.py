# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarray project
#
# INAF - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""Capability Device Component Manager Class."""
from __future__ import annotations  # allow forward references in type hints

import json
import logging
import threading
import traceback
from typing import Any, Callable, List, Optional

from ska_control_model import HealthState, ResultCode, TaskStatus
from tango import DevError, DevState

from ska_csp_lmc_common.commands.task import LeafTask, Task, TaskExecutionType
from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.manager.csp_base_component_manager import (
    CSPBaseComponentManager,
)
from ska_csp_lmc_common.manager.event_manager import CapabilityEventManager
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.observing_component import ObservingComponent

# pylint: disable=unused-argument
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments
# pylint: disable=broad-except
# pylint: disable=eval-used

module_logger = logging.getLogger(__name__)


class CapabilityComponentManager(CSPBaseComponentManager):
    """Class for Csp Capability Component Manager."""

    def __init__(
        self: CapabilityComponentManager,
        properties: ComponentManagerConfiguration,
        max_workers: Optional[int] = 5,
        update_device_property_cbk: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialize the Capability Component Manager Class

        :param properties: A class instance whose properties are the
            device properties.
        :param max_workers: the number of worker threads used by the
            ComponentManager ThreadPool
        :param update_device_property_cbk: The device method invoked
            to update the attributes. Defaults to None.
        :param logger: The device or python logger if default is None.
        """
        super().__init__(
            op_state_model=None,
            health_model=None,
            properties=properties,
            update_device_property_cbk=update_device_property_cbk,
            logger=logger,
        )

        self.device_properties = properties

        self._data_handler = self._instantiate_data_handler(logger)

        self.evt_manager = CapabilityEventManager(
            self._data_handler.update, 100, logger=logger
        )

        self.attributes_to_be_subscribed = [
            "state",
            "healthstate",
            "adminmode",
            # "simulationMode",
        ]

        self.property_master_fqdn = None
        self.master_device_name = None
        self.master_attribute_name = None
        self.subsystem_name = None
        self.master_dev = None

        self._populate_capability_specifics()

        self._sw_version = []
        self._hw_version = []
        self._fw_version = []
        self._device_id = []
        self._devices_fqdn = []

    @property
    def devices_id(self: CapabilityComponentManager) -> list[int]:
        """
        Return the list of the online subsystems id.
        """
        return self._device_id

    @property
    def devices_fw_version(self: CapabilityComponentManager) -> List[str]:
        """
        Return the list of the online subsystems firmware version.
        """
        return self._fw_version

    @property
    def devices_sw_version(self: CapabilityComponentManager) -> List[str]:
        """
        Return the list of the online subsystems software version.
        """
        return self._sw_version

    @property
    def devices_hw_version(self: CapabilityComponentManager) -> List[str]:
        """
        Return the list of the online subsystems hardware version.
        """
        return self._hw_version

    @property
    def devices_admin_mode(
        self: CapabilityComponentManager,
    ) -> List[str]:
        """
        Return the list of subsystems admin mode.
        """
        return self._data_handler.get_device_dict_entry("admin_mode")

    @property
    def devices_state(self: CapabilityComponentManager) -> List[str]:
        """
        Return the list of subsystems state.
        """
        return self._data_handler.get_device_dict_entry("state")
        # return self._fsp_data.list_of_state

    @property
    def devices_health_state(
        self: CapabilityComponentManager,
    ) -> List[str]:
        """
        Return the list of subsystems health state.
        """
        return self._data_handler.get_device_dict_entry("health_state")

    @property
    def devices_fqdn(self: CapabilityComponentManager) -> List[str]:
        """
        Return the list of subsystems FQDNs.
        """
        return self._data_handler.get_device_dict_entry("fqdn")

    @property
    def devices_json(self: CapabilityComponentManager) -> str:
        """
        Return a JSON string with the overall subdevices status.

        :return: the serialized JSON string otherwise an empty string.
        """
        try:
            return json.dumps(self._data_handler.json_dict)
        except TypeError as e:
            self.logger.error(f"Error in serialize JSON: {e}")
            return ""

    @property
    def devices_deployed(self: CapabilityComponentManager) -> int:
        """
        Return the number of devices deployed in the system.
        """
        return self._data_handler.json_dict["devices_deployed"]

    #
    # Protected Methods
    #

    def _create_init_callbacks(self: CapabilityComponentManager):
        """Method not used by this device"""
        self.logger.debug("Method not used")

    def _create_stop_callbacks(self: CapabilityComponentManager):
        """Method not used by this device"""
        self.logger.debug("Method not used")

    def _instantiate_data_handler(
        self: CapabilityComponentManager,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate the specific data handler that is meant to
        update the capability attributes"""
        raise NotImplementedError(
            "The specific DataHandler has to be instantiated at specialization"
        )

    def _populate_capability_specifics(
        self: CapabilityComponentManager,
    ) -> None:
        """Specify the specific variables of the Capability
        To be specialized in child class."""
        raise NotImplementedError(
            "The method to define the capability specifics"
            "needs to be specialized"
        )

    def _read_device_id(
        self: CapabilityComponentManager, device_online
    ) -> None:
        """Read the Software, firmware and hardware version attributes
          from an online devices. To be specialized

        :param device_online: the subsystem deviceProxy
        """
        raise NotImplementedError(
            "The method to read the device id needs to be specialized"
        )

    def _read_device_version(
        self: CapabilityComponentManager, device_online
    ) -> None:
        """Read the Software, firmware and hardware version attributes
          from an online devices.

        :param device_online: the subsystem deviceProxy
        """
        raise NotImplementedError(
            "The method to read the device version needs to be specialized"
        )

    #
    # Method for initialization
    #
    def _connect_to_master_device(
        self: CapabilityComponentManager,
        property_master_fqdn: str,
        master_device_name: str,
    ) -> None:
        """Retrieve the attribute of "master device" that allows to retrieve
        the fqdn of the subsystems devices.
        Establish a connection to the "master device" and access
        the attribute storing this information.
        "Master device" is the device that stores the information
        (e.g. CSPController, CBFAllocator, ...)

        :param: property_master_fqdn: The property of the Capability Device
            storing the information about the fqdn of the "master device"
        :param: master_device_name: The name of the "master device"

        :return: true a tuple with task status and result code.
        :raises: ValueError exception if the connection with the master fails.
        """
        master_fqdn = getattr(self.device_properties, property_master_fqdn)
        master_dev = Component(
            fqdn=master_fqdn,
            name=master_device_name,
            logger=self.logger,
        )
        _, result_code = self._connect_device(master_dev)
        if result_code == ResultCode.FAILED:
            raise ValueError(f"Connection with {master_dev} failed")
        self.master_dev = master_dev

    def _add_subsystem_components(
        self: CapabilityComponentManager, devices_fqdn: List[str]
    ):
        """
        Create the componets for the subordinate devices.
        """
        for device_fqdn in devices_fqdn:
            component = ObservingComponent(
                fqdn=device_fqdn,
                name=self.subsystem_name + device_fqdn[-2:],
                logger=self.logger,
            )
            self._components[device_fqdn] = component

    def _init_and_subscribe_devices(
        self: CapabilityComponentManager,
        list_of_devices,
        task_callback: Optional[Callable] = None,
    ) -> None:
        """
        Establish the connection with the devices to monitor: VCCs
        (Mid only), FSPs, PST beams, etc.

        :param list_of_devices: the list of devices FQDNs
        :param task_callback: registered method invoked when
            connection task ends.
        """
        self._data_handler.init(list_of_devices)
        self._start_communicating(task_callback)

    def _handler_to_invoke(
        self: CapabilityComponentManager,
        stop_event: threading.Event,
        status: Optional[TaskStatus] = None,
        result: Optional[tuple[ResultCode, str]] = None,
        progress: int | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ):
        """
        Callback invoked when the connection with all the monitored
        devices is completed.

        :param task_name: the name of the executed task
        :param result_code: the task result
        :param task_status: the task status code, if any
        :param result_msg: the message associated with the task result code.
        """
        if result and result[0] in [ResultCode.FAILED, ResultCode.OK]:

            if result[0] == ResultCode.FAILED:
                self._update_device_property_cbk(
                    "healthstate", HealthState.DEGRADED
                )
                if not self.online_components:
                    self._update_device_property_cbk(
                        "healthstate", HealthState.FAILED
                    )
                    self._update_device_property_cbk("state", DevState.FAULT)
                    self.is_communicating = False
                    return
            elif result[0] == ResultCode.OK:
                self.logger.info(
                    "Number of subsystems connected: "
                    f"{len(self.online_components)}"
                )

                if self._update_device_property_cbk:
                    self._update_device_property_cbk(
                        "healthstate", HealthState.OK
                    )
            self.is_communicating = True
            for _, component in self.online_components.items():
                if stop_event.is_set():
                    return
                # to remove when events are configured for these attributes
                # component.proxy.poll_attribute("functionmode", 1000)
                self._read_device_id(component)
                self._read_device_version(component)
                self.logger.info(f"Subscribe attribute for {component.fqdn}")
                self.subscribe_attributes_for_change_events(
                    component, self.attributes_to_be_subscribed
                )
            if self._update_device_property_cbk:
                self._update_device_property_cbk("state", DevState.ON)

    def _retrieve_devices_fqdn(self: CapabilityComponentManager) -> None:
        """
        Retrieve the FQDNs of the devices to be connected. By default, the
        FQDN list can be obtained directly from the `master device`.
        If necessary, this method can be customized.

        The term `master device` refers to:

        - The CSP Controller, for retrieving the list of PST beams
        - The Mid CBF Controller, for retrieving the list of VCCs and FPSs
        - The Low CBF Allocator, for retrieving the list of FSPs (Alveo)

        :return: the list with the FQDNs of the devices to monitor.
        :raises: ValueError exception if the information can't be retrieved.
        """
        devices_fqdn = []
        try:
            devices_fqdn = self.master_dev.read_attr(
                self.master_attribute_name
            )
            self.logger.info(
                "List of deployed subsystems retrieved: " f"{devices_fqdn}"
            )
        except Exception as exc:
            raise ValueError(
                f"Failure in getting {self.master_attribute_name}"
                f" from {self.master_device_name}"
            ) from exc
        return devices_fqdn

    def _connect_to_devices(self: CapabilityComponentManager) -> None:
        """
        Set up connections with all devices the CSP capability device must
        monitor.

        Steps:
        1. Retrieve the FQDNs of the devices to be monitored from the master
            device.
        2. Create a component object for each device.
        3. Initialize the capability data handler.
        4. Establish connections with the devices and subscribe to their
            attributes.
        """
        self.logger.debug(
            f"Starting connection to master device: {self.master_device_name}"
        )

        try:
            # Retrieve the list of FQDNs for the devices
            devices_fqdn = self._retrieve_devices_fqdn()

            if not devices_fqdn:
                self.logger.warning(
                    f"FQDNs of subsystems could not be retrieved from master"
                    f" device '{self.master_device_name}' using property "
                    f"'{self.master_attribute_name}'."
                )
                self._handle_connection_failure("FQDN retrieval failed.")
                return

            self.logger.info(
                "Successfully retrieved FQDNs from master device: "
                f"{self.master_device_name}"
            )
            self.logger.debug(f"Retrieved subsystem FQDNs: {devices_fqdn}")

            # Add subsystem components and initialize connections
            self._add_subsystem_components(devices_fqdn)
            self._init_and_subscribe_devices(
                devices_fqdn, self._handler_to_invoke
            )

            self.logger.info(
                "Successfully initialized and subscribed to all subsystems."
            )

        except Exception as e:
            error_message = (
                f"An error occurred during capability initialization: {e}"
            )
            self.logger.error(error_message)
            self.logger.debug(traceback.format_exc())
            self._handle_connection_failure(error_message)

    def _handle_connection_failure(
        self: CapabilityComponentManager, reason: str
    ) -> None:
        """
        Handle failures during device connection setup.

        :param reason: Description of the failure reason for logging.
        """
        self.logger.error(f"Device connection setup failed: {reason}")
        self._update_device_property_cbk("state", DevState.FAULT)
        self._update_device_property_cbk("healthstate", HealthState.FAILED)

    def _connection_helper(self: CapabilityComponentManager):
        """
        A helper function that can be customized for Mid/Low CSP.
        Its use prevents the need to specialize the entire
        `_start_communicating_thread` method in Mid/Low CSP.

        Establish the connection with the designated devices to monitor.
        """
        self._connect_to_devices()

    def _start_communicating_thread(
        self: CapabilityComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ):
        """
        Setup the communication with the master device.
        On success it gets from the master device the FQDNs
        of the subsystem's resources to monitor.
        """
        self.logger.info("Starting the communication thread")
        try:
            self.logger.info("Connecting to the master device")
            connection_timeout = int(self.device_properties.ConnectionTimeout)

            # Create and start a thread for connecting to the master device
            task_thread = threading.Thread(
                target=self._connect_to_master_device,
                args=(self.property_master_fqdn, self.master_device_name),
            )
            task_thread.start()
            self.logger.info(
                f"Task timeout set to: {connection_timeout} seconds"
            )

            # Wait for the thread to complete or timeout
            task_thread.join(connection_timeout)

            if task_thread.is_alive():
                self._main_task_executor.timeout_event.set()
                msg = (
                    f"Task execution exceeded {connection_timeout}"
                    " seconds timeout"
                )
                self.logger.warning(msg)
                raise TimeoutError(msg)

            if self.master_dev:
                self.logger.info("Going to connect to capability devices")
                self._connection_helper()
                return

            self.logger.error(
                f"Failure in connecting to the {self.master_device_name}"
            )

        except TimeoutError as te:
            self.logger.error(f"Timeout error: {te}")
        except Exception as e:
            self.logger.error("Failed to connect to the master device")
            self.logger.error(f"Error: {e}", exc_info=True)

        # Update device properties to reflect failure
        self._update_device_property_cbk("state", DevState.FAULT)
        self._update_device_property_cbk("healthstate", HealthState.FAILED)

    def _create_start_subtasks(
        self: CapabilityComponentManager, component: Component
    ) -> List[Task]:
        """
        Override the method in CSPBaseComponentManager.

        Create the task to initialize the connection
        with the component (sub-system).

        :param component: the target of the task

        :return: the LeafTask, otherwise None
        """
        if not component.proxy:
            return LeafTask(
                f"connect-{component.name}",
                self,
                "_connect_device",
                task_type=TaskExecutionType.INTERNAL,
                argin=component,
                logger=self.logger,
            )
        return None

    #
    # Public Methods
    #

    def start_communicating(self: CapabilityComponentManager) -> None:
        """
        Establish a connection with the devices to monitor. The list of
        device FQDNs is managed by the master device, which depending on the
        Telescope instance, could be the CSP Controller, the Cbf Controller
        in Mid, or the CBF Allocator in the Low telescope.
        This method is executed asynchronously because it can require a
        significant amount of time to complete.
        For this reason, it is submitted to the thread pool managed by
        the `ComponentManager`..
        """
        self._task_executor.submit(
            self._start_communicating_thread,
        )

    def off(
        self: CapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def on(
        self: CapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def standby(
        self: CapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def stop_communicating(
        self: CapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")
