"""pst Capability Monitor Data class."""

from __future__ import annotations  # allow forward references in type hints

import logging
from collections import deque

from ska_csp_lmc_common.utils.monitorlib import meanstd_data

module_logger = logging.getLogger(__name__)

# pylint: disable=too-many-instance-attributes


class MonitoringData:
    """Class that contains the monitoring attributes.
    The following operation are performed on the stored data (circular buffer):
    - moving average
    - standard deviation
    """

    def __init__(
        self: MonitoringData,
        attr_name: str,
        device_index: int,
        buffer_size: int = 10,
    ):

        self.attr_name = attr_name
        self.last_value = 0.0
        self.index = device_index
        self.device_avg = 0.0
        self.device_stddev = 0.0
        self.value_buffer = CircularBuffer(buffer_size)
        self.avg_buffer = CircularBuffer(buffer_size)
        self.stddev_buffer = CircularBuffer(buffer_size)
        self.json_data = {
            "value": 0.0,
            "avg_value": 0.0,
            "stddev_value": 0.0,
            "avg_buffer": [],
            "stddev_buffer": [],
        }

    def update(self: MonitoringData, value: float) -> None:
        """Update all data according to the input value
        :param value: float data
        """
        self.last_value = value
        self.value_buffer.append(value)
        if len(self.value_buffer) == 1:
            # the function meanstd_data does throw ErrorStatistical
            # exception if len(list)< 2
            self.device_avg = value
            self.device_stddev = 0.0
        else:
            (self.device_avg, self.device_stddev) = meanstd_data(
                self.value_buffer.get()
            )

        self.avg_buffer.append(self.device_avg)
        self.stddev_buffer.append(self.device_stddev)
        self.update_json()

    def update_json(self: MonitoringData) -> None:
        """Update the json data"""
        self.json_data["value"] = self.get_value()
        self.json_data["avg_value"] = self.get_avg()
        self.json_data["stddev_value"] = self.get_stddev()
        self.json_data["avg_buffer"] = self.get_avg_buffer()
        self.json_data["stddev_buffer"] = self.get_stddev_buffer()

    def get_value(self: MonitoringData) -> float:
        """
        Return the most updated value
        :return: the most updated value.
        """
        return self.last_value

    def get_json_data(self: MonitoringData) -> str:
        """
        Return the json data in string format. It contains the
        monitoring values: last_value, avg_value, stddev_value,
        avg_buffer, stddev_buffer

        :return: the json data in string format.
        """
        return self.json_data

    def get_avg(self: MonitoringData) -> float:
        """
        Return the average value calculated on the latest 10 values

        :return: the average value.
        """
        return self.device_avg

    def get_avg_buffer(self: MonitoringData) -> list[float]:
        """
        Return the list of the latest average values

        :return: the list of the latest average values.
        """
        return self.avg_buffer.get()

    def get_data_buffer(self: MonitoringData) -> list[float]:
        """
        Return the latest value stored in the circular buffer

        :return: the latest value stored in the circular buffer.
        """
        return self.value_buffer.get()

    def get_stddev(self: MonitoringData) -> float:
        """
        Return the standard deviation value calculated on the latest 10 values

        :return: the standard deviation value.
        """
        return self.device_stddev

    def get_stddev_buffer(self: MonitoringData) -> list[float]:
        """
        Return the list of the latest standard deviation values

        :return: the list of the latest standard deviation values.
        """
        return self.stddev_buffer.get()


class CircularBuffer:
    """Implement a circular buffer using deque"""

    def __init__(self: CircularBuffer, max_length=10):
        self.data = deque(maxlen=max_length)

    def __len__(self: CircularBuffer):
        """Called to implement the built-in function len()"""
        return len(self.data)

    def get(self: CircularBuffer):
        """get data in list format."""
        return [*self.data]

    def append(self: CircularBuffer, value):
        """Add a new entry to the right side."""
        if isinstance(value, int):
            value = float(value)
        self.data.append(value)

    def clear(self: CircularBuffer):
        """return and remove the leftmost item."""
        self.data.clear()
