"""Generic Capability Info Manager Class."""

from __future__ import annotations  # allow forward references in type hints

import copy
import logging
import threading
from typing import Any, Callable, Dict, List, Optional

from ska_control_model import AdminMode, HealthState
from tango import DevState

module_logger = logging.getLogger(__name__)

# pylint: disable=too-many-instance-attributes


class CapabilityDataHandler:
    """
    Class handling the update and access to the dictionary with the
    capability information
    """

    def __init__(
        self: CapabilityDataHandler,
        capability_name,
        task_callback: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """
        Class to handle the capability information.
        :param capability_name: name of the capability
        :param task_callback: the method invoked to update a device
            attribute
        :param logger: The device or python logger if default is None.
        """
        self.capability_name = capability_name
        self._update_device_property = task_callback
        self._json_dict = {}
        self._json_dict[self.capability_name] = []
        self._json_dict["devices_deployed"] = 0

        self._data_lock = threading.Lock()

        self.logger = logger or module_logger

        # map the tango attributes the key dictionary
        self.attrname_to_key_map = {
            "state": "state",
            "healthstate": "health_state",
            "adminmode": "admin_mode",
        }

        # map the tango attributes the key dictionary
        # this is necessary since the events come as integers,
        # not enum
        self.attrname_to_enum_class_map = {
            "healthstate": HealthState,
            "adminmode": AdminMode,
        }

        self.device_data = {
            "dev_id": 0,
            "fqdn": "",
            "state": str(DevState.UNKNOWN),
            "health_state": HealthState.UNKNOWN.name,
            "admin_mode": AdminMode.OFFLINE.name,
        }

        self.add_device_data()

    def init(self: CapabilityDataHandler, list_of_devices) -> None:
        """
        Initialize the internal json dictionary.
        """
        self._json_dict["devices_deployed"] = len(list_of_devices)
        for device_fqdn in list_of_devices:
            device_data = copy.deepcopy(self.device_data)
            if "." not in device_fqdn:
                # exclude the case of processor, i.e. low-csp/processor/0.0.0
                device_data["dev_id"] = int(device_fqdn[-2:])
            device_data["fqdn"] = device_fqdn
            self._json_dict[self.capability_name].append(device_data)
        if self._update_device_property:
            self._update_device_property("devicesJson", self._json_dict)

    @property
    def json_dict(self: CapabilityDataHandler) -> Dict:
        """
        Return the internal dictionary.
        """

        return self._json_dict

    @property
    def fqdn(self: CapabilityDataHandler) -> List[str]:
        """
        Return a list with the devices FQDNs.
        """
        return self._get_device_dict_entry("fqdn")

    def add_device_data(self):
        """Add to device_data dict the information specific to the capability
        "if not specified the dict remains with only default attribute"""

        self.logger.warning(
            "The capability device will subscribe only default attributes "
            f"to subsystems. They are: {self.device_data.keys()}"
        )

    def get_device_dict_entry(self: CapabilityDataHandler, dict_key) -> Any:
        """
        Return the information stored in the dictionary for the
        given key.
        The access to the internal dictionary is regulated by a lock.

        :param dict_key: the dictionary key string whose value is requested.

        :return: dictionary value related to the input key
        """

        with self._data_lock:
            return self._get_device_dict_entry(dict_key)

    def _get_device_dict_entry(self: CapabilityDataHandler, dict_key) -> Any:
        """
        Return the information stored in the dictionary for the
        given key.
        The access to the internal dictionary is regulated by a lock.

        :param dict_key: the dictionary key whose value is requested.

        :return: dictionary value related to the input key

        :raise: KeyError in case of invalid dictionary key
        """
        list_of_values = []
        if self._json_dict:
            for item in self._json_dict[self.capability_name]:
                try:
                    list_of_values.append(item[dict_key])
                except KeyError as e:
                    self.logger.warning(f"Invalid device dictionary key: {e}")
        return list_of_values

    def update_device_dict_entry(
        self, attr_name, entry_value, device_pos: Any = None
    ) -> None:
        """Update the content of the internal dictionary.
        The access to the internal dictionary is controlled by a lock

        :param entry_key: the key name of the entry to update into the
        dictionary
        :param entry_value: the value to store
        :param device_pos: if not None, the index of the device record into the
        dictionary list
        """

        with self._data_lock:
            self._update_device_dict_entry(attr_name, entry_value, device_pos)

    def _update_device_dict_entry(
        self, attr_name, entry_value, device_pos: Any = None
    ) -> None:
        """
        Update the content of the internal dictionary without locking.

        :param entry_key: the key name of the entry to update into the
        dictionary
        :param entry_value: the value to store
        :param device_pos: if not None, the index of the device record into
        the dictionary list
        """
        try:

            entry_key = self.attrname_to_key_map[attr_name]
            if device_pos is not None:

                # get the key dictionary associated with the attribute
                attr_name = [
                    _attr_name
                    for _attr_name, value in self.attrname_to_key_map.items()
                    if value == entry_key
                ]
                attr_name = attr_name[0]

                # for enum report the label
                if attr_name in self.attrname_to_enum_class_map:
                    entry_value = self.attrname_to_enum_class_map[attr_name](
                        entry_value
                    ).name

                self._json_dict[self.capability_name][device_pos][
                    entry_key
                ] = entry_value
            else:
                self._json_dict[entry_key] = entry_value
        except KeyError as err:
            self.logger.warning(f"Failed to update device entry: {err}")

    def _update_device_dict_and_attribute(self, attr_name, attr_value, idx):
        """
        Update the device_dict and the attribute
        This method exist in order to allow parsing of attributes
        in specializations

        :param attr_name: the name of the attribute
        :param attr_value: the value the attribute
        :param idx: the device index
        """

        self._update_device_dict_entry(attr_name, attr_value, idx)
        self.update_attribute(attr_name)

    def update(
        self: CapabilityDataHandler,
        device_fqdn: str,
        attr_name: str,
        attr_value: Any,
    ) -> None:
        """Method invoked when an event is received.

        Update the internal json structure and invoke the device
        update method for the following attributes of the device:

        :param device_fqdn: the device device FQDN
        :param attr_name: the name of the attribute with event
        :param attr_value: the value the attribute with event
        """
        self.logger.info(
            "Capability data handler update function: "
            f"dev fqdn: {device_fqdn} "
            f"dev attr_name: {attr_name}, type {type(attr_name)} "
            f"dev attr_value: {attr_value}, tp {type(attr_value)}"
        )
        with self._data_lock:
            # get the device position inside the internal dict
            idx = self.fqdn.index(device_fqdn)
            # get the device record
            item = self._json_dict[self.capability_name][idx]
            # update array element according to fqdn
            if device_fqdn == item["fqdn"]:
                self.logger.info(
                    "Capability data handler update function: "
                    f"dev index: {idx}"
                )

                if attr_name == "state":
                    attr_value = str(attr_value)

                self._update_device_dict_and_attribute(
                    attr_name, attr_value, idx
                )

                # following section was used to allow the update of only
                # the list of subscribed attributes. Seems useless since
                # the update is called  as a callback from component manager
                # and should update under control

                # if attr_name in self.list_of_subscribed_attributes:
                #    self._update_device_dict_entry(attr_name, attr_value, idx)

                # else:
                #     raise ValueError("Items not updated!")

            else:
                self.logger.warning(
                    "Error in updating %s. Entry mismatch received: %s"
                    " extracted: %s",
                    attr_name,
                    device_fqdn,
                    item["fqdn"],
                )

    def update_attribute(self, attr_name) -> None:
        """Method to update the correspondant attribute in the device"""
        keyword = self.attrname_to_key_map[attr_name]

        if self._update_device_property:
            self._update_device_property("devicesjson", self._json_dict)

            self._update_device_property(
                f"devices{attr_name}", self._get_device_dict_entry(keyword)
            )
