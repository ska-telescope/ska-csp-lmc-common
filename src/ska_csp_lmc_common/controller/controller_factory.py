from __future__ import annotations

import logging
from typing import Optional

from ska_csp_lmc_common.controller import (
    CbfControllerComponent,
    PssControllerComponent,
)
from ska_csp_lmc_common.subarray import CspSubarrayComponent, PstBeamComponent

module_logger = logging.getLogger(__name__)


class ControllerComponentFactory:
    """Class to create Component objects"""

    def __init__(
        self: ControllerComponentFactory,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Instantiate a new Controller component type object.

        :param logger: the logger to be used by this object.
        """
        self._creators = {
            "cbf": CbfControllerComponent,
            "pss": PssControllerComponent,
            "pst": PstBeamComponent,
            "subarray": CspSubarrayComponent,
        }
        self.logger = logger or module_logger

    def get_component(
        self: ControllerComponentFactory, fqdn: str, identifier: str
    ) -> CbfControllerComponent | PssControllerComponent:
        """Create a Component object for the subsystem specified
        by its FQDN.

        :param fqdn: The FQDN of the CSP subordinate sub-system
        :param identifier: a string with the name of the sub-system
        """
        creator = self._creators.get(identifier)
        if not creator:
            self.logger.error(f"identifier: {identifier}")
            self.logger.error(f"creators: {self._creators}")
            raise ValueError
        self.logger.info(f"Creating component {identifier}")
        return creator(fqdn, self.logger)
