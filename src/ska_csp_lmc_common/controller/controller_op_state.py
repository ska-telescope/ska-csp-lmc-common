# -*- coding: utf-8 -*-
#
# Code inspired by the HealthModel of the MCCS project.
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""An implementation of a opState model for ctrls."""
from __future__ import annotations

import logging
from typing import Callable, Optional

from tango import DevState

from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.model import OpStateModel

# pylint: disable=unused-argument
# pylint: disable=duplicate-code

module_logger = logging.getLogger(__name__)


__all__ = ["ControllerOpStateModel"]


class ControllerOpStateModel(OpStateModel):
    """A simple operational state model that supports.

    * DevState.ON -- when the component is powered on.

    * DevState.OFF -- when the component is powered off.

    * DevState.STANDBY -- when the component is low-power mode.

    * DevState.DISABLE -- when the component is in OFFLINE administrative mode.

    * DevState.UNKNOWN -- when communication with the component is not
      established.

    * DevState.FAILED -- when the component has failed
    """

    def __init__(
        self: ControllerOpStateModel,
        op_state_init: DevState,
        op_state_changed_callback: Callable[[DevState], None],
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialise a new instance.

        :param op_state_init: The operation state of the device at
            initialization.
        :type op_state_init: DevState
        :param op_state_changed_callback: callback to be called whenever
            the operational state of the controller (as evaluated by this
            model) changes.
        :type op_state_changed_callback: Callable
        """
        self.logger = logger or module_logger
        self._component_op_state = {}
        super().__init__(op_state_init, op_state_changed_callback, logger)

    @property
    def op_state(self: OpStateModel) -> DevState:
        """Return the operational state.

        :return: the operational state state.
        :rtype: DevState
        """
        return self._op_state

    def update_op_state(self: OpStateModel) -> None:
        """Update the operational state.

        This method calls the :py:meth:``evaluate_op_state`` method to figure
        out what the new operational state should be, and then updates the
        ``op_state`` attribute, calling the TANGO device callback if required.
        """
        op_state = self.evaluate_op_state()
        self.logger.debug(f"Final op-state: {op_state}")
        if self._op_state != op_state:
            self._op_state = op_state
            self._op_state_changed_callback(op_state)

    def evaluate_op_state(self: ControllerOpStateModel) -> DevState:
        """Compute overall operational state of the CSP controller device based
        on the fault and communication status of the controller overall,
        together with the aggregation of the operational states of the
        subordinate sub-systems components.

        :return: an overall operational state of the controller.
        :rtype: DevState
        """
        ctrl_op_state = super().evaluate_op_state()
        self.logger.debug(
            f"Initial CSP ctrl op-state: {ctrl_op_state} faulty:"
            f" {self.faulty} disabled: {self.disabled}"
        )
        if not self._component_op_state:
            return ctrl_op_state

        # if the CSP Controller opState is in FAULT for an internal error (i.e
        # not depending from the opStates of the sub-systems) this state has
        # to be maintained. The only way to exit from this state is to
        # Reset/Reinit the CSP Subarray.
        if (ctrl_op_state == DevState.FAULT) and self.faulty:
            return ctrl_op_state

        # CSP Controller state is in DISABLE when commanded by the adminMode
        # (i.e. not depending from the opState of the sub-systems). This state
        # has to be maintained until
        # adminMode has been re-enabled.
        if ctrl_op_state == DevState.DISABLE and self.disabled:
            return ctrl_op_state

        # loop on component to check if CBF has been detected
        for component, value in self._component_op_state.items():
            if component.weight:
                break
        else:
            # didn't find anything
            # No CBF detected ==> opState = FAILED
            self.logger.error("No cbf component detected!")
            return DevState.FAULT

        for component, value in self._component_op_state.items():
            # if the CBF sub-system is DISABLE, CSP is not able to operate but
            # it can continue monitoring the other sub-systems components
            if component.weight:
                ctrl_op_state = value
                if ctrl_op_state in [
                    DevState.UNKNOWN,
                    DevState.DISABLE,
                    DevState.FAULT,
                ]:
                    ctrl_op_state = DevState.FAULT
        return ctrl_op_state

    def component_op_state_changed(
        self: ControllerOpStateModel,
        component: Component,
        op_state: DevState | None,
    ) -> None:
        """Handle change in CSP sub-system ctrl operational state.

        This is a callback hook, called by the EventManager when
        the operational state of a CSP sub-system ctrl changes.

        :param component: the sub-system component whose operational state has
            changed
        :type component: :py:class:`Component`
        :param op_state: the new operational state of the sub-system ctrl.
        :type op_state: DevState
        """
        # only sub-systems controller contribute to evaluate the CSP
        # Controller state

        if "subarray" not in component.fqdn:
            # state and healthstate of csp controller depends
            # only on the state of subsystems controllers and pst-beams

            self._component_op_state[component] = component.state
            self.update_op_state()
