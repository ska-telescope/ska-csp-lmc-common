# -*- coding: utf-8 -*-
#
# Code inspired by the HealthModel of the MCCS project.
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""An implementation of a health model for controllers."""
from __future__ import annotations

import logging
from typing import Callable

from ska_control_model import AdminMode, HealthState
from tango import DevState

from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.model import HealthStateModel

# pylint: disable=too-many-branches
# pylint: disable=unused-argument
# pylint: disable=consider-using-max-builtin


module_logger = logging.getLogger(__name__)

__all__ = ["ControllerHealthModel"]


class ControllerHealthModel(HealthStateModel):
    """A simple health state model for the CSP Controller that supports.

    * HealthState.OK -- when the component is fully operative.

    * HealthState.DEGRADED -- when the component is partially operative.

    * HealthState.UNKNOWN -- when communication with the component is not
        established.

    * HealthState.FAILED -- when the component has failed

    """

    def __init__(
        self: ControllerHealthModel,
        init_state: HealthState,
        health_changed_callback: Callable[[HealthState], None],
        logger=None,
    ) -> None:
        """Initialise a new instance.

        :param init_state: The health state of the controller at
            initialization.
        :param health_changed_callback: a callback to be called when the
            health of the controller (as evaluated by this model) changes.
        :param logger: the logger for this object.
        """
        self._component_healths = {}
        self.logger = logger or module_logger
        # self._pst_beam_healths: dict[str, HealthState | None] = {}
        super().__init__(init_state, health_changed_callback, logger)

    def evaluate_health(
        self: ControllerHealthModel,
    ) -> HealthState:
        """Compute overall health of the controller.

        The overall health is based on the health and communication
        status of the CSP sub-system controllers.

        :return: an overall health of the controller
        """
        controller_health = super().evaluate_health()
        self.logger.debug(
            "Init controller health:"
            f" {HealthState(controller_health).name} faulty:"
            f" {self.faulty} disabled: {self.disabled}"
        )
        if not self._component_healths:
            return controller_health

        # if it is failed, the contribute of the other component can't
        # recover
        if controller_health == HealthState.FAILED and self.faulty:
            return controller_health

        if controller_health == HealthState.UNKNOWN and self.disabled:
            return controller_health

        for component, value in self._component_healths.items():
            if component.weight:
                break
        else:
            # didn't find anything
            # No CBF detected ==> healthState = FAILED
            self.logger.error("No cbf component detected!")
            return HealthState.FAILED

        csp_healthstate = HealthState.OK
        for component, value in self._component_healths.items():
            if component.weight and "control" in component.fqdn:
                if component.state in [
                    DevState.FAULT,
                    DevState.UNKNOWN,
                    DevState.DISABLE,
                ]:
                    return HealthState.FAILED
                if csp_healthstate < value:
                    csp_healthstate = value
            else:
                if component.admin_mode not in [
                    AdminMode.ONLINE,
                    AdminMode.ENGINEERING,
                ]:
                    # skip component not administrative enabled
                    continue
                if value != HealthState.OK or component.state in [
                    DevState.FAULT,
                    DevState.UNKNOWN,
                ]:
                    if csp_healthstate < HealthState.DEGRADED:
                        csp_healthstate = HealthState.DEGRADED
            self.logger.debug(
                f"[{component.name}] health: {value}"
                f" csp health: {csp_healthstate}"
            )
        return csp_healthstate

    def component_health_changed(
        self: ControllerHealthModel,
        component: Component,
        health_state: HealthState | None,
    ) -> None:
        """Handle change in the health of the CSP subordinate sub-systems
        controllers.

        This is a callback hook, called by the EventManager when the health
        and/or operational state of one of the CSP subordinate sub-system
        changes.

        :param component: the sub-system component whose health has changed.
        :param health_state: the new health state of the CSP sub-system.
        """
        self._component_healths[component] = component.health_state
        self.update_health()
