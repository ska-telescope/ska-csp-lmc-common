from __future__ import annotations

import logging
from typing import Optional

from ska_csp_lmc_common.component import Component


class PssControllerComponent(Component):
    """Adaptor class for the PSS Controller device."""

    def __init__(
        self: PssControllerComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        super().__init__(fqdn, "pss-controller", logger=logger)
