from ska_csp_lmc_common.component import Component


class CbfControllerComponent(Component):
    """Adaptor class for the CBF Controller device."""

    def __init__(self, fqdn, logger=None):
        super().__init__(fqdn, "cbf-controller", weight=1, logger=logger)
