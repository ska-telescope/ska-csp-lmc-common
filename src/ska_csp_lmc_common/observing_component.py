from __future__ import annotations  # allow forward references in type hints

import logging
from typing import Optional

# SKA imports
from ska_control_model import AdminMode, ObsMode, ObsState

# local imports
from ska_csp_lmc_common.component import Component

module_logger = logging.getLogger(__name__)


class ObservingComponent(Component):
    """Class to model a CSP subordinate observing device."""

    def __init__(
        self: ObservingComponent,
        fqdn: str,
        name: str,
        weight: int = 0,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        super().__init__(fqdn, name, weight, logger)
        # pylint: disable-next=pointless-string-statement
        """Initialize an Observing Component.

        :param fqdn: The Fully Qualified Domain Name of the TANGO device
            managed by this instance.
        :param name: the name of the component. Ex. 'cbf-subarray'
        :param weight: The 'weight' of the device in the evaluation of the
            Mid.CSP rolled-up status.
        :param logger: The device or python logger if default is None.
        """

        # if condition in self._id added to face component
        # with fqdn like low-cbf/processor/0.0.1
        self._id = int(fqdn[-2:]) if "." not in fqdn else fqdn.split(".")[-1]

        self._subarray_id = 0
        self.pre_configure_callback = None
        self.logger.debug(f"component fqdn {self.fqdn} id: {self._id}")
        # define a temporary variable to modify the name of the command
        self.tmp_command_name = ""

    @property
    def capability_id(self: ObservingComponent) -> int:
        """Return the capability device identification number.

        :return: The capability device ID.
        """
        return self._id

    @property
    def obs_state(self: ObservingComponent) -> ObsState:
        """Return the CSP Subarray sub-system obs_state.

        :return: the sub-system obsState if updated via events or via direct
            read, EMPTY on failure
        """
        return self._get_attribute("obsstate")

    @property
    def obs_mode(self: ObservingComponent) -> ObsMode:
        """Return the CSP Subarray sub-system obs_mode.

        :return: the sub-system obsMode if updated via events or via direct
            read, EMPTY on failure
        """
        return self._get_attribute("obsmode")

    ######################
    # Class public methods
    ######################

    def set_component_disconnected(self: ObservingComponent):
        """This method is called when the CSP TANGO Device adminMode is set to
        OFFLINE.

        In this case the CSP Device componentManager does no longer monitor the
        component and its information are reported as unknown. The component
        admin mode is not changed.
        """
        super().set_component_disconnected()
        self._attributes["obsstate"] = ObsState.EMPTY
        self._attributes["obsmode"] = ObsMode.IDLE

    def set_component_unknown(
        self: ObservingComponent, admin_mode_value: AdminMode
    ) -> None:
        """Specialized version for observing sub-system components.

        :param admin_mode_value: the value of the CSP sub-system device
            adminMode.
        :return: None
        """
        self._attributes["obsstate"] = ObsState.EMPTY
        self._attributes["obsmode"] = ObsMode.IDLE
        # self._attributes['adminmode'] = admin_mode_value
        super().set_component_unknown(admin_mode_value)

    def set_component_offline(
        self: ObservingComponent, admin_mode_value: AdminMode
    ) -> None:
        """Specialized version for observing sub-system components.

        :param admin_mode_value: the value of the CSP sub-system device
            adminMode.
        """
        super().set_component_offline(admin_mode_value)
        self._attributes["obsstate"] = ObsState.EMPTY
        self._attributes["obsmode"] = ObsMode.IDLE
        self._attributes["adminmode"] = admin_mode_value
