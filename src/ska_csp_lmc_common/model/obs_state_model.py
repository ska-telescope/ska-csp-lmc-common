# -*- coding: utf-8 -*-
#
# Code inspired by the HealthModel of the MCCS project.
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""This module implements infrastructure for obsState management in CSP.LMC
sub-system."""

from __future__ import annotations  # allow forward references in type hints

import logging
from threading import RLock
from typing import Callable, Optional

from ska_control_model import ObsState

module_logger = logging.getLogger(__name__)

__all__ = ["ObsStateModel"]

# pylint: disable=too-many-instance-attributes


class ObsStateModel:
    """A simple observing state model for observing devices."""

    def __init__(
        self: ObsStateModel,
        obs_state_init: ObsState,
        obs_state_changed_callback: Callable[[ObsState], None],
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialise a new instance.

        :param obs_state_int: the observing state of the component under
            control at initialization.
        :param obs_state_changed_callback: callback to be called whenever
            the observing state of the component under control (as evaluated
            by this model) changes.
        :param logger: a logger for this instance
        """
        self._lock = RLock()
        self._disabled = False
        self._faulty = False
        self._aborted = False
        self._action_driven = False
        self.logger = logger or module_logger
        self._obs_state = obs_state_init
        self._obs_state_changed_callback = obs_state_changed_callback
        self._obs_state_changed_callback(self._obs_state)

    @property
    def faulty(self: ObsStateModel):
        """Return whether the component is experiencing a faulty condition or
        not.

        :return: whether the component is aborted.
        """
        return self._faulty

    @property
    def aborted(self: ObsStateModel):
        """Return whether the component is experiencing a aborted condition or
        not.

        :return: whether the component is in fault.
        """
        return self._aborted

    @property
    def action_driven(self: ObsStateModel):
        """Return whether the updating of the component observing State is
        driven by actions or events (default).

        :return: whether the component obseving state update is driven by
            actions or events.
        """
        return self._action_driven

    @property
    def obs_state(self: ObsStateModel) -> ObsState:
        """The observing state of the component under control of the CSP
        Subarray TANGO Device.

        :getter: the current observing state
        :setter: set the component observing state to the updated value and
            the device callback is invoked, if defined.
        """
        return self._obs_state

    @obs_state.setter
    def obs_state(self: ObsStateModel, value: ObsState) -> None:
        """Set the component observing state to the required value. The device
        pushes an event if the current and required value are different.

        :param value: required value for the observing state
        """
        self._update_obs_state(value)

    def _update_obs_state(self: ObsStateModel, obs_state) -> None:
        """Helper method to handle the update of the observing state of a
        component and the device that controls it.

        # pylint: disable-next=fixme
        *TODO*: lock the TANGO Device AutoTangoMonitor otherwise there race
        conditions can happen.
        """
        # to update the obsState on the device.
        # pylint: disable-next=fixme
        # TODO: lock the AutoTangoMonitor otherwise there can be some race
        # conditions
        with self._lock:
            if self._obs_state != obs_state:
                self._obs_state = obs_state
                self.logger.debug(
                    f"Update obs state to: {ObsState(self._obs_state).name}"
                )
                self._obs_state_changed_callback(obs_state)

    def update_obs_state(self: ObsStateModel) -> None:
        """Update the component observing state.

        This method calls the :py:meth:``evaluate_obs_state`` method to figure
        out what the new obsstate state should be, and then updates the
        ``obs_state`` attribute, calling the callback if required.
        """
        with self._lock:
            obs_state = self.evaluate_obs_state()
            self._update_obs_state(obs_state)

    def evaluate_obs_state(
        self: ObsStateModel,
    ) -> ObsState:
        """Re-evaluate the component observing state.

        This method contains the basic logic for evaluating the observing
        state.

        This method should be extended by subclasses in order to
        define how observing state is evaluated by their particular device.

        :return: the new observing state.
        """
        if self._aborted:
            return ObsState.ABORTED
        if self._faulty:
            return ObsState.FAULT
        return ObsState.EMPTY if self._disabled else self._obs_state

    def component_fault(self: ObsStateModel, faulty: bool) -> None:
        """Handle a component experiencing or recovering from a fault.

        This method is called when the component goes
        into or out of FAULT state.

        :param faulty: whether the component has failed or not.
        """
        self._faulty = faulty
        self.update_obs_state()

    def component_aborted(self: ObsStateModel, aborted: bool) -> None:
        """Handle a component experiencing or recovering from an abort.

        This method is called when the component goes
        into or out of ABORTED state.

        :param aborted: whether the component has failed or not.
        """
        self._aborted = aborted
        self.update_obs_state()

    def component_disabled(self: ObsStateModel, disabled: bool) -> None:
        """Handle the monitoring functionalities of a TANGO Device.

        This method is called when the communication between the TANGO device
        and the component under controller is disabled/enabled via the setting
        of the administrative mode.

        :param disabled: whether the communication between the component
            and the controlling device is disabled.
        """
        self._disabled = disabled
        self.update_obs_state()

    def component_action_driven(
        self: ObsStateModel, action_driven: bool
    ) -> None:
        """Whether the updating of the component observing state is driven by
        actions or events (default).

        :param action_driven: configure the model behavior to update the
            observing state.
        """
        self.logger.debug(f"Set action driven to {action_driven}")
        self._action_driven = action_driven

    def perform_action(self: ObsStateModel, action: str) -> None:
        """Perform action to trigger the obs-state-model
        To be implemented

        :param action: The action to perform: *invoked* or *completed*
        """
        self.logger.info(f"perform action invoked with args: {action}")
