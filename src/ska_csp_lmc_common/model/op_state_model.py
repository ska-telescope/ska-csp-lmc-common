# -*- coding: utf-8 -*-
#
# Code inspired by the HealthModel of the MCCS project.
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""This module implements infrastructure for operational state management in
the CSP.LMC sub-system."""

from __future__ import annotations  # allow forward references in type hints

import logging
from typing import Callable, Optional

from tango import DevState

module_logger = logging.getLogger(__name__)


__all__ = ["OpStateModel"]


class OpStateModel:
    """A simple operational state model.

    * DevState.DISABLE -- when communication with the component is
      not established.

    * DevState.FAULT -- when the component has failed
    """

    def __init__(
        self: OpStateModel,
        op_state_init: DevState,
        op_state_changed_callback: Callable[[DevState], None],
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialise a new instance.

        :param op_state_init: the initial state of the component under
            control.
        :param op_state_changed_callback: callback to be called whenever
            there is a change to evaluated operational state.
        :param logger: a logger for this instance
        """
        self.logger = logger or module_logger
        self._faulty = False
        self._disabled = False
        self._op_state = op_state_init
        self._op_state_changed_callback = op_state_changed_callback
        self._op_state_changed_callback(self._op_state)

    @property
    def faulty(self):
        """Return whether the componend is experiencing a faulty condition or
        not.

        :return: whether the component is in fault.
        """
        return self._faulty

    @property
    def disabled(self: OpStateModel) -> bool:
        """Return whether the component is disabled or not.

        :return: whether the component is disabled.
        """
        return self._disabled

    @property
    def op_state(self: OpStateModel) -> DevState:
        """Return the component operational state.

        :return: the operational state.
        """
        return self._op_state

    def update_op_state(self: OpStateModel) -> None:
        """Update operational state.

        This method calls the :py:meth:``evaluate_op_state`` method to figure
        out what the new operational state should be, and then updates the
        ``state`` attribute, calling the callback if required.
        """
        op_state = self.evaluate_op_state()
        if self._op_state != op_state:
            self._op_state = op_state
            self._op_state_changed_callback(op_state)

    def evaluate_op_state(
        self: OpStateModel,
    ) -> DevState:
        """Re-evaluate the operational state.

        This method contains the logic for evaluating the state.

        This method should be extended by subclasses in order to
        define how state is evaluated by their particular device.

        If the CSP device opState is in FAULT for an internal error (i.e not
        depending from the opStates of the CSP sub-systems) this state has to
        be maintained. The only way to exit from this state is to
        Reset/Reinit the CSP device. In this case the faulty flag is reset to
        False.

        :return: the new state state.
        """
        if self._faulty:
            return DevState.FAULT
        if self._disabled:
            return DevState.DISABLE
        return self.op_state

    def component_fault(self: OpStateModel, faulty: bool) -> None:
        """Handle a component experiencing or recovering from a fault.

        This method is called when the component goes
        into or out of FAULT state.

        :param faulty: whether the component has failed or not
        """
        self._faulty = faulty
        self.update_op_state()

    def is_disabled(self: OpStateModel, disabled: bool) -> None:
        """Handle disabling the monitoring functionalities of a TANGO device.

        This method is called when the communication between the TANGO device
        and the component under controller is disabled/enabled via the setting
        of the adimnistrative mode.

        :param disabled: whether the communication between the component and
            the controlling TANGO device is disabled.
        """
        self._disabled = disabled
        self.update_op_state()

    # pylint: disable=unused-argument
    def perform_action(self: OpStateModel, action: str) -> None:
        """Not operative method.

        This method is required by the *CspSubarray* InitCommand class that
        must inherit from the SKABaseDevice.InitCommand because all the SKA
        attributes and logging are initialized there.
        """
        self.logger.debug(
            "Not operative method required to inherit from the SKABaseDevice"
            " InitCommand"
        )
