from __future__ import annotations

import logging
from typing import Callable

from tango import DevFailed, DeviceProxy, ErrSeverity, EventType, Except

# pylint: disable=arguments-differ
# pylint: disable=invalid-name
# pylint: disable=inconsistent-return-statements
# pylint: disable=broad-except


# pylint: disable-next=missing-class-docstring
class Connector:
    def __init__(self, fqdn, logger=None):
        """The class constructor.

        :param fqdn: string.

        The fqdn of the device.

        :param
            logger: (optional) The logger object.
        """

        self.logger = logging.getLogger(__name__) if logger is None else logger
        self.device_fqdn = fqdn
        self.deviceproxy = None
        self.deviceproxy = self._get_deviceproxy()

    def subscribe_attribute(
        self: Connector,
        attr_name: str,
        evt_type: EventType,
        callback_method: Callable,
        stateless: bool = True,
    ):
        """Subscribes to the change event on the given attribute.

        :param attr_name: string. Name of the attribute to subscribe change
            event.
        :param evt_type: event subscription type: change event, periodic ...
        :param callback_method: Name of callback method.

        :return: int. event_id returned by the Tango device server.
        :rtype: int

        """
        # log_msg = f"Subscribing attribute {attr_name}."
        # self.logger.debug(log_msg)
        event_id = self.deviceproxy.subscribe_event(
            attr_name, evt_type, callback_method, stateless=stateless
        )
        self.logger.debug(
            f"subscribed attribute {attr_name} with event"
            f" {event_id} on {self.device_fqdn}"
        )
        return event_id

    def send_command(self, command_name, command_data=None):
        """This method invokes command on the device server in synchronous
        mode.

        :param
            command_name: string. Name of the command

        :param
            command_data: (optional) void. Parameter with the command.

        :return
            Returns the command Result.
        :throws
            DevFailed in case of error.
        """
        try:
            log_msg = (
                f"Invoking {command_name} on {self.device_fqdn} synchronously."
                + f" with data: {command_data}"
            )
            self.logger.debug(log_msg)
            if command_data is not None:
                return self.deviceproxy.command_inout(
                    command_name, command_data
                )
            return self.deviceproxy.command_inout(command_name)
        except (DevFailed, Exception) as dev_failed:
            log_msg = (
                "Error in invoking command " + command_name + str(dev_failed)
            )
            self.logger.error(log_msg)
            Except.throw_exception(
                "Error in invoking command " + command_name,
                log_msg,
                "TangoClient.send_command",
                ErrSeverity.ERR,
            )

    def send_command_async(
        self, command_name, command_data=None, callback_method=None
    ):
        """This method invokes command on the device server in asynchronous
        mode.

        :param
            command_name: string. Name of the command

        :param
            command_data: (optional) void. Parameter with the command.

        :param
            callback_method: (optional) Callback function

            that should be executed after completion

            of the command execution.

        :returns
            int.

            Command identifier returned by

            the Tango device server.

        :throws
            DevFailed in case of error.
        """
        try:
            log_msg = (
                f"Invoking {command_name} on {self.device_fqdn} asynchronously"
                + f" with data: {command_data}"
            )

            self.logger.debug(log_msg)
            return self.deviceproxy.command_inout_asynch(
                command_name, command_data, callback_method
            )
        except DevFailed as dev_failed:
            log_msg = (
                "Error in invoking command " + command_name + str(dev_failed)
            )
            self.logger.error(log_msg)
            Except.throw_exception(
                "Error in invoking command " + command_name,
                log_msg,
                "TangoClient.send_command_async",
                ErrSeverity.ERR,
            )

    def get_attribute(self, attribute_name):
        """This method reads the value of the given attribute.

        :param
            attribute_name: string. Name of the attribute

        :return
            Returns the DeviceAttribute object with several fields.
            The attribute value is present in the value field of the object.
            value: Normal scalar value or NumPy array of values.

        :throws
            AttributeError in case of error.
        """
        try:
            log_msg = f"Reading attribute {attribute_name}."
            self.logger.debug(log_msg)
            return self.deviceproxy.read_attribute(attribute_name)
        except AttributeError as attribute_error:
            log_msg = (
                attribute_name + "Attribute not found" + str(attribute_error)
            )
            self.logger.error(log_msg)
            Except.throw_exception(
                attribute_name + "Attribute not found",
                log_msg,
                "TangoClient.get_attribute",
                ErrSeverity.ERR,
            )

    def set_attribute(self, attribute_name, value):
        """This method writes the value to the given attribute.

        :param
            attribute_name: string. Name of the attribute

        :param
            value: The value to be set. For non SCALAR attributes,
            it may be any sequence of sequences.

        :return
            None

        :throw
            AttributeError in case of error.
        """
        try:
            log_msg = f"Setting attribute {attribute_name}: {value}."
            self.logger.debug(log_msg)
            self.deviceproxy.write_attribute(attribute_name, value)
        except AttributeError as attribute_error:
            log_msg = (
                attribute_name + "Attribute not found" + str(attribute_error)
            )
            self.logger.error(log_msg)
            Except.throw_exception(
                attribute_name + "Attribute not found",
                log_msg,
                "TangoClient.set_attribute",
                ErrSeverity.ERR,
            )

    def unsubscribe_attribute(self, event_id):
        """Unsubscribes a client from receiving the event specified by
        event_id.

        :param
            event_id: int. Event id of the subscription

        :return
            None.
        """
        # log_msg = f"Unsubscribing attribute event {event_id}."
        # self.logger.info(log_msg)
        self.deviceproxy.unsubscribe_event(event_id)
        self.logger.debug(
            f"unsubscribed event" f" {event_id} on {self.device_fqdn}"
        )

    def _get_deviceproxy(self) -> DeviceProxy | None:
        """Returns device proxy for given FQDN."""

        if not self.deviceproxy:
            try:
                self.deviceproxy = DeviceProxy(self.device_fqdn)
            except DevFailed as df:
                self.logger.error(df.args[0].reason)
                Except.re_throw_exception(
                    df,
                    "DeviceProxy failed",
                    "Failed to create DeviceProxy of " + str(self.device_fqdn),
                    "connect()",
                    ErrSeverity.ERR,
                )
        return self.deviceproxy

    def is_alive(self):
        """
        Verify the device is on-line.
        If the target device is defined into the TANGO DB but is not running,
        the DeviceProxy call returns with success, but at the first attempt to
        issue a command on the target device, an exception is thrown (reason =
        API_DeviceNotExported).

        :raise: exception on connection failure
        """
        if not self.deviceproxy:
            self.logger.warning(
                f"Device {self.deviceproxy} is not running for"
                f" {self.device_fqdn} device!"
            )
            return
        try:
            self.deviceproxy.ping()
            self.logger.info(f"Ping ok for {self.device_fqdn} device")
        except DevFailed as df:
            Except.re_throw_exception(
                df,
                "Ping device proxy failed",
                "Failed to ping device" + str(self.device_fqdn),
                "failed.Connector.ping()",
                ErrSeverity.ERR,
            )
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"self.device_fqdn received error {e}")
            Except.re_throw_exception(
                "Ping device proxy failed: " + str(e),
                "Failed to ping device " + str(self.device_fqdn),
                "failed.Connector.ping()",
                ErrSeverity.ERR,
            )
