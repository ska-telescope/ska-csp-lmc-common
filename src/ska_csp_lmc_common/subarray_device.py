# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarray project
#
# INAF - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""CSP.LMC Common CspSubarray.

CSP subarray functionality is modeled via a TANGCSP.LMC Common Class for the
CSPSubarray TANGO Device.
"""

# Python standard library
from __future__ import absolute_import, annotations

import functools
import json
import logging
from collections import defaultdict
from typing import Any, Callable, List, Optional, Tuple

# tango imports
import tango
from ska_control_model import AdminMode, HealthState, ObsMode, ObsState

# Additional import
from ska_tango_base import SKASubarray
from ska_tango_base.commands import (
    FastCommand,
    ResultCode,
    SubmittedSlowCommand,
)
from ska_tango_base.faults import CommandError, StateModelError
from tango import AttrWriteType, DebugIt, DevState
from tango.server import attribute, command, device_property, run

# SKA imports
from ska_csp_lmc_common.manager import CSPSubarrayComponentManager

# local imports
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.subarray import (
    CspJsonValidator,
    SubarrayHealthModel,
    SubarrayObsStateModel,
    SubarrayOpStateModel,
)

from . import release

MAX_REPORTED_CONCURRENT_COMMANDS = 16
MAX_REPORTED_QUEUED_COMMANDS = 64

# pylint: disable=unnecessary-pass
# pylint: disable=protected-access
# pylint: disable=arguments-differ
# pylint: disable=signature-differs
# pylint: disable=super-init-not-called
# pylint: disable=redefined-outer-name
# pylint: disable=invalid-name
# pylint: disable=no-member
# pylint: disable=unused-argument
# pylint: disable=attribute-defined-outside-init
# pylint: disable=too-many-instance-attributes

__all__ = ["CspSubarray", "main"]


class CspSubarray(SKASubarray):
    """CSP subarray functionality is modeled via a TANGO CSP.LMC Common Class
    for the CSPSubarray TANGO Device.

    **Device Properties**

        CspController
            - The TANGO address of the CspController.
            - Type:'DevString'

        CbfSubarray
            - CBF sub-element sub-array TANGO device FQDN
            - Type:'DevString'

        PssSubarray
            - PST sub-element sub-array TANGO device FQDN.
            - Type:'DevString'

        PstBeams
            - PST sub-element PstBeams TANGO devices FQDNs
            - Type:'DevVarStringArray'
    """

    def init_device(self: CspSubarray) -> None:
        """Override the Base Classes *init_device* method to change the
        asynchronous callback sub-model from pull to push sub-model."""
        # to use the push model in command_inout_asynch (the one with the
        # callback parameter), change the global TANGO model to PUSH_CALLBACK.
        apiutil = tango.ApiUtil.instance()
        apiutil.set_asynch_cb_sub_model(tango.cb_sub_model.PUSH_CALLBACK)
        super().init_device()
        self._test_alarm = False
        self._obs_mode = ObsMode.IDLE

    def _init_state_model(self: CspSubarray) -> None:
        """Override the health, operational and observing State models:

        current CSP.LMC implementation does no longer rely on the SKA State
        models and associated State Machine library.
        """
        super()._init_state_model()
        self.health_model = SubarrayHealthModel(
            self._health_state, self.update_subarray_health_state, self.logger
        )
        # override the OpStateModel and ObsStateModel
        self.op_state_model = SubarrayOpStateModel(
            DevState.INIT, self.update_subarray_state, self.logger
        )
        self.obs_state_model = SubarrayObsStateModel(
            ObsState.EMPTY, self.update_subarray_obs_state, self.logger
        )
        self.set_change_event("isCommunicating", True, False)
        self._csp_command_result = ("", "")
        list_of_attributes = [
            "commandResult",
            "assignedTimingBeamIDs",
            "cbfSubarrayState",
            "pssSubarrayState",
            "pstBeamsState",
            "cbfSubarrayHealthState",
            "pssSubarrayHealthState",
            "pstBeamsHealthState",
            "cbfSubarrayAdminMode",
            "pssSubarrayAdminMode",
            "pstBeamsAdminMode",
            "cbfSubarrayObsState",
            "pssSubarrayObsState",
            "cbfSubarrayObsMode",
            "pssSubarrayObsMode",
            "pstBeamsObsState",
            "assignedResources",
            "commandTimeout",
            "testAlarm",
            "scanID",
            "obsMode",
        ]

        for attr in list_of_attributes:
            self.set_change_event(attr, True, False)
            self.set_archive_event(attr, True)

    @command(dtype_out="DevVarLongStringArray")
    @DebugIt()
    def Off(self: CspSubarray) -> Tuple[ResultCode, str]:
        """
        Turn the device off.

        Override base class command to accept the command also when
        the subarray device is in Off state

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        handler = self.get_command_object("Off")
        (result_code, unique_id) = handler()

        return ([result_code], [unique_id])

    @command(dtype_out="DevVarLongStringArray")
    @DebugIt()
    def On(self: CspSubarray) -> Tuple[ResultCode, str]:
        """
        Turn device on.

        Override base class command to accept the command also when
        the subarray device is in On state

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        handler = self.get_command_object("On")
        (result_code, unique_id) = handler()
        return ([result_code], [unique_id])

    @command(dtype_out="DevVarLongStringArray", dtype_in="DevBoolean")
    @DebugIt()
    def ForceHealthState(
        self: CSPSubarrayComponentManager,
        argin: bool,
    ) -> Tuple[ResultCode, str]:
        """Force health_state attribute
        :param task_callback: method called to update the task result
            related attributes
        :param argin: flag to force the health state to FAILED

        :return: a tuple with *ResultCode* and an informative message.
        """
        handler = self.get_command_object("ForceHealthState")
        result_code, msg = handler(argin)
        return ([result_code], [msg])

    class CspSubmittedSlowCommand(SubmittedSlowCommand):
        """Specialize the the SKA SubmittedSlowCommand class for CSP.LMC
        long running commands.
        """

        def __init__(  # pylint: disable=too-many-arguments
            self: CspSubarray.CspSubmittedSlowCommand,
            command_name: str,
            command_tracker,
            component_manager: CSPSubarrayComponentManager,
            method_name: str,
            validator,
            schema,
            attrs_dict: dict | None = None,
            callback: Callable[[bool], None] | None = None,
            logger: logging.Logger | None = None,
        ) -> None:
            """
            Initialise a new instance.

            :param command_tracker: the device's command tracker
            :param component_manager: the device's component manager
            :param callback: an optional callback to be called when this
                command starts and finishes.
            :param logger: a logger for this command to log with.
            """
            super().__init__(
                command_name,
                command_tracker,
                component_manager,
                method_name,
                callback=callback,
                logger=logger,
                validator=validator(
                    command_name, schema, attrs_dict=attrs_dict, logger=logger
                ),
            )

    def init_command_with_validator(
        self,
        _callback,
        validator,
        schema=None,
    ):
        """Set up command objects with the Json Validator"""

        # scan is none since it is an event driven action
        for command_name, method_name, state_model_hook in [
            ("AssignResources", "assign", "assign"),
            ("ReleaseResources", "release", "release"),
            ("Configure", "configure", "configure"),
            ("Scan", "scan", "scan"),
        ]:
            callback = (
                None
                if state_model_hook is None
                else functools.partial(_callback, state_model_hook)
            )
            # Initialise the attributes dict
            attrs_dict = self._create_attrs_dict(self.component_manager)
            self.register_command_object(
                command_name,
                self.CspSubmittedSlowCommand(
                    command_name,
                    self._command_tracker,
                    self.component_manager,
                    method_name,
                    validator,
                    schema,
                    attrs_dict,
                    callback=callback,
                    logger=self.logger,
                ),
            )

    def init_command_objects(self: CspSubarray) -> None:
        """Set up the command objects."""
        super().init_command_objects()

        def _callback(hook: Callable, running: bool) -> None:
            action = "invoked" if running else "completed"
            self.obs_state_model.perform_action(f"{hook}_{action}")

        self.init_command_with_validator(
            _callback, self._get_schema_validator()
        )

        for command_name, command_class, state_model_hook in [
            (
                "ReleaseAllResources",
                self.ReleaseAllResourcesCommand,
                "release",
            ),
            ("EndScan", self.EndScanCommand, None),
            ("End", self.EndCommand, None),
            ("ObsReset", self.ObsResetCommand, "obsreset"),
            ("Restart", self.RestartCommand, "restart"),
        ]:
            callback = (
                None
                if state_model_hook is None
                else functools.partial(_callback, state_model_hook)
            )
            self.register_command_object(
                command_name,
                command_class(
                    self._command_tracker,
                    self.component_manager,
                    callback=callback,
                    logger=None,
                ),
            )

        self.register_command_object(
            "ForceHealthState",
            self.ForceHealthStateCommand(
                self.component_manager,
                self.logger,
            ),
        )

        self.register_command_object(
            "Abort",
            self.AbortCommand(
                self._command_tracker,
                self.component_manager,
                callback=functools.partial(_callback, "abort"),
                logger=self.logger,
            ),
        )

    # pylint: disable=too-few-public-methods
    class ForceHealthStateCommand(FastCommand):
        """A class for CSP.LMC Subarray's ForceHealthState() command."""

        def __init__(
            self: CspSubarray.ForceHealthStateCommand,
            component_manager,
            logger: Optional[logging.Logger] = None,
        ) -> None:
            """
            Initialise a new ForceHealthStateCommand instance.

            :param health_model: the device's health state model
            :param logger: a logger for this command object to yuse
            """
            self.component_manager = component_manager
            self.logger = logger
            super().__init__(logger=logger)

        def do(
            self: CspSubarray.ForceHealthStateCommand,
            argin: bool,
        ) -> tuple[ResultCode, str]:
            """
            Stateless hook for ForceHealthState() command functionality.

            :param args: positional arguments to the command. This
                command does not take any, so this should be empty.
            :param kwargs: keyword arguments to the command. This
                command does not take any, so this should be empty.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            faulty_flag = argin
            self.component_manager.force_health_state(faulty_flag)

            if faulty_flag:
                msg = (
                    "Health state forced to "
                    f"{self.component_manager.health_model.health_state}"
                )
            else:
                msg = (
                    "Reset health state to: "
                    f"{self.component_manager.health_model.health_state}"
                )
            self.logger.info(f"{msg}")
            return ResultCode.OK, msg

    def set_component_manager(
        self: CspSubarray, cm_configuration: ComponentManagerConfiguration
    ) -> CSPSubarrayComponentManager:
        """Configure the ComponentManager for the CspSubarray TANGO device.

        This method has to be specialized in Mid.CSP and Low.CSP.

        :param cm_configuration: A class with all the device properties
            accessible as attributes
        :type cm_configuration: :py:class:`ComponentManagerConfiguration`
        """
        return CSPSubarrayComponentManager(
            self.health_model,
            self.op_state_model,
            self.obs_state_model,
            cm_configuration,
            self.update_property,
            self.logger,
        )

    def create_component_manager(
        self: CspSubarray,
    ) -> CSPSubarrayComponentManager:
        """Override the SKA BC method.

        :returns: The CSP SubarrayComponentManager
        :rtype: :py:class`CSPSubarrayComponentManager`
        """
        cm_configuration = ComponentManagerConfiguration(
            self.get_name(), self.logger
        )
        cm_configuration.add_attributes()
        return self.set_component_manager(cm_configuration)

    class InitCommand(SKASubarray.InitCommand):
        """A class for the CspSubarray's init_device() "command"."""

        def do(self: CspSubarray.InitCommand) -> Tuple[ResultCode, str]:
            """Stateless hook for device initialization.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            super().do()

            self._device._build_state = (
                f"{release.name}, {release.version}, {release.description}"
            )
            self._device._version_id = release.version

            # pylint: disable-next=fixme
            # TODO: move the following attributes to the ComponentManager class
            # Note: the _config_delay_expected could be set equal the max among
            # the sub-elements
            # sub-arrays expected times for the command
            # For tests purpose we set it  = 10
            self._device._config_delay_expected = 10
            # _timeout_expired: report the timeout flag
            self._device._timeout_expired = False

            # _failure_raised: report the failure flag
            self._device._failure_raised = False

            # _failure_message: report the failure message
            # Implemented as a dictionary
            # keys: command name ('addserachbeams', 'configurescan'..)
            # values: the message
            self._device._failure_message = defaultdict(lambda: "")

            # _list_dev_completed_task: for each long-running command report
            # the list of subordinate components that completed the task
            # Implemented as a dictionary
            # keys: the command name ('on', 'off',...)
            # values: the list of components
            self._device._list_dev_completed_task = defaultdict(lambda: [])
            # the last executed command
            self._device._last_executed_command = ""

            # _num_dev_completed_task: for each long-running command report
            # the number of subordinate components that completed the task
            # Implemented as a dictionary
            # keys: the command name ('addbeams, 'configurescan',...)
            # values: the number of components
            self._device._num_dev_completed_task = defaultdict(lambda: 0)

            # assigned CSP TimingBeam Capability IDs
            # self._device._assigned_timing_beams_str = ""
            self._device.component_manager.init()

            if self._device._admin_mode not in [
                AdminMode.ONLINE,
                AdminMode.ENGINEERING,
            ]:
                self._device.op_state_model.is_disabled(True)
                self._device.health_model.is_disabled(True)
            # Flag used to write the TimingBeams memorized attribute
            # only when required.
            # self._device.write_memorized_timing_beams = True
            return ResultCode.OK, "CSP subarray initialized"

    def is_GoToIdle_allowed(self: CspSubarray) -> bool:
        """
        Return whether the `GoToIdle` command
        may be called in the current device state.

        :raises StateModelError: command not permitted in observation state

        :return: whether the command may be called in the current device
            state
        """
        if self._obs_state not in [ObsState.READY]:
            raise StateModelError(
                "GoToIdle command not permitted in observation state"
                f"{self._obs_state.name}"
            )
        return True

    def is_Reset_allowed(self: CspSubarray) -> bool:
        """
        Return whether the `Reset` command may be called
        in the current device state.

        :return: whether the command may be called in the current device
            state
        """
        return self.get_state() != DevState.INIT

    def is_AbortCommands_allowed(self: CspSubarray) -> bool:
        """
        Execution of AbortCommands is disabled for CSPSubarray.
        An exception is raised every time that the command is invoked
        """
        raise CommandError(
            "AbortCommands is not implemented in CSPSubarray."
            "Use Abort to interrupt command execution"
            "and flush the command queue"
        )

    ##################
    # Class  methods
    ##################

    def update_subarray_state(self: CspSubarray, value: DevState) -> None:
        """Update the CSP Subarray state to the value from the aggregation
        of CSP Subarray subordinate components' states.
        Update is performed only when no CSP command is running.

        The method re-calculate the CSP Subarray healthState as result of the
        State change in one or more CSP Subarray sub-systems.

        :param value: the CSP Subarray state calculated aggregating the state
            of the CSP Subarray sub-systems
        """
        self.logger.info(f"Update state: from {self.get_state()} to {value}")
        super()._update_state(value)

    def update_subarray_health_state(
        self: CspSubarray, value: HealthState
    ) -> None:
        """Method invoked when a change happens in the CSP healthState.

        :param value: the updated value via events
        """
        # if self._health_state != value:
        self.logger.info(
            f"Update healthState from "
            f"{HealthState(self._health_state).name} to "
            f"{HealthState(value).name}"
        )
        super()._update_health_state(value)

    def update_subarray_obs_state(self: CspSubarray, value: ObsState) -> None:
        """Update the CSP Subarray obsState to the value returned by the
        EventManager that aggregates the obsState of the CSP Subarray
        subordinate components. Update is performed only when no CSP command is
        running.

        :param value: the updated value via events
        """
        if value != self._obs_state:
            self.logger.info(
                f"Update obsState device from "
                f"{ObsState(self._obs_state).name} to {ObsState(value).name}"
            )
            # self._obs_state = ObsState(value)
            # self.push_change_event("obsState", self._obs_state)
            super()._update_obs_state(ObsState(value))

    def update_property(
        self: CspSubarray, property_name: str, property_value: Any
    ) -> None:
        """General method invoked by the SubarrayComponentManager to push an
        event on a device attribute properly configured to push events from the
        device.

        :param property_name: the TANGO attribute name
        :param property_value: the attribute value
        """
        self.logger.debug(
            f"Update device property {property_name} to {property_value}"
        )
        # update the TimingBeams memorized attribute used to maintain
        # info about the timing beams already assigned to the subarray
        # NOTE: To remove when PST Capability device is implemented
        # assigned_timing_beams_str = ""
        if property_name == "assignedTimingBeamIDs":
            pass
            # for beam_id in property_value:
            #     assigned_timing_beams_str += f"{beam_id},"
            # # remove last comma
            # if assigned_timing_beams_str:
            #     assigned_timing_beams_str = assigned_timing_beams_str[:-1]
            # self.write_memorized_timing_beams = True
            # self.write_assignedTimingBeams(assigned_timing_beams_str)
        elif "pstBeams" in property_name:
            pst_property_value = json.loads(
                getattr(self, f"read_{property_name}")()
            )
            pst_property_value[property_value[0]] = property_value[1]
            property_value = json.dumps(pst_property_value)

        self.push_change_event(property_name, property_value)
        attr = getattr(self, property_name)
        if attr.is_archive_event():
            self.push_archive_event(property_name, property_value)

    def _update_csp_command_result(
        self: CspSubarray,
        command_id: str,
        command_result: tuple[ResultCode, str],
    ) -> None:
        """Method to update the result code returned from a task execution.
        On updating, the device pushes archive and change events.

        :param command_id: the ID of the CSP task as returned from the
            CommandTracker.
        :param command_result: the tuple with result code and message returned
            when the task completes.
        """
        task_name = command_id.split("_", 2)[2].lower()
        self._csp_command_result = (task_name, json.dumps(command_result[0]))
        self.push_change_event("commandResult", self._csp_command_result)
        self.push_archive_event("commandResult", self._csp_command_result)

    def _update_command_result(
        self: CspSubarray,
        command_id: str,
        command_result: tuple[ResultCode, str],
    ) -> None:
        super()._update_command_result(command_id, command_result)
        if command_result[0] is not None:
            self._update_csp_command_result(command_id, command_result)

    def _get_schema_validator(self: CspSubarray) -> CspJsonValidator:
        """Retrieve the class that handles the validation of the input
        JSON files sent as argument of AssignResources, Configure and Scan.

        This class is specialized in Mid.CSP and Low.CSP.
        """
        return CspJsonValidator

    def _create_attrs_dict(self: CspSubarray, cm: CSPSubarrayComponentManager):
        """Helper function to create a device attributes dictionary which must
        be semantically checked in the CspJsonValidator"""
        try:
            return {
                "subarray_id": cm.sub_id,
                "assigned_timing_beams_id": cm.assigned_timing_beams_ids,
                "deployed_timing_beams_id": self.PstBeams,
            }
        # pylint: disable-next=broad-except
        except Exception:
            return None

    # -----------------
    # Device Properties
    # -----------------

    CspController = device_property(
        dtype="DevString",
    )

    CbfSubarray = device_property(
        dtype="DevString",
    )

    PssSubarray = device_property(
        dtype="DevString",
    )

    PstBeams = device_property(
        dtype="DevVarStringArray",
    )

    ConnectionTimeout = device_property(dtype="DevUShort", default_value=60)

    PingConnectionTime = device_property(dtype="DevUShort", default_value=5)

    DefaultCommandTimeout = device_property(
        dtype="DevUShort", default_value=10
    )

    # ----------
    # Attributes
    # ----------

    adminMode = attribute(
        dtype=AdminMode,
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
        label="adminMode",
        doc=(
            "The admin mode reported for this device. It may interpret the"
            "current device condition and condition of all managed devices"
            "to set this. Most possibly an aggregate attribute."
        ),
    )

    scanID = attribute(
        dtype="DevULong64",
        label="scanID",
        access=AttrWriteType.READ_WRITE,
    )

    cbfSubarrayState = attribute(
        dtype="DevState",
        label="cbfSubarrayState",
    )

    pssSubarrayState = attribute(
        dtype="DevState",
        label="pssSubarrayState",
    )

    cbfSubarrayHealthState = attribute(
        dtype="DevEnum",
        label="cbfSubarrayHealthState",
        doc="CBF Subarray Health State",
        enum_labels=[
            "OK",
            "DEGRADED",
            "FAILED",
            "UNKNOWN",
        ],
    )

    pssSubarrayHealthState = attribute(
        dtype="DevEnum",
        label="pssSubarrayHealthState",
        doc="PSS Subarray Health State",
        enum_labels=[
            "OK",
            "DEGRADED",
            "FAILED",
            "UNKNOWN",
        ],
    )

    cbfSubarrayAdminMode = attribute(
        dtype="DevEnum",
        label="cbfSubarrayAdminMode",
        doc="CBF Subarray Admin Mode",
        enum_labels=[
            "ON-LINE",
            "OFF-LINE",
            "ENGINEERING",
            "NOT-FITTED",
            "RESERVED",
        ],
    )

    pssSubarrayAdminMode = attribute(
        dtype="DevEnum",
        label="pssSubarrayAdminMode",
        doc="PSS Subarray Admin Mode",
        enum_labels=[
            "ON-LINE",
            "OFF-LINE",
            "ENGINEERING",
            "NOT-FITTED",
            "RESERVED",
        ],
    )
    cbfSubarrayObsState = attribute(
        dtype=ObsState,
        label="cbfSubarrayObsState",
        doc="The CBF subarray observing state.",
    )

    pssSubarrayObsState = attribute(
        dtype=ObsState,
        label="pssSubarrayObsState",
        doc="The PSS subarray observing state.",
    )

    cbfSubarrayObsMode = attribute(
        dtype=ObsMode,
        label="cbfSubarrayObsSMode",
        doc="The CBF subarray observing mode.",
    )

    pssSubarrayObsMode = attribute(
        dtype=ObsMode,
        label="pssSubarrayObsMode",
        doc="The PSS subarray observing Mode.",
    )

    pssSubarrayAddress = attribute(
        dtype="DevString",
        label="pssSubarrayAddress",
        doc="The PSS sub-element sub-array FQDN.",
    )

    cbfSubarrayAddress = attribute(
        dtype="DevString",
        label="cbfSubarrayAddress",
        doc="The CBF sub-element sub-array FQDN.",
    )

    validScanConfiguration = attribute(
        dtype="DevString",
        label="validScanConfiguration",
        doc="Store the last valid scan configuration.",
    )

    commandTimeout = attribute(
        dtype="DevUShort",
        access=AttrWriteType.READ_WRITE,
        max_value=100,
        min_value=0,
        label="commandTimeout",
        doc=("Report the set command timeout"),
    )

    numOfDevCompletedTask = attribute(
        dtype="DevUShort",
        label="numOfDevCompletedTask",
        doc="Number of devices that completed the task",
    )

    timeoutExpiredFlag = attribute(
        dtype="DevBoolean",
        polling_period=1000,
        label="timeoutExpiredFlag",
        doc="The timeout flag for a CspSubarray command.",
    )

    failureRaisedFlag = attribute(
        dtype="DevBoolean",
        polling_period=1000,
        label="failureRaisedFlag",
        doc="The failure flag for a CspSubarray command.",
    )

    cmdFailureMessage = attribute(
        dtype="DevString",
        label="cmdFailureMessage",
        doc="The failure message on command execution",
    )

    isCmdInProgress = attribute(
        dtype="DevBoolean",
        label="isCmdInProgress",
    )

    assignedTimingBeamIDs = attribute(
        dtype=("DevUShort",),
        max_dim_x=16,
        label="assignedTimingBeamIDs",
        doc="List of TimingBeam IDs assigned to the  CSP sub-array",
    )

    # assignedTimingBeams = attribute(
    #     dtype="DevString",
    #     access=AttrWriteType.READ_WRITE,
    #     memorized=True,
    #     hw_memorized=True,
    #     label="assignedTimingBeams",
    #     doc="String with the list of TimingBeam IDs assigned "
    #     "to the  CSP sub-array",
    # )

    assignedTimingBeamsState = attribute(
        dtype=("DevState",),
        max_dim_x=16,
        label="assignedTimingBeamsState",
        doc="State of the assigned TimingBeams",
    )

    assignedTimingBeamsHealthState = attribute(
        dtype=("DevUShort",),
        max_dim_x=16,
        label="assignedTimingBeamsHealthState",
        doc="HealthState of the assigned TimingBeams",
    )

    assignedTimingBeamsObsState = attribute(
        dtype=("DevUShort",),
        max_dim_x=16,
        label="assignedTimingBeamsObsState",
        doc="ObsState of the assigned TimingBeams",
    )

    assignedTimingBeamsAdminMode = attribute(
        dtype=("DevUShort",),
        max_dim_x=16,
        label="assignedTimingBeamsAdminMode",
        doc="AdminMode of the assigned TimingBeams",
    )

    pstBeamsAddress = attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="pstBeamsAddress",
        doc="PST sub-element PstBeam TANGO device FQDNs.",
    )

    listOfDevCompletedTask = attribute(
        dtype=("DevString",),
        max_dim_x=100,
        label="listOfDevCompletedTask",
        doc="List of devices that completed the task",
    )

    pstBeamsState = attribute(
        dtype="DevString",
        label="pstBeamsState",
        doc=(
            "Report the state of the pst beams assigned to the subarray as a"
            " JSON formatted string."
        ),
    )

    pstBeamsObsState = attribute(
        dtype="DevString",
        label="pstBeamsObsState",
        doc=(
            "Report the observing state of the pst beams assigned to the"
            " subarray as a JSON formatted string"
        ),
    )

    pstBeamsHealthState = attribute(
        dtype="DevString",
        label="pstBeams Health",
        doc=(
            "Report the health state of the pst beams assigned to the subarray"
            " as a JSON formatted string"
        ),
    )

    pstBeamsAdminMode = attribute(
        dtype="DevString",
        label="pstBeams adminMode",
        doc=(
            "Report the administration mode of the beams assigned to the"
            " subarray as JSON formatted string"
        ),
    )

    commandResult = attribute(
        dtype=("str",),
        # Always the last result (unique_id, result_code, task_result)
        max_dim_x=2,
        access=AttrWriteType.READ,
        doc=(
            "command_name, result_code \n"
            "Gives the name and the result_code of latest command execution"
        ),
    )

    isCommunicating = attribute(
        dtype="DevBoolean",
        access=AttrWriteType.READ,
        doc=(
            "Whether the device is communicating with the component under"
            " control"
        ),
    )

    @attribute(
        dtype="DevBoolean",
        doc="flag to test alarm",
    )
    def testAlarm(self: CspSubarray) -> bool:
        """Flag to test alarm in CSP.LMC AlarmHandler.

        :return: whether the flag is set/unset.
        """
        return self._test_alarm

    @testAlarm.write
    def testAlarm(self: CspSubarray, argin: bool) -> None:
        """
        Set the flag to test the alarm in CSP.LMC AlarmHandler

        :param argin: True/False
        """
        self._test_alarm = argin
        self.push_change_event("testAlarm", self._test_alarm)

    @attribute(
        dtype="DevString",
        label="configurationID",
        doc="The configuration identifier (string)",
    )
    def configurationID(self: CspSubarray) -> str:
        """Report the configuration ID string.

        return: a string with the ID of the received configuration
            when correctly validated, otherwise an empty string.
        """
        return self.component_manager.config_id

    obsMode = attribute(
        dtype=(ObsMode,),
        max_dim_x=16,
        label="obsmode",
        doc="Active observation modes",
    )

    assignedTimingBeamsState = attribute(
        dtype=("DevState",),
        max_dim_x=16,
        label="assignedTimingBeamsState",
        doc="State of the assigned TimingBeams",
    )

    # ---------------
    # General methods
    # ---------------

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""

    def delete_device(self) -> None:
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        self.logger.debug("Deleting device!")
        if self.component_manager and self.component_manager.evt_manager:
            self.component_manager.evt_manager.stop()
            self.component_manager.evt_manager.unsubscribe_all_events()
        if self.component_manager._main_task_executor:
            self.logger.info("Shutdown LRC main executor")
            self.component_manager._main_task_executor.stop_event.set()
            self.component_manager._main_task_executor.shutdown(wait_flag=True)
        # PROTECTED REGION END #    //  CspSubarray.delete_device

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def GoToIdle(self) -> Tuple[ResultCode, str]:
        """Set the subarray obsState to IDLE.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        handler = self.get_command_object("End")
        (result_code, message) = handler()
        return [[result_code], [message]]

    # ------------------
    # Attributes methods
    # ------------------

    @attribute(dtype="str")
    def buildState(self: CspSubarray) -> str:
        """
        Read the Build State of the device.

        :return: the build state of the device
        """
        return self.component_manager.csp_build_state

    @attribute(dtype="str")
    def versionId(self: CspSubarray) -> str:
        """
        Read the Version Id of the device.

        :return: the version id of the device
        """
        return self.component_manager._csp_version

    def read_adminMode(self: CspSubarray):
        """
        Read the Admin Mode of the device.

        :return: Admin Mode of the device
        :rtype: AdminMode
        """
        return self._admin_mode

    def is_adminMode_allowed(
        self: CspSubarray, request_type: tango.AttReqType
    ):
        """Verify the adminMode can be modified ."""
        if request_type == tango.AttReqType.READ_REQ or (
            self._obs_state == ObsState.EMPTY
            and request_type == tango.AttReqType.WRITE_REQ
        ):
            return True
        raise StateModelError(
            "adminMode can't be modified when the CSP.LMC Subarrays is"
            f" in observing state {self._obs_state.name}"
        )

    def write_adminMode(self: CspSubarray, value: AdminMode) -> None:
        """Set the administration mode for the whole CSP element.

        :param value: one of the administration mode value (ON-LINE,\
            OFF-LINE, ENGINEERING, NOT-FITTED, RESERVED).
        :raises: ValueError if an invalid adminmode value is specified.
        """
        self.logger.debug(f"Set device adminMode to {AdminMode(value).name}")
        self.component_manager._store_admin_mode = (
            self.component_manager._admin_mode
        )
        self.component_manager._admin_mode = value

        if value == AdminMode.NOT_FITTED:
            self.admin_mode_model.perform_action("to_notfitted")
        elif value == AdminMode.OFFLINE:
            self.admin_mode_model.perform_action("to_offline")
            if self.component_manager.is_communicating:
                self.component_manager.stop_communicating()
        elif value == AdminMode.ENGINEERING:
            self.admin_mode_model.perform_action("to_engineering")
            self.component_manager.start_communicating()
        elif value == AdminMode.ONLINE:
            self.admin_mode_model.perform_action("to_online")
            self.component_manager.start_communicating()
        elif value == AdminMode.RESERVED:
            self.admin_mode_model.perform_action("to_reserved")
        else:
            raise ValueError(f"Unknown adminMode {value}")

    def read_scanID(self: CspSubarray) -> int:
        """Return the scanID attribute.

        :return: the Subarray scanID
        """
        return self.component_manager._scan_id

    def write_scanID(self: CspSubarray, value: int) -> None:
        """Set the scanID attribute.

        :param The Subarray scanID
        """
        self.component_manager._scan_id = value

    def read_cbfSubarrayState(self) -> DevState:
        """Return the state of the CBF Subarray.

        :return: The correspondent CBF Subarray Operational State
        """
        try:
            return self.component_manager.components[self.CbfSubarray].state
        except KeyError:
            self.logger.warning(
                "Cbf Subarray is unavailable, can't read state."
            )
            return DevState.DISABLE
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"Cannot read Cbf Subarray State. Error: {e}")
            return DevState.UNKNOWN

    def read_pssSubarrayState(self) -> DevState:
        """Return the state of the PSS Subarray, if deployed.
        If the PSS sub-system is not deployed, the state is reported
        as UNKNOWN.

        :return: The correspondent PSS Subarray Operational State
        """
        try:
            return self.component_manager.components[self.PssSubarray].state
        except KeyError:
            self.logger.warning(
                "Pss Subarray is unavailable, can't read state."
            )
            return DevState.DISABLE
        # # pylint: disable-next=broad-except
        # except Exception as e:
        #     self.logger.error(f"Cannot read Pss Subarray State. Error: {e}")
        #     return DevState.UNKNOWN

    def read_cbfSubarrayHealthState(self) -> HealthState:
        """Return the health state of the CBF Subarray.

        :return: The correspondent CBF Subarray Health State
        """
        try:
            return self.component_manager.components[
                self.CbfSubarray
            ].health_state
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(
                f"Cannot read Cbf Subarray HealthState. Error: {e}"
            )
            return HealthState.UNKNOWN

    def read_pssSubarrayHealthState(self) -> HealthState:
        """Return the health state of the PSS Subarray.
        If the PSS sub-system is not deployed, the health state is reported
        as UNKNOWN.

        :return: The correspondent PSS Subarray Health State
        """
        try:
            return self.component_manager.components[
                self.PssSubarray
            ].health_state
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(
                f"Cannot read Pss Subarray HealthState. Error: {e}"
            )
            return HealthState.UNKNOWN
        # PROTECTED REGION END #    //  CspSubarray.pssSubarrayHealthState_read

    def read_cbfSubarrayAdminMode(self) -> AdminMode:
        """Return admin mode of the CBF subarray.
        If the sub-system is not deployed, the admin mode is
        reported as NOT_FITTED.

        :return: The correspondent CBF Subarray Administration Mode
        """
        if self.CbfSubarray:
            try:
                return self.component_manager.components[
                    self.CbfSubarray
                ].admin_mode
            # pylint: disable-next=broad-except
            except Exception as e:
                self.logger.error(
                    f"Cannot read Cbf Subarray AdminMode. Error: {e}"
                )
        return AdminMode.NOT_FITTED

    def read_pssSubarrayAdminMode(self) -> AdminMode:
        """Return admin mode of the PSS subarray.
        If the sub-system is not deployed, the admin mode is
        reported as NOT_FITTED.

        :return: The correspondent PSS Subarray Administration Mode
        """
        if self.PssSubarray:
            try:
                return self.component_manager.components[
                    self.PssSubarray
                ].admin_mode
            # pylint: disable-next=broad-except
            except Exception as e:
                self.logger.error(
                    f"Cannot read Pss Subarray AdminMode. Error: {e}"
                )
        return AdminMode.NOT_FITTED

    def read_cbfSubarrayObsState(self) -> ObsState:
        """Return the cbfSubarrayObsState attribute.

        :return: The correspondent CBF Subarray Observing State
        """
        try:
            return self.component_manager.components[
                self.CbfSubarray
            ].obs_state
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"Cannot read Cbf Subarray ObsState. Error: {e}")
            return ObsState.EMPTY

    def read_pssSubarrayObsState(self) -> ObsState:
        """Return the pssSubarrayObsState attribute.

        :return: The correspondent PSS Subarray Observational State
        """
        try:
            return self.component_manager.components[
                self.PssSubarray
            ].obs_state
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"Cannot read Pss Subarray ObsState. Error: {e}")
            return ObsState.EMPTY

    def read_pssSubarrayObsMode(self) -> ObsMode:
        """Return the pssSubarrayObsMode attribute.

        :return: The correspondent PSS Subarray Observational Mode
        """
        try:
            return self.component_manager.components[self.PssSubarray].obs_mode
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"Cannot read Pss Subarray ObsMode. Error: {e}")
            return ObsMode.IDLE

    def read_cbfSubarrayObsMode(self) -> ObsMode:
        """Return the cbfSubarrayObsMode attribute.

        :return: The correspondent CBF Subarray Observational Mode
        """
        try:
            return self.component_manager.components[self.CbfSubarray].obs_mode
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"Cannot read Cbf Subarray ObsMode. Error: {e}")
            return ObsMode.IDLE

    def read_pssSubarrayAddress(self) -> str:
        """Return the pssSubarrayAddress attribute.

        :return: The PSS sub-element sub-array FQDN.
        """
        if self.PssSubarray:
            return self.PssSubarray
        self.logger.error("Cannot read Pss Subarray Addr.")
        return ""

    def read_cbfSubarrayAddress(self) -> str:
        """Return the cbfSubarrayAddress attribute.

        :return: The CBF sub-element sub-array FQDN.
        """
        if self.CbfSubarray:
            return self.CbfSubarray
        self.logger.error("Cannot read Cbf Subarray Addr.")
        return ""

    def read_validScanConfiguration(self) -> str:
        """Return the validScanConfiguration attribute.

        :return: the JSON string containing the last valid scan configuration.
        """
        return self.component_manager.valid_scan_configuration

    def read_commandTimeout(self) -> int:
        """Return the commandTimeout attribute.

        :return: The duration of the command timeout.

        """
        return self.component_manager.command_timeout

    def write_commandTimeout(self, value):
        """
        Set the value of the command timeout.

        Configure the timeout applied to each command. The specified
        value shall be > 0.

        Negative values raise a *TypeError* exception. This is the expected
        behavior because the attribute has been defined as DevUShort.
        A zero value raises a *ValueError* exception.

        :raises: TypeError or tango.DevFailed depending on whether the
                specified value is negative or zero if a negative value.
        """
        self.component_manager.command_timeout = value

    def read_numOfDevCompletedTask(self) -> int:
        """Return the numOfDevCompletedTask attribute.

        :return: Number of devices that completed the task

        NOTE: not implemented
        """
        return self._num_dev_completed_task[self._last_executed_command]

    def read_assignedTimingBeamIDs(self) -> Tuple[int]:
        """Return the assignedTimingBeamIDs attribute.

        :return: List of TimingBeam IDs assigned to the  CSP sub-array
        """
        return self.component_manager.assigned_timing_beams_ids

    # def read_assignedTimingBeams(self) -> None:
    #     """Return the assignedTimingBeamIDs attribute.

    #     :return: Write the TimingBeam IDams assigned to the  CSP sub-array

    #     NOTE: not implemented
    #     """
    #     return self._assigned_timing_beams_str

    # def write_assignedTimingBeams(self, assigned_timing_beams) -> None:
    #     """Return the assignedTimingBeamIDs attribute.

    #     :return: Write the string with TimingBeam IDs assigned to the
    #         CSP sub-array. This attribute is memorized so that at
    #         device restart it can be recovered.
    #         The correspondent Spectrum attribute can't be configured
    #         as memorized attribute.
    #         Temporary solution to use until the introduction of the
    #         PST Processing Mode Capability device.
    #         CSP sub-array. This attribute is memorized so that at device
    #         restart it can be recovered.
    #         The correspondent Spectrum attribute can't be configured as
    #         memorized attribute.
    #         Temporary solution to use until the introduction of the PST
    #         Processing Mode Capability device.
    #     """

    #     self._assigned_timing_beams_str = assigned_timing_beams
    #     self.logger.debug(f"memorized value: {assigned_timing_beams}")
    #     if not self.write_memorized_timing_beams:
    #         self.logger.warning(
    #             "attribute assignedTimingBeams is written"
    #             " only at initialization!"
    #         )
    #         return
    #     assigned_timing_beams_ids = []
    #     if assigned_timing_beams:
    #         assigned_timing_beams_ids = list(
    #             map(int, self._assigned_timing_beams_str.split(","))
    #         )
    #     self.logger.debug(
    #         f"list of assigned timing beams: " f"{assigned_timing_beams_ids}"
    #     )
    #     if self.component_manager:
    #         self.component_manager.assigned_timing_beams_ids = (
    #             assigned_timing_beams_ids
    #         )
    #     self.write_memorized_timing_beams = False

    def read_assignedTimingBeamsState(self) -> Tuple[DevState]:
        """Return the assignedTimingBeamsState attribute.

        :return: a tuple with the state of the assigned timing beams.
            Otherwise an empty tuple.
        """
        if self.component_manager:
            self.logger.debug(
                f"list of assigned timing beam states : "
                f"{self.component_manager.assigned_timing_beams_state}"
            )
            return self.component_manager.assigned_timing_beams_state
        return ()

    def read_assignedTimingBeamsHealthState(self) -> Tuple[int]:
        """Return the assignedTimingBeamsHealthState attribute.

        :return: a tuple with the health state of the assigned timing beams.
            Otherwise an empty tuple.
        """
        if self.component_manager:
            return self.component_manager.assigned_timing_beams_health_state
        return ()

    def read_assignedTimingBeamsObsState(self) -> Tuple[int]:
        """Return the assignedTimingBeamsObsState attribute.

        :return: ObsState of the assigned TimingBeams

        NOTE: not implemented
        """
        return (0,)

    def read_assignedTimingBeamsAdminMode(self) -> Tuple[int]:
        """Return the assignedTimingBeamsAdminMode attribute.

        :return: AdminMode of the assigned TimingBeams

        NOTE: not implemented
        """
        return (0,)

    def read_pstBeamsAddress(self) -> List[str]:
        """The list with the FQDNs of the deployed PST Beams.

        :return: PST sub-element PstBeam TANGO device FQDNs.
        """
        if self.PstBeams:
            return self.PstBeams
        return [""]

    def read_pstBeamsState(self) -> str:
        """Return the JSON string with the state of the
        assigned PST Beams.

        :return: A JSON formatted string with the state of the pst Beams
            assigned to the subarray.
        """
        self.logger.debug(
            f"Online component: {self.component_manager.online_components}"
        )
        pst_beams_state_dict = {
            fqdn: c.state.name
            for fqdn, c in self.component_manager.online_components.items()
            if fqdn in self.PstBeams
        }
        return json.dumps(pst_beams_state_dict)

    def read_pstBeamsObsState(self) -> str:
        """Return the obsState values for the PST Beams.

        :return: A JSON formatted string with the observing state of the PST
            Beams assigned to the subarray.
        """

        pst_beams_obs_state_dict = {
            fqdn: ObsState(c.obs_state).name
            for fqdn, c in self.component_manager.online_components.items()
            if fqdn in self.PstBeams
        }
        return json.dumps(pst_beams_obs_state_dict)

    def read_pstBeamsHealthState(self) -> str:
        """Return the health state values of the PST Beams.

        :return: A JSON formatted string with the health state of the PST beams
            assigned to the subarray.
        """
        pst_beams_health_state_dict = {
            fqdn: HealthState(c.health_state).name
            for fqdn, c in self.component_manager.online_components.items()
            if fqdn in self.PstBeams
        }
        return json.dumps(pst_beams_health_state_dict)

    def read_pstBeamsAdminMode(self) -> str:
        """Return the administration Mode of the PST Beams.

        :return: A JSON formatted string with the administration mode of the
            PST Beams assigned to the subarray.
        """
        pst_beams_admin_mode_dict = {
            fqdn: AdminMode(c.admin_mode).name
            for fqdn, c in self.component_manager.online_components.items()
            if fqdn in self.PstBeams
        }
        return json.dumps(pst_beams_admin_mode_dict)

    def read_timeoutExpiredFlag(self) -> bool:
        """Return the timeoutExpiredFlag attribute.

        :return: The timeout flag for a CspSubarray command.
        """
        return self._timeout_expired

    def read_failureRaisedFlag(self) -> bool:
        """Return the failureRaisedFlag attribute.

        :return: The failure flag for a CspSubarray command
        """
        return self._failure_raised

    def read_isCmdInProgress(self) -> bool:
        """Return the isCmdInProgress attribute.

        NOTE: not implemented
        """
        return False

    def read_cmdFailureMessage(self) -> str:
        """Return the cmdFailureMessage attribute.

        :return: The failure message on command execution
        """
        return self._failure_message[self._last_executed_command]

    def read_listOfDevCompletedTask(self) -> List[str]:
        """Return the listOfDevCompletedTask attribute.

        :return: List of devices that completed the task

        NOTE: not implemented
        """
        return self._list_dev_completed_task[self._last_executed_command]

    def read_commandResult(self) -> Tuple[ResultCode, str]:
        """Return the command result attribute.

        :return: the name and the result_code of latest command execution
        """
        return self._csp_command_result

    def read_isCommunicating(self: CspSubarray) -> bool:
        """Whether the TANGO device is communicating with the controlled
        component.

        :return: Whether the device is communicating with the component under
            control.
        """
        return self.component_manager.is_communicating

    def read_obsMode(self: CspSubarray) -> List[ObsMode]:
        """Return the obsModes attribute.
        TODO: implementation will be moved to CM and rewritten so that it
        relies on the content of the configuration json file.

        :return: a tuple with the state of the assigned timing beams.
            Otherwise an empty tuple.
        """

        return self.component_manager._obs_modes


# ----------
# Run server
# ----------


# pylint: disable-next=missing-function-docstring
def main(args=None, **kwargs):
    return run((CspSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
