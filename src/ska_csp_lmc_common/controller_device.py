from __future__ import annotations

import json
import logging
from typing import Any, List, Tuple

import tango
from ska_control_model import AdminMode, HealthState
from ska_tango_base import SKAController
from ska_tango_base.commands import ResultCode
from tango import AttrWriteType, DebugIt, DevState
from tango.server import attribute, command, device_property, run

from ska_csp_lmc_common.controller import (
    ControllerHealthModel,
    ControllerOpStateModel,
)
from ska_csp_lmc_common.manager import CSPControllerComponentManager
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)

MAX_REPORTED_CONCURRENT_COMMANDS = 16
MAX_REPORTED_QUEUED_COMMANDS = 64

module_logger = logging.getLogger(__name__)

# pylint: disable=unnecessary-pass
# pylint: disable=protected-access
# pylint: disable=attribute-defined-outside-init
# pylint: disable=arguments-differ
# pylint: disable=signature-differs
# pylint: disable=super-init-not-called
# pylint: disable=redefined-outer-name
# pylint: disable=invalid-name
# pylint: disable=unused-argument
# pylint: disable=fixme


class CspController(SKAController):
    """CSP Controller functionality is modeled via a Common Class for the CSP
    Controller TANGO Devices (Mid and Low).

    **Device Properties:**

        ConnectionTimeout
            - The maximum time to wait for the connection with a sub-system
            - Type:'DevUShort'
            - Default Value: 60

        PingConnectionTime
            - The time to wait between connection attempts
            - Type:'DevUShort'
            - Default Value: 5

        DefaultCommandTimeout
            - Deafult timeout for LRC execution
            - Type:'DevUShort'
            - Default Value: 5

        CspCbf
            - FQDN of the CBF Controller
            - Type:'DevString'

        CspPss
            - TANGO FQDN of the PSS Controller
            - Type:'DevString'

        CspPstBeams
            - TANGO FQDNs of the deployed PST Beams
            - Type:'DevVarStringArray'.

        CspSubarrays
            - TANGO FQDN of the CSP.LMC Subarrays
            - Type:'DevVarStringArray'
    """

    def init_device(self: CspController) -> None:
        """Override the Base Classes *init_device* method to change the
        asynchronous callback sub-model from pull to push sub-model."""
        # to use the push model in command_inout_asynch (the one with the
        # callback parameter), change the global TANGO model to PUSH_CALLBACK.
        # TODO: check if to be deleted
        apiutil = tango.ApiUtil.instance()
        apiutil.set_asynch_cb_sub_model(tango.cb_sub_model.PUSH_CALLBACK)
        super().init_device()
        self._test_alarm = False

    def _init_state_model(self: CspController) -> None:
        """Override the health and operational State models:

        current CSP.LMC implementation does no longer rely on the SKA State
        models and associated State Machine library.
        """
        super()._init_state_model()

        self.health_model = ControllerHealthModel(
            self._health_state, self.update_ctrl_health_state, self.logger
        )
        # override the OpStateModel and ObsStateModel
        self.op_state_model = ControllerOpStateModel(
            DevState.INIT,
            self.update_ctrl_state,
            self.logger,
        )
        self._csp_command_result = ("", "")
        self.set_change_event("isCommunicating", True, False)

        list_of_attributes = [
            "commandResult",
            "cspCbfState",
            "cspPssState",
            "cspPstBeamsState",
            "cspCbfHealthState",
            "cspPssHealthState",
            "cspPstBeamsHealthState",
            "cspCbfAdminMode",
            "cspPssAdminMode",
            "cspPstBeamsAdminMode",
            "commandTimeout",
            "testAlarm",
        ]

        for attr in list_of_attributes:
            self.set_change_event(attr, True, False)
            self.set_archive_event(attr, True)

    def delete_device(self: CspController) -> None:
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspSubarray.delete_device) ENABLED START #
        if not self.component_manager:
            return
        if self.component_manager.evt_manager:
            self.component_manager.evt_manager.stop()
            self.component_manager.evt_manager.unsubscribe_all_events()

        self.logger.info("Shutdown LRC main executor")
        if self.component_manager._main_task_executor:
            self.component_manager._main_task_executor.stop_event.set()
            self.component_manager._main_task_executor.shutdown(wait_flag=True)
        # PROTECTED REGION END #    //  CspSubarray.delete_device

    def set_component_manager(
        self: CspController, cm_configuration: ComponentManagerConfiguration
    ) -> CSPControllerComponentManager:
        """Configure the ComponentManager for the CSP Controller device. This
        method has to be specialized in Mid.CSP and Low.CSP.

        :param cm_configuration: A class with all the device properties
            accessible as attributes

        :return: The CSP Controller ComponentManager
        """
        self.logger.debug("setting component manager")
        self.logger.debug(
            f"op_state_model {self.op_state_model._component_op_state}"
        )
        self.logger.debug(
            f"health_model {self.health_model._component_healths}"
        )
        self.logger.debug(f"cm_configuration {cm_configuration}")
        self.logger.debug(f"update_property {self.update_property}")
        return CSPControllerComponentManager(
            self.op_state_model,
            self.health_model,
            cm_configuration,
            self.update_property,
            logger=self.logger,
        )

    def create_component_manager(
        self: CspController,
    ) -> CSPControllerComponentManager:
        """Override the base method.

        :returns: The CSP ControllerComponentManager
        """
        cm_configuration = ComponentManagerConfiguration(
            self.get_name(), self.logger
        )
        cm_configuration.add_attributes()
        return self.set_component_manager(cm_configuration)

    def update_ctrl_state(self: CspController, value: DevState) -> None:
        """Update the CSP Controller state to the value returned by the
        Operational State Model.

        :param value: the CSP Controller state calculated as an aggregation of
            the CSP Controller sub-systems states.
        """
        self.logger.info(f"Update State from {self.get_state()} to {value}")
        super()._update_state(value)

    def update_ctrl_health_state(
        self: CspController, value: HealthState
    ) -> None:
        """Update the CSP Controller healthState as aggregation of the CSP sub-
        systems' health states.

        :param value: the updated health state value
        """
        self.logger.info(
            f"Update healthState from "
            f"{HealthState(self._health_state).name}"
            f" to {HealthState(value).name}"
        )
        super()._update_health_state(value)

    def update_property(
        self: CspController, property_name: str, property_value: Any
    ) -> None:
        """General method invoked by the ComponentManager to push an event on a
        device attribute properly configured to push events from the device.
        In the case of PstBeams only, entry to be updated has to be inserted
        as a list of [key, value].

        :param property_name: the TANGO attribute name
        :param property_value: the attribute value
        """
        # property_name = cspPstBeamsState
        if "cspPstBeams" in property_name:
            pst_property_value = json.loads(
                getattr(self, f"read_{property_name}")()
            )
            pst_property_value[property_value[0]] = property_value[1]
            property_value = json.dumps(pst_property_value)

        self.push_change_event(property_name, property_value)
        self.logger.info(
            f"Pushed event on {property_name} " f"with value: {property_value}"
        )
        attr = getattr(self, property_name)
        if attr.is_archive_event():
            self.push_archive_event(property_name, property_value)

    def _update_csp_command_result(
        self: CspController,
        command_id: str,
        command_result: tuple[ResultCode, str],
    ) -> None:
        """Method to update the result code returned from a task execution.
        On updating, the device pushes archive and change events.

        :param command_id: the ID of the CSP task as returned from the
            CommandTracker.
        :param command_result: the tuple with result code and message returned
            when the task completes.
        """
        task_name = command_id.split("_", 2)[2].lower()
        self._csp_command_result = (task_name, json.dumps(command_result[0]))
        self.push_change_event("commandResult", self._csp_command_result)
        self.push_archive_event("commandResult", self._csp_command_result)

    def _update_command_result(
        self: CspController,
        command_id: str,
        command_result: tuple[ResultCode, str],
    ) -> None:
        super()._update_command_result(command_id, command_result)
        if command_result[0] is not None:
            self._update_csp_command_result(command_id, command_result)

    #
    # Device commands section
    #
    class InitCommand(SKAController.InitCommand):
        """Class to handle the CSP.LMC Controller Init command."""

        def do(self: CspController.InitCommand) -> Tuple(ResultCode, str):
            """Invoke the *init* command on the Controller ComponentManager.

            :return: a tuple with the command result code and an informative
                message.
            """
            # NOTE: in InitCommand parent class, the healthState of the device
            # is set to OK. To make the HealthStateModel work fine, the initial
            # value of the healthState must be set to OK, too.
            super().do()
            # device._health_state = HealthState.UNKNOWN
            # This is a temporary line to make the component_manager able to
            # manage old initialization
            self._device.component_manager.init()
            # configure the commandResult attribute to push event from the
            # device.
            # self._device.set_change_event("commandResult", True, True)
            if self._device._admin_mode not in [
                AdminMode.ONLINE,
                AdminMode.ENGINEERING,
            ]:
                self._device.op_state_model.is_disabled(True)
                self._device.health_model.is_disabled(True)
            return ResultCode.OK, "Csp Controller Init command finished"

    def is_On_allowed(self: CspController) -> bool:
        """
        Return whether the `On` command may be called in the current device
        state.

        :return: whether the command may be called in the current device
            state
        :raise: `DevFailed` exception if the device is not in one of the
            following states: ON, STANDBY,OFF
        """
        return self.get_state() in [
            DevState.STANDBY,
            DevState.ON,
            DevState.OFF,
        ]

    @command(
        dtype_in="DevVarStringArray",
        doc_in=(
            "If the array length is 0, the command applies to the whole CSP"
            " Element. If the array length is > 1, each array element"
            " specifies the FQDN of the CSP SubElement to switch ON."
        ),
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def On(self: CspController, argin: List[str]) -> Tuple[ResultCode, str]:
        """Turn device on.

        :param List of fqdn of controller's subsystems to operate.

        :return: A tuple containing a return code and a string message
            indicating status. The message is for information purpose only.
        """
        command = self.get_command_object("On")
        (return_code, message) = command(argin)
        return [[return_code], [message]]

    def is_Off_allowed(self: CspController) -> bool:
        """
        Return whether the `Off` command may be called in the current device
        state.

        :return: whether the command may be called in the current device
            state
        :raise: `DevFailed` exception if the device is not in one of the
            following states: ON, STANDBY,OFF
        """
        return self.get_state() in [
            DevState.STANDBY,
            DevState.OFF,
            DevState.ON,
        ]

    @command(
        dtype_in="DevVarStringArray",
        doc_in=(
            "If the array length is 0, the command applies to the whole CSP"
            " Element. If the array length is > 1, each array element"
            " specifies the FQDN of the CSP SubElement to switch ON."
        ),
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def Off(self: CspController, argin: List[str]) -> Tuple[ResultCode, str]:
        """Turn device off.

        :param List of fqdn of controller's subsystems to operate

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        command = self.get_command_object("Off")
        (return_code, message) = command(argin)
        return [[return_code], [message]]

    def is_Standby_allowed(self: CspController) -> bool:
        """
        Return whether the `Standby` command may be called in the current
        device state.

        :return: whether the command may be called in the current device
            state
        :raise: `DevFailed` exception if the device is not in one of the
            following states: ON, STANDBY,OFF
        """
        return self.get_state() in [
            DevState.ON,
            DevState.OFF,
            DevState.STANDBY,
        ]

    @command(
        dtype_in="DevVarStringArray",
        doc_in=(
            "If the array length is 0, the command applies to the whole CSP"
            " Element. If the array length is > 1, each array element"
            " specifies the FQDN of the CSP SubElement to switch ON."
        ),
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def Standby(
        self: CspController, argin: List[str]
    ) -> Tuple[ResultCode, str]:
        """Turn device standby.

        :param List of fqdn of controller's subsystems to operate

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        command = self.get_command_object("Standby")
        (return_code, message) = command(argin)
        return ([return_code], [message])

    def is_Reset_allowed(self: CspController) -> bool:
        """
        Return whether the `Reset` command may be called in the current device
            state.

        :return: whether the command may be called in the current device
            state
        """
        return self.get_state() not in [DevState.INIT]

    # -----------------
    # Device Properties
    # -----------------

    ConnectionTimeout = device_property(dtype="DevUShort", default_value=300)

    PingConnectionTime = device_property(dtype="DevUShort", default_value=5)

    DefaultCommandTimeout = device_property(
        dtype="DevUShort", default_value=10
    )

    CspCbf = device_property(
        dtype="DevString",
    )

    CspPss = device_property(
        dtype="DevString",
    )

    CspPstBeams = device_property(
        dtype="DevVarStringArray",
    )

    CspSubarrays = device_property(
        dtype="DevVarStringArray",
    )

    # ----------
    # Attributes
    # ----------

    commandResult = attribute(
        dtype=("str",),
        # Always the last result (unique_id, result_code, task_result)
        max_dim_x=2,
        access=AttrWriteType.READ,
        doc=(
            "command_name, result_code \n"
            "Gives the name and the result_code of latest command execution"
        ),
    )

    isCommunicating = attribute(
        dtype="DevBoolean",
        access=AttrWriteType.READ,
        doc=(
            "Whether the device is communicating with the component under"
            " control"
        ),
    )

    adminMode = attribute(
        dtype=AdminMode,
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
        doc=(
            "The admin mode reported for this device. It may interpret the"
            "current device condition and condition of all managed devices"
            "to set this. Most possibly an aggregate attribute."
        ),
    )

    commandTimeout = attribute(
        dtype="DevUShort",
        access=AttrWriteType.READ_WRITE,
        label="commandTimeout",
        max_value=100,
        min_value=0,
        doc=("Report the set command timeout"),
    )

    cspCbfState = attribute(
        dtype="DevState",
        label="cspCbfState",
        # polling_period=3000,
        doc=(
            "The CBF sub-element Device State. Allowed values are ON, STANDBY,"
            " OFF, DISABLE, ALARM, FAULT, UNKNOWN"
        ),
    )

    cspPssState = attribute(
        dtype="DevState",
        label="cspPssState",
        # polling_period=3000,
        doc=(
            "The PSS sub-element Device State. Allowed values are ON, STANDBY,"
            " OFF, DISABLE, ALARM, FAULT, UNKNOWN"
        ),
    )

    cspPstBeamsState = attribute(
        dtype="DevString",
        label="cspPstBeamsState",
        doc=(
            "Report the state of the pst beams assigned as a"
            " JSON formatted string."
        ),
    )

    cspCbfHealthState = attribute(
        dtype="DevEnum",
        label="cspCbfHealthState",
        # polling_period=3000,
        # abs_change=1,
        doc="The CBF sub-element healthState.",
        enum_labels=[
            "OK",
            "DEGRADED",
            "FAILED",
            "UNKNOWN",
        ],
    )

    cspPssHealthState = attribute(
        dtype="DevEnum",
        label="cspPssHealthState",
        # polling_period=3000,
        # abs_change=1,
        doc="The PSS sub-element healthState",
        enum_labels=[
            "OK",
            "DEGRADED",
            "FAILED",
            "UNKNOWN",
        ],
    )

    cspPstBeamsHealthState = attribute(
        dtype="DevString",
        label="cspPstBeamsHealthState",
        doc="Report the health state of the pst beams as"
        " a JSON formatted string",
    )

    cbfControllerAddress = attribute(
        dtype="DevString",
        doc="TANGO FQDN of the CBF sub-element Master",
    )

    pssControllerAddress = attribute(
        dtype="DevString",
        doc="TANGO FQDN of the PSS sub-element Master",
    )

    pstBeamsAddresses = attribute(
        dtype=("str",),
        max_dim_x=16,
        doc="TANGO FQDNs of the PST Beams",
    )

    cspCbfAdminMode = attribute(
        dtype="DevEnum",
        access=AttrWriteType.READ_WRITE,
        label="cspCbfAdminMode",
        # polling_period=3000,
        # abs_change=1,
        doc=(
            "The CBF sub-subsystem adminMode. Allowed values are ON-LINE, "
            "ENGINEERING, OFF-LINE, NOT-FITTED, RESERVED"
        ),
        enum_labels=[
            "ON-LINE",
            "OFF-LINE",
            "ENGINEERING",
            "NOT-FITTED",
            "RESERVED",
        ],
    )

    cspPssAdminMode = attribute(
        dtype="DevEnum",
        access=AttrWriteType.READ_WRITE,
        label="cspPssAdminMode",
        # polling_period=3000,
        # abs_change=1,
        doc=(
            "The PSS sub-system adminMode. Allowed values are ON-LINE, "
            "ENGINEERING, OFF-LINE, NOT-FITTED, RESERVED"
        ),
        enum_labels=[
            "ON-LINE",
            "OFF-LINE",
            "ENGINEERING",
            "NOT-FITTED",
            "RESERVED",
        ],
    )

    cspPstBeamsAdminMode = attribute(
        dtype="DevString",
        label="cspPstBeamsAdminMode",
        doc=(
            "Report the administration mode of the beams"
            " as JSON formatted string"
        ),
    )

    numOfDevCompletedTask = attribute(
        dtype="DevUShort",
        label="numOfDevCompletedTask",
        doc="Number of devices that completed the task",
    )

    @attribute(dtype="str")
    def buildState(self: CspController) -> str:
        """
        Read the Build State of the device.

        :return: the build state of the device
        """
        return self.component_manager.csp_build_state

    @attribute(dtype="str")
    def versionId(self: CspController) -> str:
        """
        Return the SW version of the current device.

        The SW version is equal to the release number
        of the CSP Helm chart and also of the containerized
        image.

        :return: the version id of the device
        """
        return self.component_manager._csp_version

    @attribute(dtype="str")
    def cbfVersion(self: CspController) -> str:
        """Return the SW version of the CBF Controller.

        :return: the CBFVersion attribute.
        """
        return self.component_manager._cbf_version

    @attribute(dtype="str")
    def pssVersion(self: CspController) -> str:
        """Return the SW version of the PSS Controller.

        :return: the PSSVersion attribute.
        """
        return self.component_manager._pss_version

    @attribute(dtype=(str,), max_dim_x=16)
    def pstVersion(self: CspController) -> List[str]:
        """Return the SW version of the PST Beams.

        :return: the PSTVersion attribute.
        """
        return self.component_manager._pst_version

    @attribute(dtype="str")
    def jsonComponentVersions(self: CspController) -> str:
        """Return a JSON formatted string with the SW of all the
        CSP Controller subordinate sub-systems.

        Example:

        '{"mid_csp_cbf/sub_elt/controller": "0.11.4",
        "mid-csp/subarray/01": "0.16.3",
        "mid-csp/subarray/02": "0.16.3",
        "mid-csp/subarray/03": "0.16.3"}'

        :return: JSON formatted string.
        """
        return self.component_manager._json_component_versions

    @attribute(
        dtype="DevBoolean",
        doc="flag to test alarm",
    )
    def testAlarm(self: CspController) -> bool:
        """Flag to enable/disable to test the AlarmHandler."""
        return self._test_alarm

    @testAlarm.write
    def testAlarm(self: CspController, argin: bool) -> None:
        """
        Enable/Disable the testAlarm flag.Used to test the
        CSP.LMC AlarmHandler when deployed.

        :param argin: True/False
        """
        self._test_alarm = argin
        self.push_change_event("testAlarm", self._test_alarm)

    def read_adminMode(self: CspController) -> AdminMode:
        """
        Return the admin Mode of the CSP sub-system.

        :return: admin Mode of the device
        """
        return self._admin_mode

    def write_adminMode(self: CspController, value: AdminMode) -> None:
        """Set the administration mode for the whole CSP sub-system.

        :param value: one of the administration mode value (ON-LINE,\
            OFF-LINE, ENGINEERING, NOT-FITTED, RESERVED).
        """
        self.logger.debug(f"Set device adminMode to {AdminMode(value).name}")
        self.component_manager._store_admin_mode = (
            self.component_manager._admin_mode
        )
        self.component_manager._admin_mode = value

        if value == AdminMode.NOT_FITTED:
            self.admin_mode_model.perform_action("to_notfitted")
        elif value == AdminMode.OFFLINE:
            self.admin_mode_model.perform_action("to_offline")
            if self.component_manager.is_communicating:
                try:
                    self.component_manager.stop_communicating()
                except ValueError as err:
                    # restore the orignal value
                    self.component_manager._admin_mode = (
                        self.component_manager._store_admin_mode
                    )
                    if self.component_manager._admin_mode == AdminMode.ONLINE:
                        self.admin_mode_model.perform_action("to_online")
                    else:
                        self.admin_mode_model.perform_action("to_engineering")
                    raise ValueError(err) from err
        elif value == AdminMode.ENGINEERING:
            self.admin_mode_model.perform_action("to_engineering")
            self.component_manager.start_communicating()
        elif value == AdminMode.ONLINE:
            self.admin_mode_model.perform_action("to_online")
            self.component_manager.start_communicating()
        elif value == AdminMode.RESERVED:
            self.admin_mode_model.perform_action("to_reserved")
        else:
            raise ValueError(f"Unknown adminMode {value}")

    def read_commandTimeout(self) -> int:
        """Return the commandTimeout attribute.

        :return: The duration of the command timeout in sec..

        """
        return self.component_manager.command_timeout

    def write_commandTimeout(self, value):
        """
        Set the value of the command timeout in sec.

        Configure the timeout applied to each command. The specified
        value shall be > 0.

        Negative values raise a *TypeError* exception. This is the expected
        behavior because the attribute has been defined as DevUShort.
        A zero value raises a *ValueError* exception.

        :raises: TypeError or tango.DevFailed depending on whether the
                specified value is negative or zero if a negative value.
        """
        self.component_manager.command_timeout = value

    def read_cspCbfState(self: CspController) -> DevState:
        """Return the state value of the CBF Controller."""
        try:
            return self.component_manager.components[self.CspCbf].state
        except KeyError:
            self.logger.warning("Cbf is unavailable, can't read state.")
            return DevState.DISABLE
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"Cannot read Cbf State. Error: {e}")
            return DevState.UNKNOWN

    def read_cspPssState(self: CspController) -> DevState:
        """Return the the state value of the PSS Controller.."""
        try:
            return self.component_manager.components[self.CspPss].state
        except KeyError:
            self.logger.warning("Pss is unavailable, can't read state.")
            return DevState.DISABLE
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"Cannot read Pss State. Error: {e}")
            return DevState.UNKNOWN

    def read_cspPstBeamsState(self: CspController) -> str:
        """Return the state of the deployed PST beams."""
        # set Pst Beams default values for State
        pst_beams_state_dict = {}
        if self.CspPstBeams:

            pst_beams_state_list = [DevState.UNKNOWN] * len(self.CspPstBeams)
            pst_beams_state_dict.update(
                zip(self.CspPstBeams, pst_beams_state_list)
            )
            # build the Json string with info
            if self.component_manager:
                for (
                    fqdn,
                    component,
                ) in self.component_manager.components.items():
                    if fqdn in self.component_manager.PstBeamsFqdn:
                        pst_beams_state_dict[fqdn] = component.state.name
        pst_beams_state_str = json.dumps(pst_beams_state_dict)
        return pst_beams_state_str

    def read_cspCbfHealthState(self: CspController) -> DevState:
        """Return the health state value of the CBF Controller."""
        try:
            return self.component_manager.components[self.CspCbf].health_state
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"Cannot read Cbf HealthState. Error: {e}")
            return HealthState.UNKNOWN

    def read_cspPssHealthState(self: CspController) -> DevState:
        "Return the health state value of the PSS Controller." ""
        try:
            return self.component_manager.components[self.CspPss].health_state
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"Cannot read Pss HealthState. Error: {e}")
            return HealthState.UNKNOWN

    def read_cspPstBeamsHealthState(self: CspController) -> DevState:
        """Return the health state of the deployed PST beams."""
        # set Pst Beams default values for the healthState
        pst_beams_health_dict = {}
        if self.CspPstBeams:

            pst_beams_health_list = [HealthState.UNKNOWN] * len(
                self.CspPstBeams
            )
            pst_beams_health_dict.update(
                zip(self.CspPstBeams, pst_beams_health_list)
            )
            # build the Json string with info
            if self.component_manager:
                for (
                    fqdn,
                    component,
                ) in self.component_manager.components.items():
                    if fqdn in self.CspPstBeams:
                        pst_beams_health_dict[fqdn] = HealthState(
                            component.health_state
                        ).name
        pst_beams_health_str = json.dumps(pst_beams_health_dict)
        return pst_beams_health_str

    def read_cbfControllerAddress(self: CspController) -> str:
        """Return the FQDN of the CBF Controller"""
        if self.CspCbf:
            return self.CspCbf
        self.logger.info("Cbf Control device not deployed")
        return ""

    def read_pssControllerAddress(self: CspController) -> str:
        """Return the FQDN of the PSS Controller."""
        if self.CspPss:
            return self.CspPss
        self.logger.info("Pss Control device not deployed")
        return ""

    def read_pstBeamsAddresses(self: CspController) -> List[str]:
        """Return the FQDN of the deployed PST beams"""
        if self.CspPstBeams:
            return self.CspPstBeams
        self.logger.info("Pst Beam devices not deployed")
        return [
            "",
        ]

    def read_cspCbfAdminMode(self: CspController) -> AdminMode:
        """Return the admin mode of the CBF Controller."""
        if self.CspCbf:
            try:
                return self.component_manager.components[
                    self.CspCbf
                ].admin_mode
            # pylint: disable-next=broad-except
            except Exception as e:
                self.logger.error(f"Cannot read Cbf AdminMode. Error: {e}")
        return AdminMode.NOT_FITTED

    def write_cspCbfAdminMode(self: CspController, value) -> AdminMode:
        """Set the admin mode pf the CBF Controller.

        Set the CBF sub-element *adminMode* attribute value.

        :param value: one of the administration mode value (ON-LINE,\
            OFF-LINE, ENGINEERING, NOT-FITTED, RESERVED).
        :return: None
        :raises: tango.DevFailed when there is no DeviceProxy providing \
            interface to the CBF sub-element Master, or an exception is \
            caught in command execution.
        """
        pass

    def read_cspPssAdminMode(self: CspController) -> AdminMode:
        """Return the admin mode of the PSS Controller."""
        if self.CspPss:
            try:
                return self.component_manager.components[
                    self.CspPss
                ].admin_mode
            # pylint: disable-next=broad-except
            except Exception as e:
                self.logger.error(f"Cannot read Pss AdminMode. Error: {e}")
        return AdminMode.NOT_FITTED

    def write_cspPssAdminMode(self: CspController, value) -> AdminMode:
        """Set the admin mode pf the PSS Controller

        NOTE: not implemented
        """

        pass

    def read_cspPstBeamsAdminMode(self: CspController) -> str:
        """Return the admin mode of the deployed PST beams."""
        # set Pst Beams default values for the adminMode
        pst_beams_adminmode_dict = {}
        if self.CspPstBeams:
            pst_beams_adminmode_list = [AdminMode.NOT_FITTED] * len(
                self.CspPstBeams
            )
            pst_beams_adminmode_dict.update(
                zip(self.CspPstBeams, pst_beams_adminmode_list)
            )
            # build the Json string with info
            if self.component_manager:
                for (
                    fqdn,
                    component,
                ) in self.component_manager.components.items():
                    if fqdn in self.CspPstBeams:
                        pst_beams_adminmode_dict[fqdn] = AdminMode(
                            component.admin_mode
                        ).name
        pst_beams_adminmode_str = json.dumps(pst_beams_adminmode_dict)
        return pst_beams_adminmode_str

    def read_numOfDevCompletedTask(self: CspController):
        """Return the numOfDevCompletedTask attribute.

        NOTE: not implemented
        """
        pass

    def read_cspSubarrayAddresses(
        self: CspController,
    ) -> List[str]:
        """Return the list with the CspSubarrays' addresses (FQDNs).
        The list is defined as a Device Property
        If the Device Property is not defined, it returns an empty list
        """
        if self.CspSubarrays:
            return self.CspSubarrays
        return []

    def read_listOfDevCompletedTask(self: CspController):
        """Return the list of the devices (FQDNs) that have completed the last
        required task.

        NOTE: not implemented
        """
        pass

    def read_commandResult(self: CspController) -> Tuple[str, ResultCode]:
        """Return the commandResult value. Implemented as a list of 2 strings:

        - element 0: CSP task name
        - element 1: ResultCode value

        This attribute is used to signal the client the completion of
        the execution of a non-blocking task.
        """
        return self._csp_command_result

    def read_isCommunicating(self: CspController) -> bool:
        """Whether the TANGO device is communicating with the controlled
        component."""
        return self.component_manager.is_communicating

    @attribute(
        dtype="DevBoolean",
        doc="Attribute used to enable/disable the CBF simulation mode",
        label="cbfSimulationMode",
    )
    def cbfSimulationMode(self: CspController) -> bool:
        """Return the Simulation Mode of the CBF subsystem.

        :return: Cbf Simulation mode.
        """
        return self.component_manager.get_cbf_simulation_mode()

    @cbfSimulationMode.write
    def cbfSimulationMode(
        self: CspController, value: cbfSimulationMode
    ) -> None:
        """
        Set the Simulation Mode of the CBF subsystem.

        :param value: desired Simulation Mode of the device.
        """
        self.component_manager.set_cbf_simulation_mode(value)


# ----------
# Run server
# ----------


# pylint: disable-next=missing-function-docstring
def main(args=None, **kwargs):
    return run((CspController,), args=args, **kwargs)


if __name__ == "__main__":
    main()
