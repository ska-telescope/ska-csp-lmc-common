###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project

Latest release
--------------

1.0.1
-----
- Controller checks that all subarrays are EMPTY before setting the adminmode value to OFFLINE  
- Removed code related to storing the assignTimingBeams attribute  
- PST beams can be deallocated from a subarray even if their state is not OFF. 

1.0.0
-------
- Add the TaskTracker class to handle the progress of a task.
- Fix the main task executor to properly run the tasks.
- Renamed the classes and files to use Main instead of Root.
- Rework commands (fix SKB-637, SKB-501, SKB-629, SKB-505, SKB-689)
  - update abort
  - update command timeout
  - implement command map as class
  - update initialisation process and init command
  - include capabilities in the initialisation process
  - update standby
  - update on/off
  - update scan
  - update gotoidle/reset/restart
  - update loadDishCfg
  - update assign/release/releaseAll resources
  - clean code
  - fix restart command release pst beam (fix SKB-708)
  - update documentation
  - writing adminmode allowed only on ObsState.EMPTY (SKB-621)

0.28.0
-------
- Added coherence check for PST beam IDs to be assigned:
  - Ensured that the list of beams to assign is a subset of the deployed PST beams.
- update pst observationMode attribute to pstProcessingMode  

0.27.2
-------
- Fix SKB-591

0.27.1
-------
- Reset obsMode after abort (fix SKB-493)

0.27.0
-------
- Update tel model to v1.19.2
- Review of the JSON parser.

0.26.0
-------
- Add CspCapability abstract class
- Add CspCapability base class device implementation
- Add monitoring lib to calculate avg and stddev
- Add MonitoringData class

0.25.1
-------
- Fix obsMode bug (SKB-472)

0.25.0
-------
- Extend CspJsonValidator class to perform semantic checks on JSON config file
- Override the obsMode attribute and change the type to array of obsMode values
- Implement evaluation of the obsMode based on Configure json input

0.24.0
------
- Update documentation
- Update tel model to v1.18.1

0.23.0
------
- Update to:
  - SKA BC 1.0.0
  - Telescope Model 1.16.0
- Updated tests to use adminMode ENGINEERING

0.22.1
------
- common tools for test result data collection (resources)
- Implement JSON stringent validation criteria (strictness=2 in TelModel validate).
  Modified the example files and tests to ensure they pass the stringent validation criteria.

0.22.0
------
- Add CHANGELOG to documentation
- Fix versionId value
- Moved to SKA Base Classes 0.20.2 and pytango 9.5.0

0.21.0
------
- update RTD building procedure according to STS-553
- update RTD documentation
- Add subscription of LRC command attributes.
- Off command relies also on the LRC status value (not only to
  the state transition)to detect the end of the command.
- remove old simulated devices and integration tests
- enable scanID attribute subscription
- update bdd testing utilities to enable forcing state, obsstate 
  and healthstate on simulated devices in tests

0.20.1
------
- Method to test command rejection in testing/test_bdd_utils

0.20.0
------
- Use Telescope Model 1.14.0
- fix error in _pre_cbf_configure 

0.19.2
------
- Fix handling of abort command during resources assignment.

0.19.1
------
- Report the correct value for the CSP.LMC Subarray configurationID attribute.

0.19.0
------
- Push archive events on State, healthState and obsState attributes.
- Removed unused attributes from Controller and Subarray devices.
- Added ForceHealthState Fast command in CSP.LMC Subarray. Updated the set of ComponentCommand to
  include the ForceHEalthState one.
- Added is_cmd_succeded flag into command observer to pass the command result to the completed callback.
- Controller sna Subarray devices export a R/W alarm falg used to test the AlarmHandler device.
- Controller defines a method to force the healthstate of the CSP.LMC Subarrays to FAILED.

0.18.4
------
Fix issues in PST re-initialization in k8s-test

0.18.3
------
- Change of logic for pst reinitialization in k8s-tests

0.18.2
------
- Fix handling of the admin mode update

0.18.1
------
- Update telescope model to version 1.9.1

0.18.0
------
- Semplified attribute subscription. At subscription the attribute name is converted in
- Semplified attribute subscription. At subscription the attribute name is converted in
  lower case.
- Implemented abstract method to connect to capabilites in the init_callbacks module.
- At disconnection only the attributes of the subordinate sub-systems of the device are
  unsubscribed.
- Specialized the EventManager class for the Capability devices.
- Added possibility to read/write the CBF simulationMode attribute via the controller 
  attribute cbfSimulationMode.

0.17.7
------
- Update sphinx packages, add them to pyproject.toml and use .readthedocs.yaml to build documentation
- Fix errors in building documentations
- Updated the jsonComponentVersions attribute to report only the SW of the CSP Controller
  subordinate devices
- Fix bugs with pst beam device: when disconnected/offile/unknown its obsState has
  to be reported as IDLE, not EMPTY.
- Changed the AssignRelease component commands to handle PST beam obsState.
- Set the correct result code (as a tuple) in mocked_connector.
- Fix a bad bug in ComponentAssign and ComponentRelease commands

0.17.6
------
- Command timeout default values set as Device Property of a device.
- Command timeout is configurable via TANGO attribute commandTimeout.
- Update assignedResources attribute with the list of the FQDNs of the assigned resources
- Update initialization process
- Disable simulators (k8s-test) since don't work with pytango 9.4.2
- Update libraries:
  - pytango v 9.4.2
  - ska-tango-base 0.19.1
  - ska-tango-images-pytango-builder:9.4.3
  - ska-tango-images-pytango-runtime:9.4.3
- Update charts:
  - ska-tango-util 0.4.7
  - ska-tango-base 0.4.7
  - ska-tango-images-tango-dsconfig 1.5.12
  - tango-itango = 9.4.3
- update .make subsystem to master

0.17.5
------
- udpate telescope model to v. 1.8.0
- remove pyproject contstraint version of python (from 3.10.6 to ~3.10.6)  

0.17.4
------
- Implemented the methoed to evaluate the software version ID using the information
  inside the release file.
- Override the TANGO Attributes versionId and buildState in the CSP.LMC Subarray
  device to report the proper values in Mid and Low.

0.17.3
------
- Possibility to specialize gotoidle and configure Component Command for low/mid
- Fix bug multiple setting of adminmode ONLINE put Subarray in FAULT
- Fix bug wrong error message if command not allowed

0.17.2
------
- Update attributes 

0.17.1
------
- Update charts in particular
  - ska-tango-util to v0.4.5
  - ska-tango-base to v0.4.4
- Update test environment
  - add Long Running Commands management

0.17.0
------
- Scan command is completed when all subsystems are scanning

0.16.0
------
- Update to BC v0.18.1
- Implementation of cspJsonValidator

0.15.0
------
- Update to BC v0.17.0
- Implement Abort command behavior for Subarrays
- Configure attributes that report subsystem states and modes to push archive and change events
- Add CSP.LMC devices and subsystems software version ID attributes

0.14.1
------
- Insert synchronization time.sleep during connection (ComponentInit)

0.14.0
------
- Update pytango to 9.3.6 and pytango images (ska-tango-images-pytango-builder:9.3.35 and ska-tango-images-pytango-runtime:9.3.22)

0.13.0
------
- Integration of PST beam 
- refactor of json validation mechanism

0.12.2
------
- check on EventManager about event subscription: don't subscribe attributes multiple times
- new method to check failures in ComponentCommand (``failure_detect``)
- obsState model set to action_driven in Assign/ReleaseResources
- new method to force attribute read in failure conditions (``force_attribute_update``)

0.12.1
------
- Restructuring of EventManager: introduction of BaseEventManager

0.12.0
------
- Changes in command execution. Implementation of ``TaskExecutor`` to enqueue commands issued by the devices,  
  ``MacroComponentCommand`` for managing sequential commands on subsystems,  
  ``SubsystemExecutor`` to execute subsystems commands in threads and 
  ``TaskPriorityScheduler`` for prioritizing subsystem commands.

0.11.15
-------
- Update numpy version to 1.23.0 and python version to 3.10

0.11.14
-------
- AdminMode is forwarded to subsystems
- Use of Mocked connector to generate events in testing

0.11.13
-------
- Update simulators and testing/test_bdd_utils to enable simulation of the timeout and exception for assign/release resources commands.

0.11.12
-------
- Implemented LRC status and result attributes for Assign/Release/ReleaseAll Resources (Subarray)

0.11.11
-------
- Update simulators and testing/test_bdd_utils to enable simulation of the timeout for commands.

0.11.10
-------
- Implemented order of execution for off command.

0.11.9
------
- Implemented LRC status and result attributes only for On/Off and Standby commands (Controller and Subarray)
- Improvement of the adminMode handling and forwarding.
- Adopted the mechanism of the event queue implemented in BC 13 to fix the problem of race conditions.

0.11.8
------
- Fix issue in reading isCommunicating attribute in test_bdd_util module.
- Fix override of adminMode in csp devices.

0.11.7
------
- Setting of adminMode forwarded to subsystems
- Update logging, essentially a lot of INFO messages have been move to DEBUG.

0.11.6
------
- Refactoring of the integration tests
- Implementation of the pytest bdd utilities

0.11.5
------
- Refactoring of the unit tests
- Test code to be shared among tests and mid/low projects moved to 
  src/ska_csp_lmc_common/testing
- Common fixture functions defined in testing/plugins. These are available to tests
  using *pytest_plugins* in conftest.py file.
- skip helm chart publishing in gitlab.

0.11.4
------
- Update to the new CI/CD machinery. 
- Use of Poetry to manage packages and dependencies. 
- Removed import of ska-tmc-common. 
- Large lint and reformat of the code. 

0.11.3
------
- Use of semaphore to synchronize the execution of power commands in the CSP Controller.
- Removed the StepComponent commands.
- Re-organization of the composnents commands in base, observing and macro commands.

0.11.2
------
- Upgraded the Controller and Subarray ComponentManagers, as well as the EventManager,
  to handle other attributes in addition to the main SKA SCM attributes.
- Modified the ObservingComponent class to use the capability_id instead of subarray_id, to
  report the subarray/beam identification numer. subarray_id property is used only
  by PstBeams to report their subarray affiliation.
- implemented the TANGO attribute isCOmmunicating on CSP Subarray and Controller devices to
  report the status of communication between the device and the controlled component.

0.11.1
------
- Upgrade of the Observing State Model to enable/disable updating of the observing state via events
  during long-running tasks
- Modified the handling of the abort of a command.
- Update Subarray/Observing device simulators to set the observing state as soon as a long running command 
  is invoked.

0.11.0
------ 
- Use SKA BC ver. 0.11.3
- Eliminated the use of the State Machine mechanism implemented via the pytransition library.
- Implemented classes for HealthStateModel, OpStateModel and ObsStateModel.
- Added the TANGO attribute commandResult to report the completion of a non-blocking
  command.
- Started re-implementation of the On command for CSP Controller and Subarray.

0.10.4
------
- Remove component's subscription of events from lock in event manager (fix bug CT-565)

0.10.3
------
- When initialization fails the HealthState is now set to FAILED on controller and subarray.

0.10.2
------
- Update reading of state, AdminMode, HealthState, ObsState attributes in controller and subarray

0.10.1
------
- Added simulated device for Subsystem controllers using TangoSimLib

0.10.0
------
- Finished implementation of refactoring for Subarray and Controller
- Implemented Component Manager for Controller
- Implemented StepComponentCommand for Controller commands (single and multiple)
- Implemented StepCtrlObserver

0.8.2
-----
- Migration to ska-tango-base 0.10.1, ska-telescope-model 1.3.0
- Updated pipeline images to those into the CAR repo.
- Modified assign/release resource, configure and scan to support ADR-35.
- Updated Json  manager module (csp_manage_json.py).

0.9.0
-----
- Use ska-tango-base ver. 0.10.1.
- Refactoring package version.
- Moved old file (sources and tests) to old_common dirs.
- Added the new files as per the planned refactoring.
- Started working on CSP.LMC Common Subarray, implementing connection and subscription of events.
- Added device Property *PingConnectionTime* to configure the waited time before retrying the connection.
- (**component.py**) added filtering of event errors raised by the keep-alive thread (every 10 secs.)
- (**event_manager.py**) add method to un-register CSP Subarray callback when all events have been unsubscribed for an attribute
- (**component.py**): fix a bug to catch *KeyError* exception.
- (**subarray_device.py**): added the event un-subscription when the device is deleted.
- (**component.py**): Removed a bug from the *__eq__* method of the Component class.
- New tests to check events on obsState.
- Added the method unsubscribe_attribute. Now the member *_event_id* is defined as a dictionary where are stored the attributes and the corresponding event_id returned at subscription time.
- (**event_manager.py**): added check on the existence of the callback invoked on the CSP device (controller/subarray)
- (**decorators.py**): removed unused code
- Developed tests for the new code and to cover error/exception in the EventManager and Component classes.
- (**mocked.py**): add the side effect for the unsubscribe_attribute.
- (**csplmc_dsconfig.json**): Set the value for the ConnectionTimeout to have a timeout of 3 sec for connection.
- (**connectort.py**): Modified subscribe_attribute to set stateless to True as default.
- (**component.py**): Added *__hash__*, *__key* and *__eq__* methods to make Component class hashable and usable as key in a dictionary.
- *updated _push_event()* callback to handle errors and loss of connection.Push an object of type CspEvent to the EventManager.
- (**event_manager.py**): the *_evt_callback* method handles the CspEvent object and invokes the proper function to evaluate the wrap-up value for the State and healthState.
- (**subarray_device.py**): defined method to update the healthState.
- (**mocked.py**): mocked of *Connector.is_alive()* method
- (**test_subarray_device.py**): tests for events on *State* and *healthState*
- Added the init_device method to CspSubarray device to initialize the member _health_state attribute otherwise pylint complains with the following error:'[E0203(access-member-before-definition), CspSubarray.update_subarray_health_state]. Access to member '_health_state' before its definition'.

0.8.1
-----
- Fix global timeout problem on configuration (YANDA support)

0.8.0
-----
- created new repository on Gitlab (ska-csp-lmc-common) to separate the project from mid-csp and low-csp
- updated pipeline for the new repository
- updated documentation for the new repository

0.7.5
-----
- CspMaster: On/Standby commands: update the master state after switching on/off the subarrays
- CspMaster: use push_change_event from code to update the master State attribute.
- CspSubarray: work to resolve bug skb-49

0.7.4
-----
- OFF command when sub-element is ON-EMPTY
- Added transaction id to the common section of ADR-18/22 json config file.

0.7.3
-----
- Solved dead-lock on Abort Command during Configuration
- Restart Command check the if subelements are EMPTY
- Check failures on GoToIdle execution

0.7.2
-----
- Initialization of CSP Subarray align its state ans obsstate to sub-elements.

0.7.1
-----
- Off command in Subarray drives each sub-element component to the final OFF/EMPTY state.
- CspMaster On/Standby commands are executed synchronously

0.7.0
-----
- Align CSP.LMC to be compliant with the SKA Base Classes API: add of 
  Assign/ReleaseResources commands.
- Add transaction ID for the new commands

0.6.12
------
- support to ADR18/22

0.6.11
------
- fix bug in setup.py

0.6.10
------
- use of ska-log-transaction via transaction_id decorator.

0.6.9
-----
- use lmcbaseclasses 0.6.5

0.6.8
-----
- Removed the CspSubarrayStateModel: no more used. This state model was not correct becuase it
  did not register the callback to push events on obsState attribute 

0.6.7
-----
- Fixed a bug in re-configuration process.

0.6.6
-----
- Added Restart and Reset

0.6.5
-----
- Implemented Abort command. 
- Use mutex to avoid Abort invkoking while the configuring thread is
  checking the global CSP observing state.

0.6.4
-----
- CSP Subarrays are enabled when the  On command is invoked on the CspMaster
- fix EndScan: command invoked synchronously
- GoToIdle now invoked synchronously

0.6.3
-----
- use ska-python-buildenv and runtime 9.3.2
- use lmcbaseclasses ver 0.6.3
- Add support for Off and ObsReset commands
- Fix type_in argument for Scan (now it accepts a DevString instead of a DevStringVarArray)
- EndScan to discuss actual implementation

0.6.2
-----
- Finalized support for Configure command.
- Added GoToIdle command subclassing the ActionCommand class.

0.6.1
-----
- adding support for Configure command

0.6.0
-----
- start implementing lmcbaseclasses 0.6.0

0.5.12
------
- push event on obsState
- set subarray to CONFIGURING at Configure command reception
- modified __configure_scan thread to catch the CbfSubarray CONFIGURING
  obsState 

0.5.11
------
- call to DeviceProxy instead of tango.DeviceProxy to have the deviceemocking works
  properly
- removed the cbfOutputLinks forwarded attribute and updated the configuration
  files for the TANGO DB.
- set the lmcbaseclasses version < 0.6.0

0.5.10
------
- fix a minor bug

0.5.9
-----
- implemented the ADR-4 and ADR-10 decisions:

  * the json file contains the unique configuration ID
    and the list of the outputlinks, channelAverageMap and
    received addresses for CBF
  * the Scan command specifies as argument the scan Id
- added the isCmdInProgress attribute to get information
  about the execution state of a command

0.5.8
-----
- use ska-logging 0.3.0 and lmcbasclasses 0.5.2

0.5.7
-----
- removed the problem with forwarded attributes of array of strings.
  When the root attribute of a forwarded attribute is an array of 
  strings the devices crashes.
  Moved forwarded attributes to TANGO attributes.
- modify install_requires package in setup.py: use of 
  ska-logging version < 0.3.0 (0.3.0 has changed 
  importing and current lmcbaseclasses package is not
  updated, yet.).

0.5.6
-----
- set the exec flag before the thread start
- re-enable the cmd_ended_cb callback registration with
  the asynchronous commands
- configure thread lock to access commad execution flag
- removed some typos
- more logs

0.5.5
-----
- reduced the sleep time inside monitoring threads in CspMaster.py and CspSubarray.py
- the Csp State attribute is updated in the event callback only when the device is
  not running any power command.
- don't register the cmd_ended_cb callback with the asynchronous commands issued to power
  the CSP Element. When the CbfMaster is already in the requested State, the exception thrown
  by it is caught after the end of the thread and the class attribute _cmd_execution_state 
  (even if reset to IDLE inside the thread) is still equal RUNNING.  This causes the device
  failure if a new power command is issued on it. Maybe this issue is related to the PyTango
  issue with threading and I/O.


0.5.3
-----
- Use lmcbaseclasses = 0.5.0
- Moved ConfigureScan command to Configure.
- Moved EndSB() method to GoToIdle().
- CspMaster: moved attribute with "alarm" string in the name to "failure".
- Removed the default polling for xxCmdDurationExpected/Measured attributes.
- Removed a bug in the Off() method.
- CspSubarray: removed severe errors from the delete_device() method.
- Commented out the line with connection to PssSubarray.
- Removed a bug in passing arguments to the SubarrayRejectDecorator.
- Updated the csp-lmc-common TANGO DB configuration to resolve
  forwarded attributes not addressed.
- Install in editable mode the csp-lmc-common package to install also
  lmcbaseclasses package and its dependencies.
- Modified .gitlab-ci.yml file to execute linting and generate metrics.
- Still not resolved the issue to combine coverage outputs of 
  the different repository projects (csp-lmc-common and csp-lmc-mid).

0.4.0
-----
- Use lmcbaseclasses = 0.4.1
- ska-python-buildenv 9.3.1
- ska-python-runtime 9.3.1

0.3.0
-----
- Use lmcbaseclasses version >=0.2.0
