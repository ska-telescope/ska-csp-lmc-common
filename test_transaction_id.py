from ska_csp_lmc_common.utils.decorators import transaction_id
import mock
# pylint: disable=super-init-not-called
# pylint: disable=signature-differs
# pylint: disable=invalid-name
# pylint: disable=missing-function-docstring


# Tests for the transaction_id fixture.
# This fixture function works either with positional or keyword arguments.
# If keyword arguments are specified, the fixture is independent from the
# keyword name.

@transaction_id
def component_manager_command(callback, **argin):
    return argin

def test_transaction_id_not_present():
    """A test that shows that the transaction id is added to the argin when a
    positional arguments is passed to the decorated function."""
    argin = {"id": "test_id"}

    argin = component_manager_command(mock.MagicMock(), **argin)
    assert "transaction_id" in argin

def test_transaction_id_if_present():
    """If the transaction id is already present in argin it is not changed by
    the decorator."""
    argin = {"transaction_id": "test_id"}
    argin = component_manager_command(mock.MagicMock(), **argin)
    assert argin["transaction_id"] == "test_id"
