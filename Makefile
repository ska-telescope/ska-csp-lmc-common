#
# Project makefile for Mid CSP:LMC project. You should normally only need to modify
# DOCKER_REGISTRY_USER and PROJECT below.
#

#
# DOCKER_REGISTRY_HOST, DOCKER_REGISTRY_USER and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for DOCKER_REGISTRY_HOST and overwrites
# DOCKER_REGISTRY_USER and PROJECT to give a final Docker tag of
# artefact.skao.int 
#
PROJECT = ska-csp-lmc-common

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= sim-csp

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= test

# HELM_CHART the chart name
HELM_CHART ?= common-csp-umbrella
HELM_CHARTS = sim-cbf/ sim-pss/ sim-pst/ ska-csp-lmc-common/
HELM_CHARTS_TO_PUBLISH = ska-csp-lmc-common
HELM_BUILD_PUSH_SKIP = true

# K8S_UMBRELLA_CHART_PATH Path of the umbrella chart to work with
K8S_UMBRELLA_CHART_PATH ?= charts/common-csp-umbrella/

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400
# Helm version
HELM_VERSION = v3.3.1
# kubectl version
KUBERNETES_VERSION = v1.19.2
PYTANGO_BUILDER_VERSION = 9.5.0

CI_PROJECT_DIR ?= .

KUBE_CONFIG_BASE64 ?=  ## base64 encoded kubectl credentials for KUBECONFIG
KUBECONFIG ?= /etc/deploy/config ## KUBECONFIG location


XAUTHORITYx ?= ${XAUTHORITY}
THIS_HOST := $(shell ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY := $(THIS_HOST):0
JIVE ?= false# Enable jive
MINIKUBE ?= true ## Minikube or not

# Test runner - run to completion job in K8s
# name of the pod running the k8s_tests
TEST_RUNNER = test-makefile-runner-$(CI_JOB_ID)-$(KUBE_NAMESPACE)-$(HELM_RELEASE)

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
# include OCI Images support 
include .make/oci.mk 

# include k8s support 
include .make/k8s.mk 

# include Helm Chart support 
include .make/helm.mk 

# Include Python support 
include .make/python.mk 

# include raw support 
include .make/raw.mk 

# include core make support 
include .make/base.mk

# include your own private variables for custom deployment configuration 
-include PrivateRules.mak 

# Chart for testing
K8S_CHART = common-csp-umbrella
K8S_CHARTS = $(K8S_CHART)

K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-tango-images-tango-itango:9.3.7 ## TODO: UGUR docker image that will be run for testing purpose
CI_JOB_ID ?= local##pipeline job id
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server
K8S_TEST_RUNNER = test-runner-$(CI_JOB_ID)##name of the pod running the k8s-test

OCI_IMAGES = ska-csp-lmc-common

ITANGO_DOCKER_IMAGE = $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-itango:9.3.7

#PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src:/app/src/ska_csp_lmc_common KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=$(RELEASE_NAME) TANGO_HOST=$(TANGO_HOST)
PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src:/app/src/ska_csp_lmc_common

MARK_UN ?= unit

ADDITIONAL_AFTER_PYTEST ?= -n 4
PYTHON_VARS_AFTER_PYTEST = -k $(MARK_UN) --forked --count=$(COUNT) $(ADDITIONAL_AFTER_PYTEST) -vv --timeout=20
                        #--disable-pytest-warnings $(ADDITIONAL_AFTER_PYTEST) --count=$(COUNT) -vv --durations=10 --durations-min=1.0


PYTHON_BUILD_TYPE = non_tag_pyproject

PYTHON_LINT_TARGET = src/ tests/

# C0114(missing-module-docstring) not enough to disable in file 
# C0103(invalid-name) not enough to disable in file 
# R0401(cyclic-import) not enough to disable in file
# R0801(duplicate-code) not enough to disable in file 
PYTHON_SWITCHES_FOR_PYLINT = --disable=R0401,R0801,C0103,C0114

ifneq ($(CI_REGISTRY),)
K8S_TEST_TANGO_IMAGE = --set sim-cbf.simcsp.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set sim-cbf.simcsp.image.registry=$(CI_REGISTRY)/ska-telescope/ska-csp-lmc-common \
	--set sim-pss.simcsp.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set sim-pss.simcsp.image.registry=$(CI_REGISTRY)/ska-telescope/ska-csp-lmc-common \
	--set sim-pst.simcsp.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set sim-pst.simcsp.image.registry=$(CI_REGISTRY)/ska-telescope/ska-csp-lmc-common \
	--set ska-csp-lmc-common.cspcommon.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set ska-csp-lmc-common.cspcommon.image.registry=$(CI_REGISTRY)/ska-telescope/ska-csp-lmc-common

K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/ska-csp-lmc-common/ska-csp-lmc-common:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
K8S_TEST_TANGO_IMAGE = --set sim-cbf.simcsp.image.tag=$(VERSION) \
	--set sim-pss.simcsp.image.tag=$(VERSION) \
	--set sim-pst.simcsp.image.tag=$(VERSION) \
	--set ska-csp-lmc-common.cspcommon.image.tag=$(VERSION)
K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-csp-lmc-common:$(VERSION)
endif

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.exposeAllDS=true \
	--set global.tango_host=$(TANGO_HOST) \
	--set ska-tango-base.display=$(DISPLAY) \
	--set ska-tango-base.xauthority=$(XAUTHORITY) \
	--set ska-tango-base.jive.enabled=$(JIVE) \
	${K8S_TEST_TANGO_IMAGE} 

# override python.mk python-pre-test target
python-pre-test:
	@echo "python-pre-test: running with: $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) pytest $(PYTHON_VARS_AFTER_PYTEST) \
	 --cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml $(PYTHON_TEST_FILE)"

test-requirements:
	@poetry export --without-hashes --dev --format requirements.txt --output tests/requirements.txt

k8s-pre-test: python-pre-test test-requirements

CONTAINER_FOR_TESTING ?= artefact.skao.int/ska-tango-images-pytango-builder:$(PYTANGO_BUILDER_VERSION)

dev-container: 
	@echo "using $(CONTAINER_FOR_TESTING)"; \
	if [[ "$$(docker ps -aq -f name=container-for-test-common)" ]]; then \
		docker rm -f container-for-test-common; \
	fi; \
	docker run -d -v "$$(pwd)":/app --name container-for-test-common $(CONTAINER_FOR_TESTING) /bin/bash -c "sleep infinity" ; \
	docker exec container-for-test-common /bin/bash -c "poetry config virtualenvs.create false && poetry install" && \
	docker exec -ti container-for-test-common /bin/bash ; \
	docker rm -f container-for-test-common ; \
	make clean;\

COUNT ?= 1
#MARK := pipeline #default
TEST_FOLDER := simulated-system

ifeq ($(MARK), all)
 SEL_TEST=-m pipeline
else ifeq ($(MARK), nightly)
 SEL_TEST=
else 
 SEL_TEST=-m $(MARK)
endif

# set different switches for in cluster: --true-context
k8s-test: PYTHON_VARS_AFTER_PYTEST := \
			--disable-pytest-warnings --count=$(COUNT) --timeout=300 --forked --true-context --log-cli-level=INFO \
			$(SEL_TEST) -k $(TEST_FOLDER)

k8s-pre-template-chart: k8s-pre-install-chart

clean: #remove cache files after test
	@ sudo rm -rf $$(find . -name __pycache__) build .pytest_cache

requirements: ## Install Dependencies
	poetry install

.PHONY: all test up down help k8s show lint logs describe mkcerts localip namespace delete_namespace ingress_check kubeconfig kubectl_dependencies helm_dependencies rk8s_test k8s_test rlint

# used in gitlab  to trigger mid/low pipelines

BRANCH_COMMON = $(shell if [[ ! -z $$CI_COMMIT_BRANCH ]]; then \
					echo $$CI_COMMIT_BRANCH; \
				elif [[ ! -z $$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME ]]; then \
					echo $$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME; \
				fi)

trigger-pipeline: 
	@BRANCH_COMMON="$(BRANCH_COMMON)"; \
	repo="$(repo)"; \
	if [[ ! -z $$BRANCH_COMMON ]]; then \
		CHECK=$$(curl -s --head https://gitlab.com/ska-telescope/ska-csp-lmc-$$repo/-/tree/$$BRANCH_COMMON | head -n 1 | grep "HTTP/2 [23].."); \
	fi; \
	if [[ $$repo == mid ]]; then \
		id=25578388; \
		token=glptt-0780995b5eb8bd12ebb91b9c66cd8f36bbc5a4f3; \
	elif [[ $$repo == low ]]; then \
		id=25588162; \
		token=glptt-4907f5fa12f3062f8e19c18771f599e1ea060b6d; \
	else \
		echo "No repo specified. Aborting"; \
	fi ; \
	if [[ ! -z $$CHECK ]]; then\
		echo "triggering $(BRANCH_COMMON)"; \
		curl --request POST \
  			--form token=$$token\
  			--form ref=$(BRANCH_COMMON) \
			--form "variables[COMMIT_OF_COMMON]=$$CI_COMMIT_SHORT_SHA" \
			--form "variables[PIPELINE_ID]=$$CI_CONCURRENT_ID" \
  			https://gitlab.com/api/v4/projects/$$id/trigger/pipeline; \
	else \
		echo "triggering master"; \
		curl --request POST \
  			--form token=$$token \
			--form ref=master \
  			--form "variables[BRANCH_FROM_COMMON]=$(BRANCH_COMMON)" \
			--form "variables[COMMIT_OF_COMMON]=$$CI_COMMIT_SHORT_SHA" \
			--form "variables[PIPELINE_ID]=$$CI_CONCURRENT_ID" \
  			https://gitlab.com/api/v4/projects/$$id/trigger/pipeline; \
	fi 
