import requests, json, sys

branch_name = "data-mining-test-results"
project_id = int(sys.stdin.read().strip())

url = f"https://gitlab.com/api/v4/projects/{project_id}/pipelines"
headers = {"PRIVATE-TOKEN": "glpat-mY4rMb_XMx1Qscfxusi2"}
params = {"ref": branch_name, "source": "schedule"}

response = requests.get(url, headers=headers, params=params)

pipelines = json.loads(str(response.content).split("'")[1])

for pipeline in pipelines: 
    url_2 = f'{url}/{pipeline["web_url"].split("/")[-1]}'
    pip_id = url_2.split("/")[-1]
    print(pip_id)