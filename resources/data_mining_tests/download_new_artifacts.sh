#!/bin/bash
PROJECT_ID=25588162 #Low
PROJECT_ID=25588162

# Run the Python script to generate the list of folder names
folder_names=$(echo "$PROJECT_ID" | python3 get_pipelines.py)
new_pipelines=""

# Iterate over each folder name
while IFS= read -r folder_name; do
    if [ ! -d "$folder_name" ]; then
        mkdir "$folder_name"
        echo "Folder '$folder_name' created successfully."
        echo "downloading artifacts..."
        job_names=$(echo -e "$PROJECT_ID\n$folder_name" | python3 get_jobs.py)
        while IFS= read -r job_name; do
           if [ ! -d "$folder_name/$job_name" ]; then 
                mkdir $folder_name/$job_name
                (echo -e "$PROJECT_ID\n$folder_name\n$job_name" | python3 download_artifacts.py)
                if [ -f "$folder_name/$job_name/artifacts.zip" ]; then
                    unzip $folder_name/$job_name/artifacts.zip -d $folder_name/$job_name
                    rm $folder_name/$job_name/artifacts.zip
                    (echo -e "$PROJECT_ID\n$folder_name\n$job_name" | python3 populate_dataset.py)
                    rm -rf repo
                fi
            fi
        done <<< "$job_names"
    else
        echo "Folder '$folder_name' already exists."
    fi
done <<< "$folder_names"

