import json
import os
from datetime import datetime
import git
import yaml
import csv, sys

project_id, pipeline_id, job_id = sys.stdin.read().strip().split("\n")

proj = "low" if project_id==25588162 else "mid"

path = f"{pipeline_id}/{job_id}"

log_file_path = f"{path}/trace.log"
report_file_path = f"{path}/build/reports/report.json"
cucumber_file_path = f"{path}/build/reports//cucumber.json"

with open(log_file_path) as file:
    logs = file.readlines()
with open(report_file_path) as file:
    report_json = json.load(file)
with open(cucumber_file_path) as file:
    cucumber_json = json.load(file)
    
#---------------------------------
# Retrieve namespace and commit_id

for log in logs:
    if "Namespace: " in log:
        namespace = log.split("Namespace: ")[-1].replace("\n", "").split(" ")[0] #not_used
        commit_id = namespace.split("low")[-1].split("-")[1]
        break

print(f"{commit_id=}")
print(f"{namespace=}")

#--------------------------------------
# Retrieve the version of sw under test

PROJ_NAME = "ska-csp-lmc-low"
repo = git.Repo.clone_from(f"git@gitlab.com:ska-telescope/ska-csp-lmc-{proj}.git", "repo", branch="master")
commit = repo.commit(commit_id)
tree = commit.tree
blob = tree[f"charts/{proj}-csp-umbrella/Chart.yaml"]
umbrella_chart = blob.data_stream.read().decode()
umbrella_chart = yaml.safe_load(umbrella_chart)
for chart in umbrella_chart["dependencies"]:
    if chart["name"] == PROJ_NAME:
       csp_lmc_version = chart["version"]
summary={}

#-------------------
# Summary of results
for result in "skipped", "passed", "total", "failed":
    try:
        summary[result] = report_json["summary"][result]
    except KeyError:
        summary[result] = 0
        
print(f"Failed: {summary['failed']}")
print(f"Passed: {summary['passed']}")
if "xfailed" in summary:
    print(f"Xfailed: {summary['xfailed']}")
print(f"Skipped: {summary['skipped']}")

#-----------------------------
# Populate list with test data

csv_list = [["job_id", "csp_lmc_version", "subsystem_type", "test_name", "test start time", "outcome", "duration", "setup outcome", 
            "setup duration", "step_kw", "step_name", "step_outcome", "step_duration"]]

test_duration = 0

cucumber_elements = []
somma = 0
for i in range(len(cucumber_json)):
    cucumber_elements.append(cucumber_json[i]["elements"])

# eliminate list of lists
cucumber_elements = [item for sublist in cucumber_elements for item in sublist]

test_ids = [element["id"] for element in cucumber_elements]
num_of_tests = len(report_json["tests"])
print(f"{num_of_tests=}")

for i in range(num_of_tests):
    setup = {}
    test_entry = report_json["tests"][i]
    test_start_time = test_entry["call"]["stdout"].split("|")[1]
    test_id = test_entry["keywords"][0]
    if test_id != test_ids[i]:
        raise ValueError("test_id not correspondant")

    test_name = test_id.split("test_")[-1].replace("_", " ")
    setup_duration = round(test_entry["setup"]["duration"], 3)
    setup_outcome = test_entry["setup"]["outcome"]
    
    test_outcome = test_entry["call"]["outcome"]
    if test_outcome == "skipped":
        continue
    test_duration = round(test_entry["call"]["duration"], 3)

    # evaluate if real or emulated subsystem
    test_folder = test_entry["nodeid"].split("tests")[1].split("step_defs")[0].replace("/", "")
    if test_folder == "integration":
        subsystem_type = "real" 
    elif test_folder == "simulated-subsystems":
        subsystem_type = "emulated"
    else: 
        raise ValueError(f"test folder not recognized: {test_folder}")

    # 
    steps = cucumber_elements[i]["steps"]
    for step in steps:
        step_kw = step["keyword"]
        step_name = step["name"]   
        step_outcome = step["result"]["status"]       
        step_duration = step["result"]["duration"]/10e8
        step_err_msg = ""
        # if step_outcome == "failed":
        #     step_err_msg = step["result"]["error_message"]
        #     if step_err_msg == "":
        #         continue
            
        csv_list.append([job_id, csp_lmc_version, subsystem_type, test_name, test_start_time,
                     test_outcome, test_duration, setup_outcome, setup_duration, step_kw, step_name, step_outcome, step_duration])
        if step_outcome == "failed":
            break

#-------------------
# Append to CSV file

with open("csv_output.csv", 'a') as file:
    # Create a CSV writer object
    writer = csv.writer(file, delimiter="\t")
    
    # Write data to the CSV file row by row
    for row in csv_list:
        writer.writerow(row)