import sys, requests

headers = {"PRIVATE-TOKEN": "glpat-mY4rMb_XMx1Qscfxusi2"}

project_id, pipeline = sys.stdin.read().strip().split("\n")

url = "https://gitlab.com/api/v4/projects/" \
    f"{project_id}/pipelines/{pipeline}/jobs"

n_p = 1
there_is_new_page = True
jobs = []
while there_is_new_page: 
    params = {"page": n_p}
    response = requests.get(url, headers=headers, params=params,)
    there_is_new_page = "next" in response.links
    n_p = n_p+1
    jobs += response.json()

for job in jobs:
    if ("k8s-test" in job["name"] or "no-pst" in job["name"]) and "stop" not in job["name"]:
        print(job["id"])


