import sys, requests

headers = {"PRIVATE-TOKEN": "glpat-mY4rMb_XMx1Qscfxusi2"}

project_id, pipeline, job_id = sys.stdin.read().strip().split("\n")

print(job_id)

url = f"https://gitlab.com/api/v4/projects/{project_id}/jobs/{job_id}/"

response = requests.get(f"{url}artifacts", headers=headers)
# Check if the request was successful
if response.status_code == 200:
    # Save the downloaded artifacts to a file
    with open(f"{pipeline}/{job_id}/artifacts.zip", "wb") as f:
        f.write(response.content)
    print("Artifacts downloaded successfully.")
else:
    print(f"Failed to download artifacts. Status code: {response.status_code}")


response = requests.get(f"{url}trace", headers=headers)
# Check if the request was successful
if response.status_code == 200:
    # Save the downloaded artifacts to a file
    with open(f"{pipeline}/{job_id}/trace.log", "wb") as f:
        f.write(response.content)
    print("Trace downloaded successfully.")
else:
    print(f"Failed to download trace. Status code: {response.status_code}")

