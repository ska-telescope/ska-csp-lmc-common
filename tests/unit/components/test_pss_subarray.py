import logging

import pytest

from ska_csp_lmc_common.subarray import PssSubarrayComponent

module_logger = logging.getLogger(__name__)


@pytest.fixture(scope="function")
def pss_subarray():
    pss_subarray = PssSubarrayComponent("low-pss/subarray/01")
    return pss_subarray


def test_pss_subarray_fqdn(pss_subarray):
    assert pss_subarray.fqdn == "low-pss/subarray/01"


def test_pss_subarray_name(pss_subarray):
    assert pss_subarray.name == "pss-subarray-01"
