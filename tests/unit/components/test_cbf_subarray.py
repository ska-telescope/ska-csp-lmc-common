import logging

import pytest

from ska_csp_lmc_common.subarray import CbfSubarrayComponent

module_logger = logging.getLogger(__name__)


@pytest.fixture(scope="function")
def cbf_subarray():
    cbf_subarray = CbfSubarrayComponent("low-cbf/subarray/01")
    return cbf_subarray


def test_cbf_subarray_fqdn(cbf_subarray):
    assert cbf_subarray.fqdn == "low-cbf/subarray/01"


def test_cbf_subarray_name(cbf_subarray):
    assert cbf_subarray.name == "cbf-subarray-01"
