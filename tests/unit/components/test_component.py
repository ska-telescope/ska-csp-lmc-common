import logging

import mock
import pytest
import tango
from mock import MagicMock
from ska_control_model import AdminMode, HealthState, ObsState

# Tango imports
from tango import DevState, EventType

from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.observing_component import ObservingComponent

# Local imports
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector
from ska_csp_lmc_common.testing.poller import probe_poller

# SKA imports


module_logger = logging.getLogger(__name__)


def fake_send_command_async(command_name, command_data, callback_method):
    tango.Except.throw_exception(
        "Commandfailed",
        "This is error message for devfailed",
        " ",
        tango.ErrSeverity.ERR,
    )


@pytest.fixture(scope="function")
@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def component():
    """Instantiate a generic Component manager and connect it to the controlled
    device."""
    cmp = Component(
        fqdn="example/control/0",
        name="cbf-control",
        weight=1,
        logger=module_logger,
    )
    cmp.connect()
    return cmp


# test properties
def test_fqdn(component):
    """Test the Component fqdn value is properly configured."""
    assert component.fqdn == "example/control/0"


def test_disconnected_component_attrs_values():
    """Test the main SKA State and Modes attributes when a component is not
    connected to the controlled component.

    Expected behavior:
    Default values are returned:
    State: UNKNOWN
    healthState: UNKNOWN
    adminMode: NOT_FITTED
    obsState: EMPTY
    """
    obs_component = ObservingComponent(
        fqdn="example/subarray/01", name="obs-component", weight=1
    )
    assert obs_component.state == DevState.DISABLE
    assert obs_component.health_state == HealthState.UNKNOWN
    assert obs_component.admin_mode == AdminMode.OFFLINE
    assert obs_component.obs_state == ObsState.EMPTY
    assert not obs_component.proxy


def test_connected_component(component):
    """Test the proxy is defined for a connected component."""
    assert component.proxy


def test_component_read_attribute(component):
    """Test read_attribute method returns the value read from the controlled
    device device."""
    assert component.read_attr("state") == DevState.STANDBY
    assert component.read_attr("ondurationexpected") == 1
    assert component.read_attr("adminmode") == AdminMode.ONLINE
    assert component.read_attr("healthState") == HealthState.OK


def test_component_write_attribute(component):
    """Test write_attribute method write the value to the controlled device
    device."""
    component.write_attr("ondurationexpected", 7)
    assert component.read_attr("ondurationexpected") == 7


def test_run_on_command_throw_exception(component):
    def component_run_raise_exception(argument=None, callback=None):
        tango.Except.throw_exception(
            "Command failed",
            "This is error message for devfailed",
            " ",
            tango.ErrSeverity.ERR,
        )

    with mock.patch("ska_csp_lmc_common.component.Component.run") as mock_run:
        mock_run.side_effect = component_run_raise_exception
        with pytest.raises(tango.DevFailed):
            component.run()
            probe_poller(component, "state", DevState.OFF, time=1)


def test_subscribe_attribute(component):
    len_old_event_id = len(component.event_id)
    evt_callback = MagicMock()
    evt_callback.return_value = True
    component.subscribe_attribute(
        "state", EventType.CHANGE_EVENT, evt_callback
    )
    assert len(component.event_id) == len_old_event_id + 1


def test_subscribe_command_attribute(component):
    evt_callback = MagicMock()
    evt_callback.return_value = True
    evt_id, ret_code, exc = component.subscribe_lrc_attribute(
        "state", EventType.CHANGE_EVENT, evt_callback
    )
    assert not len(component.event_id)
    assert evt_id == -1


@pytest.fixture(scope="function")
@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def observing_component():
    obs_component = ObservingComponent(
        fqdn="example/subarray/01", name="obs-component", weight=1
    )
    obs_component.connect()
    return obs_component


def test_observing_component_obs_state(observing_component):
    assert observing_component.obs_state == ObsState.EMPTY


def test_observing_component_weight(observing_component):
    assert observing_component.weight == 1


def test_unsubscribe_attribute(observing_component):
    """Test component un-subscription of event on an attribute."""

    attrs_list = ["state", "healthstate", "temperature"]
    evt_callback = MagicMock()
    evt_callback.return_value = True
    for attr in attrs_list:
        observing_component.subscribe_attribute(
            attr, EventType.CHANGE_EVENT, evt_callback
        )
    assert observing_component.event_attrs.sort() == attrs_list.sort()
    assert len(observing_component.event_id) == 2
    observing_component.unsubscribe_attribute(
        [
            "state",
        ]
    )
    assert len(observing_component.event_id) == 1
    observing_component.unsubscribe_attribute()
    assert not len(observing_component.event_id)


@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_unsubscribe_attribute_with_key_error_exception(observing_component):
    """Test component un-subscription of event on an attribute when the event
    id is not valid.

    Expected result: no change in the number of subscribed attributes.
    """
    attrs_list = [
        "state",
        "healthstate",
        "test-key-error-exception",
        "test-tango-exception",
    ]
    evt_callback = MagicMock()
    evt_callback.return_value = True
    for attr in attrs_list:
        observing_component.subscribe_attribute(
            attr, EventType.CHANGE_EVENT, evt_callback
        )
    assert observing_component.event_attrs.sort() == attrs_list.sort()
    assert len(observing_component.event_id) == 2
    observing_component.unsubscribe_attribute(
        ["test-tango-exception", "test-key-error-exception"]
    )
    assert len(observing_component.event_id) == 2
