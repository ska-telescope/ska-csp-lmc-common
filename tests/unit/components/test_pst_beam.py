import mock
import pytest

from ska_csp_lmc_common.subarray import PstBeamComponent
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector


@pytest.fixture(scope="function")
@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def pst_beam():
    pst_beam = PstBeamComponent("low-pst/beam/01")
    pst_beam.connect()
    return pst_beam


def test_pst_beam_fqdn(pst_beam):
    assert pst_beam.fqdn == "low-pst/beam/01"


def test_pst_beam_name(pst_beam):
    assert pst_beam.name == "pst-beam-01"


@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_pst_beam_subarray_id(pst_beam):
    pst_beam.subarray_id = 3
    assert pst_beam.subarray_id == 3
