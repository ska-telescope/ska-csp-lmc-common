import logging

from ska_csp_lmc_common.capability.capability_monitor import MonitoringData

module_logger = logging.getLogger(__name__)


def test_class_initial_value():
    """Test the initial value of the attributes"""

    buffer_size = 10
    data_storage = MonitoringData("testdatarate", 1, buffer_size)

    json_init = {
        "value": 0.0,
        "avg_value": 0.0,
        "stddev_value": 0.0,
        "avg_buffer": [],
        "stddev_buffer": [],
    }

    assert data_storage.get_value() == 0.0
    assert data_storage.get_avg() == 0.0
    assert data_storage.get_avg_buffer() == []
    assert data_storage.get_data_buffer() == []
    assert data_storage.get_json_data() == json_init
    assert data_storage.get_stddev() == 0.0
    assert data_storage.get_stddev_buffer() == []


def test_class_update_values():
    """Test the initial value of the attributes"""

    buffer_size = 7
    data_storage = MonitoringData("testdatarate", 1, buffer_size)
    data_list = [0.0, 10, 25, 40, 50]
    for new_value in data_list:
        data_storage.update(new_value)

    json_final = {
        "value": 50,
        "avg_value": 25.0,
        "stddev_value": 20.61553,
        "avg_buffer": [0.0, 5.0, 11.66667, 18.75, 25.0],
        "stddev_buffer": [0.0, 7.07107, 12.58306, 17.5, 20.61553],
    }

    assert data_storage.get_value() == 50
    assert data_storage.get_avg() == 25.0
    assert data_storage.get_avg_buffer() == ([0.0, 5.0, 11.66667, 18.75, 25.0])
    assert data_storage.get_data_buffer() == [0.0, 10.0, 25.0, 40.0, 50.0]
    assert data_storage.get_json_data() == json_final
    assert data_storage.get_stddev() == 20.61553
    assert data_storage.get_stddev_buffer() == (
        [0.0, 7.07107, 12.58306, 17.5, 20.61553]
    )


def test_circular_buffer():
    """Test the initial value of the attributes"""

    buffer_size = 3
    data_storage = MonitoringData("testdatarate", 1, buffer_size)
    data_list = [0.0, 10, 25, 40, 50]
    for new_value in data_list:
        data_storage.update(new_value)

    json_final = {
        "value": 50,
        "avg_value": 38.33333,
        "stddev_value": 12.58306,
        "avg_buffer": [11.66667, 25.0, 38.33333],
        "stddev_buffer": [12.58306, 15.0, 12.58306],
    }

    assert len(data_storage.get_data_buffer()) == buffer_size
    assert data_storage.get_value() == 50
    assert data_storage.get_avg() == 38.33333
    assert data_storage.get_avg_buffer() == [11.66667, 25.0, 38.33333]
    assert data_storage.get_data_buffer() == [25.0, 40.0, 50.0]
    assert data_storage.get_json_data() == json_final
    assert data_storage.get_stddev() == 12.58306
    assert data_storage.get_stddev_buffer() == [12.58306, 15.0, 12.58306]


def test_circular_buffer_size_and_clear():
    """Test the initial value of the attributes"""

    buffer_size = 50
    data_storage = MonitoringData("testdatarate", 1, buffer_size)
    for new_value in range(71):
        data_storage.update(new_value)

    assert len(data_storage.get_avg_buffer()) == buffer_size
    assert len(data_storage.get_data_buffer()) == buffer_size
    assert len(data_storage.get_stddev_buffer()) == buffer_size
