import logging

from mock import mock
from ska_control_model import AdminMode, HealthState
from tango import DevState

from ska_csp_lmc_common.capability.capability_data_handler import (
    CapabilityDataHandler,
)

module_logger = logging.getLogger(__name__)

capability_name = "test_cap"
list_of_devices = ["dummy/test/01", "dummy/test/02", "dummy/test/03"]


def test_init_data_handler():
    """Test that the json_dit property of the data handler is
    correctly populated at initializiation"""

    dummy_callback = mock.MagicMock()
    cdh = CapabilityDataHandler(capability_name, dummy_callback, module_logger)
    cdh.init(list_of_devices)

    assert cdh._json_dict["devices_deployed"] == 3
    assert len(cdh._json_dict["test_cap"]) == 3
    for i in range(3):
        assert cdh.json_dict["test_cap"][i]["dev_id"] == i + 1
        assert cdh.json_dict["test_cap"][i]["fqdn"] == list_of_devices[i]
        assert cdh.json_dict["test_cap"][i]["state"] == "UNKNOWN"
        assert (
            cdh.json_dict["test_cap"][i]["health_state"]
            == HealthState.UNKNOWN.name
        )
        assert (
            cdh.json_dict["test_cap"][i]["admin_mode"]
            == AdminMode.OFFLINE.name
        )


def test_data_handler_update():
    """Test that the json_dict property of the data handler is
    correctly populated at initializiation"""

    dummy_callback = mock.MagicMock()
    cdh = CapabilityDataHandler(capability_name, dummy_callback, module_logger)
    cdh.init(list_of_devices)

    # update state on device 01
    cdh.update("dummy/test/01", "state", DevState.OFF)
    assert cdh.json_dict["test_cap"][0]["state"] == "OFF"
    dummy_callback.assert_any_call("devicesJson", cdh.json_dict)
    dummy_callback.assert_called_with(
        "devicesstate", ["OFF", "UNKNOWN", "UNKNOWN"]
    )

    # update healthstate on device 02
    cdh.update("dummy/test/02", "healthstate", HealthState.OK)
    assert cdh.json_dict["test_cap"][1]["health_state"] == "OK"
    dummy_callback.assert_any_call("devicesJson", cdh.json_dict)
    dummy_callback.assert_called_with(
        "deviceshealthstate", ["UNKNOWN", "OK", "UNKNOWN"]
    )

    # update adminmode on device 03
    cdh.update("dummy/test/03", "adminmode", AdminMode.ONLINE)
    assert cdh.json_dict["test_cap"][2]["admin_mode"] == "ONLINE"
    dummy_callback.assert_any_call("devicesJson", cdh.json_dict)
    dummy_callback.assert_called_with(
        "devicesadminmode", ["OFFLINE", "OFFLINE", "ONLINE"]
    )
