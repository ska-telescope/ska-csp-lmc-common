from __future__ import annotations

import logging
import time
from typing import Optional

import mock
import pytest
from ska_control_model import HealthState, ResultCode, TaskStatus
from tango import DevState

from ska_csp_lmc_common.capability import (
    CapabilityComponentManager,
    CapabilityDataHandler,
)
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import TestBase

module_logger = logging.getLogger(__name__)

test_properties_pst_capability = {
    "CspController": "common-csp/control/0",
    "ConnectionTimeout": 3,
    "PingConnectionTime": 1,
    "DefaultCommandTimeout": 5,
}


def callback(attr_name, attr_value):
    module_logger.debug(f"attr_name: {attr_name} value; {attr_value}")


@pytest.fixture
def mock_completed_callback():
    """Fixture for a mock completed callback"""
    return mock.MagicMock()


class MockCspCapabilityComponentManager(CapabilityComponentManager):
    def _populate_capability_specifics(self):
        """Specify the specific variables of the Capability"""

        self.master_device_name = "csp-controller"
        self.property_master_fqdn = "CspController"
        self.master_attribute_name = "pstBeamsAddresses"
        self.subsystem_name = "pst-"

    def _instantiate_data_handler(
        self: MockCspCapabilityComponentManager,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate the specific dPstata handler that is meant to
        update the capability attributes"""
        dummy_callback = mock.MagicMock()
        return CapabilityDataHandler(
            "common-pst-capability", dummy_callback, module_logger
        )

    def _read_device_id(self: MockCspCapabilityComponentManager, component):
        return component.name[-2:]

    def _read_device_version(
        self: MockCspCapabilityComponentManager, device_online
    ):
        pass


@pytest.fixture(scope="function")
def capability_component_manager_disconnected(mock_completed_callback):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_pst_capability
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        return MockCspCapabilityComponentManager(
            properties=cm_conf,
            update_device_property_cbk=mock_completed_callback,
            logger=module_logger,
        )


@pytest.fixture(scope="function")
@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def capability_component_manager():
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_pst_capability
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        capability_component_manager = MockCspCapabilityComponentManager(
            properties=cm_conf,
            update_device_property_cbk=callback,
            logger=module_logger,
        )
        capability_component_manager.init()

        capability_component_manager.start_communicating()
        probe_poller(
            capability_component_manager, "is_communicating", True, time=3
        )

        devices_dict = dict()
        pst_fqdn_list = capability_component_manager.devices_fqdn
        for fqdn in pst_fqdn_list:
            pst_id = f"pst-0{pst_fqdn_list.index(fqdn)+1}"

            devices_dict.update({pst_id: fqdn})
        TestCapabilityComponentManager.devices_list = devices_dict
        return capability_component_manager


class TestCapabilityComponentManager(TestBase):
    def test_capability_cm_not_connected(
        self, capability_component_manager_disconnected
    ):
        probe_poller(
            capability_component_manager_disconnected,
            "is_communicating",
            False,
            time=3,
        )
        assert not capability_component_manager_disconnected.devices_state

    def test_capability_cm_connected(self, capability_component_manager):
        """
        Test the connection with the VCC devices and the initial values for the
        main attributes.
        """

        probe_poller(
            capability_component_manager,
            "devices_state",
            [
                "OFF",
                "OFF",
            ],
            time=10,
        )
        probe_poller(
            capability_component_manager,
            "devices_admin_mode",
            [
                "ONLINE",
                "ONLINE",
            ],
            time=4,
        )
        probe_poller(
            capability_component_manager,
            "devices_health_state",
            ["OK", "OK"],
            time=4,
        )
        assert capability_component_manager.devices_deployed == len(
            capability_component_manager.devices_fqdn
        )

    @mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_capability_failure_connection_to_controller(
        self,
        capability_component_manager_disconnected,
        mock_completed_callback,
    ):
        """
        Verify that the device's state and health status are set to
        FAULT/FAILED when the method _connect_device returns with
        failure.
        The connection with the master device fails with no timeout
        condition on connection.
        """
        with mock.patch(
            "ska_csp_lmc_common.manager.csp_base_component_manager."
            "CSPBaseComponentManager._connect_device"
        ) as mock_connect:
            mock_connect.return_value = TaskStatus.COMPLETED, ResultCode.FAILED

            capability_component_manager_disconnected.init()
            capability_component_manager_disconnected.start_communicating()
            time.sleep(3)
            mock_completed_callback.assert_called_with(
                "healthstate", HealthState.FAILED
            )

    @mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_capability_proxy_to_connector_failed(
        self,
        capability_component_manager_disconnected,
        mock_completed_callback,
    ):
        """
        Verify that the device's state and health status are set to
        FAULT/FAILED when the connection with the master device fails
        with no timeout condition on connection.
        """

        def side_effect_failure():
            raise ValueError("Error in connecting")

        with mock.patch(
            "ska_csp_lmc_common.component.Component.connect"
        ) as mock_connect:
            mock_connect.side_effect = side_effect_failure

            capability_component_manager_disconnected.init()
            capability_component_manager_disconnected.start_communicating()
            time.sleep(4)
            mock_completed_callback.assert_called_with(
                "healthstate", HealthState.FAILED
            )

    def test_capability_proxy_to_connector_failed_with_timeout(
        self,
        capability_component_manager_disconnected,
        mock_completed_callback,
    ):
        """
        Verify that the device's state and health status are set to
        FAULT/FAILED when the connection with the master device fails.
        The connection with master fails with a timeout condition.
        """
        capability_component_manager_disconnected.init()
        capability_component_manager_disconnected.start_communicating()
        time.sleep(4)
        mock_completed_callback.assert_called_with(
            "healthstate", HealthState.FAILED
        )

    @mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_capability_failure_in_connection_with_one_pst(
        self,
        capability_component_manager_disconnected,
        mock_completed_callback,
    ):
        """
        Verify that the device's state and health status are set to
        ON/DEGRADED when the connection with one of the PST beams
        fails.
        """

        def side_effect_callable(*args, **kwargs):
            """
            Helper function returning a different value at
            each call.
            """
            # Use the mock's call_count to determine the return value
            # The first call to read_attr raises an exception
            # while the other returns the state to OFF.
            if mock_connect.call_count == 1:
                raise ValueError("Error in reading")
            else:
                return DevState.OFF

        with mock.patch(
            "ska_csp_lmc_common.observing_component"
            ".ObservingComponent.read_attr"
        ) as mock_connect:
            mock_connect.side_effect = side_effect_callable

            capability_component_manager_disconnected.init()
            capability_component_manager_disconnected.start_communicating()
            probe_poller(
                capability_component_manager_disconnected,
                "is_communicating",
                True,
                time=4,
            )
            mock_completed_callback.assert_any_call(
                "healthstate", HealthState.DEGRADED
            )
            mock_completed_callback.assert_any_call("state", DevState.ON)

    @mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_capability_retrieve_fqdns_failure(
        self,
        capability_component_manager_disconnected,
        mock_completed_callback,
    ):
        """
        Verify that the device's state and health status are set to
        ON/DEGRADED when the connection with one of the PST beams
        fails.
        """

        def side_effect_callable(*args, **kwargs):
            """
            Helper function returning a different value at
            each call.
            """
            # Use the mock's call_count to determine the return value
            # The first call to read_attr raises an exception
            # while the other returns the state to OFF.
            if mock_connect.call_count == 1:
                return DevState.ON
            else:
                return []

        with mock.patch(
            "ska_csp_lmc_common.component.Component.read_attr"
        ) as mock_connect:
            mock_connect.side_effect = side_effect_callable

            capability_component_manager_disconnected.init()
            capability_component_manager_disconnected.start_communicating()
            time.sleep(2)
            assert capability_component_manager_disconnected
            probe_poller(
                capability_component_manager_disconnected.master_dev,
                "fqdn",
                "common-csp/control/0",
                time=4,
            )
            mock_completed_callback.assert_any_call("state", DevState.FAULT)
            mock_completed_callback.assert_any_call(
                "healthstate", HealthState.FAILED
            )
            assert (
                not capability_component_manager_disconnected.is_communicating
            )
