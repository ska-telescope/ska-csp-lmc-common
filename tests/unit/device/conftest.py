import pytest
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup


@pytest.fixture
def change_event_callbacks() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of Tango device change event callbacks with
    asynchrony support.

    :return: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "adminMode",
        "obsState",
        "state",
        "healthState",
        "isCommunicating",
        "longRunningCommandProgress",
        "longRunningCommandResult",
        "longRunningCommandStatus",
        timeout=5,
    )
