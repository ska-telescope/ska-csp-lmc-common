from __future__ import annotations

import json
import logging
import time

# from functools import partial
from typing import Any

import mock
import pytest

# Tango imports
import tango
from ska_control_model import (
    AdminMode,
    HealthState,
    ObsMode,
    ObsState,
    TaskStatus,
)
from ska_control_model.pst_processing_mode import PstProcessingMode
from ska_tango_base.commands import ResultCode
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevState
from tango.test_context import DeviceTestContext

from ska_csp_lmc_common import release
from ska_csp_lmc_common.subarray import PstBeamComponent
from ska_csp_lmc_common.testing import mock_config
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector

# Local imports
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import (
    TestBaseSubarray,
    load_json_file,
)

# from ska_csp_lmc_common.testing.test_utils import SideEffect

# Device test case
device_to_load = {
    "path": "tests/unit/csplmc_dsconfig.json",
    "package": "ska_csp_lmc_common",
    "device": "comsub1",
}

devices_list = [
    "common-cbf/subarray/01",
    "common-pss/subarray/01",
    "common-pst/beam/01",
    "common-pst/beam/02",
]

module_logger = logging.getLogger(__name__)


@pytest.fixture
@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_subarray_init(device_under_test: DeviceTestContext):
    """Perform the CSP subarray initialization with PST beams that can already
    belong to the subarray (re-initialization).

    The Connector class is mocked through the MockedConnector class.
    """
    # create the events to simulate the state after connection
    subarray_init(device_under_test)


def subarray_init(device_under_test: DeviceTestContext):
    # TestCspSubarray.create_events("state", DevState.OFF)
    # TestCspSubarray.create_events("healthstate", HealthState.OK)
    # at the end of the device initialization, the device is
    # OFFLINE in DISABLE state and UNKNOWN healthState
    probe_poller(device_under_test, "state", DevState.DISABLE, time=1)
    assert device_under_test.adminmode == AdminMode.OFFLINE
    assert device_under_test.obsstate == ObsState.EMPTY
    assert device_under_test.healthstate == HealthState.UNKNOWN
    device_under_test.adminMode = AdminMode.ENGINEERING
    probe_poller(device_under_test, "isCommunicating", True, time=5)
    probe_poller(device_under_test, "adminMode", AdminMode.ENGINEERING, time=1)
    time.sleep(0.5)
    probe_poller(device_under_test, "state", DevState.OFF, time=5)
    probe_poller(device_under_test, "healthstate", HealthState.OK, time=5)


@pytest.fixture
@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_subarray_init_with_all_subsystems(
    device_under_test: DeviceTestContext,
):
    """Perform the CSP subarray initialization with all PST beams already
    belonging to the subarray (re-initialization).

    The Connector class is mocked through the MockedConnector class.
    """
    with mock.patch.object(
        PstBeamComponent, "subarray_id", new_callable=mock.PropertyMock
    ) as mocked_subarray_id:
        mocked_subarray_id.return_value = 1

        # at the end of the device initialization, the device is
        # OFFLINE in DISABLE state and UNKNOWN healthState
        subarray_init(device_under_test)
        pst_beams_health = device_under_test.pstBeamsHealthState
        pst_health_dict = json.loads(pst_beams_health)
        assert pst_health_dict["common-pst/beam/01"] == "OK"
        assert pst_health_dict["common-pst/beam/02"] == "OK"
        pst_beams_state = device_under_test.pstBeamsState
        pst_state_dict = json.loads(pst_beams_state)
        assert pst_state_dict["common-pst/beam/01"] == "OFF"
        assert pst_state_dict["common-pst/beam/02"] == "OFF"
        probe_poller(
            device_under_test,
            "pssSubarrayAdminMode",
            AdminMode.ENGINEERING,
            time=3,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayAdminMode",
            AdminMode.ENGINEERING,
            time=3,
        )


@pytest.fixture
@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_subarray_init_with_no_pst(device_under_test: DeviceTestContext):
    """Perform the CSP subarray initialization with no PST beam
    belonging to the subarray (re-initialization).
    """
    # with mock.patch.object(
    #     PstBeamComponent, "subarray_id", new_callable=mock.PropertyMock
    # ) as mocked_subarray_id:
    #     mocked_subarray_id.return_value = 0

    subarray_init(device_under_test)
    probe_poller(device_under_test, "state", DevState.OFF, time=5)


@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
class TestCspSubarray(TestBaseSubarray):
    def test_subarray_first_initialization(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """Perform the CSP first initialization, with PST Beams not assigned:

        they return 0 for the subarray_id.
        """
        assert device_under_test.adminmode == AdminMode.OFFLINE
        probe_poller(device_under_test, "state", DevState.DISABLE, time=1)
        assert device_under_test.healthstate == HealthState.UNKNOWN
        assert device_under_test.obsstate == ObsState.EMPTY
        device_under_test.adminMode = AdminMode.ENGINEERING
        probe_poller(
            device_under_test, "adminMode", AdminMode.ENGINEERING, time=1
        )
        probe_poller(device_under_test, "state", DevState.OFF, time=5)
        probe_poller(device_under_test, "obsmode", (ObsMode.IDLE,), time=3)

    def test_adminmode_change_not_allowed_in_idle(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_no_pst: pytest.fixture,
    ):
        """Test the CSP Subarray adminMode is not allowed to be changed
        when the subarray is not EMPTY."""

        # Force the devices to ON/IDLE
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        assert device_under_test.adminmode == AdminMode.ENGINEERING
        with pytest.raises(
            tango.DevFailed, match=r"adminMode can't be modified"
        ):
            device_under_test.adminmode = AdminMode.ONLINE
            assert device_under_test.adminmode == AdminMode.ENGINEERING

    def test_subarray_on_cmd_not_allowed(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """Test that off command is rejected if DevState is DISABLE"""
        probe_poller(device_under_test, "state", DevState.DISABLE, time=5)
        with pytest.raises(tango.DevFailed) as df:
            device_under_test.On()
        assert df._excinfo[1].args[0].reason == "API_CommandNotAllowed"

    def test_subarray_off_cmd_not_allowed(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """Test that off command is rejected if DevState is DISABLE"""
        probe_poller(device_under_test, "state", DevState.DISABLE, time=5)
        with pytest.raises(tango.DevFailed) as df:
            device_under_test.Off()
        assert df._excinfo[1].args[0].reason == "API_CommandNotAllowed"

    def test_subarray_init_with_pst_beam_read_exception(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """Perform the CSP initialization, with both beams raising an exception
        when the adminmode is read."""

        def side_effect_callable(*args, **kwargs):
            """
            Helper function returning a different value at
            each call.
            """
            # Use the mock's call_count to determine the return value
            # The first call to read_attr raises an exception
            # while the other returns the state to OFF.
            if args == "state":
                return DevState.OFF
            else:
                raise ValueError("Error in reading")

        # here we mock the read_attr() method for the pst_beam component.
        with mock.patch(
            "ska_csp_lmc_common.subarray.pst_beam.PstBeamComponent.read_attr"
        ) as mock_read_attr:
            # The mocked read_attr method returns a ValueError exception,
            # any other call returns a default value.

            mock_read_attr.side_effect = side_effect_callable
            assert device_under_test.adminmode == AdminMode.OFFLINE
            probe_poller(device_under_test, "state", DevState.DISABLE, time=1)
            device_under_test.adminMode = AdminMode.ENGINEERING
            probe_poller(device_under_test, "isCommunicating", True, time=5)
            probe_poller(
                device_under_test,
                "cbfSubarrayAdminMode",
                AdminMode.ENGINEERING,
                time=5,
            )
            pst_beams_admin_mode = {}
            assert device_under_test.pstBeamsAdminMode == json.dumps(
                pst_beams_admin_mode
            )
            probe_poller(device_under_test, "obsmode", (ObsMode.IDLE,), time=3)

    def test_subarray_init_with_cbf_read_exception(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """Perform the CSP initialization, with CBF raising an exception
        when the adminMode is read."""
        # here we mock the read_attr() method for the cbf_subarray component.
        with mock.patch(
            "ska_csp_lmc_common.subarray.cbf_subarray."
            "CbfSubarrayComponent.read_attr"
        ) as mock_read_attr:
            # The mocked read_attr method returns a ValueError exception,
            # any other call returns a default value.
            mock_read_attr.side_effect = ValueError("Failure in reading")
            assert device_under_test.adminmode == AdminMode.OFFLINE
            probe_poller(device_under_test, "state", DevState.DISABLE, time=1)
            device_under_test.adminMode = AdminMode.ENGINEERING
            probe_poller(device_under_test, "isCommunicating", False, time=5)

    def test_command_timeout(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        assert device_under_test.commandTimeout == 5
        device_under_test.commandTimeout = int(6)
        assert device_under_test.commandTimeout == 6

    def test_command_timeout_negative_value(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """
        Test commandTimeout when a negative value is specified.
        """
        with pytest.raises(TypeError):
            device_under_test.commandTimeout = -2
            assert device_under_test.commandTimeout == 5

    def test_command_timeout_null_value(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """
        Test commandTimeout when a null value is specified.
        """
        with pytest.raises(tango.DevFailed) as df:
            device_under_test.commandTimeout = 0
            assert device_under_test.commandTimeout == 5
        # df is ExcpetionInfo type
        # df.value is DevFailed type
        assert (
            "ValueError: Try to set timeout to 0.Timeout shall be > 0"
            in df.value.args[0].desc
        )

    def test_subarray_subsystem_attributes(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """
        Test SCM sub-systems attributes before connection.
        """
        assert device_under_test.cbfSubarrayAddress == "common-cbf/subarray/01"
        assert device_under_test.cbfSubarrayAdminMode == AdminMode.OFFLINE
        assert device_under_test.cbfSubarrayState == DevState.DISABLE
        assert device_under_test.cbfSubarrayHealthState == HealthState.UNKNOWN
        assert device_under_test.cbfSubarrayObsState == ObsState.EMPTY
        assert device_under_test.pssSubarrayAddress == "common-pss/subarray/01"
        assert device_under_test.pssSubarrayAdminMode == AdminMode.OFFLINE
        assert device_under_test.pssSubarrayState == DevState.DISABLE
        assert device_under_test.pssSubarrayHealthState == HealthState.UNKNOWN
        assert device_under_test.pssSubarrayObsState == ObsState.EMPTY
        assert device_under_test.pstBeamsAddress == (
            "common-pst/beam/01",
            "common-pst/beam/02",
        )
        assert device_under_test.pstBeamsAdminMode == "{}"
        assert device_under_test.pstBeamsState == "{}"
        assert device_under_test.pstBeamsHealthState == "{}"
        assert device_under_test.pstBeamsObsState == "{}"

    def test_version_info(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test the information reported by the version attributes.

        Information reported by the CSP sub-systems are generated by mocks
        The values are defined in the mocked_attribute module.
        """
        assert device_under_test.versionId == release.version
        build_state = (
            f"{release.name}, {release.version}, " + f"{release.description}"
        )
        assert device_under_test.buildState == build_state

    def test_testAlarm(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """Test the testAlarm verifying the written
        value and the push of the change events.
        """
        assert not device_under_test.testAlarm
        callback_alarm = mock.Mock()
        device_under_test.subscribe_event(
            "testAlarm", tango.EventType.CHANGE_EVENT, callback_alarm
        )
        device_under_test.testAlarm = True
        assert device_under_test.testAlarm
        probe_poller(callback_alarm, "call_count", 2, time=2)

    def test_scan_id_write_mehod(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """Verify set value for scanID_"""
        device_under_test.scanID = 10
        assert device_under_test.scanID == 10

    def test_subarray_event_success_change_adminMode(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test the CSP device attributes related to subordinate components
        when an event on the adminMode is generated by the subordinate
        components."""
        assert device_under_test.state() == DevState.OFF
        assert device_under_test.adminMode == AdminMode.ENGINEERING
        probe_poller(
            device_under_test,
            "cbfSubarrayAdminMode",
            AdminMode.ENGINEERING,
            time=5,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayAdminMode",
            AdminMode.ENGINEERING,
            time=5,
        )
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(
            device_under_test, "cbfSubarrayAdminMode", AdminMode.ONLINE, time=5
        )
        assert device_under_test.pssSubarrayAdminMode == AdminMode.ONLINE

    def test_subarray_change_event_on_health(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """Test update of the CSP Subarray healthState when all the CSP
        subordinate components fire healthState=OK events.

        Expected result:
        PST beam1 assigned to subarray healthState = OK
        PST beam2 not assigned
        PSS healthState = OK
        CBF healthState = OK
        CSP Subarray healthState = OK
        """
        # create events for the devices in devices_list
        TestCspSubarray.create_events("healthstate", HealthState.OK)
        TestCspSubarray.raise_event("healthstate")

        assert device_under_test.healthState == HealthState.OK
        assert device_under_test.cbfSubarrayHealthState == HealthState.OK
        assert device_under_test.pssSubarrayHealthState == HealthState.OK
        pst_beams_health_str = device_under_test.pstBeamsHealthState
        pst_beams_health_dict = json.loads(pst_beams_health_str)
        for _, health in pst_beams_health_dict.items():
            assert health in ["OK", "UNKNOWN"]

    def test_subarray_change_event_on_health_with_pss_degraded(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test healthState of a CSP subarray when all the CSP Subarray
        subordinate components healthState is OK except the PSS.

        Expected result:
        PSS healthState = DEGRADED
        CBF healthState = OK
        CSP Subarray healthState = DEGRADED
        """
        # create events for the devices in devices_list
        assert device_under_test.state() == DevState.OFF
        assert device_under_test.healthstate == HealthState.OK
        TestCspSubarray.create_events("healthstate", HealthState.OK)
        TestCspSubarray.create_events(
            "healthstate",
            HealthState.DEGRADED,
            ["common-pss/subarray/01"],
        )
        TestCspSubarray.raise_event("healthstate")

        probe_poller(
            device_under_test, "healthstate", HealthState.DEGRADED, time=5
        )
        assert device_under_test.cbfSubarrayHealthState == HealthState.OK
        assert device_under_test.pssSubarrayHealthState == HealthState.DEGRADED

    def test_subarray_change_event_on_health_with_errs(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test CSP subarray healthState when one of the subordinate sub-system
        (PSS Subarray) receives a API_EventTimeout error on subscribed
        healthState and the PSS Subarray is not running (exception
        API_CantConnectToDevice received on ping).

        Expected value:
        - PSS Subarray healthState: UNKNOWN
        - CBF Subarray healthState: OK
        - CSP Subarray healthState: DEGRADED
        """

        # modify the healthstate event for PSS to DEGRADED
        TestCspSubarray.create_events_with_errs(
            "healthstate",
            devices=[
                "common-pss/subarray/01",
            ],
        )
        TestCspSubarray.raise_event(
            "healthstate",
            devices=[
                "common-pss/subarray/01",
            ],
        )

        probe_poller(
            device_under_test, "healthstate", HealthState.DEGRADED, time=5
        )
        assert device_under_test.cbfSubarrayHealthState == HealthState.OK
        assert device_under_test.pssSubarrayHealthState == HealthState.UNKNOWN

    def test_subarray_change_event_on_state_with_pss_errs(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test CSP subarray State and healthState when, after successful
        initialization, an API_EventTimeout error is received on PSS Subarray
        subscribed attributes and the PSS Subarray device is not running
        (exception API_CantConnectToDevice received on ping).

        Expected value:
        - PSS Subarray state: UNKNOWN
        - PSS Subarray healthState: UNKNOWN
        - CBF Subarray state: ON
        - CSP Subarray state: ON
        - CSP Subarray healthState: DEGRADED
        """
        # create events for the devices in devices_list
        TestCspSubarray.create_events("state", DevState.ON)
        # modify the state event for PSS to UNKNOWN
        TestCspSubarray.create_events_with_errs(
            "state",
            devices=[
                "common-pss/subarray/01",
            ],
        )
        TestCspSubarray.raise_event("state")

        probe_poller(device_under_test, "state", DevState.ON, time=5)
        probe_poller(
            device_under_test, "healthState", HealthState.DEGRADED, time=1
        )
        assert device_under_test.cbfSubarrayState == DevState.ON
        assert device_under_test.pssSubarrayState == DevState.UNKNOWN
        assert device_under_test.pssSubarrayhealthState == HealthState.UNKNOWN

    def test_subarray_change_event_on_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test State of a CSP subarray when its subordinate components raise a
        change event on the State attribute."""
        # create events for the devices in devices_list
        TestCspSubarray.create_events("state", DevState.ON)
        TestCspSubarray.raise_event("state")
        probe_poller(device_under_test, "state", DevState.ON, time=1)

        assert device_under_test.cbfSubarrayState == DevState.ON
        assert device_under_test.pssSubarrayState == DevState.ON

    def test_subarray_change_event_on_state_with_cbf_fault(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """Test CSP subarray state CBF Subarray State transitions to FAULT and
        the healthState to FAILED. The CSP is initialized with all the PST
        belonging to it. Expected value:

        - CBF Subarray state: FAULT
        - CBF Subarray healthState: FAILED
        - CSP Subarray state: FAULT
        - CSP Subarray healthState: FAILED
        """
        # create events onState for the devices in devices_list
        TestCspSubarray.create_events("state", DevState.ON)
        # modify the state event for CBF Subarray to FAULT
        TestCspSubarray.create_events(
            "state",
            DevState.FAULT,
            devices=[
                "common-cbf/subarray/01",
            ],
        )
        TestCspSubarray.raise_event("state")
        # when a sub-system transitions to FAULT state, the sub-system
        # healthState should transition to FAILED: the CSP subarray should
        # receive two events, one on the State and the other on the
        # healthState.
        # It's not CSP Subarray responsability to set the healthState to
        # FAILED when the CBF Subarray transitions to FAULT.
        # In the test is simulatd this situation
        TestCspSubarray.create_events(
            "healthstate",
            HealthState.FAILED,
            devices=[
                "common-cbf/subarray/01",
            ],
        )
        TestCspSubarray.raise_event(
            "healthstate",
            devices=[
                "common-cbf/subarray/01",
            ],
        )

        probe_poller(device_under_test, "state", DevState.FAULT, time=5)
        probe_poller(
            device_under_test, "healthState", HealthState.FAILED, time=2
        )
        assert device_under_test.cbfSubarrayState == DevState.FAULT
        assert device_under_test.cbfSubarrayhealthState == HealthState.FAILED
        assert device_under_test.pssSubarrayState == DevState.ON
        assert device_under_test.pssSubarrayhealthState == HealthState.OK

    def test_subarray_change_event_on_state_with_cbf_errs(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """Test CSP subarray state and healthState when an API_EventTimeout
        error is received from a CBF Subarray event and the CBF Subarray device
        is not running (exception API_CantConnectToDevice received on ping).
        The CSP is initialized with only PST Beam 01 belonging to it. Expected
        value:

        - CBF Subarray state: UNKNOWN
        - CBF Subarray healthState: UNKNOWN
        - CSP Subarray state: FAULT
        - CSP Subarray healthState: FAILED
        - PST Beams healthState: unchanged (OK)
        - PSS Subarray healthState: unchanged (OK)
        """
        # create events for the devices in devices_list
        TestCspSubarray.create_events("state", DevState.ON)
        # modify the state event for CBF to FAULT
        TestCspSubarray.create_events_with_errs(
            "state",
            devices=[
                "common-cbf/subarray/01",
            ],
        )
        TestCspSubarray.raise_event("state")
        probe_poller(device_under_test, "state", DevState.FAULT, time=1)
        probe_poller(
            device_under_test, "healthState", HealthState.FAILED, time=1
        )
        assert device_under_test.cbfSubarrayState == DevState.UNKNOWN
        assert device_under_test.healthState == HealthState.FAILED
        assert device_under_test.pssSubarrayState == DevState.ON
        assert device_under_test.pssSubarrayhealthState == HealthState.OK
        pst_beams_health = device_under_test.pstBeamsHealthState
        pst_health_dict = json.loads(pst_beams_health)
        assert pst_health_dict["common-pst/beam/01"] == "OK"
        assert pst_health_dict["common-pst/beam/02"] == "OK"

    def test_subarray_change_event_on_obs_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """Test CSP subarray obsState when all CSP Subarray are in not
        transitional state  but differs from the CBF Subarray value initialized
        to IDLE. The CSP is initialized with only PST Beam 01 assigned to it.
        Expected value:

        - CBF Subarray obsState: IDLE
        - CSP Subarray obsState: IDLE
        - PSS Subarray obsState: READY
        - PST Beam 01 obsState: READY
        """
        # create events for the devices in devices_list
        TestCspSubarray.create_events("obsstate", ObsState.READY)
        # modify the obsState event for CBF to ObsState.IDLE
        TestCspSubarray.create_events(
            "obsstate",
            ObsState.IDLE,
            devices=[
                "common-cbf/subarray/01",
            ],
        )
        TestCspSubarray.raise_event("obsstate")
        pst_beams_obsstate = device_under_test.pstBeamsObsState
        pst_obsstate_dict = json.loads(pst_beams_obsstate)
        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=2)
        assert device_under_test.cbfSubarrayObsState == ObsState.IDLE
        assert device_under_test.pssSubarrayObsState == ObsState.READY
        assert pst_obsstate_dict["common-pst/beam/01"] == "READY"

    def test_subarray_change_event_on_obs_state_with_pss_configuring(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """Test CSP subarray obsState when all CSP Subarray are in CONFIGURING
        except the CBF Subarray that is READY. Expected value:

        - CBF Subarray obsState: READY
        - PSS Subarray obsState: CONFIGURING
        - PST beams1: CONFIGURING
        - CSP Subarray obsState: CONFIGURING
        """
        # create events for the devices in devices_list
        TestCspSubarray.create_events("obsstate", ObsState.READY)
        # modify the obsState event for PTB Beam 1 to ObsState CONFIGURING
        TestCspSubarray.create_events(
            "obsstate",
            ObsState.CONFIGURING,
            devices=[
                "common-pst/beam/01",
            ],
        )
        TestCspSubarray.raise_event("obsstate")

        probe_poller(
            device_under_test, "obsstate", ObsState.CONFIGURING, time=5
        )
        assert device_under_test.cbfSubarrayObsState == ObsState.READY
        assert device_under_test.pssSubarrayObsState == ObsState.READY
        pst_beams_obstate = device_under_test.pstBeamsObsState
        pst_obs_dict = json.loads(pst_beams_obstate)
        assert pst_obs_dict["common-pst/beam/01"] == "CONFIGURING"

    def test_releaseresources_succeeded_no_pst(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_no_pst: pytest.fixture,
    ):
        """Test the CSP Subarray AssignResources Command with simulated
        configuration without PST beams."""

        # Force the devices to ON/IDLE
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        # exercise the device
        [[result], [command_id]] = device_under_test.ReleaseResources(
            '{"subarray_id": 1,"dish":{"receptor_ids":["SKA001", "SKA022"]}}'
        )
        assert result == ResultCode.QUEUED
        # verify the results
        probe_poller(
            device_under_test,
            "longRunningCommandStatus",
            "COMPLETED",
            time=5,
            command_id=command_id,
        )
        pst_beams_state = device_under_test.pstBeamsState
        pst_beams_state_dict = json.loads(pst_beams_state)
        assert not pst_beams_state_dict

    def test_release_all_resources_succeeded_with_no_pst(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_no_pst: pytest.fixture,
    ):
        """Test the CSP Subarray AssignResources Command with simulated
        configuration without PST beams."""

        # Force the devices to ON/IDLE
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        # exercise the device
        _, command_id = device_under_test.ReleaseAllResources()
        # verify the results
        probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=5)
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=5,
            command_id=command_id,
        )

    def test_assign_release_pst_beams(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """Test the CSP Subarray ReleaseResources Command with PST beams."""

        # Force the devices to ON/IDLE
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )

        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.EMPTY
        )

        # assign resources including 2 PST beams
        assignresources_input = load_json_file(
            "test_AssignResources_basic.json", string=True
        )
        # invoke command and wait for its completion
        _, command_id = device_under_test.AssignResources(
            assignresources_input
        )
        # verify the results
        probe_poller(
            device_under_test,
            "longRunningCommandStatus",
            "COMPLETED",
            time=10,
            command_id=command_id,
        )
        assert len(device_under_test.assignedTimingBeamIDs) == 2
        assert device_under_test.assignedTimingBeamIDs[0] == 1
        assert device_under_test.assignedTimingBeamIDs[1] == 2

        # verify the results
        probe_poller(
            device_under_test,
            "obsstate",
            ObsState.IDLE,
            time=10,
            command_id=command_id,
        )
        # force the PST beams to ON (this should be done by the CSP Controller)
        TestCspSubarray.raise_event_on_pst_beams("state", DevState.ON)
        # invoke ReleaseResources removing only 1 PST beams
        _, command_id = device_under_test.ReleaseResources(
            '{"subarray_id": 1,"dish":{ '
            + '"receptor_ids":["SKA001","SKA036"]},'
            + '"pst": {"beams_id":[1]}}'
        )
        # verify the results
        probe_poller(
            device_under_test,
            "commandResult",
            ("releaseresources", "0"),
            time=10,
        )

        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=10,
            command_id=command_id,
        )

        assert len(device_under_test.assignedTimingBeamIDs) == 1
        assert device_under_test.assignedTimingBeamIDs[0] == 2

    # --------------------------ASSIGN RESOURCES--------------------------

    @pytest.fixture
    def assignresources_completed(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test the CSP Subarray AssignResources Command with simulated
        configuration without PST beams."""
        with mock.patch(
            "ska_csp_lmc_common.subarray.cbf_subarray."
            "CbfSubarrayComponent.validated_resources"
        ) as mock_validated, mock.patch(
            "ska_csp_lmc_common.subarray.pss_subarray."
            "PssSubarrayComponent.validated_resources"
        ) as mock_validated, mock.patch(
            "ska_csp_lmc_common.subarray_device.CspSubarray."
            "AssignResourcesCommand.json_configuration_parser"
        ) as mocked_split_resources_assign:
            return_of_split_resource = {
                "common-cbf/subarray/01": {"receptors_ids": ["01", "02"]},
                "common-pss/subarray/01": {"beam_ids": ["03", "04"]},
            }
            mocked_split_resources_assign.return_value = (
                return_of_split_resource
            )
            mock_validated.side_effect = [
                1,
                2,
            ]
            # _, command_id = device_under_test.On([])
            # Force the devices to ON/IDLE
            TestCspSubarray.set_subsystems_and_go_to_state(
                device_under_test, DevState.ON
            )

            _, command_id = device_under_test.AssignResources(
                '{"subarray_id": 1,"dish":{ '
                + '"receptor_ids":["0001","0002"]}}'
            )
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=5)
            probe_poller(
                device_under_test,
                "longRunningCommandAttributes",
                (0, "COMPLETED"),
                time=3,
                command_id=command_id,
            )

    def test_assignresources_with_empty_json(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_no_pst: pytest.fixture,
    ):
        """Test the CSP Subarray AssignResources with wrong configuration.

        Expected result: CSP Subarray maintains its observing state.

        """
        assert device_under_test.obsstate == ObsState.EMPTY
        # device_under_test.On()
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )

        with pytest.raises(
            tango.DevFailed, match=r"Input json is Empty, command is rejected"
        ):
            device_under_test.AssignResources("{}")

    # TODO: does not work
    def test_assignresources_with_invalid_json(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_no_pst: pytest.fixture,
    ):
        """Test the CSP Subarray AssignResources with wrong configuration.

        Expected result: CSP Subarray maintains its observing state.

        """
        command_id = None
        assert device_under_test.obsstate == ObsState.EMPTY
        # device_under_test.On()
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        with pytest.raises(Exception):
            _, command_id = device_under_test.AssignResources(
                '{"subarray_id"}'
            )

        probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=5)

    @pytest.mark.parametrize(
        "initial_obsstate",
        [
            ObsState.RESOURCING,
            ObsState.CONFIGURING,
            ObsState.SCANNING,
            ObsState.ABORTING,
            ObsState.ABORTED,
            ObsState.RESETTING,
            ObsState.FAULT,
            ObsState.RESTARTING,
        ],
    )
    def test_assignresources_not_allowed_obsstate(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configure command when invoked from an obsstate
        not allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        with pytest.raises(tango.DevFailed):
            _, command_id = device_under_test.assignresources(
                load_json_file("test_AssignResources_basic.json", string=True)
            )

    @pytest.mark.parametrize(
        "initial_state",
        [DevState.OFF, DevState.STANDBY, DevState.FAULT, DevState.INIT],
    )
    def test_assignresources_not_allowed_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_state: DevState,
    ):
        """Test CSP Subarray configure command when invoked from a state not
        allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, initial_state
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        module_logger.warning(device_under_test.state())
        with pytest.raises(tango.DevFailed):
            device_under_test.assignresources(
                load_json_file("test_AssignResources_basic.json", string=True)
            )

    @pytest.mark.parametrize("initial_obsstate", [ObsState.EMPTY])
    def test_assignresources_success_on_cbf_pss(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when it is successful for both CBF
        and PSS."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        assignresources_input = load_json_file(
            "test_AssignResources_basic.json", string=True
        )
        module_logger.warning(
            f"Config input type: {type(assignresources_input)}"
        )

        _, command_id = device_under_test.assignresources(
            assignresources_input
        )

        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)
        probe_poller(
            device_under_test,
            "commandresult",
            ("assignresources", str(ResultCode.OK.value)),
            time=10,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=1,
            command_id=command_id,
        )

        probe_poller(
            device_under_test, "cbfSubarrayObsState", ObsState.IDLE, time=1
        )
        probe_poller(
            device_under_test, "pssSubarrayObsState", ObsState.IDLE, time=1
        )

    @pytest.mark.parametrize("initial_obsstate", [ObsState.EMPTY])
    def test_assignresources_success_only_cbf(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when the JSON configuration file
        does not have an entry for PSS configuration."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        assignresources_input = load_json_file(
            "test_AssignResources_basic.json", string=True, remove=["pss"]
        )

        _, command_id = device_under_test.assignresources(
            assignresources_input
        )

        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=3)
        probe_poller(
            device_under_test,
            "commandresult",
            ("assignresources", str(ResultCode.OK.value)),
            time=1,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=1,
            command_id=command_id,
        )
        probe_poller(
            device_under_test, "cbfSubarrayObsState", ObsState.IDLE, time=1
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            initial_obsstate,
            time=1,
        )

    @pytest.mark.parametrize("initial_obsstate", [ObsState.EMPTY])
    def test_assignresources_success_only_pss(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when the JSON configuration file
        does not have an entry for CBF configuration."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )

        assignresources_input = load_json_file(
            "test_AssignResources_basic.json",
            string=True,
            remove=["dish"],
        )

        _, command_id = device_under_test.assignresources(
            assignresources_input
        )

        probe_poller(device_under_test, "obsstate", initial_obsstate, time=3)
        probe_poller(
            device_under_test,
            "commandresult",
            ("assignresources", str(ResultCode.OK.value)),
            time=1,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            initial_obsstate,
            time=1,
        )
        probe_poller(
            device_under_test, "pssSubarrayObsState", ObsState.IDLE, time=1
        )

    @pytest.mark.parametrize("initial_obsstate", [ObsState.EMPTY])
    def test_no_subsystem_to_assignresources(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when the JSON configuration file
        does not specify any entry for CSP sub-systems."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )

        assignresources_input = load_json_file(
            "test_AssignResources_basic.json",
            string=True,
            remove=["dish", "pss", "pst"],
        )

        _, command_id = device_under_test.assignresources(
            assignresources_input
        )

        probe_poller(device_under_test, "obsstate", initial_obsstate, time=3)
        probe_poller(
            device_under_test,
            "commandresult",
            ("assignresources", str(ResultCode.FAILED.value)),
            time=1,
        )

        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (3, "COMPLETED"),
            time=2,
            command_id=command_id,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            initial_obsstate,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            initial_obsstate,
            time=1,
        )

    @pytest.mark.parametrize("initial_obsstate", [ObsState.EMPTY])
    def test_assignresources_pss_in_wrong_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray assign resources when the sub-system is not
        in the proper observing state.

        Currently, if the subsystem is not in the proper observing state,
        its assignresources is skipped, while the other ones are normally
        configured. So, ResultCode of the command is OK.
        """
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        TestCspSubarray.raise_event_on_pss_subarray("obsstate", ObsState.READY)
        assignresources_input = load_json_file(
            "test_AssignResources_basic.json", string=True
        )

        _, command_id = device_under_test.assignresources(
            assignresources_input
        )

        probe_poller(
            device_under_test,
            "commandresult",
            ("assignresources", str(ResultCode.OK.value)),
            time=10,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=1,
            command_id=command_id,
        )

        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=3)
        probe_poller(
            device_under_test, "cbfSubarrayObsState", ObsState.IDLE, time=1
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            ObsState.READY,
            time=1,
        )

    # --------------------RELEASE RESOURCES---------------------------

    @pytest.mark.parametrize(
        "initial_obsstate",
        [
            ObsState.RESOURCING,
            ObsState.CONFIGURING,
            ObsState.SCANNING,
            ObsState.ABORTING,
            ObsState.ABORTED,
            ObsState.RESETTING,
            ObsState.FAULT,
            ObsState.RESTARTING,
        ],
    )
    def test_releaseresources_not_allowed_obsstate(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configure command when invoked from an obsstate
        not allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        with pytest.raises(tango.DevFailed):
            device_under_test.releaseresources(
                load_json_file("test_AssignResources_basic.json", string=True)
            )

    @pytest.mark.parametrize(
        "initial_state",
        [DevState.OFF, DevState.STANDBY, DevState.FAULT, DevState.INIT],
    )
    def test_releaseresources_not_allowed_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_state: DevState,
    ):
        """Test CSP Subarray configure command when invoked from a state not
        allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, initial_state
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        module_logger.warning(device_under_test.state())
        with pytest.raises(tango.DevFailed):
            device_under_test.releaseresources(
                load_json_file("test_AssignResources_basic.json", string=True)
            )

    @pytest.mark.parametrize("initial_obsstate", [ObsState.IDLE])
    def test_releaseresources_success_on_cbf_pss(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
        mock_config_init: mock,
    ):
        """Test CSP Subarray configuration when it is successful for both CBF
        and PSS."""

        # Force the CSP Subarray to ON/EMPTY
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        TestCspSubarray.raise_event_on_pst_beams("state", DevState.ON)
        assignresources_input = load_json_file(
            "test_AssignResources_basic.json", string=True
        )
        module_logger.warning(
            f"Config input type: {type(assignresources_input)}"
        )

        _, command_id = device_under_test.releaseresources(
            assignresources_input
        )

        probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=10)
        probe_poller(
            device_under_test,
            "commandresult",
            ("releaseresources", str(ResultCode.OK.value)),
            time=10,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            ObsState.EMPTY,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            ObsState.EMPTY,
            time=1,
        )

    @pytest.mark.parametrize("initial_obsstate", [ObsState.IDLE])
    def test_releaseresources_success_only_cbf(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when the JSON configuration file
        does not have an entry for PSS configuration."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        TestCspSubarray.raise_event_on_pst_beams("state", DevState.ON)
        assignresources_input = load_json_file(
            "test_AssignResources_basic.json",
            string=True,
            remove=["pss", "pst"],
        )

        _, command_id = device_under_test.releaseresources(
            assignresources_input
        )

        probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=3)
        probe_poller(
            device_under_test,
            "commandresult",
            ("releaseresources", str(ResultCode.OK.value)),
            time=1,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=1,
            command_id=command_id,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            ObsState.EMPTY,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            initial_obsstate,
            time=1,
        )

    @pytest.mark.parametrize("initial_obsstate", [ObsState.IDLE])
    def test_releaseresources_success_only_pss(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when the JSON configuration file
        does not have an entry for CBF configuration."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        TestCspSubarray.raise_event_on_pst_beams("state", DevState.ON)
        assignresources_input = load_json_file(
            "test_AssignResources_basic.json",
            string=True,
            remove=["dish", "pst"],
        )

        _, command_id = device_under_test.releaseresources(
            assignresources_input
        )

        probe_poller(device_under_test, "obsstate", initial_obsstate, time=30)
        probe_poller(
            device_under_test,
            "commandresult",
            ("releaseresources", str(ResultCode.OK.value)),
            time=1,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=1,
            command_id=command_id,
        )
        # probe_poller(
        #     device_under_test,
        #     "longRunningCommandResult",
        #     0,
        #     time=1,
        #     command_id=command_id,
        # )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            initial_obsstate,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            ObsState.EMPTY,
            time=10,
        )

    @pytest.mark.parametrize("initial_obsstate", [ObsState.IDLE])
    def test_no_subsystem_to_releaseresources(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when the JSON configuration file
        does not specify any entry for CSP sub-systems."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )

        assignresources_input = load_json_file(
            "test_AssignResources_basic.json",
            string=True,
            remove=["dish", "pss", "pst"],
        )

        _, command_id = device_under_test.releaseresources(
            assignresources_input
        )

        probe_poller(device_under_test, "obsstate", initial_obsstate, time=3)
        probe_poller(
            device_under_test,
            "commandresult",
            ("releaseresources", str(ResultCode.FAILED.value)),
            time=1,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (3, "COMPLETED"),
            time=1,
            command_id=command_id,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            initial_obsstate,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            initial_obsstate,
            time=1,
        )

    @pytest.mark.parametrize("initial_obsstate", [ObsState.IDLE])
    def test_releaseresources_pss_in_wrong_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray release resources when the sub-system
        is not in the proper observing state.

        Currently, if the subsystem is not in the proper observing state,
        its release resources is skipped, while the other ones are normally
        configured. So, ResultCode of the command is OK."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        TestCspSubarray.raise_event_on_pst_beams("state", DevState.ON)
        TestCspSubarray.raise_event_on_pss_subarray("obsstate", ObsState.READY)
        probe_poller(
            device_under_test, "pssSubarrayObsState", ObsState.READY, time=3
        )
        assignresources_input = load_json_file(
            "test_AssignResources_basic.json", string=True
        )

        _, command_id = device_under_test.releaseresources(
            assignresources_input
        )

        probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=3)
        probe_poller(
            device_under_test,
            "commandresult",
            ("releaseresources", str(ResultCode.OK.value)),
            time=1,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=1,
            command_id=command_id,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            ObsState.EMPTY,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            ObsState.READY,
            time=1,
        )

    # ------------------RELEASE ALL RESOURCES------------------------

    @pytest.mark.parametrize(
        "initial_obsstate",
        [
            ObsState.RESOURCING,
            ObsState.CONFIGURING,
            ObsState.SCANNING,
            ObsState.ABORTING,
            ObsState.ABORTED,
            ObsState.RESETTING,
            ObsState.FAULT,
            ObsState.RESTARTING,
        ],
    )
    def test_releaseallresources_not_allowed_obsstate(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configure command when invoked from an obsstate
        not allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        with pytest.raises(tango.DevFailed):
            device_under_test.releaseallresources()

    @pytest.mark.parametrize(
        "initial_state",
        [DevState.OFF, DevState.STANDBY, DevState.FAULT, DevState.INIT],
    )
    def test_releaseallresources_not_allowed_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_state: DevState,
    ):
        """Test CSP Subarray configure command when invoked from a state not
        allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, initial_state
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        module_logger.warning(device_under_test.state())
        with pytest.raises(tango.DevFailed):
            device_under_test.releaseallresources()

    # ---------------------CONFIGURE RESOURCES------------------------

    @pytest.mark.parametrize(
        "initial_obsstate",
        [
            ObsState.EMPTY,
            ObsState.RESOURCING,
            ObsState.CONFIGURING,
            ObsState.SCANNING,
            ObsState.ABORTING,
            ObsState.ABORTED,
            ObsState.RESETTING,
            ObsState.FAULT,
            ObsState.RESTARTING,
        ],
    )
    def test_configure_not_allowed_obsstate(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configure command when invoked from an obsstate
        not allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        with pytest.raises(tango.DevFailed):
            device_under_test.configure(
                load_json_file("test_ConfigureScan_basic.json", string=True)
            )

    @pytest.mark.parametrize(
        "initial_state",
        [DevState.OFF, DevState.STANDBY, DevState.FAULT, DevState.INIT],
    )
    def test_configure_not_allowed_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_state: DevState,
    ):
        """Test CSP Subarray configure command when invoked from a state not
        allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, initial_state
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        module_logger.warning(device_under_test.state())
        with pytest.raises(tango.DevFailed):
            device_under_test.configure(
                load_json_file("test_ConfigureScan_basic.json", string=True)
            )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_success_on_cbf_pss_pst(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when it is successful for both CBF
        and PSS."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        config_input = load_json_file(
            "test_ConfigureScan_basic.json", string=True
        )
        module_logger.warning(f"Config input type: {type(config_input)}")

        # send configure with only information for CBF and PSS
        config_input_dict = load_json_file("test_ConfigureScan_basic.json")

        _, command_id = device_under_test.configure(
            json.dumps(config_input_dict)
        )

        probe_poller(device_under_test, "obsstate", ObsState.READY, time=5)
        probe_poller(
            device_under_test,
            "commandresult",
            ("configure", str(ResultCode.OK.value)),
            time=3,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            ObsState.READY,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            ObsState.READY,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pstBeamsObsState",
            '{"common-pst/beam/01": "READY", "common-pst/beam/02": "READY"}',
            time=1,
        )
        config_dict = json.loads(config_input)
        assert (
            device_under_test.configurationID
            == config_dict["common"]["config_id"]
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_success_only_cbf(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when the JSON configuration file
        does not have an entry for PSS configuration."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )

        config_input = load_json_file(
            "test_ConfigureScan_basic.json", string=True, remove=["pss", "pst"]
        )

        device_under_test.configure(config_input)
        probe_poller(device_under_test, "obsstate", ObsState.READY, time=5)

        probe_poller(
            device_under_test,
            "commandresult",
            ("configure", str(ResultCode.OK.value)),
            time=1,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            ObsState.READY,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            initial_obsstate,
            time=1,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_success_only_pss(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when the JSON configuration file
        does not have an entry for CBF configuration."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )

        config_input = load_json_file(
            "test_ConfigureScan_basic.json", string=True, remove=["cbf"]
        )

        with pytest.raises(
            tango.DevFailed,
            match=r"Validation 'CSP config 2.5' Missing key: 'cbf'",
        ):
            # remove interface to validate the json
            device_under_test.configure(config_input)

        probe_poller(device_under_test, "obsstate", initial_obsstate, time=3)
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            initial_obsstate,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            initial_obsstate,
            time=1,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_no_subsystem_to_configure(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when the JSON configuration file
        does not specify any entry for CSP sub-systems."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )

        config_input = load_json_file(
            "test_ConfigureScan_basic.json",
            string=True,
            remove=["cbf", "pss", "pst"],
        )
        with pytest.raises(
            tango.DevFailed,
            match=r"Validation 'CSP config 2.5' Missing key: 'cbf'",
        ):
            device_under_test.configure(config_input)

        probe_poller(device_under_test, "obsstate", initial_obsstate, time=3)

        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            initial_obsstate,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            initial_obsstate,
            time=1,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_pss_in_wrong_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when the one of the sub-system
        (PSS) is not in the proper observing state.

        Currently, if the subsystem is not in the proper observing state,
        its configuration is skipped, while the other ones are normally
        configured. So, ResultCode of the command is OK.
        """
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        TestCspSubarray.raise_event_on_pss_subarray("obsstate", ObsState.EMPTY)

        TestCspSubarray.raise_event_on_pst_beams("state", DevState.ON)
        TestCspSubarray.raise_event_on_pst_beams("obsstate", ObsState.IDLE)
        config_input = load_json_file(
            "test_ConfigureScan_basic.json", string=True
        )

        device_under_test.logginglevel = 5
        device_under_test.Configure(config_input)

        probe_poller(device_under_test, "obsstate", ObsState.READY, time=4)
        probe_poller(
            device_under_test,
            "commandresult",
            ("configure", str(ResultCode.OK.value)),
            time=1,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            ObsState.READY,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            ObsState.EMPTY,
            time=1,
        )

    def test_configure_with_empty_json(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """Test CSP Subarray configuration when the one of the sub-system
        (PSS) is not in the proper observing state."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )

        with pytest.raises(
            tango.DevFailed, match=r"Input json is Empty, command is rejected"
        ):
            device_under_test.Configure("{}")

    def test_gotoidle(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ):
        """Test the successful configuration of the CSP Subarray."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.READY
        )
        evt_id = device_under_test.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["obsState"],
        )
        change_event_callbacks.assert_change_event("obsState", ObsState.READY)
        assert device_under_test.cbfSubarrayObsState == ObsState.READY
        assert device_under_test.pssSubarrayObsState == ObsState.READY
        _, command_id = device_under_test.GoToIdle()
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=6,
            command_id=command_id,
        )
        change_event_callbacks.assert_change_event("obsState", ObsState.IDLE)
        device_under_test.unsubscribe_event(evt_id)
        assert device_under_test.pssSubarrayObsState == ObsState.IDLE
        assert device_under_test.cbfSubarrayObsState == ObsState.IDLE

    def test_scan(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test CSP Subarray scan command."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.READY
        )
        probe_poller(device_under_test, "obsstate", ObsState.READY, time=5)

        device_under_test.Scan(json.dumps({"scan_id": 11}))
        probe_poller(device_under_test, "obsstate", ObsState.SCANNING, time=5)
        assert device_under_test.scanID == 11
        probe_poller(
            device_under_test, "cbfSubarrayObsState", ObsState.SCANNING, time=5
        )

    def test_scan_with_cbf_failure(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test the scan command failure when the CBF Subarray throws an
        exception during the scan.

        Expected results: CSP Subarray obsState: FAULT PSS Subarray obsState:
        SCANNING
        """

        # Configure the mocked connector to raise a FAULT event on
        # obsState for the cbf subarray
        mock_config.mock_event = True
        mock_config.mock_event_value = ObsState.FAULT
        mock_config.mock_fail_device = "common-cbf/subarray/01"

        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.READY
        )
        probe_poller(device_under_test, "obsstate", ObsState.READY, time=5)

        device_under_test.Scan('{"scan_id":12}')

        probe_poller(device_under_test, "obsstate", ObsState.FAULT, time=5)
        probe_poller(
            device_under_test, "pssSubarrayObsState", ObsState.SCANNING, time=5
        )

    @pytest.mark.parametrize(
        "initial_obsstate",
        [
            ObsState.EMPTY,
            ObsState.IDLE,
            ObsState.RESOURCING,
            ObsState.CONFIGURING,
            ObsState.ABORTING,
            ObsState.ABORTED,
            ObsState.RESETTING,
            ObsState.FAULT,
            ObsState.RESTARTING,
        ],
    )
    def test_endscan_not_allowed_obsstate(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configure command when invoked from an obsstate
        not allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        with pytest.raises(tango.DevFailed):
            device_under_test.endscan()

    @pytest.mark.parametrize(
        "initial_state",
        [DevState.OFF, DevState.STANDBY, DevState.FAULT, DevState.INIT],
    )
    def test_endscan_not_allowed_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_state: DevState,
    ):
        """Test CSP Subarray configure command when invoked from a state not
        allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, initial_state
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        module_logger.warning(device_under_test.state())
        with pytest.raises(tango.DevFailed):
            device_under_test.endscan()

    @pytest.mark.parametrize("initial_obsstate", [ObsState.READY])
    def test_endscan_success_on_cbf_pss(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        initial_obsstate: ObsState,
    ):
        """Test CSP Subarray configuration when it is successful for both CBF
        and PSS."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )

        device_under_test.Scan(json.dumps({"scan_id": 11}))
        probe_poller(device_under_test, "obsstate", ObsState.SCANNING, time=3)
        device_under_test.endscan()
        probe_poller(device_under_test, "obsstate", ObsState.READY, time=3)
        probe_poller(
            device_under_test,
            "commandresult",
            ("endscan", str(ResultCode.OK.value)),
            time=10,
        )
        probe_poller(
            device_under_test,
            "cbfSubarrayObsState",
            ObsState.READY,
            time=1,
        )
        probe_poller(
            device_under_test,
            "pssSubarrayObsState",
            ObsState.READY,
            time=1,
        )

    @pytest.mark.parametrize(
        ["init_obsstate", "init_state"],
        (
            (ObsState.CONFIGURING, DevState.ON),
            (ObsState.SCANNING, DevState.ON),
            (ObsState.READY, DevState.ON),
            (ObsState.IDLE, DevState.ON),
            (ObsState.RESETTING, DevState.ON),
        ),
    )
    def test_abort_allowed(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        init_obsstate: ObsState,
        init_state: DevState,
    ):
        """Test CSP Subarray abort is allowed when the CSP Subarray is in the
        proper state."""

        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, init_state
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, init_obsstate
        )
        probe_poller(
            device_under_test,
            "obsState",
            init_obsstate,
            time=1,
        )
        with mock.patch(
            "ska_csp_lmc_common.manager.subarray_component_manager"
            ".CSPSubarrayComponentManager.abort"
        ) as mock_run:
            mock_run.return_value = (TaskStatus.IN_PROGRESS, "abort started")
            device_under_test.abort()
            mock_run.assert_called_once()

    @pytest.mark.parametrize(
        ["init_obsstate", "init_state"],
        ((ObsState.FAULT, DevState.ON), (ObsState.ABORTED, DevState.ON)),
    )
    def test_restart_allowed(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        init_obsstate: ObsState,
        init_state: DevState,
    ):
        """Test CSP Subarray restart is allowed when the CSP Subarray is in the
        proper state."""

        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, init_state
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, init_obsstate
        )
        with mock.patch(
            "ska_csp_lmc_common.manager.subarray_component_manager."
            "CSPSubarrayComponentManager.restart"
        ) as mock_restart:
            mock_restart.return_value = (ResultCode.QUEUED, "restart started")
            device_under_test.restart()
            assert device_under_test.obsstate == ObsState.RESTARTING
            mock_restart.assert_called_once()

    @pytest.mark.parametrize(
        ["init_obsstate", "init_state"],
        (
            (ObsState.EMPTY, DevState.ON),
            (ObsState.EMPTY, DevState.OFF),
            (ObsState.ABORTING, DevState.ON),
            (ObsState.ABORTING, DevState.OFF),
            (ObsState.FAULT, DevState.ON),
            (ObsState.FAULT, DevState.OFF),
        ),
    )
    def test_abort_not_allowed(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        init_obsstate: ObsState,
        init_state: DevState,
    ):
        """Test CSP Subarray abort command when invoked from an obsstate not
        allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, init_state
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, init_obsstate
        )
        with pytest.raises(tango.DevFailed):
            device_under_test.abort()

    @pytest.mark.parametrize(
        ["init_obsstate", "init_state"],
        (
            (ObsState.EMPTY, DevState.ON),
            (ObsState.EMPTY, DevState.OFF),
            (ObsState.IDLE, DevState.ON),
            (ObsState.IDLE, DevState.OFF),
            (ObsState.READY, DevState.ON),
            (ObsState.READY, DevState.OFF),
            (ObsState.CONFIGURING, DevState.ON),
            (ObsState.CONFIGURING, DevState.OFF),
            (ObsState.ABORTING, DevState.ON),
            (ObsState.ABORTING, DevState.OFF),
        ),
    )
    def test_obsreset_not_allowed(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        init_obsstate: ObsState,
        init_state: DevState,
    ):
        """Test CSP Subarray obsreset command when invoked from an obsstate not
        allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, init_state
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, init_obsstate
        )
        with pytest.raises(tango.DevFailed):
            device_under_test.obsreset()

    @pytest.mark.parametrize(
        ["init_obsstate", "init_state"],
        (
            (ObsState.EMPTY, DevState.ON),
            (ObsState.EMPTY, DevState.OFF),
            (ObsState.IDLE, DevState.ON),
            (ObsState.IDLE, DevState.OFF),
            (ObsState.READY, DevState.ON),
            (ObsState.READY, DevState.OFF),
            (ObsState.CONFIGURING, DevState.ON),
            (ObsState.CONFIGURING, DevState.OFF),
            (ObsState.ABORTING, DevState.ON),
            (ObsState.ABORTING, DevState.OFF),
        ),
    )
    def test_restart_not_allowed(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
        init_obsstate: ObsState,
        init_state: DevState,
    ):
        """Test CSP Subarray restart command when invoked from an obsstate not
        allowed."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, init_state
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, init_obsstate
        )
        with pytest.raises(tango.DevFailed):
            device_under_test.restart()

    def test_subarray_reset_not_allowed(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """Test CSP Subarray reset command when invoked from not allowed
        states."""
        TestCspSubarray.raise_event_on_cbf_subarray("state", DevState.INIT)
        probe_poller(device_under_test, "state", DevState.INIT, time=1)
        with pytest.raises(tango.DevFailed):
            device_under_test.reset()

    def test_abort_configure_and_restart(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """Test the successful Abort execution."""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        TestCspSubarray.raise_event_on_pst_beams("state", DevState.ON)
        TestCspSubarray.raise_event_on_pst_beams("obsstate", ObsState.IDLE)
        config_input = load_json_file(
            "test_ConfigureScan_basic.json", string=True
        )
        module_logger.warning(f"Config input type: {type(config_input)}")
        device_under_test.Configure(config_input)
        time.sleep(3)

        [_, [command_id]] = device_under_test.Abort()
        # NOTE: next check has to be changed to check result code against ABORT
        # when CSP move to BC 13
        probe_poller(
            device_under_test,
            "longRunningCommandStatus",
            "COMPLETED",
            command_id=command_id,
            time=5,
        )
        assert device_under_test.longrunningcommandresult == (
            command_id,
            '[0, "Task abort completed with result OK"]',
        )

        probe_poller(device_under_test, "obsState", ObsState.ABORTED, time=10)

    def test_abort_scan(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test the successful of abort a scan."""

        # Force the sub-systems to ON/READY state
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.READY
        )
        # exercise the device invoking scan/abort
        device_under_test.Scan('{"scan_id":11}')
        probe_poller(device_under_test, "obsstate", ObsState.SCANNING, time=5)

        # verify the scanning state
        probe_poller(
            device_under_test, "cbfSubarrayObsState", ObsState.SCANNING, time=3
        )
        probe_poller(
            device_under_test, "pssSubarrayObsState", ObsState.SCANNING, time=3
        )

        [_, [command_id]] = device_under_test.Abort()
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            command_id=command_id,
            time=5,
        )
        probe_poller(device_under_test, "obsstate", ObsState.ABORTED, time=3)
        assert device_under_test.cbfSubarrayObsState == ObsState.ABORTED
        assert device_under_test.pssSubarrayObsState == ObsState.ABORTED
        assert device_under_test.longrunningcommandresult == (
            command_id,
            '[0, "Task abort completed with result OK"]',
        )

    @pytest.mark.parametrize(
        [
            "pss_subarray_state",
            "cbf_subarray_state",
            "pst_beam_state",
            "command_result",
        ],
        (
            (DevState.ON, DevState.ON, DevState.ON, ResultCode.OK),
            (DevState.FAULT, DevState.ON, DevState.ON, ResultCode.QUEUED),
            (DevState.ON, DevState.FAULT, DevState.ON, ResultCode.QUEUED),
            (DevState.ON, DevState.ON, DevState.FAULT, ResultCode.QUEUED),
        ),
    )
    def test_subarray_reset_allowed(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        pss_subarray_state: DevState,
        cbf_subarray_state: DevState,
        pst_beam_state: DevState,
        command_result: ResultCode,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """
        Test CSP Subarray reset command is called when invoked from
        allowed operational states.
        """
        with mock.patch(
            "ska_csp_lmc_common.manager.subarray_component_manager."
            "CSPSubarrayComponentManager.reset"
        ) as mock_reset:
            mock_reset.return_value = (ResultCode.OK, "reset allowed")
            assert device_under_test.state() == DevState.OFF

            TestCspSubarray.set_subsystems_and_go_to_state(
                device_under_test, DevState.ON
            )
            result, msg = device_under_test.reset()
            mock_reset.assert_called_once()

    def test_restart_clear_abort_flag(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Verify that restart reset the MainTaskExecutor abort flag"""
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.ABORTED
        )
        with mock.patch(
            "ska_csp_lmc_common.commands.main_task_executor."
            "MainTaskExecutor.clear_abort_flag"
        ) as mock_func, mock.patch(
            "ska_csp_lmc_common.manager."
            "subarray_component_manager.CSPSubarrayComponentManager."
            "_restart"
        ) as mock_restart:
            mock_func.side_effect = mock.MagicMock()
            mock_restart.side_effect = mock.MagicMock()
            [result, _] = device_under_test.Restart()
            assert result == ResultCode.QUEUED
            mock_func.assert_called_once()
            mock_restart.asser_called_once()

    @pytest.mark.parametrize(
        ["pss_obsstate", "expected_value"],
        (
            (ObsState.READY, ObsState.EMPTY),
            (ObsState.IDLE, ObsState.EMPTY),
            (ObsState.SCANNING, ObsState.EMPTY),
            (ObsState.CONFIGURING, ObsState.CONFIGURING),
        ),
    )
    def test_restart_with_pss_not_fault_aborted(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        pss_obsstate: ObsState,
        expected_value: ObsState,
        test_subarray_init: pytest.fixture,
        mock_config_init: mock,
    ):
        """Test the execution of the Restart command when the CSP Subarray is
        in FAULT observing state but one of the sub-system is not in ABORTED or
        FAULT state.

        Input data before Restart invocation:

        - PSS Subarray obsState [READY, ObsState.IDLE,
                                 ObsState.SCANNING,
                                 ObsState.CONFIGURING]
            (ObsReset is not allowed from this state!)

        - CBF Subarray obsState FAULT
        Expected behavior:
        The Restart commands is invoked on the CBF Subarray
        and it transitions to EMPTY. The sequences of Abort-Restart commands is
        invoked on the PSS subarray and it first transitions to ABORTED and
        then to EMPTY
        """

        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.FAULT
        )

        # force the PSS subarray to FAULT obsState
        TestCspSubarray.raise_event_on_pss_subarray("obsstate", pss_obsstate)
        probe_poller(
            device_under_test, "pssSubarrayObsState", pss_obsstate, time=3
        )

        assert device_under_test.pssSubarrayObsState == pss_obsstate
        assert device_under_test.cbfSubarrayObsState == ObsState.FAULT

        device_under_test.Restart()
        probe_poller(device_under_test, "obsstate", expected_value, time=3)

    def test_abortcommands_is_disabled(
        self: TestCspSubarray, device_under_test: DeviceTestContext
    ):
        """
        Test that an exception is raised when AbortCommands is invoked
        """
        with pytest.raises(tango.DevFailed):
            device_under_test.AbortCommands()

    @pytest.mark.parametrize(
        "attribute,value",
        [
            ("State", DevState.ON),
            ("HealthState", HealthState.OK),
            ("AdminMode", AdminMode.ONLINE),
            ("ObsState", ObsState.READY),
        ],
    )
    def test_subarray_update_subsystems_attributes_on_events(
        self: TestCspSubarray,
        attribute: str,
        value: Any,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """ """
        mock_callback = {}
        for subsystem in ["Cbf", "Pss", "Pst"]:
            mock_callback[subsystem] = mock.MagicMock()
            if subsystem == "Pst":
                device_under_test.subscribe_event(
                    f"PstBeams{attribute}",
                    tango.EventType.CHANGE_EVENT,
                    mock_callback[subsystem],
                )
            else:
                device_under_test.subscribe_event(
                    f"{subsystem}Subarray{attribute}",
                    tango.EventType.CHANGE_EVENT,
                    mock_callback[subsystem],
                )

        TestCspSubarray.raise_event_on_cbf_subarray(attribute.lower(), value)
        TestCspSubarray.raise_event_on_pss_subarray(attribute.lower(), value)
        TestCspSubarray.raise_event_on_pst_beams(attribute.lower(), value)

        for subsystem in ["Cbf", "Pss"]:
            probe_poller(
                device_under_test,
                f"{subsystem}Subarray{attribute}",
                value,
                time=7,
            )

        value_str = (
            str(value) if attribute == "State" else str(value).split(".")[1]
        )
        cmp_str = (
            '{"common-pst/beam/01": "'
            + value_str
            + '", "common-pst/beam/02": "'
            + value_str
            + '"}'
        )
        probe_poller(
            device_under_test,
            f"PstBeams{attribute}",
            cmp_str,
            time=2,
        )

        time.sleep(0.2)
        # time sleep is necessary to let the event be pushed

        for subsystem in ["Cbf", "Pss"]:
            assert (
                mock_callback[subsystem]
                .call_args[0][0]
                .attr_value.name.lower()
                == f"{subsystem.lower()}subarray{attribute.lower()}"
            )
            assert (
                mock_callback[subsystem].call_args[0][0].attr_value.value
                == value
            )

        assert (
            mock_callback["Pst"].call_args[0][0].attr_value.name.lower()
            == f"pstbeams{attribute.lower()}"
        )
        assert mock_callback["Pst"].call_args[0][0].attr_value.value == cmp_str

    @mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_subarray_archive_events(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ):
        """
        Test the archive events on State, healthState and obsState pushed
        from the subarray device.
        """
        with mock.patch.object(
            PstBeamComponent,
            "pstprocessingmode",
            new_callable=mock.PropertyMock,
        ) as mocked_pstprocessingmode:
            mocked_pstprocessingmode.return_value = (
                PstProcessingMode.PULSAR_TIMING
            )
            callback_state = mock.Mock()
            callback_health_state = mock.Mock()
            device_under_test.subscribe_event(
                "state", tango.EventType.ARCHIVE_EVENT, callback_state
            )
            device_under_test.subscribe_event(
                "healthstate",
                tango.EventType.ARCHIVE_EVENT,
                callback_health_state,
            )
            # initialize the Subarray device
            subarray_init(device_under_test)
            # expect count=2: one at sub-scription, the other when the value
            # changes after initialization
            assert callback_state.call_count == 2
            assert callback_health_state.call_count == 2
            # subscribe the obsState attribute for archive events
            callback_obs_state = mock.Mock()
            device_under_test.subscribe_event(
                "obsstate", tango.EventType.ARCHIVE_EVENT, callback_obs_state
            )
            for attribute in [
                "obsState",
                "longRunningCommandStatus",
            ]:
                device_under_test.subscribe_event(
                    attribute,
                    tango.EventType.CHANGE_EVENT,
                    change_event_callbacks[attribute],
                )
            change_event_callbacks.assert_change_event(
                "obsState", ObsState.EMPTY
            )
            change_event_callbacks.assert_change_event(
                "longRunningCommandStatus", ()
            )
            TestCspSubarray.set_subsystems_and_go_to_state(
                device_under_test, DevState.ON
            )
            TestCspSubarray.set_subsystems_and_go_to_obsstate(
                device_under_test, ObsState.IDLE
            )

            change_event_callbacks.assert_change_event(
                "obsState", ObsState.IDLE
            )
            config_input = load_json_file(
                "test_ConfigureScan_basic.json",
                string=True,
                remove=["pss", "pst"],
            )

            [[result_code], [command_id]] = device_under_test.configure(
                config_input
            )
            assert result_code == ResultCode.QUEUED
            change_event_callbacks.assert_change_event(
                "obsState", ObsState.CONFIGURING
            )
            change_event_callbacks.assert_change_event(
                "longRunningCommandStatus", (command_id, "STAGING")
            )
            change_event_callbacks.assert_change_event(
                "longRunningCommandStatus", (command_id, "QUEUED")
            )
            change_event_callbacks.assert_change_event(
                "longRunningCommandStatus", (command_id, "IN_PROGRESS")
            )
            change_event_callbacks.assert_change_event(
                "longRunningCommandStatus", (command_id, "COMPLETED")
            )
            change_event_callbacks.assert_change_event(
                "obsState", ObsState.READY, lookahead=2
            )
            # EMPTY at subscription
            # IDLE - CONFIGURING - READY
            assert callback_obs_state.call_count >= 3

    def test_force_health_state(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init: pytest.fixture,
    ):
        """Test the ForceHealthState command.
        When the command argument is True, the subarray healthState
        is forced to FAILED.
        """
        assert device_under_test.healthState == HealthState.OK
        result_code, _ = device_under_test.ForceHealthState(True)
        assert result_code == ResultCode.OK
        assert device_under_test.healthState == HealthState.FAILED
        device_under_test.ForceHealthState(False)
        assert device_under_test.healthState == HealthState.OK

    def test_obsreset_rejected(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """Test the execution of the ObsRest command when the CSP Subarray is
        in ABORTED observing and all the sub-systems are in ABORTED state.

        Input data before ObsReset invocation: PSS Subarray obsState ABORTED
        CBF Subarray obsState ABORTED Expected behavior: PSS Subarray obsState
        IDLE CBF Subarray obsState IDLE
        """

        # Force the sub-systems to ON/ABORTED state
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.ABORTED
        )

        probe_poller(device_under_test, "obsstate", ObsState.ABORTED, time=2)
        result = device_under_test.ObsReset()
        result_code, msg = result
        assert result_code[0] == ResultCode.REJECTED

    def test_restart(
        self: TestCspSubarray,
        device_under_test: DeviceTestContext,
        test_subarray_init_with_all_subsystems: pytest.fixture,
    ):
        """
        Test the CSP Subarray Restart Command with PST beamsand
        CBF subarray in FAULT
        """

        # Force the devices to ON/IDLE
        TestCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )

        TestCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.EMPTY
        )

        # assign resources including 2 PST beams
        assignresources_input = load_json_file(
            "test_AssignResources_basic.json", string=True
        )
        # invoke command and wait for its completion
        _, command_id = device_under_test.AssignResources(
            assignresources_input
        )
        # verify the results
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=10,
            command_id=command_id,
        )
        assert len(device_under_test.assignedTimingBeamIDs) == 2
        assert device_under_test.assignedTimingBeamIDs[0] == 1
        assert device_under_test.assignedTimingBeamIDs[1] == 2
        # force the PST beams and CBF subarray to FAULT obsstate
        TestCspSubarray.raise_event_on_pst_beams("obsstate", ObsState.FAULT)
        TestCspSubarray.raise_event_on_cbf_subarray("obsstate", ObsState.FAULT)
        probe_poller(
            device_under_test,
            "obsState",
            ObsState.FAULT,
            time=10,
        )
        # invoke command and wait for its completion
        _, command_id = device_under_test.Restart()

        probe_poller(
            device_under_test,
            "longRunningCommandStatus",
            "COMPLETED",
            time=10,
            command_id=command_id,
        )
        probe_poller(
            device_under_test,
            "obsState",
            ObsState.EMPTY,
            time=2,
        )
