from __future__ import annotations

import json
import logging
import time

import mock
import pytest

# Tango imports
import tango
from mock import MagicMock, patch
from ska_control_model import AdminMode, HealthState

# SKA Base classes imports
from ska_tango_base.commands import ResultCode
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevState
from tango.test_context import DeviceTestContext

# Local imports
from ska_csp_lmc_common import release
from ska_csp_lmc_common.connector import Connector
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import TestBaseController

module_logger = logging.getLogger(__name__)


def fake_read_attribute_with_exception(attr_name):
    raise ValueError(f"Error in reading {attr_name}")


device_to_load = {
    "path": "tests/unit/csplmc_dsconfig.json",
    "package": "ska_csp_lmc_common",
    "device": "comcontroller",
}


class TestCspController(TestBaseController):
    @pytest.fixture
    @mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def init_succeeded(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """Feature to ensure that initialization succeeded (Connector mocked)
        Expected DevState: OFF."""
        self.init_process(device_under_test)

    def init_process(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """Method called at the start of each test.

        It initialize the CspController
        """
        probe_poller(device_under_test, "state", DevState.DISABLE, time=1)
        assert device_under_test.adminmode == AdminMode.OFFLINE
        assert device_under_test.healthstate == HealthState.UNKNOWN
        assert device_under_test.jsonComponentVersions == ""
        device_under_test.adminMode = AdminMode.ENGINEERING
        probe_poller(device_under_test, "isCommunicating", True, time=5)
        probe_poller(device_under_test, "state", DevState.STANDBY, time=5)
        probe_poller(
            device_under_test, "cspCbfAdminMode", AdminMode.ENGINEERING, time=5
        )
        probe_poller(
            device_under_test, "cspPssAdminMode", AdminMode.ENGINEERING, time=5
        )

        assert device_under_test.healthstate == HealthState.OK

    def test_init_succeeded_check_subsystems_attributes(
        self: TestCspController,
        init_succeeded: pytest.fixture,
        device_under_test: DeviceTestContext,
    ):
        """Test that initialization succeeded and reported state and
        healthstate for subsystems are properly updated."""
        pst_health_dict = {}
        pst_state_dict = {}
        for fqdn in device_under_test.pstBeamsAddresses:
            pst_health_dict[fqdn] = HealthState.OK.name
            pst_state_dict[fqdn] = DevState.OFF.name
        pst_health_str = json.dumps(pst_health_dict)
        pst_state_str = json.dumps(pst_state_dict)
        assert device_under_test.cspCbfState == DevState.STANDBY
        assert device_under_test.cspPssState == DevState.STANDBY
        assert device_under_test.cspCbfHealthState == HealthState.OK
        assert device_under_test.cspPssHealthState == HealthState.OK
        assert device_under_test.cspPstBeamsHealthState == pst_health_str
        assert device_under_test.cspPstBeamsState == pst_state_str
        json_version_dict = json.loads(device_under_test.jsonComponentVersions)
        assert json_version_dict["common-cbf/control/0"] == "0.16.0"
        assert json_version_dict["common-pss/control/0"] == "0.16.0"
        assert json_version_dict["common-pst/beam/01"] == "0.16.0"
        assert json_version_dict["common-pst/beam/02"] == "0.16.0"
        assert json_version_dict["common-cbf/control/0"] == "0.16.0"
        assert json_version_dict["common-cbf/control/0"] == "0.16.0"
        assert json_version_dict["common-cbf/control/0"] == "0.16.0"
        assert json_version_dict["common-csp/subarray/01"] == "0.16.0"
        assert json_version_dict["common-csp/subarray/02"] == "0.16.0"
        assert json_version_dict["common-csp/subarray/03"] == "0.16.0"

    def test_init_with_connection_failure(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """Test initialization with a failure in connection to all the sub-
        systems.

        Expected results: CspController DevState: FAULT CspController
        healthState: FAILED Sub-system state: DISABLE Sub-system healthState:
        UNKNOWN Sub-system adminMode: OFFLINE
        """
        with patch(
            "ska_csp_lmc_common.component.Connector._get_deviceproxy"
        ) as mock_connector:
            mock_connector.side_effect = ValueError("Invalid proxy")
            pst_health_dict = {}
            pst_state_dict = {}
            for fqdn in device_under_test.pstBeamsAddresses:
                pst_health_dict[fqdn] = HealthState.UNKNOWN.name
                pst_state_dict[fqdn] = DevState.DISABLE.name
            pst_health_str = json.dumps(pst_health_dict)
            pst_state_str = json.dumps(pst_state_dict)
            probe_poller(device_under_test, "state", DevState.DISABLE, time=5)
            assert device_under_test.adminmode == AdminMode.OFFLINE
            assert device_under_test.healthstate == HealthState.UNKNOWN
            device_under_test.adminMode = AdminMode.ENGINEERING

            probe_poller(device_under_test, "State", DevState.FAULT, time=10)
            assert device_under_test.cspCbfState == DevState.DISABLE
            assert device_under_test.cspPssState == DevState.DISABLE
            assert device_under_test.cspPstBeamsState == pst_state_str
            assert device_under_test.cspCbfHealthState == HealthState.UNKNOWN
            assert device_under_test.cspPssHealthState == HealthState.UNKNOWN
            assert device_under_test.cspPstBeamsHealthState == pst_health_str

    def test_init_device_not_defined(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """Test initialization with a DB_DeviceNotDefined exception on
        all sub-systems devices"""

        def raise_tango_failed():
            tango.Except.throw_exception(
                "DB_DeviceNotDefined",
                "This is error message for devfailed",
                " ",
                tango.ErrSeverity.ERR,
            )

        with patch(
            "ska_csp_lmc_common.component.Connector._get_deviceproxy"
        ) as mock_connector:
            mock_connector.side_effect = raise_tango_failed
            pst_health_dict = {}
            pst_state_dict = {}
            for fqdn in device_under_test.pstBeamsAddresses:
                pst_health_dict[fqdn] = HealthState.UNKNOWN.name
                pst_state_dict[fqdn] = DevState.DISABLE.name
            pst_health_str = json.dumps(pst_health_dict)
            pst_state_str = json.dumps(pst_state_dict)
            probe_poller(device_under_test, "state", DevState.DISABLE, time=5)
            assert device_under_test.adminmode == AdminMode.OFFLINE
            assert device_under_test.healthstate == HealthState.UNKNOWN
            device_under_test.adminMode = AdminMode.ENGINEERING

            probe_poller(device_under_test, "State", DevState.FAULT, time=10)
            assert device_under_test.cspCbfState == DevState.DISABLE
            assert device_under_test.cspPssState == DevState.DISABLE
            assert device_under_test.cspPstBeamsState == pst_state_str
            assert device_under_test.cspCbfHealthState == HealthState.UNKNOWN
            assert device_under_test.cspPssHealthState == HealthState.UNKNOWN
            assert device_under_test.cspPstBeamsHealthState == pst_health_str

    def test_command_timeout(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        assert device_under_test.commandTimeout == 5
        device_under_test.commandTimeout = int(6)
        assert device_under_test.commandTimeout == 6

    def test_command_timeout_negative_value(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """
        Test commandTimeout when a negative value is specified.
        """
        with pytest.raises(TypeError):
            device_under_test.commandTimeout = -2
            assert device_under_test.commandTimeout == 5

    def test_command_timeout_null_value(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """
        Test commandTimeout when a null value is specified.
        """
        with pytest.raises(tango.DevFailed) as df:
            device_under_test.commandTimeout = 0
            assert device_under_test.commandTimeout == 5
        # df is ExcpetionInfo type
        # df.value is DevFailed type
        assert (
            "ValueError: Try to set timeout to 0.Timeout shall be > 0"
            in df.value.args[0].desc
        )

    def test_version_info(
        self: TestCspController,
        device_under_test: DeviceTestContext,
        init_succeeded: pytest.fixture,
    ):
        """Test the information reported by the version attributes.

        Information reported by the CSP sub-systems are generated by mocks
        The values are defined in the mocked_attribute module.
        """
        assert device_under_test.versionId == release.version
        build_state = (
            f"{release.name}, {release.version}, " + f"{release.description}"
        )
        assert device_under_test.buildState == build_state
        assert device_under_test.cbfVersion == "0.16.0"
        assert device_under_test.pssVersion == "0.16.0"
        assert device_under_test.pstVersion[0] == "0.16.0"
        assert device_under_test.pstVersion[1] == "0.16.0"

    def test_ctrl_subsystem_attributes(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """
        Test SCM sub-systems attributes before connection.
        """
        assert device_under_test.cspcbfAdminMode == AdminMode.OFFLINE
        assert device_under_test.cspcbfState == DevState.DISABLE
        assert device_under_test.cspcbfhealthState == HealthState.UNKNOWN
        assert device_under_test.cbfControllerAddress == "common-cbf/control/0"
        assert device_under_test.csppssAdminMode == AdminMode.OFFLINE
        assert device_under_test.csppssState == DevState.DISABLE
        assert device_under_test.csppssHealthState == HealthState.UNKNOWN
        assert device_under_test.pssControllerAddress == "common-pss/control/0"
        pst_beams_dict = json.loads(device_under_test.csppstBeamsAdminMode)
        assert pst_beams_dict["common-pst/beam/01"] == "OFFLINE"
        assert pst_beams_dict["common-pst/beam/02"] == "OFFLINE"
        pst_beams_dict = json.loads(device_under_test.csppstBeamsState)
        assert pst_beams_dict["common-pst/beam/01"] == "DISABLE"
        assert pst_beams_dict["common-pst/beam/02"] == "DISABLE"
        pst_beams_dict = json.loads(device_under_test.csppstBeamsHealthState)
        assert pst_beams_dict["common-pst/beam/01"] == "UNKNOWN"
        assert pst_beams_dict["common-pst/beam/02"] == "UNKNOWN"

    def test_on_succeeded(
        self: TestCspController,
        device_under_test: DeviceTestContext,
        init_succeeded: pytest.fixture,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ):
        """
        Test the execution of the On command.
        Check that the events are received in the same order they were raised.
        """
        evt_id = []
        assert device_under_test.state() == tango.DevState.STANDBY
        for attribute in [
            "state",
            "longRunningCommandStatus",
            "longRunningCommandResult",
        ]:
            evt_id.append(
                device_under_test.subscribe_event(
                    attribute,
                    tango.EventType.CHANGE_EVENT,
                    change_event_callbacks[attribute],
                    # print
                )
            )
        change_event_callbacks.assert_change_event(
            "state", tango.DevState.STANDBY
        )
        change_event_callbacks.assert_change_event(
            "longRunningCommandStatus", ()
        )

        change_event_callbacks.assert_change_event(
            "longRunningCommandResult", ("", "")
        )
        [[result_code], [command_id]] = device_under_test.On([])
        assert result_code == ResultCode.QUEUED
        change_event_callbacks["longRunningCommandStatus"].assert_change_event(
            (command_id, "STAGING")
        )

        change_event_callbacks.assert_change_event(
            "longRunningCommandStatus", (command_id, "QUEUED")
        )

        change_event_callbacks.assert_change_event(
            "longRunningCommandResult",
            (
                command_id,
                json.dumps([int(ResultCode.STARTED), "on"]),
            ),
        )
        change_event_callbacks.assert_change_event(
            "longRunningCommandStatus", (command_id, "IN_PROGRESS")
        )

        change_event_callbacks.assert_change_event(
            "state", tango.DevState.ON, lookahead=3
        )

        change_event_callbacks.assert_change_event(
            "longRunningCommandResult",
            (
                command_id,
                json.dumps(
                    [
                        int(ResultCode.OK),
                        "Task on completed with result OK",
                    ]
                ),
            ),
            lookahead=3,
        )
        change_event_callbacks.assert_change_event(
            "longRunningCommandStatus", (command_id, "COMPLETED"), lookahead=3
        )

        for _id in evt_id:
            device_under_test.unsubscribe_event(_id)

        assert device_under_test.state() == DevState.ON

    def test_on_is_not_allowed(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """Test execution of the Csp Controller On command when the device is a
        State from which it is not allowed.

        Initial Device State value: DISABLE
        Expected behavior: exception thrown
        """
        # instantiate the OnCommand class
        module_logger.info(f"state = {device_under_test.state()}")
        probe_poller(device_under_test, "state", DevState.DISABLE, time=5)
        with pytest.raises(tango.DevFailed) as df:
            device_under_test.On([])
        assert df._excinfo[1].args[0].reason == "API_CommandNotAllowed"

    def test_standby_rejected(
        self: TestCspController,
        device_under_test: DeviceTestContext,
        init_succeeded: pytest.fixture,
    ):
        # force the sub-systems controllers and CspSubarray01 to ON.
        # When the standby command is sent, the status does not change
        # and the command is REJECTED
        # The other subarryas are disabled
        module_logger.info(f"state = {device_under_test.state()}")

        TestCspController.raise_event_on_subarray(
            "01",
            "state",
            DevState.ON,
        )
        TestCspController.raise_event_on_subarray(
            "02", "state", DevState.DISABLE
        )
        TestCspController.raise_event_on_subarray(
            "03",
            "state",
            DevState.DISABLE,
        )
        TestCspController.raise_event_on_cbf("state", DevState.ON)
        TestCspController.raise_event_on_pss("state", DevState.ON)
        TestCspController.raise_event_on_pst_beams("state", DevState.ON)
        pst_state_dict = {}
        for fqdn in device_under_test.pstBeamsAddresses:
            pst_state_dict[fqdn] = DevState.ON.name
        pst_state_str = json.dumps(pst_state_dict)
        probe_poller(device_under_test, "cspPssState", DevState.ON, time=5)
        probe_poller(device_under_test, "cspCbfState", DevState.ON, time=5)
        probe_poller(
            device_under_test, "cspPstBeamsState", pst_state_str, time=5
        )
        probe_poller(device_under_test, "state", DevState.ON, time=1)

        result = device_under_test.Standby([])
        probe_poller(device_under_test, "state", DevState.ON, time=1)
        # result is a list of 2 elements:
        # 1- the result code
        # 2- the result message
        result_code, msg = result
        assert result_code[0] == ResultCode.REJECTED

    def test_standby_not_allowed(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """Test that off command is rejected if DevState is DISABLE"""
        probe_poller(device_under_test, "state", DevState.DISABLE, time=5)
        with pytest.raises(tango.DevFailed) as df:
            device_under_test.Standby([])
        assert df._excinfo[1].args[0].reason == "API_CommandNotAllowed"

    def test_off_succeeded(self, device_under_test, init_succeeded):

        _, command_id = device_under_test.Off([])
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=5,
            command_id=command_id,
        )
        # result is a list of 2 elements:
        # 1- the result code
        # 2- the result message

    def test_off_not_allowed(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """Test that off command is rejected if DevState is DISABLE"""
        probe_poller(device_under_test, "state", DevState.DISABLE, time=5)
        with pytest.raises(tango.DevFailed) as df:
            device_under_test.Off([])
        assert df._excinfo[1].args[0].reason == "API_CommandNotAllowed"

    def test_ctrl_reset_not_allowed(
        cls: TestCspController,
        device_under_test: DeviceTestContext,
        init_succeeded: pytest.fixture,
    ):
        """Test CSP Controller reset command when invoked from a not allowed
        state."""
        cls.raise_event_on_cbf("state", DevState.INIT)
        probe_poller(device_under_test, "state", DevState.INIT, time=1)
        with pytest.raises(tango.DevFailed) as df:
            device_under_test.reset()
        assert df._excinfo[1].args[0].reason == "API_CommandNotAllowed"

    @pytest.mark.parametrize(
        [
            "pss_state",
            "cbf_state",
            "subarray_state",
            "pst_state",
            "command_result",
        ],
        (
            (
                DevState.ON,
                DevState.ON,
                DevState.ON,
                DevState.ON,
                ResultCode.OK,
            ),
            (
                DevState.FAULT,
                DevState.ON,
                DevState.ON,
                DevState.ON,
                ResultCode.QUEUED,
            ),
            (
                DevState.ON,
                DevState.FAULT,
                DevState.ON,
                DevState.ON,
                ResultCode.QUEUED,
            ),
            (
                DevState.ON,
                DevState.ON,
                DevState.FAULT,
                DevState.ON,
                ResultCode.QUEUED,
            ),
            (
                DevState.ON,
                DevState.ON,
                DevState.ON,
                DevState.FAULT,
                ResultCode.QUEUED,
            ),
        ),
    )
    def test_ctrl_reset_allowed(
        cls: TestCspController,
        device_under_test: DeviceTestContext,
        pss_state: DevState,
        cbf_state: DevState,
        subarray_state: DevState,
        pst_state: DevState,
        command_result: ResultCode,
        init_succeeded: pytest.fixture,
    ):
        """Test CSP Controller reset command when invoked from a set of allowed
        states."""
        assert device_under_test.state() == DevState.STANDBY
        cls.raise_event_on_cbf("state", cbf_state)
        cls.raise_event_on_pss("state", pss_state)
        cls.raise_event_on_pst_beams("state", pst_state)
        cls.raise_event_on_subarray("01", "state", subarray_state)
        cls.raise_event_on_subarray("02", "state", subarray_state)
        cls.raise_event_on_subarray("03", "state", subarray_state)

        _, command_id = device_under_test.reset()
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=10,
            command_id=command_id,
        )

    def test_reset_succeeded_on_cbf(
        cls: TestCspController,
        device_under_test: DeviceTestContext,
        init_succeeded: pytest.fixture,
    ):
        """Test Reset when CBF Controller is in FAULT while PSS and PST
        Controllers are ON.

        After Reset the CBF Controller state is STANDBY: Expected behavior: CSP
        Controller state is in STANDBY PSS Controller is ON PST Controller is
        ON
        """
        cls.set_subsystems_and_go_to_state(device_under_test, DevState.ON)
        cls.raise_event_on_cbf("state", DevState.FAULT)
        probe_poller(device_under_test, "state", DevState.FAULT, time=1)
        device_under_test.Reset()

        pst_state_dict = {}
        for fqdn in device_under_test.pstBeamsAddresses:
            pst_state_dict[fqdn] = DevState.ON.name
        pst_state_str = json.dumps(pst_state_dict)
        probe_poller(
            device_under_test, "cspCbfState", DevState.STANDBY, time=5
        )
        probe_poller(device_under_test, "cspPssState", DevState.ON, time=5)
        probe_poller(
            device_under_test, "cspPstBeamsState", pst_state_str, time=5
        )
        probe_poller(device_under_test, "State", DevState.STANDBY, time=5)

    def test_reset_invoked_on_all_sub_systems(
        cls: TestCspController,
        device_under_test: DeviceTestContext,
        init_succeeded: pytest.fixture,
    ):
        """Test Reset is invoked on all fault sub-systems controllers.

        All sub-systems are set in FAULT state. Expected behavior: the reset
        command is called on each sub-system
        """

        # force the CSP sub-systems controllers to FAULT state as well as the
        # CSP Controller
        pst_state_dict = {}
        for fqdn in device_under_test.pstBeamsAddresses:
            pst_state_dict[fqdn] = DevState.OFF.name
        pst_state_str = json.dumps(pst_state_dict)
        cls.set_subsystems_and_go_to_state(device_under_test, DevState.FAULT)
        probe_poller(device_under_test, "state", DevState.FAULT, time=1)
        device_under_test.Reset()
        # wait to the command to be issued on all sub-systems
        # to remove as soon as possible!
        probe_poller(
            device_under_test, "cspCbfState", DevState.STANDBY, time=5
        )
        probe_poller(
            device_under_test, "cspPssState", DevState.STANDBY, time=5
        )
        probe_poller(
            device_under_test, "cspPstBeamsState", pst_state_str, time=5
        )

    def test_reset_not_invoked(
        cls: TestCspController,
        device_under_test: DeviceTestContext,
        init_succeeded: pytest.fixture,
    ):
        """Test Reset command is not forwarded to the any subsystem  not in
        fault."""
        cls.set_subsystems_and_go_to_state(device_under_test, DevState.ON)

        with patch(
            "ska_csp_lmc_common.component.Component.run"
        ) as mock_component_run:
            device_under_test.Reset()
            mock_component_run.assert_not_called()

    def test_read_health_state_with_exception_in_read(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        with patch.object(
            Connector, "_get_deviceproxy", return_value=MagicMock()
        ), patch.object(
            Connector,
            "subscribe_attribute",
            side_effect=MockedConnector.fake_subscribe_attribute,
        ), patch.object(
            Connector,
            "get_attribute",
            side_effect=ValueError("Error in reading"),
        ):
            pst_health_dict = {}
            for fqdn in device_under_test.pstBeamsAddresses:
                pst_health_dict[fqdn] = HealthState.UNKNOWN.name
            pst_health_str = json.dumps(pst_health_dict)

            # NOTE: it's impossible to use the init_succeeded fixture, because
            # it use the MockedConnector class patch, but we need to patch
            # get_attribute in a different way

            probe_poller(
                device_under_test,
                "cspCbfHealthState",
                HealthState.UNKNOWN,
                time=5,
            )
            probe_poller(
                device_under_test,
                "cspPssHealthState",
                HealthState.UNKNOWN,
                time=2,
            )
            probe_poller(
                device_under_test,
                "cspPstBeamsHealthState",
                pst_health_str,
                time=2,
            )

    def test_health_state_updated_with_events(
        self: TestCspController,
        device_under_test: DeviceTestContext,
        init_succeeded: pytest.fixture,
    ):
        TestCspController.raise_event_on_cbf("healthstate", HealthState.OK)
        TestCspController.raise_event_on_pss(
            "healthstate", HealthState.DEGRADED
        )
        TestCspController.raise_event_on_pst_beams(
            "healthstate", HealthState.FAILED
        )

        probe_poller(
            device_under_test, "cspCbfHealthState", HealthState.OK, time=2
        )
        probe_poller(
            device_under_test,
            "cspPssHealthState",
            HealthState.DEGRADED,
            time=2,
        )
        pst_health_dict = {}
        for fqdn in device_under_test.pstBeamsAddresses:
            pst_health_dict[fqdn] = HealthState.FAILED.name
        pst_health_str = json.dumps(pst_health_dict)
        probe_poller(
            device_under_test, "cspPstBeamsHealthState", pst_health_str, time=2
        )

    def test_read_admin_mode_return_online(
        self: TestCspController,
        device_under_test: DeviceTestContext,
        init_succeeded: pytest.fixture,
    ):
        assert device_under_test.adminMode == AdminMode.ENGINEERING

    def test_set_admin_mode_to_online(self, device_under_test, init_succeeded):
        probe_poller(device_under_test, "isCommunicating", True, time=5)
        device_under_test.adminMode = AdminMode.ONLINE
        pst_admin_dict = {}
        for fqdn in device_under_test.pstBeamsAddresses:
            pst_admin_dict[fqdn] = AdminMode.ONLINE.name
        pst_admin_str = json.dumps(pst_admin_dict)
        probe_poller(
            device_under_test, "cspCbfAdminMode", AdminMode.ONLINE, time=7
        )
        probe_poller(
            device_under_test, "cspPssAdminMode", AdminMode.ONLINE, time=2
        )
        probe_poller(
            device_under_test, "cspPstBeamsAdminMode", pst_admin_str, time=2
        )

    @pytest.mark.parametrize(
        "attribute,value",
        [
            ("State", DevState.ON),
            ("HealthState", HealthState.OK),
            ("AdminMode", AdminMode.ONLINE),
        ],
    )
    def test_controller_update_subsystems_attributes_on_events(
        self: TestCspController,
        attribute: str,
        value: DevState | HealthState | AdminMode,
        device_under_test: DeviceTestContext,
        init_succeeded: pytest.fixture,
    ):

        mock_callback = {}
        for subsystem in ["Cbf", "Pss", "Pst"]:
            mock_callback[subsystem] = mock.MagicMock()
            if subsystem == "Pst":
                device_under_test.subscribe_event(
                    f"cspPstBeams{attribute}",
                    tango.EventType.CHANGE_EVENT,
                    mock_callback[subsystem],
                )
            else:
                device_under_test.subscribe_event(
                    f"csp{subsystem}{attribute}",
                    tango.EventType.CHANGE_EVENT,
                    mock_callback[subsystem],
                )

        TestCspController.raise_event_on_cbf(attribute.lower(), value)
        TestCspController.raise_event_on_pss(attribute.lower(), value)
        TestCspController.raise_event_on_pst_beams(attribute.lower(), value)

        for subsystem in ["Cbf", "Pss"]:
            probe_poller(
                device_under_test, f"csp{subsystem}{attribute}", value, time=7
            )

        value_str = (
            str(value) if attribute == "State" else str(value).split(".")[1]
        )

        probe_poller(
            device_under_test,
            f"cspPstBeams{attribute}",
            '{"common-pst/beam/01": "'
            + value_str
            + '", "common-pst/beam/02": "'
            + value_str
            + '"}',
            time=2,
        )

        time.sleep(0.2)
        # time sleep is necessary to let the event be pushed

        for subsystem in ["Cbf", "Pss"]:
            assert (
                mock_callback[subsystem].call_args[0][0].attr_value.name
                == f"csp{subsystem.lower()}{attribute.lower()}"
            )
            assert (
                mock_callback[subsystem].call_args[0][0].attr_value.value
                == value
            )

        assert (
            mock_callback["Pst"].call_args[0][0].attr_value.name
            == f"csppstbeams{attribute.lower()}"
        )
        assert (
            mock_callback["Pst"].call_args[0][0].attr_value.value
            == '{"common-pst/beam/01": "'
            + value_str
            + '", "common-pst/beam/02": "'
            + value_str
            + '"}'
        )

    @mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_controller_archive_events(
        self: TestCspController, device_under_test: DeviceTestContext
    ):
        """
        Test the archive events on State and healthState pushed from the
        controller device.
        """
        callback_state = mock.Mock()
        callback_health_state = mock.Mock()
        device_under_test.subscribe_event(
            "state", tango.EventType.ARCHIVE_EVENT, callback_state
        )
        device_under_test.subscribe_event(
            "healthstate", tango.EventType.ARCHIVE_EVENT, callback_health_state
        )
        # initialize the Controller device
        self.init_process(device_under_test)
        assert callback_state.call_count == 2
        assert callback_health_state.call_count == 2
