import logging

import pytest
from mock import MagicMock

from ska_csp_lmc_common.commands.task import LeafTask
from ska_csp_lmc_common.commands.task_tracker import TaskTracker

# Assuming the LeafTask class is imported from your module
# from your_module import LeafTask, TaskTracker


@pytest.fixture
def mock_target():
    """Fixture for a mock target object"""
    return MagicMock()


@pytest.fixture
def mock_logger():
    """Fixture for a mock logger"""
    return MagicMock(spec=logging.Logger)


@pytest.fixture
def mock_callback():
    """Fixture for a mock callback function"""
    return MagicMock()


@pytest.fixture
def leaf_task(mock_target, mock_callback, mock_logger):
    """Fixture for initializing a LeafTask object"""
    return LeafTask(
        name="TestTask",
        target=mock_target,
        command_name="run_command",
        task_type="device",
        completed_callback=mock_callback,
        logger=mock_logger,
    )


def test_leaf_task_initialization(leaf_task, mock_target, mock_callback):
    """Test that the LeafTask is properly initialized"""
    assert leaf_task.name == "TestTask"
    assert leaf_task.task_type == "device"
    assert leaf_task.target == mock_target
    assert leaf_task.command_name == "run_command"
    assert isinstance(leaf_task.tracker, TaskTracker)
    assert leaf_task.tracker.completed_callback == mock_callback


def test_leaf_task_logging(leaf_task, mock_logger):
    """Test logger behavior during task execution"""
    leaf_task.logger.info("Starting task")
    mock_logger.info.assert_called_with("Starting task")
