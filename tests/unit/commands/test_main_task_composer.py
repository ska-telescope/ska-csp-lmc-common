import logging

# import pytest
from mock import MagicMock
from ska_control_model import ObsState
from tango import DevState

from ska_csp_lmc_common.commands.command_map import BaseCommandMap
from ska_csp_lmc_common.commands.main_task_composer import MainTaskComposer
from ska_csp_lmc_common.commands.task import LeafTask, Task, TaskExecutionType

module_logger = logging.getLogger(__name__)

dummy_cm = MagicMock()
dummy_cm.online_components = {
    "sim_csp_cbf/subarray/01": MagicMock(),
    "sim-pss/subarray/01": MagicMock(),
    "sim-pst/beam/01": MagicMock(),
    "sim-pst/beam/02": MagicMock(),
}


def test_compose_assign_resources():
    """test that the MainTask composer compose assigneresources
    as expected from command map"""
    # All subsystems are in the right state
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.EMPTY

    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_resources_cbf": "ok"},
        "sim-pss/subarray/01": {"dummy_resources_pss": "ok"},
        "sim-pst/beam/02": {"subarray_id": 1},
        "sim-pst/beam/01": {"subarray_id": 1},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "assign",
        task_type=TaskExecutionType.PARALLEL,
        subtasks=[
            LeafTask(
                name="internal_connect_to_pst",
                target=dummy_cm,
                command_name="connect_to_pst",
                task_type=TaskExecutionType.INTERNAL,
            ),
            LeafTask(
                name="assignresources_pss",
                target=dummy_cm.online_components["sim-pss/subarray/01"],
                command_name="assignresources",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_resources_pss": "ok"},
            ),
            LeafTask(
                "assignresources_cbf",
                dummy_cm.online_components[
                    "sim_csp_cbf/subarray/01"
                ],  # Fixed here
                "assignresources",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_resources_cbf": "ok"},
            ),
        ],
        completed_callback=dummy_callback,
    )
    command_map = BaseCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "assign", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_configure_all():
    """test that the MainTask composer compose configure command
    as expected from command map"""
    # All subsystems are in the right state
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.READY
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_configure_cbf": "ok"},
        "sim-pss/subarray/01": {"dummy_configure_pss": "ok"},
        "sim-pst/beam/01": {"dummy_configure_pst1": "ok"},
        "sim-pst/beam/02": {"dummy_configure_pst2": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "configure",
        task_type=TaskExecutionType.SEQUENTIAL,
        subtasks=[
            Task(
                "configure_pst_pss",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    Task(
                        "configurescan_pst",
                        task_type=TaskExecutionType.PARALLEL,
                        subtasks=[
                            LeafTask(
                                "configurescan_pst01",
                                dummy_cm.online_components["sim-pst/beam/01"],
                                "configurescan",
                                task_type=TaskExecutionType.DEVICE,
                                argin={"dummy_configure_pst1": "ok"},
                            ),
                            LeafTask(
                                "configurescan_pst02",
                                dummy_cm.online_components["sim-pst/beam/02"],
                                "configurescan",
                                task_type=TaskExecutionType.DEVICE,
                                argin={"dummy_configure_pst2": "ok"},
                            ),
                        ],
                    ),
                    LeafTask(
                        name="configurescan_pss",
                        target=dummy_cm.online_components[
                            "sim-pss/subarray/01"
                        ],
                        command_name="configurescan",
                        task_type=TaskExecutionType.DEVICE,
                        argin={"dummy_configure_pss": "ok"},
                    ),
                ],
            ),
            LeafTask(
                name="internal_update_json",
                target=dummy_cm,
                command_name="update_json",
                task_type=TaskExecutionType.INTERNAL,
            ),
            LeafTask(
                name="configure_cbf",
                target=dummy_cm.online_components["sim_csp_cbf/subarray/01"],
                command_name="configure",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_configure_cbf": "ok"},
            ),
        ],
    )
    command_map = BaseCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "configure", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_configure_cbf_pst01():
    """test that the MainTask composer compose configure
    as expected from command map"""
    # Send command only to CBF and PST01
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.READY
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_configure_cbf": "ok"},
        "sim-pst/beam/01": {"dummy_configure_pst1": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "configure",
        task_type=TaskExecutionType.SEQUENTIAL,
        subtasks=[
            Task(
                "configure_pst_pss",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    Task(
                        "configurescan_pst",
                        task_type=TaskExecutionType.PARALLEL,
                        subtasks=[
                            LeafTask(
                                "configurescan_pst01",
                                dummy_cm.online_components["sim-pst/beam/01"],
                                "configurescan",
                                task_type=TaskExecutionType.DEVICE,
                                argin={"dummy_configure_pst1": "ok"},
                            )
                        ],
                    )
                ],
            ),
            LeafTask(
                name="internal_update_json",
                target=dummy_cm,
                command_name="update_json",
                task_type=TaskExecutionType.INTERNAL,
            ),
            LeafTask(
                name="configure_cbf",
                target=dummy_cm.online_components["sim_csp_cbf/subarray/01"],
                command_name="configure",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_configure_cbf": "ok"},
            ),
        ],
    )
    command_map = BaseCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "configure", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_configure_only_cbf():
    """test that the MainTask composer compose assigneresources
    as expected from command map"""
    # # Send command only to CBF
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.READY
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_configure_cbf": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "configure",
        task_type=TaskExecutionType.SEQUENTIAL,
        subtasks=[
            LeafTask(
                name="internal_update_json",
                target=dummy_cm,
                command_name="update_json",
                task_type=TaskExecutionType.INTERNAL,
            ),
            LeafTask(
                name="configure_cbf",
                target=dummy_cm.online_components["sim_csp_cbf/subarray/01"],
                command_name="configure",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_configure_cbf": "ok"},
            ),
        ],
    )
    command_map = BaseCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "configure", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_configure_cbf_pst01_wrong_obsstate():
    """test that the MainTask composer compose configure
    as expected from command map, removing the subsystem in
    wrong state"""
    # Send command only to CBF since PST01 is in wrong ObsState
    dummy_cm.online_components["sim_csp_cbf/subarray/01"].obs_state = (
        ObsState.READY
    )
    dummy_cm.online_components["sim-pst/beam/01"].obs_state = ObsState.FAULT
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_configure_cbf": "ok"},
        "sim-pst/beam/01": {"dummy_configure_pst1": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "configure",
        task_type=TaskExecutionType.SEQUENTIAL,
        subtasks=[
            LeafTask(
                name="internal_update_json",
                target=dummy_cm,
                command_name="update_json",
                task_type=TaskExecutionType.INTERNAL,
            ),
            LeafTask(
                name="configure_cbf",
                target=dummy_cm.online_components["sim_csp_cbf/subarray/01"],
                command_name="configure",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_configure_cbf": "ok"},
            ),
        ],
    )
    command_map = BaseCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "configure", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_scan_cbf_pst01():
    """test that the MainTask composer compose scan
    as expected from command map"""
    # All subsystems are in the right state
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.READY
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_scan_cbf": "ok"},
        "sim-pst/beam/01": {"dummy_scan_pst1": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "scan",
        task_type=TaskExecutionType.PARALLEL,
        subtasks=[
            Task(
                "scan_pst",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    LeafTask(
                        "scan_pst01",
                        dummy_cm.online_components["sim-pst/beam/01"],
                        "scan",
                        task_type=TaskExecutionType.DEVICE,
                        argin={"dummy_scan_pst1": "ok"},
                    ),
                ],
            ),
            LeafTask(
                name="scan_cbf",
                target=dummy_cm.online_components["sim_csp_cbf/subarray/01"],
                command_name="scan",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_scan_cbf": "ok"},
            ),
        ],
    )
    command_map = BaseCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "scan", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_on_controller():
    """test that the MainTask composer compose on command from controller
    as expected from command map"""
    dummy_cm.online_components = {
        "sim_csp_cbf/sub_elt/controller": MagicMock(),
        "sim-pss/controller/0": MagicMock(),
        "sim-pst/beam/01": MagicMock(),
        "sim-pst/beam/02": MagicMock(),
        "sim-csp/subarray/01": MagicMock(),
        "sim-csp/subarray/02": MagicMock(),
        "sim-csp/subarray/03": MagicMock(),
    }
    dummy_cm.online_fqdns = list(dummy_cm.online_components.keys())

    dummy_cm.resources = {}
    for fqdn, component in dummy_cm.online_components.items():
        dummy_cm.resources[fqdn] = None
        component.state = DevState.OFF

    dummy_callback = MagicMock()

    composed = Task(
        "on",
        task_type=TaskExecutionType.PARALLEL,
        subtasks=[
            Task(
                "on_csp_subs",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    LeafTask(
                        "on_csp_sub01",
                        target=dummy_cm.online_components[
                            "sim-csp/subarray/01"
                        ],
                        command_name="on",
                        task_type=TaskExecutionType.DEVICE,
                    ),
                    LeafTask(
                        "on_csp_sub02",
                        target=dummy_cm.online_components[
                            "sim-csp/subarray/02"
                        ],
                        command_name="on",
                        task_type=TaskExecutionType.DEVICE,
                    ),
                    LeafTask(
                        "on_csp_sub03",
                        target=dummy_cm.online_components[
                            "sim-csp/subarray/03"
                        ],
                        command_name="on",
                        task_type=TaskExecutionType.DEVICE,
                    ),
                ],
            ),
            Task(
                "on_pst",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    LeafTask(
                        "on_pst01",
                        target=dummy_cm.online_components["sim-pst/beam/01"],
                        command_name="on",
                        task_type=TaskExecutionType.DEVICE,
                    ),
                    LeafTask(
                        "on_pst02",
                        target=dummy_cm.online_components["sim-pst/beam/02"],
                        command_name="on",
                        task_type=TaskExecutionType.DEVICE,
                    ),
                ],
            ),
            LeafTask(
                "on_pss",
                target=dummy_cm.online_components["sim-pss/controller/0"],
                command_name="on",
                task_type=TaskExecutionType.DEVICE,
            ),
            LeafTask(
                "on_cbf",
                target=dummy_cm.online_components[
                    "sim_csp_cbf/sub_elt/controller"
                ],
                command_name="on",
                task_type=TaskExecutionType.DEVICE,
            ),
        ],
        completed_callback=dummy_callback,
    )
    command_map = BaseCommandMap()
    controller_command_map = command_map.get_controller_command_map()
    assert (
        MainTaskComposer(
            "on", controller_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_off_controller():
    """test that the MainTask composer compose off command from controller
    as expected from command map"""
    dummy_cm.online_components = {
        "sim_csp_cbf/sub_elt/controller": MagicMock(),
        "sim-pss/controller/0": MagicMock(),
        "sim-pst/beam/01": MagicMock(),
        "sim-pst/beam/02": MagicMock(),
        "sim-csp/subarray/01": MagicMock(),
        "sim-csp/subarray/02": MagicMock(),
        "sim-csp/subarray/03": MagicMock(),
    }
    dummy_cm.online_fqdns = list(dummy_cm.online_components.keys())

    dummy_cm.resources = {}
    for fqdn, component in dummy_cm.online_components.items():
        dummy_cm.resources[fqdn] = None
        component.state = DevState.ON

    dummy_callback = MagicMock()

    composed = Task(
        "off",
        task_type=TaskExecutionType.PARALLEL,
        subtasks=[
            Task(
                "off_pst",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    LeafTask(
                        "off_pst01",
                        target=dummy_cm.online_components["sim-pst/beam/01"],
                        command_name="off",
                        task_type=TaskExecutionType.DEVICE,
                    ),
                    LeafTask(
                        "off_pst02",
                        target=dummy_cm.online_components["sim-pst/beam/02"],
                        command_name="off",
                        task_type=TaskExecutionType.DEVICE,
                    ),
                ],
            ),
            LeafTask(
                "off_pss",
                target=dummy_cm.online_components["sim-pss/controller/0"],
                command_name="off",
                task_type=TaskExecutionType.DEVICE,
            ),
            LeafTask(
                "off_cbf",
                target=dummy_cm.online_components[
                    "sim_csp_cbf/sub_elt/controller"
                ],
                command_name="off",
                task_type=TaskExecutionType.DEVICE,
            ),
        ],
        completed_callback=dummy_callback,
    )
    command_map = BaseCommandMap()
    controller_command_map = command_map.get_controller_command_map()
    assert (
        MainTaskComposer(
            "off", controller_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_gotoidle_cbf_pst01():
    """test that the MainTask composer compose gotoidle
    as expected from command map"""
    # All subsystems are in the right state
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.READY
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_gotoidle_cbf": "ok"},
        "sim-pst/beam/01": {"dummy_gotoidle_pst1": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "gotoidle",
        task_type=TaskExecutionType.PARALLEL,
        subtasks=[
            Task(
                "gotoidle_pst",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    LeafTask(
                        "gotoidle_pst01",
                        dummy_cm.online_components["sim-pst/beam/01"],
                        "gotoidle",
                        task_type=TaskExecutionType.DEVICE,
                        argin={"dummy_gotoidle_pst1": "ok"},
                    ),
                ],
            ),
            LeafTask(
                name="gotoidle_cbf",
                target=dummy_cm.online_components["sim_csp_cbf/subarray/01"],
                command_name="gotoidle",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_gotoidle_cbf": "ok"},
            ),
        ],
    )
    command_map = BaseCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "gotoidle", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_reset_cbf_pst01():
    """test that the MainTask composer compose reset
    as expected from command map"""
    # All subsystems are in the right state
    for _, component in dummy_cm.online_components.items():
        component.state = DevState.FAULT
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_reset_cbf": "ok"},
        "sim-pst/beam/01": {"dummy_reset_pst1": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "reset",
        task_type=TaskExecutionType.PARALLEL,
        subtasks=[
            Task(
                "reset_pst",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    LeafTask(
                        "reset_pst01",
                        dummy_cm.online_components["sim-pst/beam/01"],
                        "reset",
                        task_type=TaskExecutionType.DEVICE,
                        argin={"dummy_reset_pst1": "ok"},
                    ),
                ],
            ),
            LeafTask(
                name="reset_cbf",
                target=dummy_cm.online_components["sim_csp_cbf/subarray/01"],
                command_name="reset",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_reset_cbf": "ok"},
            ),
        ],
    )
    command_map = BaseCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "reset", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_restart_cbf_pst01():
    """test that the MainTask composer compose restart
    as expected from command map"""
    # All subsystems are in the right state
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.FAULT
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_restart_cbf": "ok"},
        "sim-pst/beam/01": {"dummy_restart_pst1": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "restart",
        task_type=TaskExecutionType.SEQUENTIAL,
        subtasks=[
            Task(
                "restart_subsystems",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    Task(
                        "obsreset_pst",
                        task_type=TaskExecutionType.PARALLEL,
                        subtasks=[
                            LeafTask(
                                "obsreset_pst01",
                                dummy_cm.online_components["sim-pst/beam/01"],
                                "obsreset",
                                task_type=TaskExecutionType.DEVICE,
                                argin={"dummy_restart_pst1": "ok"},
                            ),
                        ],
                    ),
                    LeafTask(
                        name="restart_cbf",
                        target=dummy_cm.online_components[
                            "sim_csp_cbf/subarray/01"
                        ],
                        command_name="restart",
                        task_type=TaskExecutionType.DEVICE,
                        argin={"dummy_restart_cbf": "ok"},
                    ),
                ],
            ),
            LeafTask(
                name="internal_disconnect_from_all_pst",
                target=dummy_cm,
                command_name="disconnect_from_all_pst",
                task_type=TaskExecutionType.INTERNAL,
            ),
        ],
    )
    command_map = BaseCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "restart", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )
