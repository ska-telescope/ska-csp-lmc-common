"""This module provides test for the device executor"""

import logging

import pytest
from mock import Mock, patch
from ska_control_model import ResultCode, TaskStatus
from ska_tango_base.faults import CommandError, ResultCodeError
from tango import ConnectionFailed, DevFailed, EventType, Except

# from task_module import (
from ska_csp_lmc_common.commands.device_executor import (
    DeviceTaskExecutor,
    LrcSubscriptions,
)
from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector

module_logger = logging.getLogger(__name__)


@pytest.fixture
def device_component():
    mocked_device_component = Mock()
    mocked_device_component.getdeviceproxy = Mock()
    mocked_device_component.getdeviceproxy.get_timeout_millisec = Mock(
        return_value=5000
    )

    mocked_device_component.subscribe_lrc_attribute.return_value = (
        1,
        True,
        None,
    )
    mocked_device_component.unsubscribe_lrc_attribute = Mock(return_value=None)
    return mocked_device_component


def test_lrc_subscriptions_init_and_del():
    """Test LrcSubscriptions class inizialisation"""
    unsubscribe_mock = Mock()
    subscriptions = LrcSubscriptions(
        command_id="12345", unsubscribe_lrc_events=unsubscribe_mock
    )
    assert subscriptions.command_id == "12345"
    # The unsubscribe method is called when subscriptions is deleted
    del subscriptions
    unsubscribe_mock.assert_called_once()


def test_device_task_executor_init():
    """Test DeviceTaskExecutor initialization"""
    executor = DeviceTaskExecutor(logger=module_logger)
    assert executor.logger == module_logger


@patch("ska_csp_lmc_common.commands.device_executor.threading.Event")
def test_invoke_lrc_success(mock_event, device_component):
    """Test DeviceTaskExecutor invoke_lrc with correct callback"""

    lrc_callback = Mock()
    # Mock the current thread and event wait behavior
    mock_event_instance = Mock()
    mock_event.return_value = mock_event_instance

    executor = DeviceTaskExecutor(module_logger)

    device_component.run.return_value = [[ResultCode.QUEUED], ["12345"]]
    subscriptions = executor.invoke_lrc(
        device_component=device_component,
        lrc_callback=lrc_callback,
        command_name="TestCommand",
        command_args=("arg1", "arg2"),
    )

    assert subscriptions.command_id == device_component.run.return_value[1][0]
    # Check if the command was invoked successfully
    device_component.run.assert_called_once_with(
        "testcommand", False, ("arg1", "arg2")
    )


def test_invoke_lrc_rejected_command(device_component):
    """Test DeviceTaskExecutor invoke_lrc with rejected command"""
    lrc_callback = Mock()
    executor = DeviceTaskExecutor(logger=module_logger)

    device_component.run.return_value = [[ResultCode.REJECTED], ["12345"]]
    # Call invoke_lrc and check if the CommandError is raised
    with pytest.raises(CommandError):
        executor.invoke_lrc(
            device_component=device_component,
            lrc_callback=lrc_callback,
            command_name="TestCommand",
        )


def test_invoke_lrc_unexpected_result_code(device_component):
    """Test DeviceTaskExecutor invoke_lrc with unexpected result code"""

    lrc_callback = Mock()

    executor = DeviceTaskExecutor(logger=module_logger)
    # Mock the device_component.run method to return an unexpected result code
    device_component.run.return_value = [[ResultCode.UNKNOWN], ["12345"]]
    # Call invoke_lrc and check if the ResultCodeError is raised
    with pytest.raises(ResultCodeError):
        executor.invoke_lrc(
            device_component=device_component,
            lrc_callback=lrc_callback,
            command_name="TestCommand",
        )


def test_invoke_lrc_invalid_callback(device_component):
    """Test DeviceTaskExecutor invoke_lrc with callback validation failure"""

    device_component = Mock()

    # Invalid callback (missing **kwargs)
    def invalid_lrc_callback(status, progress):
        pass  # Does not accept **kwargs

    executor = DeviceTaskExecutor(logger=module_logger)
    # Call invoke_lrc and check if ValueError is raised due to invalid callback
    with pytest.raises(ValueError):
        executor.invoke_lrc(
            device_component=device_component,
            lrc_callback=invalid_lrc_callback,
            command_name="TestCommand",
        )


def test_invoke_lrc_unsubscribe_on_failure(device_component):
    """Test unsubscribe functionality in DeviceTaskExecutor"""
    lrc_callback = Mock()

    executor = DeviceTaskExecutor(logger=module_logger)
    # Mock device_component.run to simulate a communication failure

    device_component.run.side_effect = Except.to_dev_failed(ConnectionFailed)
    # Call invoke_lrc and check if it unsubscribes from the LRC events
    # on failure
    try:
        executor.invoke_lrc(
            device_component=device_component,
            lrc_callback=lrc_callback,
            command_name="TestCommand",
        )

    except DevFailed as df:
        module_logger.info(df)

    with pytest.raises(DevFailed):
        executor.invoke_lrc(
            device_component=device_component,
            lrc_callback=lrc_callback,
            command_name="TestCommand",
        )

        device_component.unsubscribe_attribute.assert_called_with(1)


@patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_invoke_lrc_on_component():
    """Test invoke_lrc on a Component object"""

    def lrc_callback(
        status=None,
        progress=0,
        result=None,
        error=None,
        **kwargs,
    ) -> None:
        global completed
        if status is not None:
            assert status == TaskStatus.COMPLETED
            completed = True

    global completed
    completed = False
    component = Component("mock/test/01", "test", logger=module_logger)
    component.connect()
    component.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
    executor = DeviceTaskExecutor(logger=module_logger)
    lrc_subscription = executor.invoke_lrc(component, lrc_callback, "on")
    assert lrc_subscription.command_id == "12345678_01_On"
    import time

    time_start = time.time()
    while time.time() - time_start < 5 and not completed:
        time.sleep(0.1)
    assert completed


@patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_invoke_lrc_off_component():
    """Test invoke_lrc off a Component object"""

    def lrc_callback(
        status=None,
        progress=0,
        result=None,
        error=None,
        **kwargs,
    ) -> None:
        global completed
        if status is not None:
            assert status == TaskStatus.COMPLETED
            completed = True

    global completed
    completed = False
    component = Component("mock/test/01", "test", logger=module_logger)
    component.connect()
    component.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
    executor = DeviceTaskExecutor(logger=module_logger)
    lrc_subscription = executor.invoke_lrc(component, lrc_callback, "off")
    assert lrc_subscription.command_id == "12345678_01_Off"
    import time

    time_start = time.time()
    while time.time() - time_start < 5 and not completed:
        time.sleep(0.1)
    assert completed
