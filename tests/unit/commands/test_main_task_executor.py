# -*- coding: utf-8 -*-
#
# This file is part of the SKA Tango Base project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""Tests adapted from the ska_tango_base.main_task_executor module."""
from __future__ import annotations

import concurrent.futures
import logging
import threading
import time
from unittest.mock import ANY

import pytest
from mock import MagicMock, patch
from ska_control_model import ResultCode, TaskStatus
from ska_tango_testing.mock import MockCallableGroup
from tango import EventType

from ska_csp_lmc_common.commands.main_task_executor import (
    MAX_NUM_THREADS,
    MainTaskExecutor,
)
from ska_csp_lmc_common.commands.task import LeafTask, Task, TaskExecutionType
from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.testing import mock_config
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector

module_logger = logging.getLogger(__name__)
# pylint: disable=protected-access


class TestTaskExecutor:
    """Tests of the MainTaskExecutor class."""

    @pytest.fixture()
    def max_workers(self: TestTaskExecutor) -> int:
        """
        Return the maximum number of worker threads.

        :return: the maximum number of worker threads
        """
        return MAX_NUM_THREADS

    @pytest.fixture()
    def main_task_executor(self: TestTaskExecutor) -> MainTaskExecutor:
        """
        Return the MainTaskExecutor under test.

        :param max_workers: maximum number of worker threads.

        :return: a MainTaskExecutor
        """
        csp_main_task_executor = MainTaskExecutor(logger=module_logger)
        csp_main_task_executor._submit_lock = MagicMock()
        csp_main_task_executor._abort_event = threading.Event()
        csp_main_task_executor.set_command_timeout(5)
        return csp_main_task_executor

    @pytest.fixture()
    def callbacks(
        self: TestTaskExecutor, max_workers: int
    ) -> MockCallableGroup:
        """
        Return a dictionary of callbacks with asynchrony support.

        :param max_workers: maximum number of worker threads.

        :return: a collections.default dict that returns callbacks by name.
        """
        task_callbacks = [f"task_{i}" for i in range(max_workers + 2)]
        callback_group = MockCallableGroup(*task_callbacks, "abort")
        # Wrap each callback in a MagicMock
        for task in task_callbacks:
            callback_group._callables[task] = MagicMock()
        return callback_group

    @pytest.fixture()
    def _submit_lock(self: TestTaskExecutor):
        return threading.Lock()

    @pytest.fixture()
    def _abort_event(self: TestTaskExecutor):
        return threading.Event()

    @pytest.fixture()
    def test_manager(self: TestTaskExecutor):
        class TestManager:
            def __init__(self):
                pass

            def execute_subtask1(self):
                time.sleep(0.2)  # Simulate work
                module_logger.info("--subtask1 executed")
                return TaskStatus.COMPLETED, ResultCode.OK

            def execute_subtask2(self):
                time.sleep(0.2)  # Simulate work
                module_logger.info("--subtask2 executed")
                return TaskStatus.COMPLETED, ResultCode.OK

            def execute_subtask3(self):
                time.sleep(0.5)  # Simulate work
                module_logger.info("--subtask3 executed")
                return TaskStatus.COMPLETED, ResultCode.OK

            def execute_subtask_with_exc(self):
                time.sleep(0.5)  # Simulate work
                module_logger.info("--subtask_with_exc")
                raise ValueError("Segmentation fault")

        return TestManager()

    def test_submit_multiple_tasks_and_abort(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
    ):
        """Test submitting tasks of different types"""
        # Add mock subtasks to simulate parallel execution
        subtask1 = LeafTask(
            "Subtask-1",
            test_manager,
            "execute_subtask1",
            task_type=TaskExecutionType.INTERNAL,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            test_manager,
            "execute_subtask2",
            task_type=TaskExecutionType.INTERNAL,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            test_manager,
            "execute_subtask3",
            task_type=TaskExecutionType.INTERNAL,
        )
        # Create a parallel task
        parallel_task = Task(
            "P0", [subtask1, subtask2], TaskExecutionType.PARALLEL
        )
        # Create a sequential task
        sequential_task = Task("S0", [subtask3], TaskExecutionType.SEQUENTIAL)
        mock_main_task = Task(
            "Test-Main-Task",
            [parallel_task, sequential_task],
            TaskExecutionType.SEQUENTIAL,
        )
        try:
            main_thread = threading.Thread(
                target=main_task_executor.submit_main_task,
                args=(mock_main_task,),
                kwargs={"task_callback": callbacks["task_1"]},
            )
            main_thread.start()
        except Exception as exc:
            module_logger.info(f"Test exception {exc}")

        # time.sleep(0.2)
        main_task_executor.abort(task_callback=callbacks["task_1"])
        assert main_task_executor._abort_event.is_set() is True
        callbacks["task_1"].assert_called_with(
            # task_name="Test-Main-Task",
            status=TaskStatus.IN_PROGRESS,
            result=[
                ResultCode.STARTED,
                "Abort Started",
            ],
        )

        time.sleep(5)
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.ABORTED,
            result=[
                ResultCode.ABORTED,
                mock_main_task.tracker._chain_message(),
            ],
        )

    def test_submit_multiple_tasks(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
    ):
        """Test submitting tasks of different types"""
        # Add mock subtasks to simulate parallel execution
        subtask1 = LeafTask(
            "Subtask-1",
            test_manager,
            "execute_subtask1",
            task_type=TaskExecutionType.INTERNAL,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            test_manager,
            "execute_subtask2",
            task_type=TaskExecutionType.INTERNAL,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            test_manager,
            "execute_subtask3",
            task_type=TaskExecutionType.INTERNAL,
        )
        # Create a parallel task
        parallel_task = Task(
            "P0", [subtask1, subtask2], TaskExecutionType.PARALLEL
        )
        # Create a sequential task
        sequential_task = Task("S0", [subtask3], TaskExecutionType.SEQUENTIAL)
        mock_main_task = Task(
            "Test-Main-Task",
            [parallel_task, sequential_task],
            TaskExecutionType.SEQUENTIAL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.COMPLETED,
            result=[
                ResultCode.OK,
                mock_main_task.tracker._chain_message(),
            ],
        )

    def test_submit_single_internal_task(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
    ):
        """Test submitting only one subtask"""
        # Add mock subtasks to simulate parallel execution
        subtask1 = LeafTask(
            "Subtask-1",
            test_manager,
            "execute_subtask1",
            task_type=TaskExecutionType.INTERNAL,
        )

        # Create a sequential task
        mock_main_task = Task(
            "MainTask",
            [subtask1],
            completed_callback=callbacks["task_1"],
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )

        callbacks["task_1"].assert_called_with(
            task_name="MainTask",
            status=TaskStatus.COMPLETED,
            result=[
                ResultCode.OK,
                ANY,
            ],
        )

    def test_submit_single_internal_task_raise_exc(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
    ):
        """Test submitting only one subtask"""
        # Add mock subtasks to simulate parallel execution
        subtask1 = LeafTask(
            "Subtask-1",
            test_manager,
            "execute_subtask_with_exc",
            task_type=TaskExecutionType.INTERNAL,
        )

        # Create a sequential task
        mock_main_task = Task(
            "MainTask",
            [subtask1],
            completed_callback=callbacks["task_1"],
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )

        callbacks["task_1"].assert_called_with(
            task_name="MainTask",
            status=TaskStatus.FAILED,
            result=[
                ResultCode.FAILED,
                ANY,
            ],
            # error=ValueError('Segmentation fault')
        )

    def test_submit_single_internal_task_and_abort(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
    ):
        """Test submitting only one subtask"""
        # Add mock subtasks to simulate parallel execution
        subtask1 = LeafTask(
            "Subtask-1",
            test_manager,
            "execute_subtask1",
            task_type=TaskExecutionType.INTERNAL,
        )

        # Create a sequential task
        mock_main_task = Task(
            "MainTask",
            [subtask1],
            completed_callback=callbacks["task_1"],
        )

        try:
            main_thread = threading.Thread(
                target=main_task_executor.submit_main_task,
                args=(mock_main_task,),
                kwargs={"task_callback": callbacks["task_1"]},
            )
            main_task_executor.abort(task_callback=callbacks["task_1"])
            assert main_task_executor._abort_event.is_set() is True
            callbacks["task_1"].assert_called_with(
                status=TaskStatus.IN_PROGRESS,
                result=[
                    ResultCode.STARTED,
                    "Abort Started",
                ],
            )
            # Postpone the start in order to have the time to set the abort
            # flag, otherwise the internal command may end before it
            main_thread.start()
        except Exception as exc:
            module_logger.info(f"Test exception {exc}")

        time.sleep(1)
        callbacks["task_1"].assert_called_with(
            task_name="MainTask",
            status=TaskStatus.ABORTED,
            result=[
                ResultCode.ABORTED,
                mock_main_task.tracker._chain_message(),
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_submit_single_device_task(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
    ):
        """Test submitting only one subtask"""
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_0.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)

        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )

        # Create a sequential task
        mock_main_task = Task(
            "MainTask",
            [subtask1],
            completed_callback=callbacks["task_1"],
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )

        callbacks["task_1"].assert_called_with(
            task_name="MainTask",
            status=TaskStatus.COMPLETED,
            result=[
                ResultCode.OK,
                ANY,
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_submit_single_device_task_and_abort(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
    ):
        """Test submitting only one subtask"""
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_0.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)

        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )

        # Create a sequential task
        mock_main_task = Task(
            "MainTask",
            [subtask1],
            completed_callback=callbacks["task_1"],
        )

        try:
            main_thread = threading.Thread(
                target=main_task_executor.submit_main_task,
                args=(mock_main_task,),
                kwargs={"task_callback": callbacks["task_1"]},
            )
            main_task_executor.abort(task_callback=callbacks["task_1"])
            assert main_task_executor._abort_event.is_set() is True
            callbacks["task_1"].assert_called_with(
                status=TaskStatus.IN_PROGRESS,
                result=[
                    ResultCode.STARTED,
                    "Abort Started",
                ],
            )
            # Postpone the start in order to have the time to set the abort
            # flag, otherwise the device command ends before it
            main_thread.start()
        except Exception as exc:
            module_logger.info(f"Test exception {exc}")

        time.sleep(1)
        callbacks["task_1"].assert_called_with(
            task_name="MainTask",
            status=TaskStatus.ABORTED,
            result=[
                ResultCode.ABORTED,
                mock_main_task.tracker._chain_message(),
            ],
        )

    def test_shutdown(
        self: TestTaskExecutor, main_task_executor: MainTaskExecutor
    ):
        """Test shutting down the main_task_executor"""
        with patch.object(
            concurrent.futures.ThreadPoolExecutor, "shutdown"
        ) as mock_shutdown:
            main_task_executor.shutdown()
            mock_shutdown.assert_called_once_with(wait=True)

    # def test_relaunch(
    #     self: TestTaskExecutor, main_task_executor: MainTaskExecutor
    # ):
    #     """Test relaunching the main_task_executor"""
    #     with patch.object(
    #         concurrent.futures.ThreadPoolExecutor,
    #         "__init__",
    #         return_value=None,
    #     ) as mock_init:
    #         main_task_executor.relaunch()
    #         mock_init.assert_called_once_with(
    #             max_workers=main_task_executor._max_workers
    #         )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_submit_serial_multiple_task(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
    ):
        """
        Test a sequential Task with two subtasks:
         - one with two parallel subtasks executed on 2
           different components
         - one internal subtask
        """
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_0.connect()
        component_1.connect()

        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Add mock subtasks to simulate parallel execution
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            test_manager,
            "execute_subtask3",
            task_type=TaskExecutionType.INTERNAL,
        )
        # Create a parallel task
        parallel_task = Task(
            "P0", [subtask1, subtask2], TaskExecutionType.PARALLEL
        )
        # Create a sequential task
        sequential_task = Task("S0", [subtask3], TaskExecutionType.SEQUENTIAL)
        mock_main_task = Task(
            "Test-Main-Task",
            [parallel_task, sequential_task],
            TaskExecutionType.SEQUENTIAL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.COMPLETED,
            result=[
                ResultCode.OK,
                mock_main_task.tracker._chain_message(),
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_submit_parallel_multiple_task(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
    ):
        """
        Test a parallel Task with two subtasks:
         - one with two sequential subtasks executed on 2
           different components
         - one internal subtask
        """
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Add mock subtasks to simulate parallel execution
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            test_manager,
            "execute_subtask3",
            task_type=TaskExecutionType.INTERNAL,
        )

        # Create a sequential task
        sequential_task = Task(
            "S0", [subtask1, subtask2], task_type=TaskExecutionType.SEQUENTIAL
        )

        # Create a parallel task
        mock_main_task = Task(
            "Test-Main-Task",
            [sequential_task, subtask3],
            task_type=TaskExecutionType.PARALLEL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.COMPLETED,
            result=[
                ResultCode.OK,
                mock_main_task.tracker._chain_message(),
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_main_task_executor_with_parallel_device_tasks(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
    ):
        """
        Test a Task with three parallel device subtasks executed on
        3 different components.
        Expected result:
        - the MainTask ends returning TaskStatus.COMPLETED
        - the ResultCode is UNKNONW because the lrccallback only communicates
          thee status of the comman, with no information about the result code
          and message.
        """
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Add mock subtasks to simulate parallel execution
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )

        # Create a parallel task
        mock_main_task = Task(
            "Test-Main-Task",
            [subtask1, subtask2, subtask3],
            task_type=TaskExecutionType.PARALLEL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.COMPLETED,
            result=[
                ResultCode.OK,
                mock_main_task.tracker._chain_message(),
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_main_task_executor_with_serial_device_tasks(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
    ):
        """
        Test a Task with three sequential device subtasks executed on
        3 different components.

        Expected result:
        - the MainTask ends returning TaskStatus.COMPLETED
        - the ResultCode is UNKNONW because the lrccallback only communicates
          thee status of the comman, with no information about the result code
          and message.
        """
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Create subtasks
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )

        # Create a parallel task
        mock_main_task = Task(
            "Test-Main-Task",
            [subtask1, subtask2, subtask3],
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.COMPLETED,
            result=[
                ResultCode.OK,
                mock_main_task.tracker._chain_message(),
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_main_task_executor_with_serial_device_tasks_with_timeout(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
        mock_config_init,
    ):
        """
        Test a Task with three serial device subtasks executed on
        3 different components. The execution on device 00 does not
        complete (does not push the COMPLETED event.)

        Expected result:
        - the MainTask ends returning TaskStatus.FAILED
        - the ResultCode is FAILED.
        """
        mock_config.mock_timeout = True
        mock_config.mock_fail_device = "mock/test/00"
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Create subtasks
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )

        # Create a parallel task
        mock_main_task = Task(
            "Test-Main-Task",
            [subtask1, subtask2, subtask3],
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.FAILED,
            result=[
                ResultCode.FAILED,
                mock_main_task.tracker._chain_message(),
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_main_task_executor_with_parallel_device_tasks_with_timeout(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
        mock_config_init,
    ):
        """
        Test a Task with three parallel device subtasks executed on
        3 different components. The execution on device 01 does not
        complete (does not push the COMPLETED event.)

        Expected result:
        - the MainTask ends returning TaskStatus.FAILED
        - the ResultCode is FAILED.
        """

        mock_config.mock_fail_device = "mock/test/01"
        mock_config.mock_event_failure = True
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Create subtasks
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )

        # Create a parallel task
        mock_main_task = Task(
            "Test-Main-Task",
            [subtask1, subtask2, subtask3],
            task_type=TaskExecutionType.PARALLEL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.FAILED,
            result=[
                ResultCode.FAILED,
                mock_main_task.tracker._chain_message(),
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_sequential_device_tasks_with_timeout_and_skip_flag(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
        mock_config_init,
    ):
        """
        Test a Task with three parallel device subtasks executed on
        3 different components. The execution on device 01 does not
        complete (does not push the COMPLETED event.)

        Expected result:
        - the MainTask ends returning TaskStatus.FAILED
        - the ResultCode is FAILED.
        """

        mock_config.mock_fail_device = "mock/test/01"
        mock_config.mock_event_failure = True
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Create subtasks
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            skip_subtasks=True,
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )

        # Create a parallel task
        mock_main_task = Task(
            "Test-Main-Task",
            [subtask1, subtask2, subtask3],
            task_type=TaskExecutionType.SEQUENTIAL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.FAILED,
            result=[
                ResultCode.FAILED,
                mock_main_task.tracker._chain_message(),
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_main_task_executor_several_tasks_with_timeout(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
        mock_config_init,
    ):
        """
        Test a Task with three parallel device subtasks executed on
        3 different components. The execution on device 01 does not
        complete (does not push the COMPLETED event.)

        Expected result:
        - the MainTask ends returning TaskStatus.FAILED
        - the ResultCode is FAILED.
        """
        mock_config.mock_fail_device = "mock/test/01"
        mock_config.mock_event_failure = True
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Create subtasks
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask4 = LeafTask(
            "Subtask-4",
            test_manager,
            "execute_subtask1",
            task_type=TaskExecutionType.INTERNAL,
        )
        subtask5 = LeafTask(
            "Subtask-5",
            test_manager,
            "execute_subtask2",
            task_type=TaskExecutionType.INTERNAL,
        )
        msubtask1 = Task(
            "device-tasks",
            [subtask1, subtask2, subtask3],
            task_type=TaskExecutionType.PARALLEL,
        )
        msubtask2 = Task(
            "Internal-tasks",
            [
                subtask4,
                subtask5,
            ],
            task_type=TaskExecutionType.PARALLEL,
        )

        # Create a parallel task
        mock_main_task = Task(
            "Test-Main-Task",
            [msubtask1, msubtask2],
            task_type=TaskExecutionType.SEQUENTIAL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )

        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.FAILED,
            result=[
                ResultCode.FAILED,
                mock_main_task.tracker._chain_message(),
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_main_task_executor_several_tasks_with_timeout_1(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
        mock_config_init,
    ):
        """
        Test a Task with three parallel device subtasks executed on
        3 different components. The execution on device 01 sends the
        COMPLETE event after the timeout expiration.

        Expected result:
        - the MainTask ends returning TaskStatus.FAILED
        - the ResultCode is FAILED.
        """
        mock_config.mock_timeout = True
        mock_config.mock_fail_device = "mock/test/01"
        mock_config.mock_event_failure = False
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Create subtasks
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask4 = LeafTask(
            "Subtask-4",
            test_manager,
            "execute_subtask1",
            task_type=TaskExecutionType.INTERNAL,
        )
        subtask5 = LeafTask(
            "Subtask-5",
            test_manager,
            "execute_subtask2",
            task_type=TaskExecutionType.INTERNAL,
        )
        msubtask1 = Task(
            "device-tasks",
            [subtask1, subtask2, subtask3],
            task_type=TaskExecutionType.PARALLEL,
        )
        msubtask2 = Task(
            "Internal-tasks",
            [
                subtask4,
                subtask5,
            ],
            task_type=TaskExecutionType.PARALLEL,
        )

        # Create a parallel task
        mock_main_task = Task(
            "Test-Main-Task",
            [msubtask1, msubtask2],
            task_type=TaskExecutionType.SEQUENTIAL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )

        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.FAILED,
            result=[
                ResultCode.FAILED,
                mock_main_task.tracker._chain_message(),
            ],
        )
        # wait to receive the COMPLETED from the device that is
        # late
        time.sleep(3)
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.FAILED,
            result=[
                ResultCode.FAILED,
                mock_main_task.tracker._chain_message(),
            ],
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_skip_subtasks_flag_within_parallel_tasks(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
        mock_config_init,
    ):
        """
        Given 3 devices (component_0, component_1, component_2), verify what
        happen when a subtask on component_1 fails and skip_subtasks=True in
        a parallel task.
        The serial Task is composed by two parallel subtasks: the first one
        sent a command per device, while the second execute 2 internal tasks.
        The subtask2 (executed on component_1) has the flag 'skip_subtasks'
        set to True.

        Expected result:
        - The first parallel task fails since the subtask 2 fails.
        - Since the flag 'skip_subtasks' is set, the second parallel task is
        REJECTED.
        - the MainTask ends returning TaskStatus.REJECTED
        - the ResultCode is FAILED.
        """
        mock_config.mock_cmd_fail = True
        mock_config.mock_fail_device = "mock/test/01"
        mock_config.mock_exception = True
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Create subtasks
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask4 = LeafTask(
            "Subtask-4",
            test_manager,
            "execute_subtask1",
            task_type=TaskExecutionType.INTERNAL,
        )
        subtask5 = LeafTask(
            "Subtask-5",
            test_manager,
            "execute_subtask2",
            task_type=TaskExecutionType.INTERNAL,
        )
        msubtask1 = Task(
            "device-tasks",
            [subtask1, subtask2, subtask3],
            skip_subtasks=True,
            task_type=TaskExecutionType.PARALLEL,
        )
        msubtask2 = Task(
            "Internal-tasks",
            [
                subtask4,
                subtask5,
            ],
            task_type=TaskExecutionType.PARALLEL,
        )

        # Create a parallel task
        mock_main_task = Task(
            "Test-Main-Task",
            [msubtask1, msubtask2],
            task_type=TaskExecutionType.SEQUENTIAL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        time.sleep(4)
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.REJECTED,
            result=[
                ResultCode.FAILED,
                mock_main_task.tracker._chain_message(),
            ],
            error=ANY,
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_skip_subtasks_flag_within_sequential_tasks(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
        mock_config_init,
    ):
        """
        Given 3 devices (component_0, component_1, component_2), verify what
        happen when a subtask on component_1 fails and skip_subtasks=True in
        a task type SEQUENTIAL.
        The serial Task is composed by two subtasks: the first one
        send one command per device in sequence, while the second executes
        2 internal tasks in parallel.
        The subtask2 (executed on component_1) has the flag 'skip_subtasks' set
        to True.

        Expected result:
        - The first sequential task fails since the subtask 2 fails.
        - Since the flag 'skip_subtasks' is set, the second parallel task is
        REJECTED.
        - the MainTask ends returning TaskStatus.REJECTED
        - the ResultCode is FAILED.
        """
        mock_config.mock_cmd_fail = True
        mock_config.mock_fail_device = "mock/test/01"
        mock_config.mock_exception = True  # generate an exception so that the
        # component_1 fails
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Create subtasks
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask4 = LeafTask(
            "Subtask-4",
            test_manager,
            "execute_subtask1",
            task_type=TaskExecutionType.INTERNAL,
        )
        subtask5 = LeafTask(
            "Subtask-5",
            test_manager,
            "execute_subtask2",
            task_type=TaskExecutionType.INTERNAL,
        )
        # group the subtasks in two tasks
        msubtask1 = Task(
            "device-tasks",
            [subtask1, subtask2, subtask3],
            skip_subtasks=True,
            task_type=TaskExecutionType.SEQUENTIAL,
        )
        msubtask2 = Task(
            "Internal-tasks",
            [
                subtask4,
                subtask5,
            ],
            task_type=TaskExecutionType.PARALLEL,
        )

        # Create the main sequential task
        mock_main_task = Task(
            "Test-Main-Task",
            [msubtask1, msubtask2],
            task_type=TaskExecutionType.SEQUENTIAL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        time.sleep(2)
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.REJECTED,
            result=[
                ResultCode.FAILED,
                mock_main_task.tracker._chain_message(),
            ],
            error=ANY,
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_skip_subtasks_flag_within_subtask_of_sequential_tasks(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
        mock_config_init,
    ):
        """
        Given 3 devices (component_0, component_1, component_2), verify what
        happen when a subtask on component_1 fails and skip_subtasks=True in
        a task type SEQUENTIAL.
        The serial Task is composed by two subtasks: the first one
        sends one command per device in sequence, while the second executes
        2 internal tasks in parallel.
        The subtask2 (executed on component_1) has the flag 'skip_subtasks' set
        to True.

        Expected result:
        - The first parallel task fails since the subtask 2 fails.
        - Since the flag 'skip_subtasks' is set, all the subsequent tasks
        (subtask_3 and msubtask2) are REJECTED.
        - the MainTask ends returning TaskStatus.REJECTED
        - the ResultCode is FAILED.
        """
        mock_config.mock_cmd_fail = True
        mock_config.mock_fail_device = "mock/test/01"
        mock_config.mock_exception = True  # generate an exception so that the
        # component_1 fails
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Create subtasks
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            skip_subtasks=True,
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask4 = LeafTask(
            "Subtask-4",
            test_manager,
            "execute_subtask1",
            task_type=TaskExecutionType.INTERNAL,
        )
        subtask5 = LeafTask(
            "Subtask-5",
            test_manager,
            "execute_subtask2",
            task_type=TaskExecutionType.INTERNAL,
        )
        # group the subtasks in two tasks
        msubtask1 = Task(
            "device-tasks",
            [subtask1, subtask2, subtask3],
            task_type=TaskExecutionType.SEQUENTIAL,
        )
        msubtask2 = Task(
            "Internal-tasks",
            [
                subtask4,
                subtask5,
            ],
            task_type=TaskExecutionType.PARALLEL,
        )

        # Create the main sequential task
        mock_main_task = Task(
            "Test-Main-Task",
            [msubtask1, msubtask2],
            task_type=TaskExecutionType.SEQUENTIAL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        time.sleep(2)
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.REJECTED,
            result=[
                ResultCode.FAILED,
                mock_main_task.tracker._chain_message(),
            ],
            error=ANY,
        )

    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def test_skip_subtasks_flag_in_parallel_task(
        self: TestTaskExecutor,
        main_task_executor: MainTaskExecutor,
        callbacks: MockCallableGroup,
        test_manager,
        mock_config_init,
    ):
        """
        Given a sequential task composed of two parallel tasks, verify what
        happens when a subtask of the first parallel task fails.

        Expected result:
        - The first parallel task fails since the subtask 2 fails.
        - Since the flag 'skip_subtasks' is set, the subsequent task
          msubtask2 will be rejected.
        - the MainTask ends returning TaskStatus.REJECTED
        - the ResultCode is FAILED.
        """
        mock_config.mock_cmd_fail = True
        mock_config.mock_fail_device = "mock/test/01"
        mock_config.mock_exception = True  # generate an exception so that the
        # component_1 fails
        component_0 = Component("mock/test/00", "test_0", logger=module_logger)
        component_1 = Component("mock/test/01", "test_1", logger=module_logger)
        component_2 = Component("mock/test/02", "test_2", logger=module_logger)
        component_0.connect()
        component_1.connect()
        component_2.connect()
        component_0.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_1.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        component_2.subscribe_attribute("state", EventType.CHANGE_EVENT, print)
        # Create subtasks
        subtask1 = LeafTask(
            "Subtask-1",
            component_0,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask2 = LeafTask(
            "Subtask-2",
            component_1,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask3 = LeafTask(
            "Subtask-3",
            component_2,
            "On",
            task_type=TaskExecutionType.DEVICE,
        )
        subtask4 = LeafTask(
            "Subtask-4",
            test_manager,
            "execute_subtask1",
            task_type=TaskExecutionType.INTERNAL,
        )
        subtask5 = LeafTask(
            "Subtask-5",
            test_manager,
            "execute_subtask2",
            task_type=TaskExecutionType.INTERNAL,
        )
        # group the subtasks in two tasks
        msubtask1 = Task(
            "device-tasks",
            [subtask1, subtask2, subtask3],
            task_type=TaskExecutionType.PARALLEL,
            skip_subtasks=True,
        )
        msubtask2 = Task(
            "Internal-tasks",
            [
                subtask4,
                subtask5,
            ],
            task_type=TaskExecutionType.PARALLEL,
        )

        # Create the main sequential task
        mock_main_task = Task(
            "Test-Main-Task",
            [msubtask1, msubtask2],
            task_type=TaskExecutionType.SEQUENTIAL,
        )

        main_task_executor.submit_main_task(
            mock_main_task, task_callback=callbacks["task_1"]
        )
        time.sleep(2)
        callbacks["task_1"].assert_called_with(
            task_name="Test-Main-Task",
            status=TaskStatus.REJECTED,
            result=[
                ResultCode.FAILED,
                mock_main_task.tracker._chain_message(),
            ],
            error=ANY,
        )
