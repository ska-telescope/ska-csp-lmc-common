import logging

import pytest
from mock import MagicMock

from ska_csp_lmc_common.commands.task import Task
from ska_csp_lmc_common.commands.task_tracker import TaskTracker

# Assuming Task and LeafTask are imported from your module
# from your_module import Task, LeafTask, TaskTracker


@pytest.fixture
def mock_subtask():
    """Fixture for a mock subtask object (could be a Task or LeafTask)"""
    subtask = MagicMock()
    subtask.skip_subtasks = None
    return subtask


@pytest.fixture
def mock_logger():
    """Fixture for a mock logger"""
    return MagicMock(spec=logging.Logger)


@pytest.fixture
def mock_completed_callback():
    """Fixture for a mock completed callback"""
    return MagicMock()


@pytest.fixture
def task(mock_subtask, mock_logger, mock_completed_callback):
    """Fixture for initializing a Task object"""
    return Task(
        name="TestTask",
        subtasks=[mock_subtask],
        task_type="sequential",
        completed_callback=mock_completed_callback,
        logger=mock_logger,
    )


@pytest.fixture
def task_blocker(mock_subtask, mock_logger, mock_completed_callback):
    """Fixture for initializing a Task object with the blocker
    flag set to True"""
    return Task(
        name="TestTask",
        subtasks=[mock_subtask],
        task_type="sequential",
        skip_subtasks=True,
        completed_callback=mock_completed_callback,
        logger=mock_logger,
    )


def test_task_initialization(task, mock_subtask, mock_completed_callback):
    """Test that the Task is properly initialized"""
    assert task.name == "TestTask"
    assert task.subtasks == [mock_subtask]
    assert task.task_type == "sequential"
    assert isinstance(task.tracker, TaskTracker)
    assert task.tracker.task_name == "TestTask"
    assert task.tracker.total_subtasks == 1
    assert task.tracker.completed_callback == mock_completed_callback


def test_task_logging(task, mock_logger):
    """Test logger behavior during task operations"""
    task.logger.info("Starting task")
    mock_logger.info.assert_called_with("Starting task")


def test_task_sequential_type(task):
    """Test that sequential task types are set correctly"""
    assert task.task_type == "sequential"
    # by default, if the flag is not defined, it is None
    assert task.skip_subtasks is None


def test_task_sequential_type_with_blocker(task_blocker):
    """Test that sequential task types are set correctly"""
    assert task_blocker.task_type == "sequential"
    assert task_blocker.skip_subtasks is True


@pytest.fixture
def parallel_task(mock_subtask, mock_logger, mock_completed_callback):
    """Fixture for initializing a parallel Task object"""
    return Task(
        name="ParallelTask",
        subtasks=[
            mock_subtask,
            mock_subtask,
        ],  # Two subtasks for parallel testing
        task_type="parallel",
        completed_callback=mock_completed_callback,
        logger=mock_logger,
    )


def test_subtask_skip_subtasks_flag(parallel_task):
    """Test that parallel task types are set correctly"""
    assert parallel_task.task_type == "parallel"
    assert len(parallel_task.subtasks) == 2
    assert parallel_task.subtasks[0].skip_subtasks is None
    assert parallel_task.subtasks[1].skip_subtasks is None
