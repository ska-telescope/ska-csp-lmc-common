import threading
from unittest.mock import MagicMock

from ska_control_model import ResultCode, TaskStatus
from tango import DevError, Except

from ska_csp_lmc_common.commands.task_tracker import SubtaskResult, TaskTracker


def test_task_tracker_initialization():
    # Arrange
    task_name = "TestTask"
    total_subtasks = 3
    # Act
    tracker = TaskTracker(task_name, total_subtasks)

    # Assert
    assert tracker.task_name == task_name
    assert tracker.total_subtasks == total_subtasks
    assert tracker.completed_subtasks == 0
    assert tracker.overall_status == TaskStatus.QUEUED
    assert tracker.subtasks == {}


def test_update_task_completion():
    # Arrange
    task_name = "TestTask"
    tracker = TaskTracker(task_name, 1)
    subtask_name = "Subtask1"
    tracker.subtasks[subtask_name] = SubtaskResult(
        subtask_name,
        TaskStatus.QUEUED,
        [ResultCode.UNKNOWN, "Queued"],
    )

    # Act
    tracker.update_task(
        subtask_name,
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Completed successfully"),
    )

    # Assert
    assert tracker.completed_subtasks == 1
    assert tracker.subtasks[subtask_name].status == TaskStatus.COMPLETED
    assert tracker.subtasks[subtask_name].result_code == ResultCode.OK
    assert tracker.overall_status == TaskStatus.COMPLETED


def test_update_task_failure():
    # Arrange
    task_name = "TestTask"
    task_evt = threading.Event()
    tracker = TaskTracker(task_name, 2, task_evt)
    subtask_name = "Subtask1"
    tracker.subtasks[subtask_name] = SubtaskResult(
        subtask_name, TaskStatus.QUEUED, [ResultCode.UNKNOWN, ""]
    )

    # Act
    tracker.update_task(
        subtask_name,
        status=TaskStatus.FAILED,
        result=(ResultCode.FAILED, "Failed due to error"),
    )

    # Assert
    assert tracker.completed_subtasks == 1
    assert tracker.subtasks[subtask_name].status == TaskStatus.FAILED
    assert tracker.overall_status == TaskStatus.FAILED


def test_check_completion_callback():
    # Arrange
    mock_callback = MagicMock()
    tracker = TaskTracker("TestTask", 2, completed_callback=mock_callback)

    # Act
    tracker.update_task(
        "Subtask2",
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Success"),
    )
    tracker.update_task(
        "Subtask1",
        status=TaskStatus.FAILED,
        result=[ResultCode.UNKNOWN, "Failed with unknown result"],
    )

    # Assert
    mock_callback.assert_called_with(
        task_name="TestTask",
        status=TaskStatus.FAILED,
        result=[tracker._update_overall_result(), tracker._chain_message()],
    )
    assert tracker.completed_subtasks == 2
    assert tracker.overall_status == TaskStatus.FAILED
    assert tracker.task_evt.is_set()


def test_update_task_with_error():
    # Arrange
    tracker = TaskTracker("TestTask", 1)
    subtask_name = "SubtaskWithError"
    dev_error = Except.to_dev_failed(DevError)
    tracker.subtasks[subtask_name] = SubtaskResult(
        subtask_name,
        TaskStatus.QUEUED,
        [ResultCode.UNKNOWN, ""],
    )

    # Act
    tracker.update_task(
        subtask_name,
        status=TaskStatus.FAILED,
        result=(ResultCode.FAILED, "Error occurred"),
        error=(dev_error,),
    )

    # Assert
    assert tracker.error == (dev_error,)
    assert tracker.overall_status == TaskStatus.FAILED
    assert tracker.subtasks[subtask_name].status == TaskStatus.FAILED
    assert tracker.subtasks[subtask_name].message == "Error occurred"


def test_get_progress():
    # Arrange
    task_evt = threading.Event()
    tracker = TaskTracker("TestTask", 4, task_evt)
    tracker.completed_subtasks = 2

    # Act
    progress = tracker.get_progress()

    # Assert
    assert progress == 50.0
