import logging

import mock
import pytest
from ska_control_model import AdminMode, HealthState, ObsState
from tango import DevState

from ska_csp_lmc_common.subarray import (
    SubarrayHealthModel,
    SubarrayObsStateModel,
    SubarrayOpStateModel,
)

module_logger = logging.getLogger(__name__)


def ComponentToTestObsState(fqdn, obs_state_value, weight):
    component = mock.MagicMock()
    obs_state = mock.PropertyMock(return_value=obs_state_value)
    fqdn = mock.PropertyMock(return_value=fqdn)
    weight = mock.PropertyMock(return_value=weight)
    type(component).obs_state = obs_state
    type(component).fqdn = fqdn
    type(component).weight = weight
    return component


def ComponentToTestHealthState(fqdn, health_value, state_value, weight):
    component = mock.MagicMock()
    health_state = mock.PropertyMock(return_value=health_value)
    state = mock.PropertyMock(return_value=state_value)
    fqdn = mock.PropertyMock(return_value=fqdn)
    weight = mock.PropertyMock(return_value=weight)
    if state_value == DevState.DISABLE:
        admin_mode_value = AdminMode.OFFLINE
    else:
        admin_mode_value = AdminMode.ENGINEERING
    admin_mode = mock.PropertyMock(return_value=admin_mode_value)
    type(component).health_state = health_state
    type(component).state = state
    type(component).fqdn = fqdn
    type(component).weight = weight
    type(component).admin_mode = admin_mode
    return component


@pytest.mark.parametrize(
    ["pss_obs_state", "cbf_obs_state", "pst_obs_state", "csp_obs_state"],
    (
        (ObsState.EMPTY, ObsState.EMPTY, ObsState.IDLE, ObsState.EMPTY),
        (ObsState.EMPTY, ObsState.IDLE, ObsState.IDLE, ObsState.IDLE),
        (ObsState.EMPTY, ObsState.READY, ObsState.IDLE, ObsState.READY),
        (ObsState.EMPTY, ObsState.FAULT, ObsState.IDLE, ObsState.FAULT),
        (ObsState.EMPTY, ObsState.SCANNING, ObsState.IDLE, ObsState.SCANNING),
        (
            ObsState.EMPTY,
            ObsState.CONFIGURING,
            ObsState.IDLE,
            ObsState.CONFIGURING,
        ),
        (ObsState.EMPTY, ObsState.ABORTING, ObsState.IDLE, ObsState.ABORTING),
        (
            ObsState.EMPTY,
            ObsState.RESETTING,
            ObsState.IDLE,
            ObsState.RESETTING,
        ),
        (
            ObsState.EMPTY,
            ObsState.RESTARTING,
            ObsState.IDLE,
            ObsState.RESTARTING,
        ),
        (ObsState.IDLE, ObsState.EMPTY, ObsState.IDLE, ObsState.EMPTY),
        (ObsState.READY, ObsState.EMPTY, ObsState.IDLE, ObsState.EMPTY),
        (ObsState.FAULT, ObsState.EMPTY, ObsState.IDLE, ObsState.EMPTY),
        (ObsState.SCANNING, ObsState.READY, ObsState.IDLE, ObsState.READY),
        (ObsState.READY, ObsState.READY, ObsState.SCANNING, ObsState.READY),
        (ObsState.SCANNING, ObsState.FAULT, ObsState.SCANNING, ObsState.FAULT),
        (
            ObsState.CONFIGURING,
            ObsState.READY,
            ObsState.CONFIGURING,
            ObsState.CONFIGURING,
        ),
        (
            ObsState.RESETTING,
            ObsState.FAULT,
            ObsState.RESETTING,
            ObsState.FAULT,
        ),
    ),
)
def test_subarray_obs_state_model(
    pss_obs_state, cbf_obs_state, pst_obs_state, csp_obs_state
):
    """Test the overall CSP subarray obsState as aggregation of the CSP sub-
    systems obsStates."""
    obs_state_model = SubarrayObsStateModel(ObsState.EMPTY, mock.MagicMock())
    # instantiate 3 different components
    list_of_components = []
    list_of_components.append(
        ComponentToTestObsState("low-pss/subarray/01", pss_obs_state, 0)
    )
    list_of_components.append(
        ComponentToTestObsState("low-pst/beam/01", pst_obs_state, 0)
    )
    list_of_components.append(
        ComponentToTestObsState("low-cbf/subarray/01", cbf_obs_state, 1)
    )
    for component in list_of_components:
        obs_state_model.component_obs_state_changed(
            component, component.obs_state
        )
    assert obs_state_model.obs_state == csp_obs_state


@pytest.mark.parametrize(
    [
        "pss_state",
        "pss_health",
        "cbf_state",
        "cbf_health",
        "pst_state",
        "pst_health",
        "csp_state",
        "csp_health",
    ],
    (
        (
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.OK,
        ),
        (
            DevState.ON,
            HealthState.DEGRADED,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.DEGRADED,
        ),
        (
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.DEGRADED,
            DevState.ON,
            HealthState.DEGRADED,
        ),
        (
            DevState.DISABLE,
            HealthState.UNKNOWN,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.DEGRADED,
            DevState.ON,
            HealthState.DEGRADED,
        ),
        (
            DevState.DISABLE,
            HealthState.UNKNOWN,
            DevState.ON,
            HealthState.DEGRADED,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.DEGRADED,
        ),
        (
            DevState.ON,
            HealthState.OK,
            DevState.DISABLE,
            HealthState.UNKNOWN,
            DevState.ON,
            HealthState.OK,
            DevState.FAULT,
            HealthState.FAILED,
        ),
        (
            DevState.ON,
            HealthState.OK,
            DevState.FAULT,
            HealthState.FAILED,
            DevState.ON,
            HealthState.OK,
            DevState.FAULT,
            HealthState.FAILED,
        ),
    ),
)
def test_subarray_health_and_state_model(
    pss_state,
    pss_health,
    cbf_state,
    cbf_health,
    pst_state,
    pst_health,
    csp_state,
    csp_health,
):
    """Test the overall CSP subarray State and healthState as aggregation of
    the CSP sub-systems states."""
    op_state_model = SubarrayOpStateModel(DevState.INIT, mock.MagicMock())
    health_state_model = SubarrayHealthModel(HealthState.OK, mock.MagicMock())
    # instantiate 3 different components
    list_of_components = []
    list_of_components.append(
        ComponentToTestHealthState(
            "low-pss/subarray/01", pss_health, pss_state, 0
        )
    )
    list_of_components.append(
        ComponentToTestHealthState("low-pst/beam/01", pst_health, pst_state, 0)
    )
    list_of_components.append(
        ComponentToTestHealthState(
            "low-cbf/subarray/01", cbf_health, cbf_state, 1
        )
    )
    for component in list_of_components:
        op_state_model.component_op_state_changed(component, component.state)
        health_state_model.component_health_changed(
            component, component.health_state
        )
    assert op_state_model.op_state == csp_state
    assert health_state_model.health_state == csp_health


@pytest.mark.parametrize(
    [
        "action",
        "obs_state",
    ],
    (
        ("assign_invoked", ObsState.RESOURCING),
        ("release_all_invoked", ObsState.RESOURCING),
        ("configure_invoked", ObsState.CONFIGURING),
        ("abort_invoked", ObsState.ABORTING),
        ("obsreset_invoked", ObsState.RESETTING),
        ("restart_invoked", ObsState.RESTARTING),
    ),
)
def test_perform_action_invoked(action, obs_state):
    """Test to verify transitional observing states.

    Verify the observing state of a command that triggers a
    transition when invoked.
    """
    callback = mock.MagicMock()
    obs_state_model = SubarrayObsStateModel(
        ObsState.EMPTY, callback, module_logger
    )
    obs_state_model.perform_action(action)
    assert obs_state_model.obs_state == obs_state
    expected_calls = [ObsState.EMPTY, obs_state]
    calls = callback.call_args
    calls == expected_calls
    callback.assert_called_with(obs_state)


def test_perform_action_completed():
    """Test to verify obsering states on command completion."""
    callback = mock.MagicMock()
    obs_state_model = SubarrayObsStateModel(
        ObsState.EMPTY, callback, module_logger
    )
    # create the CBF observing component
    cbf = ComponentToTestObsState("sim-cbf/subarray/01", ObsState.IDLE, 1)
    # trigger a change in its observig state
    obs_state_model.component_obs_state_changed(cbf, cbf.obs_state)
    obs_state_model.perform_action("assign_completed")
    assert not obs_state_model.action_driven
    # expected values for the mock calls
    expected_calls = [ObsState.EMPTY, ObsState.IDLE]
    # values for the mock calls
    calls = callback.call_args
    calls == expected_calls
    callback.assert_called_with(ObsState.IDLE)


def test_perform_action_wrong_command_invoked():
    callback = mock.MagicMock()
    obs_state_model = SubarrayObsStateModel(
        ObsState.EMPTY, callback, module_logger
    )
    obs_state_model.perform_action("wrong_invoked")
    callback.assert_called_with(ObsState.EMPTY)
    assert obs_state_model.obs_state == ObsState.EMPTY
    assert not obs_state_model.action_driven


def test_perform_action_wrong_command_completed():
    callback = mock.MagicMock()
    obs_state_model = SubarrayObsStateModel(
        ObsState.EMPTY, callback, module_logger
    )
    obs_state_model.perform_action("wrong_completed")
    callback.assert_called_with(ObsState.EMPTY)
    assert obs_state_model.obs_state == ObsState.EMPTY
    assert not obs_state_model.action_driven


def test_perform_action_wrong_request():
    callback = mock.MagicMock()
    obs_state_model = SubarrayObsStateModel(
        ObsState.EMPTY, callback, module_logger
    )
    obs_state_model.perform_action("assign_wrong")
    assert not obs_state_model.action_driven
    assert obs_state_model.obs_state == ObsState.EMPTY
    callback.assert_called_with(ObsState.EMPTY)


def test_perform_action_wrong_action():
    callback = mock.MagicMock()
    obs_state_model = SubarrayObsStateModel(
        ObsState.EMPTY, callback, module_logger
    )
    obs_state_model.perform_action("assignwrong")
    assert not obs_state_model.action_driven
    assert obs_state_model.obs_state == ObsState.EMPTY
    callback.assert_called_with(ObsState.EMPTY)


@pytest.mark.parametrize("action", ["deconfigure_invoked", "end_scan_invoked"])
def test_perform_action_invoked_available(action):
    """Test to verify observing states on command completion."""
    callback = mock.MagicMock()
    obs_state_model = SubarrayObsStateModel(
        ObsState.EMPTY, callback, module_logger
    )
    obs_state_model.perform_action(action)
    assert obs_state_model.action_driven
    callback.assert_called_with(ObsState.EMPTY)
