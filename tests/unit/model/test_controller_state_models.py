import logging

import mock
import pytest
from ska_control_model import AdminMode, HealthState
from tango import DevState

from ska_csp_lmc_common.controller import (
    ControllerHealthModel,
    ControllerOpStateModel,
)

module_logger = logging.getLogger(__name__)


def ComponentToTestHealthState(fqdn, health_value, state_value, weight):
    component = mock.MagicMock()
    health_state = mock.PropertyMock(return_value=health_value)
    state = mock.PropertyMock(return_value=state_value)
    fqdn = mock.PropertyMock(return_value=fqdn)
    weight = mock.PropertyMock(return_value=weight)
    if state_value == DevState.DISABLE:
        admin_mode_value = AdminMode.OFFLINE
    else:
        admin_mode_value = AdminMode.ENGINEERING
    admin_mode = mock.PropertyMock(return_value=admin_mode_value)
    type(component).health_state = health_state
    type(component).state = state
    type(component).fqdn = fqdn
    type(component).weight = weight
    type(component).admin_mode = admin_mode
    return component


@pytest.mark.parametrize(
    [
        "pss_state",
        "pss_health",
        "cbf_state",
        "cbf_health",
        "pst_state",
        "pst_health",
        "csp_state",
        "csp_health",
    ],
    (
        (
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.OK,
        ),
        (
            DevState.ON,
            HealthState.DEGRADED,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.DEGRADED,
        ),
        (
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.DEGRADED,
            DevState.ON,
            HealthState.DEGRADED,
        ),
        (
            DevState.DISABLE,
            HealthState.UNKNOWN,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.DEGRADED,
            DevState.ON,
            HealthState.DEGRADED,
        ),
        (
            DevState.DISABLE,
            HealthState.UNKNOWN,
            DevState.ON,
            HealthState.DEGRADED,
            DevState.ON,
            HealthState.OK,
            DevState.ON,
            HealthState.DEGRADED,
        ),
        (
            DevState.ON,
            HealthState.OK,
            DevState.DISABLE,
            HealthState.UNKNOWN,
            DevState.ON,
            HealthState.OK,
            DevState.FAULT,
            HealthState.FAILED,
        ),
    ),
)
def test_controller_health_and_state_model(
    pss_state,
    pss_health,
    cbf_state,
    cbf_health,
    pst_state,
    pst_health,
    csp_state,
    csp_health,
):
    """Test the overall CSP Controller State and healthState as aggregation of
    the CSP sub-systems states."""
    op_state_model = ControllerOpStateModel(DevState.INIT, mock.MagicMock())
    health_state_model = ControllerHealthModel(
        HealthState.OK, mock.MagicMock()
    )
    # instantiate 3 different components
    list_of_components = []
    list_of_components.append(
        ComponentToTestHealthState(
            "low-pss/control/0", pss_health, pss_state, 0
        )
    )
    list_of_components.append(
        ComponentToTestHealthState(
            "low-pst/control/0", pst_health, pst_state, 0
        )
    )
    list_of_components.append(
        ComponentToTestHealthState(
            "low-cbf/control/0", cbf_health, cbf_state, 1
        )
    )
    for component in list_of_components:
        op_state_model.component_op_state_changed(component, component.state)
        health_state_model.component_health_changed(component, component.state)
    assert op_state_model.op_state == csp_state
    assert health_state_model.health_state == csp_health
