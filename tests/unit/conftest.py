# pylint: disable=cyclic-import
import logging

import mock
import pytest

from ska_csp_lmc_common.manager import (
    CSPControllerComponentManager,
    CSPSubarrayComponentManager,
)

# pylint: disable-next=line-too-long
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing import mock_config
from ska_csp_lmc_common.testing.test_classes import (
    test_properties_ctrl,
    test_properties_subarray,
)

# pylint: disable=redefined-outer-name
# pylint: disable=missing-function-docstring

module_logger = logging.getLogger(__name__)


@pytest.fixture(scope="function")
def ctrl_component_manager(
    ctrl_op_state_model,
    ctrl_health_state_model,
):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_ctrl
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        ctrl_component_manager = CSPControllerComponentManager(
            ctrl_op_state_model,
            ctrl_health_state_model,
            cm_conf,
            mock.MagicMock(),
            logger=module_logger,
        )
        ctrl_component_manager.init()
        yield ctrl_component_manager


@pytest.fixture(scope="function")
def subarray_component_manager(
    subarray_op_state_model,
    subarray_health_state_model,
    subarray_obs_state_model,
):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_subarray
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        module_logger.setLevel(logging.DEBUG)
        subarray_component_manager = CSPSubarrayComponentManager(
            subarray_health_state_model,
            subarray_op_state_model,
            subarray_obs_state_model,
            cm_conf,
            mock.MagicMock(),
            logger=module_logger,
        )
        subarray_component_manager.init()
        yield subarray_component_manager


@pytest.fixture()
def mock_config_init():
    mock_config.mock_timeout = False
    mock_config.mock_exception = False
    mock_config.mock_fail_device = None
    mock_config.mock_event_failure = False
    mock_config.mock_cmd_fail = False  # the failing command
    mock_config.mock_event_value = None
    mock_config.mock_event = False
    mock_config.mock_cbk_cmd = False
    mock_config.mock_cbk_cmd_result = None
    mock_config.mock_cbk_cmd_err = False
