import copy
import json
import logging

import pytest
from mock import MagicMock

from ska_csp_lmc_common.subarray.json_schema_validator import CspJsonValidator
from ska_csp_lmc_common.testing.test_classes import load_json_file

# pylint: disable=attribute-defined-outside-init
# pylint: disable=invalid-name
# pylint: disable=missing-function-docstring

module_logger = logging.getLogger(__name__)

json_config_0_1 = {
    "interface": "https://schema.skao.int/ska-csp-configure/0.1",
    "id": "sbi-mvp01-20200325-00001-science_A",
    "frequencyBand": "1",
    "fsp": [
        {
            "fspID": 1,
            "functionMode": "CORR",
            "frequencySliceID": 1,
            "integrationTime": 1400,
            "corrBandwidth": 0,
            "channelAveragingMap": [[0, 2], [744, 0]],
            "fspChannelOffset": 0,
            "outputLinkMap": [[0, 0], [200, 1]],
        },
        {
            "fspID": 2,
            "functionMode": "CORR",
            "frequencySliceID": 2,
            "integrationTime": 1400,
            "corrBandwidth": 0,
            "channelAveragingMap": [[0, 2], [744, 0]],
            "fspChannelOffset": 744,
            "outputLinkMap": [[0, 4], [200, 5]],
        },
    ],
}

json_config_1_0 = {
    "interface": "https://schema.skao.int/ska-csp-configure/1.0",
    "subarray": {"subarrayName": "science period 23"},
    "common": {
        "id": "sbi-mvp01-20200325-00001-science_A",
        "frequencyBand": "1",
        "subarrayID": 1,
    },
    "cbf": {
        "fsp": [
            {
                "fspID": 1,
                "functionMode": "CORR",
                "frequencySliceID": 1,
                "integrationTime": 1400,
                "corrBandwidth": 0,
                "channelAveragingMap": [[0, 2], [744, 0]],
                "fspChannelOffset": 0,
                "outputLinkMap": [[0, 0], [200, 1]],
            },
            {
                "fspID": 2,
                "functionMode": "CORR",
                "frequencySliceID": 2,
                "integrationTime": 1400,
                "corrBandwidth": 0,
                "channelAveragingMap": [[0, 2], [744, 0]],
                "fspChannelOffset": 744,
                "outputLinkMap": [[0, 4], [200, 5]],
            },
        ]
    },
}

json_config_2_0 = {
    "interface": "https://schema.skao.int/ska-csp-configure/2.0",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "frequency_band": "1",
        "subarray_id": 1,
    },
    "cbf": {
        "fsp": [
            {
                "fsp_id": 1,
                "function_mode": "CORR",
                "frequency_slice_id": 1,
                "integration_factor": 1,
                "zoom_factor": 0,
                "channel_averaging_map": [[0, 2], [744, 0]],
                "channel_offset": 0,
                "output_link_map": [[0, 0], [200, 1]],
                "output_host": [[0, "192.168.0.1"], [400, "192.168.0.2"]],
                "output_mac": [[0, "06-00-00-00-00-00"]],
                "output_port": [[0, 9000, 1], [400, 9000, 1]],
            },
            {
                "fsp_id": 2,
                "function_mode": "CORR",
                "frequency_slice_id": 2,
                "integration_factor": 1,
                "zoom_factor": 1,
                "zoom_window_tuning": 650000,
                "channel_averaging_map": [[0, 2], [744, 0]],
                "channel_offset": 744,
                "output_link_map": [[0, 4], [200, 5]],
                "output_host": [[0, "192.168.0.3"], [400, "192.168.0.4"]],
                "output_mac": [[0, "06-00-00-00-00-01"]],
                "output_port": [[0, 9000, 1], [400, 9000, 1]],
            },
        ],
        "vlbi": {},
    },
    "pst": {},
}


json_assign_resources_2_0 = {
    "interface": "https://schema.skao.int/ska-csp-assignresources/2.0",
    "subarray_id": 1,
    "dish": {"receptor_ids": ["SKA001", "SKA022"]},
}

json_string_invalid_interface_version = {
    "interface": "https://schema.skao.int/ska-csp-configure/4.5",
    "subarray": {"subarray_name": "science period 23"},
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "frequency_band": "1",
        "subarray_id": 1,
    },
    "cbf": {
        "fsp": [
            {
                "fsp_id": 1,
                "function_mode": "CORR",
                "frequency_slice_id": 1,
                "integration_factor": 1400,
                "zoom_factor": 0,
                "channel_averaging_map": [[0, 2], [744, 0]],
                "channel_offset": 0,
                "output_link_map": [[0, 0], [200, 1]],
                "output_host": [[0, "192.168.0.1"], [400, "192.168.0.2"]],
                "output_mac": [[0, "06-00-00-00-00-00"]],
                "output_port": [[0, 9000, 1], [400, 9000, 1]],
            }
        ],
        "vlbi": {},
    },
}

json_string_invalid_URI = {"interface": "invaliduri.com/0.1"}


# pylint: disable-next=missing-class-docstring
class TestJsonSchemaValidator:
    def test_validate_without_interface_version(self):
        """
        If interface is not provided,
        Json is always validated
        """
        json_pre_ADR35 = {
            "subarrayID": 1,
            "dish": {"receptorIDList": ["0001", "0002"]},
        }
        json_dummy = {"test": "dummy"}
        for json_dict in [json_pre_ADR35, json_dummy]:
            input_json = json.dumps(json_dict)
            _, json_output = CspJsonValidator("whatever").validate(input_json)
            assert json_output == json_dict

    def test_reject_empty_input(self):
        """If an empty input is given an error is raised"""
        with pytest.raises(json.JSONDecodeError, match=r"Expecting value"):
            CspJsonValidator("whatever").validate("")

    def test_config_json_with_invalid_uri(self):
        """
        If an invalid uri is given, an exception is raised
        """
        json_input = json.dumps(json_string_invalid_URI)
        with pytest.raises(ValueError, match=r"Unknown schema URI kind:"):
            CspJsonValidator("whatever").validate(json_input)

    def test_validate_config_json_valid(self):
        # for json_dict in [json_config_0_1, json_config_1_0, json_config_2_0]:
        # for json_dict in [json_config_1_0, json_config_2_0]:  # Not supported
        for json_dict in [json_config_2_0]:
            json_input = json.dumps(json_dict)
            CspJsonValidator("Configure").validate(json_input)

    def test_validate_config_json_invalid(self):
        json_config_no_function_mode = copy.deepcopy(json_config_2_0)
        json_config_no_function_mode["cbf"]["fsp"][0].pop("function_mode")
        json_input = json.dumps(json_config_no_function_mode)
        with pytest.raises(
            ValueError,
            match=r"Validation 'CSP config 2.0'",
        ):
            CspJsonValidator("Configure").validate(json_input)

    def test_validate_config_json_invalid_interface(self):
        json_config_wrong_interface = copy.deepcopy(json_config_2_0)
        json_config_wrong_interface["interface"] = (
            "https://schema.skao.int/ska-csp-configure/100.0"
        )
        json_input = json.dumps(json_config_wrong_interface)
        with pytest.raises(
            ValueError, match=r"Unknown major schema version: .+"
        ):
            CspJsonValidator("Configure").validate(json_input)

    def test_validate_assignresources_config_json_no_subarray_id(self):
        assign_input = load_json_file(filename="test_AssignResources.json")
        assign_input.pop("subarray_id")
        json_input = json.dumps(assign_input)
        with pytest.raises(ValueError):
            CspJsonValidator("Assignresources").validate(json_input)

    def test_validate_assignresources_wrong_subarray_id(self):
        """Test that if the subarray_id does not match, an error is raised."""
        assign_input = load_json_file(filename="test_AssignResources.json")
        cm = MagicMock()
        cm.sub_id = 12
        attrs_dict = {"subarray_id": cm.sub_id}
        json_input = json.dumps(assign_input)
        with pytest.raises(
            ValueError, match=r"^Wrong value for the subarray ID:"
        ):
            CspJsonValidator("Assignresources", None, attrs_dict).validate(
                json_input
            )

    def test_validate_assignresources_when_pst_beam_not_deployed(self):
        """
        Test the assignresources configuration is rejected if the pst beam to
        assign is not deployed
        """
        assign_input = load_json_file(
            filename="test_AssignResources_with_PST.json"
        )
        attrs_dict = {
            "subarray_id": 1,
            "deployed_timing_beams_id": [
                "common-pst/beam/03",
                "common-pst/beam/04",
            ],
        }

        json_input = json.dumps(assign_input)
        with pytest.raises(
            ValueError,
            match=r"Incoherent configuration: the PST beam IDs"
            r" \[1, 2\] are not part of the deployed "
            r"beam IDs \[3, 4\]",
        ):
            CspJsonValidator(
                "Assignresources", attrs_dict=attrs_dict, logger=module_logger
            ).validate(json_input)

    def test_validate_configure_config_json_no_common(self):
        configure_input = load_json_file(
            filename="test_ConfigureScan_basic.json"
        )
        configure_input.pop("common")
        json_input = json.dumps(configure_input)
        with pytest.raises(ValueError):
            CspJsonValidator("Configure").validate(json_input)

    def test_validate_configure_config_json_no_subarray_id(self):
        configure_input = load_json_file(
            filename="test_ConfigureScan_basic.json"
        )
        configure_input["common"].pop("subarray_id")
        json_input = json.dumps(configure_input)
        with pytest.raises(ValueError):
            CspJsonValidator("Configure").validate(json_input)

    def test_validate_configure_wrong_subarray_id(self):
        """Test that if the subarray_id does not match, an error is raised."""
        configure_input = load_json_file(
            filename="test_ConfigureScan_basic.json"
        )
        cm = MagicMock()
        cm.sub_id = 12
        attrs_dict = {"subarray_id": cm.sub_id}
        json_input = json.dumps(configure_input)
        with pytest.raises(
            ValueError, match=r"^Wrong value for the subarray ID:"
        ):
            CspJsonValidator("Configure", None, attrs_dict).validate(
                json_input
            )
