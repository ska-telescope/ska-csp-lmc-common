import json
import logging

import pytest
from mock import MagicMock

from ska_csp_lmc_common.subarray.json_configuration_parser import (
    JsonConfigurationParser,
)
from ska_csp_lmc_common.testing.test_classes import TestBaseSubarray

# pylint: disable=invalid-name

module_logger = logging.getLogger(__name__)

configure_input = TestBaseSubarray.configuration_input()


def test_split_configure_wrong_sub_id():
    """Test that ValueError is raised if the sub_id of ComponentManager and the
    one in configuration file are not matching."""
    cm = MagicMock()
    cm.sub_id = 12
    with pytest.raises(ValueError):
        JsonConfigurationParser(cm, configure_input, module_logger).configure()


def test_split_configure_complete():
    """Test that the output dictionary is populated as expected."""
    cm = MagicMock()
    cm.sub_id = 1
    cm.CbfSubarrayFqdn = "low-cbf/subarray/01"
    cm.PssSubarrayFqdn = "low-pss/subarray/01"
    splitted_resources = JsonConfigurationParser(
        cm, configure_input, module_logger
    ).configure()

    assert list(splitted_resources.keys()) == [
        "low-cbf/subarray/01",
        "low-pss/subarray/01",
    ]
    # assert that sections relatives to subsystems are reported
    assert list(splitted_resources["low-cbf/subarray/01"].keys()) == [
        "interface",
        "subarray",
        "common",
        "cbf",
    ]
    module_logger.info(
        f'splitted_resources: {splitted_resources["low-pss/subarray/01"]}'
    )
    assert list(splitted_resources["low-pss/subarray/01"].keys()) == [
        "interface",
        "subarray",
        "common",
        "pss",
    ]

    assert (
        splitted_resources["low-cbf/subarray/01"]["cbf"]["fsp"][0]["fsp_id"]
        == 1
    )
    assert (
        splitted_resources["low-cbf/subarray/01"]["cbf"]["fsp"][1]["fsp_id"]
        == 2
    )
    assert (
        splitted_resources["low-pss/subarray/01"]["pss"]["beam_bandwidth"]
        == 300
    )
    assert (
        splitted_resources["low-pss/subarray/01"]["pss"]["beam"][0]["beam_id"]
        == 1
    )
    assert (
        splitted_resources["low-pss/subarray/01"]["pss"]["beam"][1]["beam_id"]
        == 2
    )


# def test_split_configure_no_cbf():
#     """Test that if the output dictionary has only the pss section,
#     than return an empty list."""
#     cm = MagicMock()
#     cm.sub_id = 1
#     cm.PssSubarrayFqdn = "low-pss/subarray/01"
#     configure_input.pop("cbf")
#     splitted_resources = JsonConfigurationParser(
#         cm, configure_input, module_logger
#     ).configure()
#     assert not splitted_resources


def test_split_configure_no_pss():
    """Test that the output dictionary has only the cbf section."""
    cm = MagicMock()
    cm.sub_id = 1
    cm.CbfSubarrayFqdn = "low-cbf/subarray/01"
    configure_input.pop("pss")
    splitted_resources = JsonConfigurationParser(
        cm, configure_input, module_logger
    ).configure()
    assert list(splitted_resources.keys()) == ["low-cbf/subarray/01"]
    assert list(splitted_resources["low-cbf/subarray/01"].keys()) == [
        "interface",
        "subarray",
        "common",
        "cbf",
    ]
    assert (
        splitted_resources["low-cbf/subarray/01"]["cbf"]["fsp"][0]["fsp_id"]
        == 1
    )
    assert (
        splitted_resources["low-cbf/subarray/01"]["cbf"]["fsp"][1]["fsp_id"]
        == 2
    )


def test_parse_assign_beams():
    """Test that parser puts assigned PST beams in the correct section."""

    with open(
        "tests/test_data/test_AssignResources_basic.json", encoding="utf-8"
    ) as file:
        assign_input = json.load(file)

    cm = MagicMock()
    cm.sub_id = 1
    cm.CbfSubarrayFqdn = "sim-cbf/subarray/01"
    cm.PstBeamsFqdn = ["sim-pst/beam/01", "sim-pst/beam/02"]
    splitted_resources = JsonConfigurationParser(
        cm, assign_input, module_logger
    ).assignresources()

    assert splitted_resources["sim-pst/beam/01"] == {"subarray_id": 1}
    assert splitted_resources["sim-pst/beam/02"] == {"subarray_id": 1}
