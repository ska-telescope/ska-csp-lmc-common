import time

import pytest

from ska_csp_lmc_common.timeout import Timeout

# pylint: disable=redefined-outer-name

DEFAULT_DURATION = 3


@pytest.fixture(
    params=[
        DEFAULT_DURATION,
    ]
)
def timeout(request):
    """Returns a timeout with the default duration set to 3 secs."""
    return Timeout(request.param)


def test_initialize_timeout(timeout):
    """Test the default duration value for the timeout."""
    # Instantiate Timeout object
    assert timeout.duration == DEFAULT_DURATION
    assert timeout.start_time == 0
    assert not timeout.expired()


def test_timeout_set_duration(timeout):
    """Change the timeout duration and test if the timeout is properly set."""
    timeout.set(6)
    assert timeout.duration == 6


def test_timeout_start_time(timeout):
    """Start the timeout and test if the start time is returned."""
    start_time = time.time()
    timeout.start()
    start_time = time.time()
    ret_start_time = timeout.start_time
    assert time.gmtime(ret_start_time) == time.gmtime(start_time)


def test_timeout_expired(timeout):
    """Set the timeout max duration, wait for that period and verify if the
    timeout expired."""
    timeout.start()
    time.sleep(DEFAULT_DURATION + 0.1)
    assert timeout.expired()
