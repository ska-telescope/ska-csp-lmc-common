import logging
from statistics import StatisticsError

import pytest

from ska_csp_lmc_common.utils.monitorlib import meanstd_data

module_logger = logging.getLogger(__name__)

TEST_DATASET = [
    [["ciao", "2", "3.0"]],
    [["1", "2", "3"]],
    [["1", 2, 3.0]],
    [[[22], 2, 0]],
    [[{"i": 2}, 2, 0]],
    [[]],
]


@pytest.mark.parametrize(argnames=["data"], argvalues=TEST_DATASET)
def test_meanstd_data_exceptions(data):
    """Test that the meanstd_data raise ValueError exception if
    the list contains other type than float and int"""

    with pytest.raises(ValueError):
        avg, stddev = meanstd_data(data)


TEST_DATASET2 = [
    [[1]],
]


@pytest.mark.parametrize(argnames=["data"], argvalues=TEST_DATASET2)
def test_meanstd_data_len_exceptions(data):
    """Test that the meanstd_data raise StatisticsError exception if
    the list contains <2 elements"""

    with pytest.raises(StatisticsError):
        avg, stddev = meanstd_data(data)


def test_meanstd_data_integer(data=[1, 2, 10, 18]):
    """Test the result of the function, that calculate average
    and standard deviation of the input integer list"""
    avg, stddev = meanstd_data(data)
    assert avg == 7.75
    assert stddev == 7.932


def test_meanstd_data_float(data=[1.5, 2.0, 3.0, 4.0, 7.0]):
    """Test the result of the function, that calculate average
    and standard deviation of the input float list"""
    avg, stddev = meanstd_data(data)
    assert avg == 3.5
    assert stddev == 2.17945


def test_meanstd_data_zero(data=[0.0, 0.0, 0.0]):
    """Test the result of the function, that calculate average
    and standard deviation having [0.0] as input"""
    avg, stddev = meanstd_data(data)
    assert avg == 0.0
    assert stddev == 0.0
