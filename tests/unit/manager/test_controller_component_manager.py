import functools
import logging
from unittest.mock import ANY

import mock
import pytest
from mock import PropertyMock, patch
from ska_control_model import TaskStatus
from ska_tango_base.commands import ResultCode
from ska_tango_base.faults import CommandError
from ska_tango_base.utils import generate_command_id
from ska_tango_testing.mock import MockCallableGroup
from tango import DevState

from ska_csp_lmc_common.manager import CSPControllerComponentManager
from ska_csp_lmc_common.testing import mock_config
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import (
    TestBaseController,
    test_properties_ctrl,
)

logger = logging.getLogger(__name__)


dict_of_online_components = {
    "common-cbf/control/0": mock.MagicMock(),
    "common-pss/control/0": mock.MagicMock(),
    "common-pst/beam/01": mock.MagicMock(),
    "common-pst/beam/02": mock.MagicMock(),
    "common-csp/subarray/01": mock.MagicMock(),
    "common-csp/subarray/02": mock.MagicMock(),
    "common-csp/subarray/03": mock.MagicMock(),
}


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return MockCallableGroup(
        "on_task", "reset_task", "off_task", "standby_task", timeout=7.0
    )


class NewMockedConnector(MockedConnector):
    """Class to mock the connection with a sub-system controller.

    Inherits from the base class MockedConnector and overrides the
    fake_get_Attribute method to specialize for Mid instance.
    """

    @classmethod
    def fake_set_attribute(self, attr_name):
        return mock.MagicMock()


def test_populate_subsystems_list_with_empty_argin(ctrl_component_manager):
    with patch.object(
        CSPControllerComponentManager,
        "online_components",
        new_callable=PropertyMock,
    ) as mocked_online_components:
        mocked_online_components.return_value = dict_of_online_components
        assert len(dict_of_online_components) == 7
        valid_fqdns = ctrl_component_manager._populate_subsystems_list([])
        # empty list return all online component without csp subarrays
        assert len(valid_fqdns) == 7


@patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
class TestControllerManager(TestBaseController):
    # NOTE: the fqdn of the devices in devices_list must reflect those
    # defined in  device_properties dictionary (test_properterties_ctrl)

    @pytest.fixture
    @patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
    def ctrl_component_manager_communicating(self, ctrl_component_manager):
        ctrl_component_manager._admin_mode = 2
        ctrl_component_manager.start_communicating()
        # wait to establish communication
        probe_poller(ctrl_component_manager, "is_communicating", True, time=3)

    def test_ctrl_cm_command_timeout_default(self, ctrl_component_manager):
        assert ctrl_component_manager.command_timeout == int(
            test_properties_ctrl["DefaultCommandTimeout"]
        )
        ctrl_component_manager.command_timeout = 6
        assert ctrl_component_manager.command_timeout == 6
        with pytest.raises(ValueError):
            ctrl_component_manager.command_timeout = 0
            assert ctrl_component_manager.command_timeout == 6

    def test_ctrl_cm_with_components_offline(
        self,
        ctrl_component_manager,
        mock_config_init,
    ):
        assert not ctrl_component_manager.is_communicating
        assert not len(ctrl_component_manager.online_components)

    def test_ctrl_cm_on_line_components(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        mock_config_init,
    ):
        assert len(ctrl_component_manager.online_components) == 7

    def test_components_created(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        mock_config_init,
    ):
        assert len(ctrl_component_manager.components) == 7
        cbf = TestControllerManager.devices_list["cbf-ctrl"]
        assert ctrl_component_manager.components[cbf].name == "cbf-controller"
        pss = TestControllerManager.devices_list["pss-ctrl"]
        assert ctrl_component_manager.components[pss].name == "pss-controller"
        pst1 = TestControllerManager.devices_list["pst-beam-01"]
        assert ctrl_component_manager.components[pst1].name == "pst-beam-01"
        pst2 = TestControllerManager.devices_list["pst-beam-02"]
        assert ctrl_component_manager.components[pst2].name == "pst-beam-02"
        sub1 = TestControllerManager.devices_list["subarray_01"]
        assert (
            ctrl_component_manager.components[sub1].name == "csp-subarray-01"
        )
        sub2 = TestControllerManager.devices_list["subarray_02"]
        assert (
            ctrl_component_manager.components[sub2].name == "csp-subarray-02"
        )
        sub3 = TestControllerManager.devices_list["subarray_03"]
        assert (
            ctrl_component_manager.components[sub3].name == "csp-subarray-03"
        )

    def test_populate_subsystems_list_with_empty_argin(
        self,
        ctrl_component_manager,
        mock_config_init,
    ):
        with patch.object(
            CSPControllerComponentManager,
            "online_components",
            new_callable=PropertyMock,
        ) as mocked_online_components:
            mocked_online_components.return_value = dict_of_online_components
            assert len(dict_of_online_components) == 7
            valid_fqdns = ctrl_component_manager._populate_subsystems_list([])
            # csp subarray are not counted in the list
            assert len(valid_fqdns) == 7

    def test_populate_subsystems_list_with_only_invalid_argin(
        self,
        ctrl_component_manager,
        mock_config_init,
    ):
        with patch.object(
            CSPControllerComponentManager,
            "online_components",
            new_callable=PropertyMock,
        ) as mocked_online_components:
            mocked_online_components.return_value = dict_of_online_components
            with pytest.raises(CommandError):
                ctrl_component_manager._populate_subsystems_list(
                    ["wrong-cbf/control/0"]
                )

    def test_populate_subsystems_list_with_valid_argin(
        self,
        ctrl_component_manager,
        mock_config_init,
    ):
        with patch.object(
            CSPControllerComponentManager,
            "online_components",
            new_callable=PropertyMock,
        ) as mocked_online_components:
            mocked_online_components.return_value = dict_of_online_components
            ctrl_component_manager.CspSubarrays = [
                "common-csp/subarray/01",
                "common-csp/subarray/02",
                "common-csp/subarray/03",
            ]
            valid_fqdns = ctrl_component_manager._populate_subsystems_list(
                ["common-cbf/control/0", "common-pss/control/0"]
            )
            assert len(valid_fqdns) == 2

    def test_ctrl_cm_stop_communicating(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        mock_config_init,
    ):
        ctrl_component_manager._admin_mode = 1
        ctrl_component_manager.stop_communicating()
        # wait to establish communication
        probe_poller(ctrl_component_manager, "is_communicating", False, time=3)

    def test_ctrl_cm_invoke_On_on_all_sub_systems(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks: MockCallableGroup,
        mock_config_init,
    ):
        ctrl_component_manager.on(
            [],
            functools.partial(callbacks["on_task"], generate_command_id("On")),
        )
        callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["on_task"].assert_against_call(status=TaskStatus.IN_PROGRESS)
        callbacks["on_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=[ResultCode.OK, "Task on completed with result OK"],
            lookahead=3,
        )
        probe_poller(
            ctrl_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=1,
        )

    def test_ctrl_cm_invoke_On_with_wrong_sub_systems(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):

        ctrl_component_manager.on(
            ["wrong-sub-system/control/0"],
            task_callback=functools.partial(
                callbacks["on_task"], generate_command_id("On")
            ),
        )
        callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["on_task"].assert_against_call(
            status=TaskStatus.REJECTED,
            result=[ResultCode.REJECTED, ANY],
        )

    @pytest.mark.skip(
        "The abort_commands() is going deprecated from BC 1.2."
        " Skip the test waiting the replacement command"
    )
    def test_ctrl_cm_abort_on_command(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """
        Test abort tasks.
        """
        ctrl_component_manager.on(
            [],
            task_callback=functools.partial(
                callbacks["on_task"], generate_command_id("On")
            ),
        )
        ctrl_component_manager.abort_commands()
        callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["on_task"].assert_against_call(status=TaskStatus.ABORTED)
        # assert "on" in ctrl_component_manager.observers

    def test_ctrl_cm_invoke_reset_with_cbf_in_fault(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test Controller Reset command when the CBF sub-system is in
        FAULT."""
        TestControllerManager.raise_event_on_cbf("state", DevState.FAULT)

        task_status, _ = ctrl_component_manager.reset(
            functools.partial(
                callbacks["reset_task"], generate_command_id("Reset")
            )
        )
        assert task_status == TaskStatus.QUEUED
        callbacks["reset_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["reset_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        callbacks["reset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=[ResultCode.OK, ANY],
            lookahead=5,
        )

    def test_ctrl_cm_invoke_reset_with_no_subsystem_in_fault(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test Controller Reset command when no sub-system is in
        FAULT."""
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.ON
        )

        task_status, _ = ctrl_component_manager.reset(
            task_callback=functools.partial(
                callbacks["reset_task"], generate_command_id("Reset")
            )
        )
        callbacks["reset_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["reset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            lookahead=5,
        )

    def test_ctrl_cm_invoke_reset_with_only_controller_in_fault(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test Controller Reset command when only the CSP Controller
        is in fault.
        """
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.ON
        )
        ctrl_component_manager.op_state_model.component_fault(True)
        assert ctrl_component_manager.op_state_model.faulty

        ctrl_component_manager.reset(
            task_callback=functools.partial(
                callbacks["reset_task"], generate_command_id("Reset")
            )
        )
        callbacks["reset_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["reset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            lookahead=5,
        )
        assert ctrl_component_manager.op_state_model.op_state == DevState.ON

    def test_off_with_invalid_input_argument(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
    ):
        """
        Test the Off command is REJECTED when an invalid FQDN
        is passed to the command.
        """
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.ON
        )

        result, _ = ctrl_component_manager.off(
            ["wrong/control/0"],
            task_callback=functools.partial(
                callbacks["off_task"], generate_command_id("Off")
            ),
        )
        assert result == TaskStatus.QUEUED
        callbacks["off_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["off_task"].assert_against_call(
            status=TaskStatus.REJECTED, result=[ResultCode.REJECTED, ANY]
        )

    def test_off_with_exception(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
    ):
        """
        Test the Off command is failure when an exception is
         raised in _populate_subsystems_list.
        """
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.ON
        )
        with patch.object(
            CSPControllerComponentManager,
            "_populate_subsystems_list",
        ) as mocked_populate:
            mocked_populate.side_effect = ValueError(
                "Error in populating list"
            )
            result, _ = ctrl_component_manager.off(
                ["wrong/control/0"],
                task_callback=functools.partial(
                    callbacks["off_task"], generate_command_id("Off")
                ),
            )
            assert result == TaskStatus.QUEUED
            callbacks["off_task"].assert_against_call(status=TaskStatus.QUEUED)
            callbacks["off_task"].assert_against_call(
                status=TaskStatus.FAILED, result=[ResultCode.FAILED, ANY]
            )

    def test_off_no_events_on_subsytem(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test that the execution of the Off command fails
        if no events are raised on subsystems."""

        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.ON
        )
        mock_config.mock_event_failure = True

        ctrl_component_manager.off(
            [],
            task_callback=functools.partial(
                callbacks["off_task"], generate_command_id("Off")
            ),
        )
        callbacks["off_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["off_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        # off_observer = ctrl_component_manager.observers["off"]
        # probe_poller(off_observer, "result_code", ResultCode.FAILED, time=5)
        # callbacks["off_task"].assert_against_call(
        #    status=TaskStatus.COMPLETED,
        #    result=(ResultCode.FAILED, "off completed  1/1"),
        # )
        # assert off_observer.progress == 0

    def test_off_command_with_pst_failure(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test the execution of the CSP Controller On command when the On
        command fails on the PST sub-system. The CSP Controller command has
        success because the CBF is ON, even if there is a failure of the
        command (PST failure).

        Expected result: The CSP Controller reports the success condition. CSP
        Controller failure flag set to True because the whole command fails
        """

        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.ON
        )

        mock_config.mock_exception = True
        mock_config.mock_fail_device = "common-pst/beam/01"

        # force all the sub-systems to ON
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.ON
        )

        ctrl_component_manager.off(
            [],
            task_callback=functools.partial(
                callbacks["off_task"], generate_command_id("Off")
            ),
        )
        callbacks["off_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["off_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        # callbacks["off_task"].assert_against_call(
        #    status=TaskStatus.COMPLETED,
        #    result=(ResultCode.FAILED, "off completed  1/1"),
        # )
        assert ctrl_component_manager.op_state_model.op_state == DevState.ON

    def test_off_command_with_cbf_failure(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test the execution of the CSP Controller Off command when it
        fails on the CBF sub-system.

        Expected result: The CSP Controller reports the failure condition and
        the command results is FAILED.
        """
        # Configure the test to raise an exception on CBF controller

        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.ON
        )

        mock_config.mock_exception = True
        mock_config.mock_fail_device = "common-cbf/control/0"

        ctrl_component_manager.off(
            [],
            task_callback=functools.partial(
                callbacks["off_task"], generate_command_id("Off")
            ),
        )
        callbacks["off_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["off_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        # callbacks["off_task"].assert_against_call(
        #    status=TaskStatus.COMPLETED,
        #    result=(ResultCode.FAILED, "off completed  1/1"),
        # )
        assert ctrl_component_manager.op_state_model.op_state == DevState.ON

    def test_off_command_with_pss_timeout(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test the execution of the CSP Controller Off command when the PSS
        sub-system timeouts.

        Expected result: The CSP Controller reports the
        timeout condition, but the command results is OK because the CBF is ON.

        CSP Controller state execution: success CSP Controller
        command timeout: True
        """
        # Configure the test to timeout on PSS controller
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.ON
        )

        mock_config.mock_timeout = True
        mock_config.mock_fail_device = "common-pss/control/0"

        ctrl_component_manager.off(
            [],
            task_callback=functools.partial(
                callbacks["off_task"], generate_command_id("Off")
            ),
        )
        callbacks["off_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["off_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        # observer = ctrl_component_manager.observers["off"]
        # callbacks["off_task"].assert_against_call(
        #    status=TaskStatus.COMPLETED,
        #    result=(ResultCode.FAILED, "off completed  1/1"),
        # )
        # assert observer.timeout_expired
        assert ctrl_component_manager.op_state_model.op_state == DevState.ON

    def test_on_with_invalid_input_argument(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
    ):
        """
        Test the On command is REJECTED when an invalid FQDN
        is passed to the command.
        """
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.OFF
        )

        result, _ = ctrl_component_manager.on(
            ["wrong/control/0"],
            task_callback=functools.partial(
                callbacks["on_task"], generate_command_id("On")
            ),
        )
        assert result == TaskStatus.QUEUED
        callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["on_task"].assert_against_call(
            status=TaskStatus.REJECTED, result=[ResultCode.REJECTED, ANY]
        )

    def test_on_with_exception(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
    ):
        """
        Test the On command is failure when an exception is
         raised in _populate_subsystems_list.
        """
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.OFF
        )
        with patch.object(
            CSPControllerComponentManager,
            "_populate_subsystems_list",
        ) as mocked_populate:
            mocked_populate.side_effect = ValueError(
                "Error in populating list"
            )
            result, _ = ctrl_component_manager.on(
                ["wrong/control/0"],
                task_callback=functools.partial(
                    callbacks["on_task"], generate_command_id("On")
                ),
            )
            assert result == TaskStatus.QUEUED
            callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
            callbacks["on_task"].assert_against_call(
                status=TaskStatus.FAILED, result=[ResultCode.FAILED, ANY]
            )

    def test_on_with_all_subsystems_in_on(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
    ):
        """
        Test the behavior of the On command when all CSP subsystems
        are already in ON state.

        Expected behavior: the command is not forwarded and the
        command status is reported as COMPLETED, OK
        """
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.ON
        )
        result, _ = ctrl_component_manager.on(
            [],
            task_callback=functools.partial(
                callbacks["on_task"], generate_command_id("On")
            ),
        )
        assert result == TaskStatus.QUEUED
        callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["on_task"].assert_against_call(status=TaskStatus.COMPLETED)

    def test_on_no_events_on_subsytem(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test execution of the Csp ControllerManager On command. Sub-systems
        connections are mocked an no event is fired.

        Expected behavior: command on failure.
        """
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.STANDBY
        )

        mock_config.mock_timeout = True
        mock_config.mock_fail_device = "common-cbf/control/0"

        ctrl_component_manager.on(
            [],
            task_callback=functools.partial(
                callbacks["on_task"], generate_command_id("On")
            ),
        )
        callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["on_task"].assert_against_call(status=TaskStatus.IN_PROGRESS)

        callbacks["on_task"].assert_against_call(
            status=TaskStatus.FAILED,
            result=[ResultCode.FAILED, ANY],
            lookahead=3,
        )

    def test_on_command_with_pst_failure(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test the execution of the CSP Controller On command when the On
        command fails on the PST sub-system. The CSP Controller command has
        success because the CBF is ON, even if there is a failure of the
        command (PST failure)

        Expected result:

        CSP Controller command result FAILED
        CSP Controller failure flag True
        CSP Controller state ON
        """

        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.STANDBY
        )

        mock_config.mock_exception = True
        mock_config.mock_fail_device = "common-pst/beam/01"

        ctrl_component_manager.on(
            [],
            task_callback=functools.partial(
                callbacks["on_task"], generate_command_id("On")
            ),
        )
        callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["on_task"].assert_against_call(status=TaskStatus.IN_PROGRESS)

        callbacks["on_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=[
                ResultCode.FAILED,
                ANY,
            ],
            lookahead=4,
        )
        # assert on_observer.failure_raised
        # verify the CSP Controller state is ON and no error raised.
        # assert ctrl_component_manager.op_state_model.op_state == DevState.ON
        probe_poller(
            ctrl_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=2,
        )

    def test_on_command_with_cbf_failure(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test the execution of the CSP Controller On command when the On
        command fails on the CBF sub-system.

        Expected result: The CSP Controller failure condition True CSP
        Controller command result FAILED.
        """
        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.STANDBY
        )

        mock_config.mock_exception = True
        mock_config.mock_fail_device = "common-cbf/control/0"

        ctrl_component_manager.on(
            [],
            task_callback=functools.partial(
                callbacks["on_task"], generate_command_id("On")
            ),
        )
        callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["on_task"].assert_against_call(status=TaskStatus.IN_PROGRESS)
        callbacks["on_task"].assert_against_call(
            status=TaskStatus.FAILED,
            result=[ResultCode.FAILED, ANY],
            lookahead=3,
        )

        assert (
            ctrl_component_manager.op_state_model.op_state == DevState.STANDBY
        )

    def test_on_command_with_pss_timeout(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test the execution of the CSP Controller On command when the PSS
        sub-system timeouts. Expected result: The CSP Controller reports the
        timeout comdition, with task status and result code FAILED.
        The state of CSP is ON because CBF sub-system is ON.

        Expected results:

        CSP Controller state: ON
        CSP Controller command timeout: True
        CSP Controller command result: FAILED
        """

        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.STANDBY
        )
        mock_config.mock_timeout = True
        mock_config.mock_fail_device = "common-pss/control/0"

        ctrl_component_manager.on(
            [],
            task_callback=functools.partial(
                callbacks["on_task"], generate_command_id("On")
            ),
        )
        callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["on_task"].assert_against_call(status=TaskStatus.IN_PROGRESS)

        # # verify the CSP is On because command completes successfully
        # # on CBF
        callbacks["on_task"].assert_against_call(
            status=TaskStatus.FAILED,
            result=[
                ResultCode.FAILED,
                (
                    "pss-controller: Timeout expired executing on "
                    "command on common-pss/control/0"
                ),
            ],
            lookahead=3,
        )
        assert ctrl_component_manager.op_state_model.op_state == DevState.ON

    def test_on_with_cbf_callback_error(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test CSP Controller On execution when the command fails on the CBF
        sub-system.

        The callback function registered with the CBF On command returns a
        ResultCode = FAILED.
        Expected behavior:
        CSP Controller State STANDBY
        """

        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.STANDBY
        )

        mock_config.mock_cmd_fail = True
        mock_config.mock_fail_device = "common-cbf/control/0"
        mock_config.mock_cbk_cmd_err = True

        ctrl_component_manager.on(
            [],
            task_callback=functools.partial(
                callbacks["on_task"], generate_command_id("On")
            ),
        )
        callbacks["on_task"].assert_against_call(
            status=TaskStatus.QUEUED,
        )
        callbacks["on_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS,
        )

        callbacks["on_task"].assert_against_call(
            status=TaskStatus.FAILED,
            result=[ResultCode.FAILED, ANY],
            lookahead=5,
        )
        assert (
            ctrl_component_manager.op_state_model.op_state == DevState.STANDBY
        )

    def test_on_with_pst_callback_error(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """Test the execution of the CSP ControllerManager On command when the
        command fails on one of the sub-system different from CBF Controller.

        During the execution of the On command on the PST Controller, a
        callback event with an error is received and the execution of the
        command fails.
        Expected behavior:
        - CSP Controller state ON
        - PST beam 01 state OFF
        - PST beam 02 state OFF
        - CSP Controller healthState OK
        """

        TestControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.OFF
        )

        mock_config.mock_cmd_fail = True
        mock_config.mock_fail_device = "common-pst/beam/01"
        mock_config.mock_cbk_cmd_err = True

        ctrl_component_manager.on(
            [],
            task_callback=functools.partial(
                callbacks["on_task"], generate_command_id("On")
            ),
        )
        callbacks["on_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["on_task"].assert_against_call(status=TaskStatus.IN_PROGRESS)
        callbacks["on_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=[ResultCode.FAILED, ANY],
            lookahead=5,
        )

        probe_poller(
            ctrl_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=1,
        )
        pst_beam_1 = ctrl_component_manager.PstBeamsFqdn[0]
        pst_beam_2 = ctrl_component_manager.PstBeamsFqdn[1]
        assert (
            ctrl_component_manager.online_components[pst_beam_1].state
            == DevState.OFF
        )
        assert (
            ctrl_component_manager.online_components[pst_beam_2].state
            == DevState.ON
        )

    # def test_standby_command_with_pss_timeout(
    #     self,
    #     ctrl_component_manager,
    #     ctrl_component_manager_communicating,
    #     callbacks,
    #     mock_config_init,
    # ):
    #     """Test the execution of the CSP Controller Standby command when the
    #     PSS sub-system timeouts.

    #     Expected result:

    #     The CSP Controller reports the timeout condition, but the command
    #     result is OK because the CBF is ON.
    #     The timeout flag is set.

    #     CSP Controller state execution: success
    #     CSP Controller command timeout: True
    #     """
    #     mock_config.mock_fail_device = "common-pss/control/0"
    #     mock_config.mock_timeout = True

    #     TestControllerManager.set_subsystems_and_go_to_state(
    #         ctrl_component_manager, DevState.OFF
    #     )

    #     ctrl_component_manager.standby(
    #         [],
    #         task_callback=functools.partial(
    #             callbacks["standby_task"], generate_command_id("Standby")
    #         ),
    #     )
    #     callbacks["standby_task"].assert_against_call(status=TaskStatus.QUEUED)
    #     callbacks["standby_task"].assert_against_call(
    #         status=TaskStatus.IN_PROGRESS
    #     )
    #     callbacks["standby_task"].assert_against_call(
    #         status=TaskStatus.COMPLETED,
    #         result=(ResultCode.FAILED, "standby completed  1/1"),
    #     )
    # assert (
    #     ctrl_component_manager.op_state_model.op_state
    #     == DevState.STANDBY
    # )
