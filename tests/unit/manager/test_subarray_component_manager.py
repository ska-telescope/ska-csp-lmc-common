import functools
import json
import logging
import time
from unittest.mock import ANY

import pytest
from mock import MagicMock, patch
from ska_control_model import (
    AdminMode,
    HealthState,
    ObsMode,
    ObsState,
    TaskStatus,
)

# SKA  imports
from ska_tango_base.commands import ResultCode
from ska_tango_base.utils import generate_command_id
from ska_tango_testing.mock import MockCallableGroup
from tango import DevState

from ska_csp_lmc_common.commands.task import TaskExecutionType
from ska_csp_lmc_common.connector import Connector
from ska_csp_lmc_common.manager import CSPSubarrayComponentManager
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.subarray.json_configuration_parser import (
    JsonConfigurationParser,
)

# local import
from ska_csp_lmc_common.testing import mock_config
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import (
    TestBaseSubarray,
    load_json_file,
    test_properties_subarray,
)

logger = logging.getLogger(__name__)


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return MockCallableGroup(
        "on_task",
        "reset_task",
        "off_task",
        "standby_task",
        "configure_task",
        "gotoidle_task",
        "obsreset_task",
        "restart_task",
        "abort_task",
        timeout=5.0,
    )


def test_subarray_component_manager_when_wrong_sub_systems_fqdn_specified(
    subarray_op_state_model, subarray_health_state_model
):
    """Test Subarray subarray_component_manager initialization when CSP
    Subarray device properties are wrong.

    Expected result: CSP Subarray State FAULT CSP Subarray HealthState FAILED
    """
    wrong_properties = {
        "ConnectionTimeout": "1",
        "PingConnectionTime": "1",
        "DefaultCommandTimeout": "1",
        "SubID": "1",
        "CbfSubarray": "low-cbx/subarray/01",
        "PssSubarray": "low-pss/subarray/01",
        "PstBeams": ["low-pst/beam/01", "low-pst/beam/02"],
    }
    with patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = wrong_properties
        wrong_subarray_component_manager_conf = ComponentManagerConfiguration(
            "", logger
        )
        wrong_subarray_component_manager_conf.add_attributes()
        subarray_component_manager = CSPSubarrayComponentManager(
            subarray_health_state_model,
            subarray_op_state_model,
            MagicMock(),
            wrong_subarray_component_manager_conf,
            MagicMock(),
            logger=logger,
        )
        subarray_component_manager.init()
        assert (
            subarray_component_manager.op_state_model.op_state
            == DevState.FAULT
        )
        assert (
            subarray_component_manager.health_model.health_state
            == HealthState.FAILED
        )


def test_subarray_component_manager_init_failed(subarray_component_manager):
    """Test the communicating flag after communication failure. The DeviceProxy
    call is not mocked and the connections with the CSP sub-elements
    subarrays/beams fail.

    Expected results:

    is_communitcating = False
    number of online components = 0
    """
    # pylint: disable-next=fixme
    # TODO: remove the sleep().
    subarray_component_manager.start_communicating()
    time.sleep(2)
    # wait to establish communication
    assert not subarray_component_manager.is_communicating
    assert not len(subarray_component_manager.online_components)


def print_value(value):
    print(f"New value is: {value}")


@pytest.fixture
@patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def subarray_component_manager_init(
    subarray_component_manager, mock_config_init
):
    """Perform the CSP subarray initialization with PST beams that can already
    belong to the subarray (re-initialization).

    The Connector class is mocked through the MockedConnector class.
    """
    # create the events to simulate the state after connection

    subarray_component_manager._admin_mode = AdminMode.ONLINE
    subarray_component_manager.start_communicating()
    probe_poller(subarray_component_manager, "is_communicating", True, time=3)
    subarray_component_manager._store_admin_mode = (
        subarray_component_manager._admin_mode
    )


@patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
class TestSubarrayComponentManager(TestBaseSubarray):
    def test_subarray_component_manager_components_created(
        self, subarray_component_manager, subarray_component_manager_init
    ):
        assert subarray_component_manager.sub_id == 1
        assert len(subarray_component_manager.components) == 4

        assert (
            subarray_component_manager.components[
                test_properties_subarray["CbfSubarray"]
            ].name
            == "cbf-subarray-01"
        )
        assert (
            subarray_component_manager.components[
                test_properties_subarray["PssSubarray"]
            ].name
            == "pss-subarray-01"
        )
        assert (
            subarray_component_manager.components[
                test_properties_subarray["PstBeams"][0]
            ].name
            == "pst-beam-01"
        )
        assert (
            subarray_component_manager.components[
                test_properties_subarray["PstBeams"][1]
            ].name
            == "pst-beam-02"
        )

    def test_online_components_number(
        self, subarray_component_manager, subarray_component_manager_init
    ):
        """Verify the number of online components, taking into account that pst
        beams 1 and 2 are not belonging to the subarray."""

        assert len(subarray_component_manager.online_components) == 2

    def test_create_start_task(self, subarray_component_manager):
        """
        Test the method that generates the main task responsible
        for establishing the connection with the subsystems.
        """
        task = subarray_component_manager._create_start_task()
        assert len(task.subtasks) == 4
        assert task.name == "Start-communication"
        assert task.task_type == TaskExecutionType.PARALLEL
        assert not task.completed_callback

    def test_create_stop_task(
        self, subarray_component_manager, subarray_component_manager_init
    ):
        """
        Test the method that generates the main task responsible
        for disconnecting from the subsystems.
        """
        task = subarray_component_manager._create_stop_task(
            subarray_component_manager.online_components
        )
        assert len(task.subtasks) == 2
        assert task.name == "Stop-communication"
        assert task.task_type == TaskExecutionType.PARALLEL
        assert not task.completed_callback

    def test_subarray_component_manager_stop_communicating(
        self, subarray_component_manager, subarray_component_manager_init
    ):
        """Test that after stopping comminication with the component, the
        communicating flag is False."""
        with patch.object(
            Connector, "_get_deviceproxy", return_value=MagicMock()
        ):
            subarray_component_manager.start_communicating()
            # wait to establish communication
            probe_poller(
                subarray_component_manager, "is_communicating", True, time=3
            )
            subarray_component_manager._store_admin_mode = (
                subarray_component_manager._admin_mode
            )
            subarray_component_manager._admin_mode = AdminMode.OFFLINE
            # subarray_component_manager._admin_mode=AdminMode.OFFLINE
            subarray_component_manager.stop_communicating()
            probe_poller(
                subarray_component_manager, "is_communicating", False, time=2
            )

    def test_gotoidle_from_ready(
        self,
        subarray_component_manager,
        subarray_component_manager_init,
        mock_config_init,
        callbacks,
    ):
        """Test that observation state transitions to IDLE when in READY."""
        # Force the subarray devices to ON/READY state.
        TestSubarrayComponentManager.raise_event_on_pss_subarray(
            "state", DevState.ON
        )
        TestSubarrayComponentManager.raise_event_on_cbf_subarray(
            "state", DevState.ON
        )
        # create CBF events
        TestSubarrayComponentManager.raise_event_on_pss_subarray(
            "obsstate", ObsState.READY
        )
        TestSubarrayComponentManager.raise_event_on_cbf_subarray(
            "obsstate", ObsState.READY
        )

        probe_poller(
            subarray_component_manager.obs_state_model,
            "obs_state",
            ObsState.READY,
            time=1,
        )
        probe_poller(
            subarray_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=1,
        )

        # issue the GoToIdle command
        subarray_component_manager.deconfigure(
            functools.partial(
                callbacks["gotoidle_task"], generate_command_id("GoToIdle")
            ),
        )
        callbacks["gotoidle_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["gotoidle_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=[ResultCode.OK, ANY],
            lookahead=5,
        )

        probe_poller(
            subarray_component_manager,
            "obs_state",
            ObsState.IDLE,
            time=5,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_json_configuration_parser_error(
        self,
        initial_obsstate,
        subarray_component_manager,
        subarray_component_manager_init,
        callbacks,
    ):
        """Test if an error occurs in the json configuration parsing phase.

        The ObsState of CSP won't be updated, since the command is failing. The
        command resultCode is FAILED
        """
        subarray_component_manager._sub_id = 12
        assert subarray_component_manager.is_communicating
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, initial_obsstate
        )
        config_input = load_json_file("test_ConfigureScan_basic.json")
        config_input.pop("cbf")

        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.FAILED, ANY),
        )
        probe_poller(
            subarray_component_manager,
            "obs_state",
            initial_obsstate,
            time=5,
        )
        probe_poller(
            subarray_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=1,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_no_sub_id(
        self,
        initial_obsstate,
        subarray_component_manager,
        subarray_component_manager_init,
        callbacks,
    ):
        """Test if there is no sub_id in the configuration file.

        The ObsState of CSP won't be updated, since the command is failing. The
        command resultCode is FAILED
        """
        assert subarray_component_manager.is_communicating
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, initial_obsstate
        )
        config_input = load_json_file("test_ConfigureScan_basic.json")
        config_input["common"].pop("subarray_id")

        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.FAILED, ANY),
        )
        probe_poller(
            subarray_component_manager,
            "obs_state",
            initial_obsstate,
            time=5,
        )
        probe_poller(
            subarray_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=1,
        )

    def test_restart_with_sub_systems_aborted(
        self,
        subarray_component_manager,
        subarray_component_manager_init,
        mock_config_init,
        callbacks,
    ):
        """Test the execution of the ObsRestart command when the CSP Subarray
        is in ABORTED observing and all the sub-systems are in ABORTED state.

        Input data before Restart invocation:
        PSS Subarray obsState ABORTED
        CBF Subarray obsState ABORTED

        Expected behavior:
        PSS Subarray obsState EMPTY
        CBF Subarray obsState EMPTY
        """
        assert subarray_component_manager.is_communicating
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.ABORTED
        )

        subarray_component_manager.restart(
            functools.partial(
                callbacks["restart_task"], generate_command_id("Restart")
            ),
        )
        callbacks["restart_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["restart_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=[ResultCode.OK, ANY],
            lookahead=3,
        )

        probe_poller(
            subarray_component_manager,
            "obs_state",
            ObsState.EMPTY,
            time=5,
        )

    pss_obsstate_values = [
        ObsState.READY,
        ObsState.IDLE,
        ObsState.SCANNING,
        ObsState.CONFIGURING,
    ]

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_failed(
        self,
        initial_obsstate,
        subarray_component_manager,
        subarray_component_manager_init,
        callbacks,
    ):
        """Test a failure in the ConfigureCommand when an invalid JSON
        is specified as input of the command.

        Note: This case should never happen since the JSON configuration
        is validated as soon as the TANGO command is issued on the
        device.

        The ObsState of CSP stays in initial_obsstate, since the configuration
        is failing. The command resultCode is FAILED
        """
        assert subarray_component_manager.is_communicating
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, initial_obsstate
        )
        argin = {"test": "dummy"}
        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **argin,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        # callbacks["configure_task"].assert_against_call(status=TaskStatus.IN_PROGRESS)
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.FAILED, ANY),
        )
        probe_poller(
            subarray_component_manager,
            "obs_state",
            initial_obsstate,
            time=5,
        )

    def test_configure_with_empty_sections(
        self,
        subarray_component_manager,
        subarray_component_manager_init,
        callbacks,
    ):
        """Test the value of the configurationID when a JSON with
        empty sections for sub-systems is passed as input argument.

        The obsState of CSP maintains its initial value (initial_obsstate),
        since the configuration is failing. The command resultCode is FAILED
        """
        assert subarray_component_manager.is_communicating
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.IDLE
        )

        config_input = load_json_file("test_ConfigureScan_basic.json")
        config_input.pop("cbf")
        config_input.pop("pss")
        config_input.pop("pst")
        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input,
        )
        assert not subarray_component_manager.config_id
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.FAILED, ANY),
        )
        json_configuration_parser = JsonConfigurationParser(
            subarray_component_manager, config_input
        )
        assert (
            subarray_component_manager.config_id
            == json_configuration_parser.get_id()
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_cbf_and_pss(
        self,
        initial_obsstate,
        subarray_component_manager,
        subarray_component_manager_init,
        callbacks,
    ):
        """Test configuration when both CSP and CBF are present in
        configuration file.

        The ObsState of CSP goes to READY The command resultCode is OK
        """
        assert subarray_component_manager.is_communicating
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, initial_obsstate
        )
        # retrieve the dictionary with the JSON configuration
        config_input_dict = load_json_file("test_ConfigureScan_cbf_pss.json")
        subarray_component_manager.command_timeout = 15
        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input_dict,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED, lookahead=3
        )

        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS, lookahead=3
        )

        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.COMPLETED, lookahead=3
        )
        probe_poller(
            subarray_component_manager,
            "obs_state",
            ObsState.READY,
            time=10,
        )
        json_configuration_parser = JsonConfigurationParser(
            subarray_component_manager, config_input_dict
        )
        assert (
            subarray_component_manager.config_id
            == json_configuration_parser.get_id()
        )

        # the CspJsonValidator validate schemas with strict check.
        # To pass the validation, transaction_id key has to be removed
        # because not supported in Low schemas.
        validated_config_str = (
            subarray_component_manager.valid_scan_configuration
        )
        validated_config_dict = json.loads(validated_config_str)
        validated_config_dict.pop("transaction_id")

        assert validated_config_dict == config_input_dict

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_only_cbf(
        self,
        initial_obsstate,
        subarray_component_manager,
        subarray_component_manager_init,
        callbacks,
    ):
        """Test if in the configuration file pss entry is missing.

        The ObsState of CSP goes to READY, since the configuration is forwarded
        to CBF. The command resultCode is OK
        """
        assert subarray_component_manager.is_communicating
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, initial_obsstate
        )
        config_input = load_json_file("test_ConfigureScan_basic.json")
        config_input.pop("pss")
        config_input.pop("pst")

        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.COMPLETED, lookahead=2
        )
        probe_poller(
            subarray_component_manager,
            "obs_state",
            ObsState.READY,
            time=5,
        )
        probe_poller(
            subarray_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=1,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_only_pss(
        self,
        initial_obsstate,
        subarray_component_manager,
        subarray_component_manager_init,
        callbacks,
    ):
        """Test if in the configuration file cbf entry is missing.

        The configuration is not forwarded, since it can't be validated.
        ResultCode is FAILED
        """
        assert subarray_component_manager.is_communicating
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, initial_obsstate
        )
        config_input = load_json_file("test_ConfigureScan_basic.json")
        config_input.pop("cbf")

        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        # callbacks["configure_task"].assert_against_call(
        #    status=TaskStatus.IN_PROGRESS
        # )
        # callbacks["configure_task"].assert_against_call(
        #    status=TaskStatus.COMPLETED,
        #    result=(ResultCode.OK, "configure completed  1/1"),
        # )
        probe_poller(
            subarray_component_manager,
            "obs_state",
            initial_obsstate,
            time=5,
        )
        probe_poller(
            subarray_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=1,
        )

    @pytest.mark.parametrize(
        [
            "mock_event",
            "fail_device",
            "event_value",
            "result_code",
            "task_status",
        ],
        (
            (
                False,
                None,
                None,
                str(ResultCode.OK.value),
                str(TaskStatus.ABORTED.name),
            ),
            (
                True,
                "common-pss/subarray/01",
                ObsState.IDLE,
                str(ResultCode.FAILED.value),
                str(TaskStatus.ABORTED.name),
            ),
            (
                True,
                "common-cbf/subarray/01",
                ObsState.FAULT,
                str(ResultCode.FAILED.value),
                str(TaskStatus.ABORTED.name),
            ),
        ),
    )
    def test_configure_when_abort_invoked(
        self,
        mock_event,
        fail_device,
        event_value,
        result_code,
        task_status,
        subarray_component_manager,
        subarray_component_manager_init,
        mock_config_init,
        callbacks,
    ):
        """Test Configure task when abort is invoked."""
        mock_config.mock_event = mock_event
        mock_config.mock_fail_device = fail_device
        mock_config.mock_event_value = event_value
        assert subarray_component_manager.is_communicating
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.IDLE
        )
        config_input = load_json_file("test_ConfigureScan_cbf_pss.json")

        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        subarray_component_manager.abort(
            functools.partial(
                callbacks["abort_task"], generate_command_id("Abort")
            ),
        )
        callbacks["abort_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        callbacks["abort_task"].assert_against_call(
            status=TaskStatus.COMPLETED, lookahead=5
        )

        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.ABORTED, lookahead=5
        )

        for component in subarray_component_manager.online_components.values():
            subarray_component_manager.obs_state_model.component_obs_state_changed(  # noqa:E501
                component, component.obs_state
            )
        expected_csp_obs_state = (
            subarray_component_manager.obs_state_model.obs_state
        )
        assert (
            subarray_component_manager.obs_state_model.obs_state
            == expected_csp_obs_state
        )

    def test_subarray_cm_invoke_reset_with_cbf_in_fault(
        self,
        subarray_component_manager,
        subarray_component_manager_init,
        callbacks,
    ):
        """Test Subarray Reset command when the CBF sub-system is in FAULT."""
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.IDLE
        )
        TestSubarrayComponentManager.raise_event_on_cbf_subarray(
            "state", DevState.FAULT
        )

        subarray_component_manager.reset(
            functools.partial(
                callbacks["reset_task"], generate_command_id("Reset")
            ),
        )
        callbacks["reset_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["reset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=[ResultCode.OK, ANY],
            lookahead=3,
        )

    def test_subarray_cm_invoke_reset_with_no_subsystem_in_fault(
        self,
        subarray_component_manager,
        subarray_component_manager_init,
        callbacks,
    ):
        """Test Controller Reset command when the CBF sub-system is in
        FAULT."""
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.IDLE
        )

        subarray_component_manager.reset(
            functools.partial(
                callbacks["reset_task"], generate_command_id("Reset")
            ),
        )
        callbacks["reset_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["reset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            lookahead=3,
        )

    def test_subarray_cm_invoke_reset_with_only_subarray_in_fault(
        self,
        subarray_component_manager,
        subarray_component_manager_init,
        callbacks,
    ):
        """Test Subarray Reset command when the CBF sub-system is in
        FAULT."""
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.IDLE
        )
        subarray_component_manager.op_state_model.component_fault(True)
        assert subarray_component_manager.op_state_model.faulty

        subarray_component_manager.reset(
            functools.partial(
                callbacks["reset_task"], generate_command_id("Reset")
            ),
        )
        callbacks["reset_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["reset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=[ResultCode.OK, ANY],
            lookahead=5,
        )
        assert (
            subarray_component_manager.op_state_model.op_state == DevState.ON
        )

    @pytest.mark.parametrize(
        [
            "mock_event",
            "fail_device",
            "event_value",
            "result_code",
            "task_status",
        ],
        (
            (
                False,
                None,
                None,
                str(ResultCode.OK.value),
                str(TaskStatus.ABORTED.name),
            ),
        ),
    )
    def test_abort_invoked(
        self,
        mock_event,
        fail_device,
        event_value,
        result_code,
        task_status,
        subarray_component_manager,
        subarray_component_manager_init,
        mock_config_init,
    ):
        """Test Configure task when abort is invoked."""
        mock_config.mock_event = mock_event
        mock_config.mock_fail_device = fail_device
        mock_config.mock_event_value = event_value
        assert subarray_component_manager.is_communicating
        TestSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.IDLE
        )
        config_input = load_json_file("test_ConfigureScan_cbf_pss.json")

        def my_update_command_info(
            command_id,
            status=None,
            progress=None,
            result=None,
            exception=None,
        ):
            return 0

        mock_update_command_info = functools.partial(
            my_update_command_info,
            generate_command_id("Configure"),
        )
        _, command_id = subarray_component_manager.configure(
            mock_update_command_info, **config_input
        )

        mock_update_command_info = functools.partial(
            my_update_command_info, generate_command_id("Abort")
        )
        _, command_id = subarray_component_manager.abort(
            task_callback=mock_update_command_info
        )

        probe_poller(
            subarray_component_manager, "obs_state", ObsState.ABORTED, time=20
        )

        probe_poller(
            subarray_component_manager, "obs_modes", [ObsMode.IDLE], time=10
        )
