import logging

import mock
import pytest
from mock import MagicMock
from ska_control_model import AdminMode, HealthState, ObsState
from tango import DevState

from ska_csp_lmc_common.controller import CbfControllerComponent  # noqa: F401
from ska_csp_lmc_common.controller import PssControllerComponent  # noqa: F401
from ska_csp_lmc_common.manager import EventManager
from ska_csp_lmc_common.observing_component import ObservingComponent
from ska_csp_lmc_common.subarray import (  # noqa: F401
    CbfSubarrayComponent,
    PssSubarrayComponent,
    PstBeamComponent,
    SubarrayHealthModel,
    SubarrayObsStateModel,
    SubarrayOpStateModel,
)
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector

module_logger = logging.getLogger(__name__)


@pytest.fixture(scope="function")
def event_manager():
    """Initialize the EventManager with the health, operational and observing
    state model."""

    def ctrl_callback(state):
        return state

    def ctrl_health_callback(health_state):
        return health_state

    health_model = SubarrayHealthModel(ctrl_callback, ctrl_health_callback)
    health_model.is_disabled(False)
    op_state_model = SubarrayOpStateModel(DevState.DISABLE, ctrl_callback)
    obs_state_model = SubarrayObsStateModel(ObsState.EMPTY, ctrl_callback)
    evt_mgr = EventManager(
        "controller",
        mock.MagicMock(),
        health_model,
        op_state_model,
        obs_state_model,
    )
    return evt_mgr


def test_event_ids_list_update_on_subscribe_success(event_manager):
    """Test that on successfull subscription, the list with the event_ids for
    the subscribed component is updated and the length is equl to the number of
    the subscribed events.

    :param event_manager: the EventManager instance
    :type event_manager: class EventManager
    """
    with event_manager.lock:
        len_old_list_of_components = len(event_manager._list_of_components)
    component = MagicMock()
    component.fqdn = "mid_csp_cbf/controller/0"
    component.subscribe_attribute.return_value = True, None
    event_manager.subscribe_event(component, "state", "change_event")
    with event_manager.lock:
        assert "state" in event_manager.attrs_subscribed[component.fqdn]
        assert (
            len(event_manager._list_of_components)
            == len_old_list_of_components + 1
        )


def test_event_ids_list_not_updated_on_subscribe_failure(event_manager):
    """Test that on failure in subscription, the list with event_ids for the
    subscribed component not updated.

    :param event_manager: the EventManager instance
    :type event_manager: class EventManager
    """
    # mock the Component and the callback invoked on the
    # Csp device (controller or subarray)
    with event_manager.lock:
        len_old_list_of_components = len(event_manager._list_of_components)
    component = MagicMock()
    component.fqdn = "mid_csp_cbf/controller/0"
    # here we set the condition of subscription failure
    component.subscribe_attribute.return_value = False, None
    event_manager.subscribe_event(component, "state", "change_event")
    with event_manager.lock:
        assert not event_manager.attrs_subscribed
        assert (
            len(event_manager._list_of_components)
            == len_old_list_of_components
        )


def test_event_callback_failure_when_no_wrapper_function_exist(
    event_manager, caplog
):
    """Test exception in EventManager.evt_callback when a wrapper function such
    as evaluate_csp_* does not exists for the subscribed attribute."""
    caplog.set_level(logging.DEBUG)
    # mock a CspEvent
    event = mock.MagicMock()
    event.attr_name = "temperature"
    event.value = 1
    msg_found = 0
    event_manager.evt_callback(event)
    for message in caplog.messages:
        if message.find(
            "Function '_evaluate_csp_temperature' does not exist"
        ) or message.find("No callback found for attribute temperature"):
            msg_found += 1
    assert msg_found


def test_event_callback_with_exception_in_wrapper_function(
    event_manager, caplog
):
    """Test exception in EventManager.evt_callback when a wrapper function such
    as evaluate_csp_* raises an exception."""
    caplog.set_level(logging.DEBUG)
    event = mock.MagicMock()
    event.attr_name = "state"
    event.value = 1
    msg_found = 0
    with mock.patch(
        "ska_csp_lmc_common.manager.EventManager._evaluate_csp_state"
    ) as mock_wrapper_func:
        mock_wrapper_func.side_effect = ValueError(
            "Wrapper function does not exist"
        )
        event_manager.evt_callback(event)
        for message in caplog.messages:
            if message.find("Wrapper function does not exist"):
                msg_found += 1
        assert msg_found


def test_event_healthstate_wrapper_with_device_admin_offline(event_manager):
    """Test the EventManager wrapper function called to evaluate the CSP
    healthState when a sub-system fires a change event on its healthState.

    Test that a CSP Controller/Subarray subordinate component different from
    CBF does not affect the CSP Controller/Subarray healthState when its
    adminMode is not ONLINE/ENGINEERING.
    """
    component_pss = mock.MagicMock()
    component_pss.name = "pss"
    component_pss.fqdn = "low-pss/subarray/01"
    component_pss.admin_mode = AdminMode.OFFLINE
    component_pss.weight = 0
    component_pss.health_state = HealthState.UNKNOWN
    csp_event = mock.MagicMock()
    csp_event.attr_name = "healthstate"
    csp_event.value = HealthState.UNKNOWN
    csp_event.component = component_pss
    event_manager.evt_callback(csp_event)
    component_cbf = mock.MagicMock()
    component_cbf.name = "cbf"
    component_cbf.fqdn = "low-cbf/subarray/01"
    component_cbf.admin_mode = AdminMode.ONLINE
    component_cbf.weight = 1
    csp_event = mock.MagicMock()
    csp_event.attr_name = "healthstate"
    csp_event.value = HealthState.OK
    component_cbf.health_state = csp_event.value
    csp_event.component = component_cbf
    event_manager.evt_callback(csp_event)
    assert event_manager.health_model.health_state == HealthState.OK


@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_event_manager_unsubscribe_attr(event_manager):
    """Test the EventManager un-subscription of events on a set of
    attributes."""
    attrs_list = ["state", "healthstate", "obsstate"]
    component = ObservingComponent(fqdn="mock/subarray/01", name="test")
    component.connect()
    for attr_name in attrs_list:
        event_manager.subscribe_event(component, attr_name, "change_event")
    with event_manager.lock:
        assert 3 == len(event_manager.attrs_subscribed[component.fqdn])
    event_manager.unsubscribe_event_on_component(
        component,
        [
            "state",
        ],
    )
    with event_manager.lock:
        assert 2 == len(event_manager.attrs_subscribed[component.fqdn])
    event_manager.unsubscribe_event_on_component(
        component, ["obsstate", "healthstate"]
    )
    with event_manager.lock:
        assert not len(event_manager.attrs_subscribed[component.fqdn])


@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_manger_unsubscribe_attrs_alreday_unsubscribed_on_component(
    event_manager,
):
    """Test the EventManager un-subscription of events on a set of attributes
    when these are already unsubscribed on the component."""
    attrs_list = ["state", "healthstate", "obsstate"]
    component = ObservingComponent(fqdn="mock/subarray/01", name="test")
    component.connect()
    for attr_name in attrs_list:
        event_manager.subscribe_event(component, attr_name, "change_event")
    with event_manager.lock:
        assert 3 == len(event_manager.attrs_subscribed[component.fqdn])
    # disconnect the component unsubscribing all the events
    component.disconnect()
    event_manager.unsubscribe_event_on_component(
        component,
        [
            "state",
        ],
    )
    event_manager.unsubscribe_event_on_component(
        component, ["obsstate", "healthstate"]
    )
    with event_manager.lock:
        assert not len(event_manager.attrs_subscribed[component.fqdn])


@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_event_manager_callback_unregister(event_manager):
    attrs_list = ["state", "healthstate", "obsstate"]
    component = []
    for num in range(0, 3):
        component.append(
            ObservingComponent(
                fqdn=f"mock/subarray/0{num}", name=f"test0{num}"
            )
        )
        print(f"component {component[num].fqdn}")
        component[num].connect()
    for num in range(0, 3):
        for attr_name in attrs_list:
            event_manager.subscribe_event(
                component[num], attr_name, "change_event"
            )

    for num in range(0, 3):
        event_manager.unsubscribe_event_on_component(
            component[num],
            [
                "state",
                "obsstate",
            ],
        )
        with event_manager.lock:
            assert len(event_manager.attrs_subscribed[component[num].fqdn])


@mock.patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
def test_event_ids_list_is_empty_when_unsubscribe_all_events(event_manager):
    """Test the event_ids list for each subscribed component is empty when all
    events are unsubscribed."""
    attrs_list = ["state", "healthstate", "obsstate"]
    component = []
    for num in range(0, 3):
        component.append(
            ObservingComponent(
                fqdn=f"mock/subarray/0{num}", name=f"test0{num}"
            )
        )
        component[num].connect()
    for num in range(0, 3):
        for attr_name in attrs_list:
            event_manager.subscribe_event(
                component[num], attr_name, "change_event"
            )
    event_manager.unsubscribe_all_events()
    for num in range(0, 3):
        with event_manager.lock:
            assert not len(event_manager.attrs_subscribed[component[num].fqdn])


@pytest.mark.parametrize("device", ["controller", "subarray"])
@pytest.mark.parametrize("component", ["Cbf", "Pss", "Pst"])
@pytest.mark.parametrize(
    "attribute,value",
    [
        ("State", DevState.OFF),
        ("HealthState", HealthState.DEGRADED),
        ("AdminMode", AdminMode.OFFLINE),
    ],
)
def test_update_subsystem_attribute(device, component, attribute, value):
    """Test the method that is invoked when a subsystem
    operational state is changed"""

    update_property_callback = mock.MagicMock()

    evt_mgr = EventManager(
        device,
        update_property_callback,
        mock.MagicMock(),
        mock.MagicMock(),
        mock.MagicMock(),
        logger=module_logger,
    )

    evt = mock.MagicMock()
    evt.attr_name = attribute.lower()
    evt.value = value

    prefix, number, component_name = (
        ("csp", "0", component)
        if device == "controller"
        else ("", "01", f"{component.lower()}Subarray")
    )

    if component == "Pst":
        evt.component = PstBeamComponent("mid-pst/beam/01")
    else:
        evt.component = eval(f"{component}{device.capitalize()}Component")(
            f"mid-{component.lower()}/{device}/{number}"
        )

    evt_mgr._update_subsystem_attribute(evt)

    if component == "Pst":
        component_name = "PstBeams" if device == "controller" else "pstBeams"
        value_str = (
            str(value) if attribute == "State" else str(value).split(".")[1]
        )
        update_property_callback.assert_called_with(
            f"{prefix}{component_name}{attribute}",
            ["mid-pst/beam/01", value_str],
        )
    else:
        update_property_callback.assert_called_with(
            f"{prefix}{component_name}{attribute}", value
        )
